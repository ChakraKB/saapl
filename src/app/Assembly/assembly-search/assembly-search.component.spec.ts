import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssemblySearchComponent } from './assembly-search.component';

describe('AssemblySearchComponent', () => {
  let component: AssemblySearchComponent;
  let fixture: ComponentFixture<AssemblySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssemblySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssemblySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
