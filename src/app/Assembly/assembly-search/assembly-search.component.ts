import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { CompleterService } from 'ng2-completer';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { Location } from '@angular/common';
import * as $ from 'jquery';
import { SalesorderService } from '../../services/salesorder/salesorder.service';
import { AssemblyService } from '../../services/assembly/assembly.service';
import { DataTableDirective } from 'angular-datatables';
import { error } from 'util';

@Component({
  selector: 'app-assembly-search',
  templateUrl: './assembly-search.component.html',
  styleUrls: ['./assembly-search.component.css']
})
export class AssemblySearchComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  unitid: any;
  wono: any;
  others: any;
  comp: any = {};
  wolist: any;
  allwo: any;
  itembom: any;
  checkeditems: any;
  showDialog: boolean = false;
  show_dialog_loader: boolean = false;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private completerService: CompleterService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private _fb: FormBuilder,
    private location: Location,
    private assemblyservice: AssemblyService,
    private salesservice: SalesorderService
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.checkeditems = [];
    this.getAllwO();
    this.persons = [];
    setTimeout(() => {
      this.dtTrigger.next();
    }, 500);
  }

  getAllwO() {
    this.show_loader = true;
    this.salesservice.getAssOrders(this.unitid).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.allwo = res.data;
        this.wolist = this.completerService.local(this.allwo, 'sc_no', 'sc_no');
      }
    })
  }

  getwodetails(data) {
    if (data) {
      this.checkeditems = [];
      var obj = data.originalObject;
      this.show_loader = true
      this.assemblyservice.GetListByWO(obj.id).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.persons = [];
      if (this.dtElement.dtInstance) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        })
      } else
        this.dtTrigger.next();
    }
  }

  calcpendingqty(i) {
    var pending = this.itembom[i].quantity - (this.itembom[i].stk_alloc_qty + this.itembom[i].wip_qty + this.itembom[i].already_issued_qty)
    // var pending = this.itembom[i].quantity - (this.itembom[i].already_issued_qty)
    return pending
  }

  checkauthorized(status, item, i) {
    this.itembom[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.itembom[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  uncheckall() {
    for (var i = 0; i < this.itembom.length; i++) {
      this.itembom[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  showbom(data) {
    this.showDialog = true;
    this.itembom = data.child_list
  }

  close() {
    this.showDialog = false;
  }

  request() {
    if (!this.checkeditems.length) {
      this.notif.warn("Warning", "Please select any item", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      console.log("checkeditems", this.checkeditems);
      this.showDialog = false;
      this.show_loader = true;
      this.assemblyservice.CreateAsmblyReq(this.checkeditems, this.unitid).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.getAllwO();
          this.reset();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error("Error", "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    }
  }

  reset() {
    this.persons = [];
    this.itembom = [];
    if (this.dtElement.dtInstance) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      })
    } else
      this.dtTrigger.next();
    this.wono = "";
  }
}
