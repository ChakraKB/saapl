import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreFormComponent } from './are-form.component';

describe('AreFormComponent', () => {
  let component: AreFormComponent;
  let fixture: ComponentFixture<AreFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
