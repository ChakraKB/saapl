import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { InvoiceService } from '../../services/invoice/invoice.service';
declare let swal: any;

@Component({
  selector: 'app-are-form',
  templateUrl: './are-form.component.html',
  styleUrls: ['./are-form.component.css']
})
export class AreFormComponent implements OnInit {
  statusarray: any;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  areform: FormGroup;
  scrolltime: number;
  show_loader: boolean = false;
  Are: any = {}
  formdata: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private invoiceservice: InvoiceService) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.areform = fb.group({
      'id': [null],
      'range': [null, Validators.required],
      'to': [null, Validators.required],
      'ceadress': [null, Validators.required],
      'licence': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.getare()
  }

  areformdetails: any

  getare() {
    this.show_loader = true;
    this.invoiceservice.getAre().subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.areformdetails = res.data[0];

        this.Are.id = this.areformdetails.id;
        this.Are.range = this.areformdetails.range;
        this.Are.to = this.areformdetails.to_are;
        this.Are.ceadress = this.areformdetails.central_excise_address;
        this.Are.licence = this.areformdetails.license
        this.Are.createddate = this.areformdetails.created_date
      }
    })
  }

  formObj: any = {
    range:
      {
        required: "Range required",
      },
    to:
      {
        required: "To required",
      },
    ceadress:
      {
        required: "Central Excise Address required",
      },
    licence:
      {
        required: "Licence required",
      }

  }

  addeditare(data) {
    if (this.areform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.areform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else {

      if (data.id) {
        console.log("data", data)
        this.formdata =
          {
            'id': data.id,
            'range': data.range,
            'to_are': data.to,
            'central_excise_address': data.ceadress,
            'license': data.licence,
            'created_date': this.Are.createddate
          }
        this.invoiceservice.updateAre(this.formdata).subscribe(res => {
          if (res.status == "success") {
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        })
      } else {
        this.formdata =
          {
            'range': data.range,
            'to_are': data.to,
            'central_excise_address': data.ceadress,
            'license': data.licence
          }
        this.invoiceservice.CreateAre(this.formdata).subscribe(res => {
          if (res.status == "success") {
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        })
      }

    }
  }

  resetform() {
    // this.areform.reset();
  }

}
