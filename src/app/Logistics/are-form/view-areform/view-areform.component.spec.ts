import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAreformComponent } from './view-areform.component';

describe('ViewAreformComponent', () => {
  let component: ViewAreformComponent;
  let fixture: ComponentFixture<ViewAreformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAreformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAreformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
