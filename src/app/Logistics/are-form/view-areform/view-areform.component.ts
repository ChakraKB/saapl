import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { InvoiceService } from '../../../services/invoice/invoice.service';
import { SalesorderService } from '../../../services/salesorder/salesorder.service';
import * as _ from "underscore";
@Component({
  selector: 'app-view-areform',
  templateUrl: './view-areform.component.html',
  styleUrls: ['./view-areform.component.css']
})
export class ViewAreformComponent implements OnInit {
  show_loader: boolean = false;
  invid: any;
  invoicedetails: any;
  invLineitems: any;
  saleorders: any;
  salesorderdetails: any;
  Totalqty: any;
  InvTotal: any;
  addressone: any;
  areformdetails: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private location: Location,
    private invoiceservice: InvoiceService,
    private acroute: ActivatedRoute,
    private salesorder: SalesorderService) {
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.invid = params.id;
        console.log("urlparams", this.invid);
      }
    });
  }
  today: any;
  salesdate: any;

  ngOnInit() {
    this.getorders();
    this.getaredefaults()
    this.getinvoicedetails();
    let date = new Date();
    this.alterdateformat(date);
    this.today = this.salesdate
    console.log("podate", this.today)
  }

  alterdateformat(date) {
    this.salesdate = formatDate(date);
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }

  back() {
    this.location.back();
  }

  getaredefaults() {
    this.show_loader = true
    this.invoiceservice.getAre().subscribe(res => {
      this.show_loader = false
      this.areformdetails = res.data[0]
    })
  }

  getorders() {
    this.show_loader = true;
    this.salesorder.getAllsalescontract(this.unitid)
      .subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.saleorders = _.map(res.data, function (num) {
            num.contactid = num.supplier.id;
            return num;
          });
        }
      });
  }

  getinvoicedetails() {
    this.show_loader = true;
    this.invoiceservice.getareformByInvoice(this.invid).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.invoicedetails = res.data
        this.invLineitems = this.invoicedetails.areList;
        var Totalqty = 0;
        var InvTotal = 0;
        _.forEach(this.invLineitems, function (val) {
          var qty = +val.prod_quantity;
          var subtotal = +val.subtotal;
          Totalqty += qty;
          InvTotal += subtotal;
        })
        this.Totalqty = Totalqty;
        this.InvTotal = InvTotal;
        // setTimeout(() => {
        //   this.salesorderdetails = _.findWhere(this.saleorders, { 'id': this.invoicedetails.postInvoiceDetails[0].packSlipDetails.salesOrderDetails.sales_order_id })
        //   console.log("salesorderdetails", JSON.stringify(this.salesorderdetails))
        //   if (this.salesorderdetails) {
        //     this.show_loader = false;
        //   }
        // }, 1000);
      }
    })
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Export Invoice</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
