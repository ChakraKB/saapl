import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostInvoiceComponent } from './post-invoice.component';

describe('PostInvoiceComponent', () => {
  let component: PostInvoiceComponent;
  let fixture: ComponentFixture<PostInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
