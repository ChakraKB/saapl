import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router } from '@angular/router';
import { ItemsService } from '../../services/items/items.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { UserService } from '../../services/user/user.service';
import { StoresService } from '../../services/stores/stores.service';
import { InvoiceService } from '../../services/invoice/invoice.service';
import { HsnService } from '../../services/hsn/hsn.service'
import { isUndefined } from 'util';
declare let swal: any;
@Component({
  selector: 'app-post-invoice',
  templateUrl: './post-invoice.component.html',
  styleUrls: ['./post-invoice.component.css']
})
export class PostInvoiceComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  toastertime: any;
  tosterminlength: any;
  statusarray: any;
  tabletimeout: any;
  userdetails: any;
  containerForm: FormGroup;
  totalquantity: any;
  total: any;
  invdate: any;
  salesdate: any
  allorders: any;
  contactlist: any;
  itemlist: any;
  allitems: any;
  payterms: any;
  selectedinvLine: any;
  selectedcon: any;
  protected dataService: CompleterData;
  show_loader: boolean = false;
  show_dialog_loader: boolean = false;
  empty: boolean = true;
  selectedorder: any;
  selectedorderid: any;
  customer: any;
  from: any;
  to: any;
  payterm: any;
  currency: any;
  deliveryterm: any;
  shipdate: any;
  salesorderarray: any;
  orderno: any;
  invno: any;
  checkeditems: any[];
  salesrowselected: any;
  salesrow: any;
  showDialog: boolean = false;
  customerdetails: any;
  nodata: boolean = true;
  allcontacts: any;
  saleorders: any;
  contactsinvoice: any;
  masterdetails: any = {};
  packingslipdetails: any;
  postInvoiceDetails: any;
  invvalue: any;
  exfactorydate: any;
  Stuffing: any;
  containerdetails: any;
  carryby: any;
  allPorts: any;
  allpayterms: any
  alldeliveryterms: any;
  delterms: any;
  allcurrencies: any;
  modearray: any;
  allhsn: any;
  selectedcontact: any;
  companystate: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private itemservice: ItemsService,
    private userservices: UserService,
    private completerService: CompleterService,
    private salesorder: SalesorderService,
    private router: Router,
    private storeservice: StoresService,
    private invoiceservice: InvoiceService,
    private hsnservice: HsnService,
  ) {
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.companystate = AppConstant.APP_CONST.COMPANY_STATE;
    console.log("userdetails", this.userdetails.username)
    this.carryby = AppConstant.APP_CONST.Carryby;
    this.modearray = AppConstant.APP_CONST.Mode;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.containerForm = this._fb.group({
      itemRows: this._fb.array([])
    });
  }

  ngOnInit() {
    this.getPorts();
    this.getallhsn();
    this.getpaymentterms();
    this.getdeliveryterms();
    this.getcurrency();
    this.getcontacts()
    this.getorders();
    this.checkeditems = [];
    this.addNewRow();
  }

  getcontainerform(containerForm) {
    return containerForm.get('itemRows').controls
  }

  initItemRows() {
    return this._fb.group({
      id: "",
      containertype: "",
      containerno: "",
    });
  }

  addNewRow() {
    this.nodata = true;
    const control = <FormArray>this.containerForm.controls["itemRows"];
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    const control = <FormArray>this.containerForm.controls["itemRows"];
    control.removeAt(index);
  }

  getPorts() {
    this.userservices.getAllPorts()
      .subscribe(res => {
        if (res) {
          this.allPorts = this.completerService.local(res.data, 'port_name', 'port_name');
        }
      });
  }

  getallhsn() {
    this.hsnservice.getallHsn().subscribe(res => {
      if (res.status == "success") {
        this.allhsn = this.completerService.local(res.data, 'hsn_code', 'hsn_code');
      }
    })
  }

  getpaymentterms() {
    this.userservices.getallpayterms()
      .subscribe(res => {
        if (res) {
          this.allpayterms = res.data;
          this.payterms = this.completerService.local(res.data, 'term', 'term');
        }
      });
  }

  getdeliveryterms() {
    this.userservices.getAllDeliveryTerms()
      .subscribe(res => {
        if (res) {
          this.alldeliveryterms = res.data;
          this.delterms = this.completerService.local(res.data, 'term', 'term');
        }
      });
  }
  getcurrency() {
    this.salesorder.getAllCurrencies().subscribe(res => {
      this.allcurrencies = this.completerService.local(res.data, 'short_name', 'short_name');
    })
  }

  getcontacts() {
    this.show_loader = true;
    this.contactlist = [];
    this.userservices.getContactsByCat("Customer")
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  getorders() {
    this.show_loader = true;
    this.contactlist = [];
    this.salesorder.getAllsalescontract(this.unitid)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.saleorders = _.map(res.data, function (num) {
            num.contactid = num.supplier.id;
            return num;
          });
        }
      });
  }

  selectcont(contact) {
    var contactinv = [];
    this.allorders = [];
    this.packingslipdetails = [];
    this.checkeditems = []
    this.nodata = true;
    if (contact) {
      this.show_loader = true;
      this.selectedcontact = contact.originalObject;
      console.log("contact", contact.originalObject)
      this.storeservice.getpackingSLipbyCustomer(contact.originalObject.id).subscribe(res => {
        this.nodata = false;
        this.show_loader = false;
        console.log("packinghslips", res.data);
        this.showDialog = true;
        this.salesorderarray = res.data;
      })
    }
  }

  selectorder(order) {
    if (order) {
      this.empty = false;
      this.checkeditems = [];
      this.selectedorder = _.findWhere(this.saleorders, { id: order.value });
      console.log("selectedorder", JSON.stringify(this.selectedorder));
      this.selectedorderid = this.selectedorder.id;
      this.customerdetails = this.selectedorder.supplier;
      this.customer = this.selectedorder.supplier.supplier_name;
      this.from = this.selectedorder.port_loading.port_name;
      this.to = this.selectedorder.port_discharge.port_name;
      this.payterm = this.selectedorder.payment_term_id;
      this.currency = this.selectedorder.currency_id.short_name;
      this.deliveryterm = this.selectedorder.delivery_inco_term;
      this.shipdate = this.selectedorder.shipment_date;
      this.masterdetails.shipmode = this.selectedorder.shipment_mode
      this.getorderdetails(this.selectedorderid);
    }
  }

  getorderdetails(id) {
    this.show_loader = true;
    this.storeservice.getpackingSLipbyOrder(id, this.unitid)
      .subscribe(res => {
        this.nodata = false;
        this.show_loader = false;
        if (res.status == "success") {
          this.showDialog = true;
          this.salesorderarray = res.data
          console.log("salesdetail", JSON.stringify(this.salesorderarray));
        } else {
          this.notif.error('Error', res.message, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      });
  }

  checkauthorized(status, item, i) {
    this.salesorderarray[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.salesorderarray[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }
  selectorderrow() {
    var salesrow = [];
    var same_port: boolean = true;
    if (_.isEmpty(this.checkeditems)) {
      this.notif.warn('Warning', "Please select any slip items", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      for (var i = 0; i < this.checkeditems.length; i++) {
        salesrow.push(this.checkeditems[i].id);
        if (this.checkeditems[i].addr_valid_flag == 1) {
          same_port = false
        }
      }
      this.salesrowselected = salesrow;
      this.showDialog = false
      if (!same_port) {
        swal({
          title: 'Port Mismatch !',
          text: "This packing slip have different ports !",
          type: 'info',
          width: 500,
          padding: 10,
          confirmButtonColor: '#ffaa00',
          confirmButtonText: 'Ok !',
        }).then((result) => {
          if (result.value) {
            this.showalert()
          }
        })
      } else {
        this.showalert()
      }
    }
  }

  showalert() {
    this.show_loader = true;
    this.storeservice.getpackingSLipdetails(this.salesrowselected).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        console.log("packingslipdetails", JSON.stringify(res.data))
        this.packingslipdetails = res.data;
        _.map(this.packingslipdetails, function (num) {
          num.taxtotal = 0;
          num.cgstpercent = 0;
          num.sgstpercent = 0;
          num.igstpercent = 0;
          return num
        });
        var salesordermasterdetails = _.findWhere(this.saleorders, { 'id': this.packingslipdetails[0].salesOrderDetails.sales_order_id });

        // ================== SET DETAILS ==================
        this.from = salesordermasterdetails.port_loading.port_name;
        this.to = salesordermasterdetails.port_discharge.port_name;
        this.payterm = salesordermasterdetails.payment_term_id;
        this.currency = salesordermasterdetails.currency_id.short_name
        this.deliveryterm = salesordermasterdetails.delivery_inco_term;
        this.shipdate = this.splitdate(salesordermasterdetails.shipment_date);
        this.masterdetails.shipmode = salesordermasterdetails.shipment_mode;
        // ================== SET DETAILS ==================

        _.map(this.packingslipdetails, function (num) {
          return num.subtotal = num.slip_quantity * num.salesOrderDetails.price
        });
      } else {
        this.notif.error('Error', res.message, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    })
  }

  calcinvtotal() {
    var total = 0
    if (this.packingslipdetails) {
      _.forEach(this.packingslipdetails, function (obj) {
        var withtax = (+obj.subtotal) + (+obj.taxtotal)
        total += withtax
      })
      this.invvalue = total
      return total;
    }
  }

  selectedhsn(data, i) {
    if (data) {
      this.packingslipdetails[i].commodity_name = data.originalObject.prod_description;
      if (this.selectedcontact.address_state.id == this.companystate.id) {
        var taxtotal = (parseFloat(this.packingslipdetails[i].subtotal) * parseFloat(data.originalObject.tax_rate)) / 100
        this.packingslipdetails[i].taxtotal = taxtotal;
        this.packingslipdetails[i].cgstpercent = (data.originalObject.tax_rate) / 2;
        this.packingslipdetails[i].sgstpercent = (data.originalObject.tax_rate) / 2;
        this.packingslipdetails[i].igstpercent = 0
      } else {
        var taxtotal = (parseFloat(this.packingslipdetails[i].subtotal) * parseFloat(data.originalObject.tax_rate)) / 100
        this.packingslipdetails[i].taxtotal = taxtotal;
        this.packingslipdetails[i].cgstpercent = 0
        this.packingslipdetails[i].sgstpercent = 0
        this.packingslipdetails[i].igstpercent = data.originalObject.tax_rate
      }
    } else {
      this.packingslipdetails[i].cgstpercent = 0
      this.packingslipdetails[i].sgstpercent = 0
      this.packingslipdetails[i].igstpercent = 0
      this.packingslipdetails[i].taxtotal = 0
    }
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
    {
      date: {
        year: +newdate[2],
        month: +newdate[1],
        day: +newdate[0]
      },
      "formatted": d
    }
    console.log("day", altereddate)
    return altereddate
  }

  saveentry() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (!this.invno) {
      validdata = false;
      validationerrorMsg.push("Please enter Invoice No");
    }
    if (!this.invdate) {
      validdata = false;
      validationerrorMsg.push("Please select Invoice date");
    }
    if (!this.customer) {
      validdata = false;
      validationerrorMsg.push("Please select Customer");
    }
    if (!this.from) {
      validdata = false;
      validationerrorMsg.push("Please select Port of loading");
    }
    if (!this.to) {
      validdata = false;
      validationerrorMsg.push("Please select Port of discharge");
    }
    if (!this.payterm) {
      validdata = false;
      validationerrorMsg.push("Please select Payment term");
    }
    if (!this.currency) {
      validdata = false;
      validationerrorMsg.push("Please select Currency");
    }
    if (!this.deliveryterm) {
      validdata = false;
      validationerrorMsg.push("Please select Delivery term");
    }
    // if (!this.exfactorydate) {
    //   validdata = false;
    //   validationerrorMsg.push("Please select Exfactorydate date");
    // }
    this.postInvoiceDetails = []
    if (this.packingslipdetails) {
      for (var i = 0; i < this.packingslipdetails.length; i++) {
        var data =
        {
          "sc_no": this.packingslipdetails[i].sc_no,
          "line_no": this.packingslipdetails[i].line_no,
          "hsn_code": this.packingslipdetails[i].hsn,
          "commodity_name": this.packingslipdetails[i].commodity_name,
          "sgst": this.packingslipdetails[i].sgstpercent,
          "cgst": this.packingslipdetails[i].cgstpercent,
          "igst": this.packingslipdetails[i].igstpercent,
          "tax_value": this.packingslipdetails[i].taxtotal,
          "part_no": this.packingslipdetails[i].part_no,
          "component": this.packingslipdetails[i].component,
          "component_desc": this.packingslipdetails[i].component_desc,
          "prod_quantity": this.packingslipdetails[i].slip_quantity,
          "ship_date": this.packingslipdetails[i].salesOrderDetails.pconfirm_date,
          "price": this.packingslipdetails[i].salesOrderDetails.price,
          "subtotal": this.packingslipdetails[i].subtotal,
          "packSlipDetails": { 'id': this.packingslipdetails[i].id }
        }
        if (isUndefined(this.packingslipdetails[i].hsn) || !this.packingslipdetails[i].hsn) {
          validdata = false;
          validationerrorMsg.push("Please Select Hsn Code for : " + this.packingslipdetails[i].part_no);
        } else
          this.postInvoiceDetails.push(data);
      }
    }
    if (!this.masterdetails.precarriage) {
      validdata = false;
      validationerrorMsg.push("Please enter precarriage place");
    }
    if (!this.masterdetails.precarriedby) {
      validdata = false;
      validationerrorMsg.push("Please select precarried by");
    }
    if (!this.masterdetails.cdestination) {
      validdata = false;
      validationerrorMsg.push("Please enter country of destination");
    }
    if (!this.masterdetails.fdestination) {
      validdata = false;
      validationerrorMsg.push("Please enter final destination");
    }
    // if (!this.masterdetails.commodity) {
    //   validdata = false;
    //   validationerrorMsg.push("Please enter commodity");
    // }
    // // if (!this.masterdetails.areno) {
    // //   validdata = false;
    // //   validationerrorMsg.push("Please enter ARE1.No");
    // // }
    // // if (!this.masterdetails.vessel) {
    // //   validdata = false;
    // //   validationerrorMsg.push("Please enter vessel no");
    // // }
    // if (!this.Stuffing) {
    //   validdata = false;
    //   validationerrorMsg.push("Please enter Stuffing place");
    // }
    console.log("postInvoiceDetails", this.postInvoiceDetails)

    this.containerdetails = []
    for (var i = 0; i < this.containerForm.value.itemRows.length; i++) {
      var containerdetails =
      {
        "container_type": this.containerForm.value.itemRows[i].containertype,
        "container_no": this.containerForm.value.itemRows[i].containerno,
      }
      if (!this.containerForm.value.itemRows[i].containertype || !this.containerForm.value.itemRows[i].containerno) {
      } else
        this.containerdetails.push(containerdetails);
    }

    if (validdata) {
      var sc = _.findWhere(this.allorders, { value: this.orderno });
      var formdata =
      {
        'packing_slip_ids': this.salesrowselected.toString(),
        'containerDetails': this.containerdetails,
        'postInvoiceDetails': this.postInvoiceDetails,
        'invoice_no': this.invno,
        'invoice_date': this.invdate.formatted,
        'unit_id': this.unitid,
        'customer': this.customer,
        'port_of_loading': this.from,
        'port_of_discharge': this.to,
        'payment_terms': this.payterm,
        'currency': this.currency,
        'delivery_terms': this.deliveryterm,
        'exfactory_date': _.isEmpty(this.exfactorydate) ? null : this.exfactorydate.formatted,
        'invoice_value': this.invvalue,
        'pre_carriage_place': this.masterdetails.precarriage,
        'pre_carriage_by': this.masterdetails.precarriedby,
        'country_of_destination': this.masterdetails.cdestination,
        'final_destination': this.masterdetails.fdestination,
        'shipment_mode': this.masterdetails.shipmode,
        'commodity_name': this.masterdetails.commodity,
        'hsn_code': this.masterdetails.hsn,
        'are1no': this.masterdetails.areno,
        'stuffing_place': this.Stuffing,
        'vessel_no': this.masterdetails.vessel,
        'remarks': this.masterdetails.remarks,
      }
      console.log(JSON.stringify(formdata))
      this.show_loader = true;
      this.invoiceservice.CreatePostInvoice(formdata)
        .subscribe(res => {
          this.show_loader = false;
          console.log("res", res.data);
          if (res.status == "success") {
            this.notif.success('Success', res.message, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
            this.router.navigate(['/postInvoiceList'])
          } else {
            this.notif.error('Error', res.message, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        });
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }

  resetcontact() {
    this.orderno = "";
    this.customer = "";
    this.from = "";
    this.to = "";
    this.payterm = "";
    this.currency = "";
    this.deliveryterm = "";
    this.shipdate = "";
    this.masterdetails.shipmode = ""
    this.showDialog = false;
  }

  resetform() {
    this.invno = "";
    this.invdate = "";
    this.invvalue = "";
    this.orderno = "";
    this.customer = "";
    this.salesorderarray = [];
    this.packingslipdetails = [];
    this.from = "";
    this.to = "";
    this.payterm = "";
    this.currency = "";
    this.deliveryterm = "";
    this.shipdate = "";
    this.nodata = true;
    this.showDialog = false;
    this.Stuffing = "";
    this.masterdetails = {};
    this.containerForm.reset();
  }

}
