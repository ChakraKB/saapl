import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostinvoiceListComponent } from './postinvoice-list.component';

describe('PostinvoiceListComponent', () => {
  let component: PostinvoiceListComponent;
  let fixture: ComponentFixture<PostinvoiceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostinvoiceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostinvoiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
