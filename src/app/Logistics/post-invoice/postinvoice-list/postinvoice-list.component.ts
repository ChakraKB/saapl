import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { AppConstant } from '../../../app.constant';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
class Person {
  role_name: string;
  description: string;
}

@Component({
  selector: 'app-postinvoice-list',
  templateUrl: './postinvoice-list.component.html',
  styleUrls: ['./postinvoice-list.component.css']
})
export class PostinvoiceListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  persons: Person[] = [];
  show_loader: boolean = false;
  unitid: any;
  constructor(private http: Http) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
  }

  ngOnInit() {
    this.LoadAngTable();
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.POSTINVOICE.GET + this.unitid)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

}
