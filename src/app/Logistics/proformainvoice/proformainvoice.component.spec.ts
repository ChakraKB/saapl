import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProformainvoiceComponent } from './proformainvoice.component';

describe('ProformainvoiceComponent', () => {
  let component: ProformainvoiceComponent;
  let fixture: ComponentFixture<ProformainvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProformainvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProformainvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
