import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { UserService } from '../../services/user/user.service';
import { StoresService } from '../../services/stores/stores.service'
import { HsnService } from '../../services/hsn/hsn.service';
import { InvoiceService } from "../../services/invoice/invoice.service"
@Component({
  selector: 'app-proformainvoice',
  templateUrl: './proformainvoice.component.html',
  styleUrls: ['./proformainvoice.component.css']
})
export class ProformainvoiceComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  toastertime: any;
  tosterminlength: any;
  statusarray: any;
  tabletimeout: any;
  userdetails: any;
  orderForm: FormGroup;
  totalquantity: any;
  total: any;
  invdate: any;
  salesdate: any
  allorders: any;
  contactlist: any;
  itemlist: any;
  allitems: any;
  payterms: any;
  selectedinvLine: any;
  selectedcon: any;
  protected dataService: CompleterData;
  show_loader: boolean = false;
  empty: boolean = true;
  selectedorder: any;
  selectedorderid: any;
  customer: any;
  from: any;
  to: any;
  payterm: any;
  currency: any;
  deliveryterm: any;
  shipdate: any;
  salesorderarray: any;
  orderno: any;
  invno: any;
  checkeditems: any[];
  salesrowselected: any;
  salesrow: any;
  showDialog: boolean = false;
  customerdetails: any;
  nodata: boolean = true;
  show_dialog_loader: boolean = false;
  packingslipdetails: any;
  invvalue: any;
  saleorders: any;
  allcontacts: any;
  contactsinvoice: any;
  allhsn: any;
  invid: any;
  url: any;
  viewurl: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private userservices: UserService,
    private completerService: CompleterService,
    private salesorder: SalesorderService,
    private router: Router,
    private storeservice: StoresService,
    private hsnservice: HsnService,
    private invoiceservice: InvoiceService,
    private acroute: ActivatedRoute,
  ) {
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    console.log("userdetails", this.userdetails.username)
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.invid = params.id;
        console.log("urlparams", this.invid);
      }
    });
    this.url = this.router.url
    this.orderForm = this.fb.group({
      itemRows: this.fb.array([])
    });

  }

  ngOnInit() {
    this.viewurl = "/viewproformainv/" + this.invid;
    this.getorders();
    this.getcontacts();
    this.getallhsn();
    this.checkeditems = [];
    if (this.invid) {
      this.viewproforma(this.invid)
    }
  }

  getcontacts() {
    this.show_loader = true;
    this.contactlist = [];
    this.userservices.getContactsByCat('Customer')
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  getallhsn() {
    this.hsnservice.getallHsn().subscribe(res => {
      if (res.status == "success") {
        this.allhsn = this.completerService.local(res.data, 'hsn_code', 'hsn_code');
      }
    })
  }

  getorders() {
    this.show_loader = true;
    this.contactlist = [];
    this.salesorder.getAllsalescontract(this.unitid)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.saleorders = _.map(res.data, function (num) {
            num.contactid = num.supplier.id;
            return num;
          });
        }
      });
  }

  selectcont(contact) {
    var contactinv = [];
    this.allorders = [];
    if (contact) {
      console.log("contact", contact.originalObject)
      _.filter(this.saleorders, function (num) {
        if (num.contactid == contact.originalObject.id) {
          contactinv.push(num)
        }
      });
      this.contactsinvoice = contactinv
      this.allorders = this.masterservice.formatDataforDropdown("sc_no", "id", this.contactsinvoice);
      console.log("coninv", this.allorders);
    }
  }


  selectorder(order) {
    if (order) {
      this.empty = false;
      this.checkeditems = [];
      this.selectedorder = _.findWhere(this.saleorders, { id: order.value });
      console.log("selectedorder", JSON.stringify(this.selectedorder));
      this.selectedorderid = this.selectedorder.id;
      this.customerdetails = this.selectedorder.supplier;
      this.customer = this.selectedorder.supplier.supplier_name;
      this.from = this.selectedorder.port_loading.port_name;
      this.to = this.selectedorder.port_discharge.port_name;
      this.payterm = this.selectedorder.payment_term_id;
      this.currency = this.selectedorder.currency_id.short_name;
      this.deliveryterm = this.selectedorder.delivery_inco_term;
      this.shipdate = this.selectedorder.shipment_date;
      this.getorderdetails(this.selectedorderid);
    }
  }

  getorderdetails(id) {
    this.show_loader = true;
    this.salesorder.getByID(id)
      .subscribe(res => {
        this.nodata = false;
        this.show_loader = false;
        if (res) {
          // this.showDialog = true;
          this.salesorderarray = res.data.salesOrderDetails
          console.log("salesdetail", JSON.stringify(this.salesorderarray));
        }
      });
  }

  calcsubtotal(qty, price) {
    var q = +qty;
    var p = +price;
    var sub = q * p;
    return sub
  }

  checkauthorized(status, item, i) {
    this.salesorderarray[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item.id)
    } else {
      this.salesorderarray[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  selectorderrow() {
    var salesrow = [];
    if (_.isEmpty(this.checkeditems)) {
      this.notif.warn('Warning', "Please select any slip items", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      for (var i = 0; i < this.checkeditems.length; i++) {
        salesrow.push(this.checkeditems[i]);
      }
      this.salesrowselected = salesrow
      this.storeservice.getpackingSLipdetails(this.salesrowselected).subscribe(res => {
        if (res.status == "success") {
          this.show_dialog_loader = false;
          console.log("packingslipdetails", JSON.stringify(res.data))
          this.packingslipdetails = res.data;
          _.map(this.packingslipdetails, function (num) {
            return num.subtotal = num.prod_quantity * num.salesOrderDetails.price
          });
          var total = 0
          _.forEach(this.packingslipdetails, function (obj) {
            total += obj.subtotal
          })
          this.invvalue = total;
          this.showDialog = false
        }
      })
    }
  }

  saveentry() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (!this.customer) {
      validdata = false;
      validationerrorMsg.push("Please select Customer");
    }
    if (!this.orderno) {
      validdata = false;
      validationerrorMsg.push("Please select order");
    }
    if (!this.invno) {
      validdata = false;
      validationerrorMsg.push("Please enter Invoice No");
    }
    if (!this.invdate) {
      validdata = false;
      validationerrorMsg.push("Please select Invoice date");
    }

    // if (this.salesorderarray) {
    //   for (let i = 0; i < this.salesorderarray.length; i++) {
    //     if (this.salesorderarray[i].PQty > 0) {
    //       console.log("salesorder", this.salesorderarray[i])
    //     } else {
    //       validdata = false;
    //       validationerrorMsg.push("Please Enter Production quantity");
    //     }
    //   }
    // }

    if (validdata) {

      var formdata =
      {
        "unit_id": this.unitid,
        "sales_order_id": { "id": this.selectedorderid },
        "customer_id": this.customerdetails.id,
        "customer_name": this.customerdetails.supplier_name,
        "invoice_no": this.invno,
        "invoice_date": this.invdate.formatted,
        "proformaDetails": this.salesorderarray
      }

      console.log("formdata", formdata)
      this.show_loader = true;
      this.invoiceservice.CreateProformaInvoice(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
          this.resetform();
          this.router.navigate(['/proformaInvoicelist'])
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        console.log(JSON.stringify(err));
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }
  invdetails: any
  ordersno: any
  viewproforma(invid) {
    this.show_loader = true;
    this.invoiceservice.getProformaInvoiceByID(invid).subscribe(res => {
      if (res.status == "success") {
        console.log("order", res.data)
        this.invdetails = res.data
        this.selectedorder = res.data.sales_order_id
        console.log("selectedorder", JSON.stringify(this.selectedorder));
        this.selectedorderid = this.selectedorder.id;
        this.invno = this.invdetails.invoice_no;
        this.ordersno = this.selectedorder.sc_no;
        this.invdate = this.splitdate(this.invdetails.invoice_date)
        this.customerdetails = this.selectedorder.supplier;
        this.customer = this.selectedorder.supplier.supplier_name;
        this.from = this.selectedorder.port_loading.port_name;
        this.to = this.selectedorder.port_discharge.port_name;
        this.payterm = this.selectedorder.payment_term_id;
        this.currency = this.selectedorder.currency_id.short_name;
        this.deliveryterm = this.selectedorder.delivery_inco_term;
        this.shipdate = this.selectedorder.shipment_date;
        this.salesorderarray = res.data.proformaDetails

      }
    })
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
    {
      date: {
        year: +newdate[2],
        month: +newdate[1],
        day: +newdate[0]
      },
      "formatted": d
    }
    console.log("day", altereddate)
    return altereddate
  }

  resetform() {
    this.invno = "";
    this.invdate = "";
    this.orderno = "";
    this.customer = "";
    this.salesorderarray = [];
    this.packingslipdetails = [];
    this.salesrowselected = [];
    this.from = "";
    this.to = "";
    this.payterm = "";
    this.currency = "";
    this.deliveryterm = "";
    this.shipdate = "";
    this.nodata = true;
    this.showDialog = false
  }
}
