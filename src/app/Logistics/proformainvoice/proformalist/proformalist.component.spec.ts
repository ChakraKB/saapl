import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProformalistComponent } from './proformalist.component';

describe('ProformalistComponent', () => {
  let component: ProformalistComponent;
  let fixture: ComponentFixture<ProformalistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProformalistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProformalistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
