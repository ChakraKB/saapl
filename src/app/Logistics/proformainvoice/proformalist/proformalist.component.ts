import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { MasterService } from '../../../services/master.service';
import { AppConstant } from '../../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { InvoiceService } from '../../../services/invoice/invoice.service'
class Person {
  role_name: string;
  description: string;
}

@Component({
  selector: 'app-proformalist',
  templateUrl: './proformalist.component.html',
  styleUrls: ['./proformalist.component.css']
})
export class ProformalistComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  persons: Person[] = [];
  unitid: any;
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;

  constructor(
    private invoiceservice: InvoiceService,
    private notif: NotificationsService, ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.LoadAngTable();
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.invoiceservice.getAllProformaInvoice(this.unitid).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.persons = res.data
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }


}
