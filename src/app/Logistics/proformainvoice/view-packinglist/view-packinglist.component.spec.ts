import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPackinglistComponent } from './view-packinglist.component';

describe('ViewPackinglistComponent', () => {
  let component: ViewPackinglistComponent;
  let fixture: ComponentFixture<ViewPackinglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPackinglistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPackinglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
