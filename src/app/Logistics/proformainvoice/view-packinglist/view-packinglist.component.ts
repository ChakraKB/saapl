import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { InvoiceService } from '../../../services/invoice/invoice.service';
import { SalesorderService } from '../../../services/salesorder/salesorder.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from "underscore";
@Component({
  selector: 'app-view-packinglist',
  templateUrl: './view-packinglist.component.html',
  styleUrls: ['./view-packinglist.component.css']
})
export class ViewPackinglistComponent implements OnInit {
  show_loader: boolean = false;
  invid: any;
  invoicedetails: any;
  invLineitems: any;
  saleorders: any;
  salesorderdetails: any;
  Totalqty: any;
  netweight: any;
  Grossweight: any;
  addressone: any;
  cuscopy: any;
  finalcopy: any
  url: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private location: Location, private invoiceservice: InvoiceService, private router: Router,
    private acroute: ActivatedRoute, private salesorder: SalesorderService) {
    this.url = this.router.url;
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.invid = params.id;
        console.log("urlparams", this.invid);
      }
    });
  }

  ngOnInit() {
    this.getorders();
    this.getinvoicedetails();
    this.cuscopy = "/viewpackinglist/" + this.invid;
    this.finalcopy = "/viewpackinglistfinal/" + this.invid;
  }

  getorders() {
    this.salesorder.getAllsalescontract(this.unitid)
      .subscribe(res => {
        if (res) {
          this.saleorders = _.map(res.data, function (num) {
            num.contactid = num.supplier.id;
            return num;
          });
        }
      });
  }

  // getinvoicedetails() {
  //   this.show_loader = true;
  //   this.invoiceservice.getPostInvoiceByID(this.invid).subscribe(res => {
  //     this.show_loader = false;
  //     this.invoicedetails = res.data
  //     this.invLineitems = this.invoicedetails.postInvoiceViewDetails;
  //     var Totalqty = 0;
  //     var netweight = 0;
  //     var grossweight = 0;
  //     _.forEach(this.invLineitems, function (val) {
  //       _.forEach(val.postInvoiceList, function (key) {
  //         var qty = +key.packSlipDetails.slip_quantity;
  //         var net_weight = +key.packSlipDetails.net_weight;
  //         var gross_weight = +key.packSlipDetails.gross_weight;
  //         Totalqty += qty;
  //         netweight += net_weight;
  //         grossweight += gross_weight
  //       })
  //     })
  //     this.Totalqty = Totalqty;
  //     this.netweight = netweight;
  //     this.Grossweight = grossweight;
  //   })
  // }
  shipaddress: any
  getinvoicedetails() {
    this.show_loader = true;
    this.invoiceservice.GetExportPslipByID(this.invid).subscribe(res => {
      this.show_loader = false;
      this.invoicedetails = res.data
      this.invLineitems = this.invoicedetails.expPackListView;
      this.shipaddress = this.invoicedetails.ship_address.split('##');
      var Totalqty = 0;
      var netweight = 0;
      var grossweight = 0;
      _.forEach(this.invLineitems, function (val: any) {
        grossweight += +val.gross_weight;
        _.forEach(val.groupBoxDetails, function (key: any) {
          key.gross_weight = key.net_weight + key.box_weight;
          var qty = +key.prod_quantity;
          netweight += +key.net_weight;
          Totalqty += qty;
        })
      })
      this.Totalqty = Totalqty;
      this.netweight = netweight;
      this.Grossweight = grossweight;
      // setTimeout(() => {
      //   this.salesorderdetails = _.findWhere(this.saleorders, { 'id': this.invoicedetails.postInvoiceDetails[0].packSlipDetails.salesOrderDetails.id })
      //   console.log("salesorderdetails", JSON.stringify(this.salesorderdetails))

      //   var d = this.salesorderdetails.supplier.address;
      //   this.addressone = d.split('##');
      //   this.show_loader = false;
      // }, 1000);
    })
  }

  back() {
    this.location.back();
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
    <head>
    <title>EXPORT PACKING LIST</title>
    <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
    <style>
    .print-view>tbody>tr>td,
    .print-view>tbody>tr>th,
    .print-view>tfoot>tr>td,
    .print-view>tfoot>tr>th,
    .print-view>thead>tr>td,
    .print-view>thead>tr>th {
      border: 1px solid #333 !important; padding:2px 3px !important
    }
    .print-view .print-label p { font-weight : 500 !important;}
    .print-view .print-label { font-weight : 500 !important;}
    .print-view .cs { background-color:#ccc !important;}
    .print-view .cs td { background-color:#ccc !important;}
     table.print-view td p {
     margin: 0!important;
    }
    @page {
      size: auto;
    }

  table.report-container {
  page-break-after:always;
  }
  thead.report-header {
  display:table-header-group;
  }
  tfoot.report-footer {
  display:table-footer-group;
  } 
  </style>
  </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }

}
