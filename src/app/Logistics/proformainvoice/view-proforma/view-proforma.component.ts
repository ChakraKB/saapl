import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { InvoiceService } from '../../../services/invoice/invoice.service';
import { SalesorderService } from '../../../services/salesorder/salesorder.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from "underscore";
declare var require: any;
@Component({
  selector: 'app-view-proforma',
  templateUrl: './view-proforma.component.html',
  styleUrls: ['./view-proforma.component.css']
})
export class ViewProformaComponent implements OnInit {
  show_loader: boolean = false;
  invid: any;
  invoicedetails: any;
  invLineitems: any;
  saleorders: any;
  salesorderdetails: any;
  Totalqty: any;
  InvTotal: any;
  addressone: any;
  url: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private location: Location, private invoiceservice: InvoiceService, private router: Router,
    private acroute: ActivatedRoute, private salesorder: SalesorderService) {
    this.url = this.router.url;
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.invid = params.id;
        console.log("urlparams", this.invid);
      }
    });
  }
  finalcopy: any;
  cuscopy: any
  ngOnInit() {
    this.getorders();
    this.getinvoicedetails();
    this.cuscopy = "/viewproforma/" + this.invid;
    this.finalcopy = "/viewproformafinal/" + this.invid;
  }

  back() {
    this.location.back();
  }

  getorders() {
    this.salesorder.getAllsalescontract(this.unitid)
      .subscribe(res => {
        if (res) {
          this.saleorders = _.map(res.data, function (num) {
            num.contactid = num.supplier.id;
            return num;
          });
        }
      });
  }
  hsnnames: any;
  commoditynames: any;
  shipaddress: any
  getinvoicedetails() {
    this.show_loader = true;
    this.invoiceservice.getExportInvoiceByID(this.invid).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.invoicedetails = res.data;
        this.shipaddress = this.invoicedetails.ship_address.split('##');
        console.log("shipaddress", this.shipaddress)
        var hsns = [].slice.call(this.invoicedetails.expinvgroup);
        this.hsnnames = hsns.map(f => f.hsnCode).join(', ');
        this.commoditynames = hsns.map(f => f.commodityName).join(', ');
        this.invLineitems = this.invoicedetails.expinvgroup;
        var Totalqty = 0;
        var InvTotal = 0;
        // _.forEach(this.invLineitems, function (val) {
        //   _.forEach(val.inv_grouping_list, function (key) {
        //     _.forEach(key.part_details, function (data) {
        //       var qty = +data.box_quantity;
        //       var subtotal = +data.subtotal;
        //       Totalqty += qty;
        //       InvTotal += subtotal;
        //     })
        //   });
        // })

        _.forEach(this.invLineitems, function (val) {
          _.forEach(val.part_details, function (data) {
            var qty = +data.prod_quantity;
            var subtotal = +data.subtotal;
            Totalqty += qty;
            InvTotal += subtotal;
          })
        })
        this.Totalqty = Totalqty;
        this.InvTotal = InvTotal;
      }
    })
  }

  amountinwords() {
    var converter = require('number-to-words');
    return converter.toWords(this.InvTotal);
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
        <title>EXPORT INVOICE</title>
        <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
        <style>
        .print-view>tbody>tr>td,
        .print-view>tbody>tr>th,
        .print-view>tfoot>tr>td,
        .print-view>tfoot>tr>th,
        .print-view>thead>tr>td,
        .print-view>thead>tr>th {
          border: 1px solid #333 !important; padding:2px 3px !important
        }
        .print-view .print-label p { font-weight : 500 !important;}
        .print-view .print-label { font-weight : 500 !important;}
        .print-view .cs { background-color:#ccc !important;}
        .print-view .cs td { background-color:#ccc !important;}
         table.print-view td p {
         margin: 0!important;
        }
        @page {
          size: auto;
        }

      table.report-container {
      page-break-after:always;
      }
      thead.report-header {
      display:table-header-group;
      }
      tfoot.report-footer {
      display:table-footer-group;
      } 
      </style>
      </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
