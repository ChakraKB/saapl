import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlipPopupComponent } from './slip-popup.component';

describe('SlipPopupComponent', () => {
  let component: SlipPopupComponent;
  let fixture: ComponentFixture<SlipPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlipPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlipPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
