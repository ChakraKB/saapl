import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlipViewComponent } from './slip-view.component';

describe('SlipViewComponent', () => {
  let component: SlipViewComponent;
  let fixture: ComponentFixture<SlipViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlipViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlipViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
