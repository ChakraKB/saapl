import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { InvoiceService } from '../../services/invoice/invoice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { AppConstant } from '../../app.constant';
import * as _ from "underscore";
@Component({
  selector: 'app-slip-view',
  templateUrl: './slip-view.component.html',
  styleUrls: ['./slip-view.component.css']
})
export class SlipViewComponent implements OnInit {
  show_loader: boolean = false;
  url: any;
  slipdetails: any;
  invid: any;
  toastertime: any;
  tosterminlength: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(private location: Location,
    private invoiceservice: InvoiceService,
    private router: Router,
    private acroute: ActivatedRoute,
    private notificate: NotificationsService, ) {
    this.url = this.router.url;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;

    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.invid = params.id;
        console.log("urlparams", this.invid);
      }
    });
  }

  ngOnInit() {
    this.getslipdetails();
  }

  getslipdetails() {
    this.show_loader = true;
    this.invoiceservice.getSlipview(this.unitid, this.invid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.slipdetails = res.data;
        console.log("details", this.slipdetails)
      }
    }, err => {
      this.show_loader = false;
      this.notificate.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      });
    });
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
    <head>
    <title>EXPORT PACKING LIST</title>
    <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
    <style>
    .print-view>tbody>tr>td,
    .print-view>tbody>tr>th,
    .print-view>tfoot>tr>td,
    .print-view>tfoot>tr>th,
    .print-view>thead>tr>td,
    .print-view>thead>tr>th {
      border: 1px solid #333 !important; padding:2px 3px !important
    }
    .print-view .print-label p { font-weight : 500 !important;}
    .print-view .print-label { font-weight : 500 !important;}
    .print-view .cs { background-color:#ccc !important;}
    .print-view .cs td { background-color:#ccc !important;}
     table.print-view td p {
     margin: 0!important;
    }
    @page {
      size: auto;
    }
  table.report-container {page-break-after:always;}
  thead.report-header {
  display:table-header-group;
  }
  tfoot.report-footer {
  display:table-footer-group;
  } 
  </style>
  </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }

  back() {
    this.location.back();
  }

}
