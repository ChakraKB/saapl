import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import * as __ from 'lodash';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router } from '@angular/router';
import { ItemsService } from '../../services/items/items.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { UserService } from '../../services/user/user.service';
import { StoresService } from '../../services/stores/stores.service';
import { BinService } from '../../services/bin/bin.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef;
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  toastertime: any;
  tosterminlength: any;
  statusarray: any;
  tabletimeout: any;
  userdetails: any;
  orderForm: FormGroup;
  totalquantity: any;
  total: any;
  invdate: any;
  salesdate: any
  allorders: any;
  contactlist: any;
  itemlist: any;
  allitems: any;
  payterms: any;
  selectedinvLine: any;
  selectedcon: any;
  protected dataService: CompleterData;
  show_loader: boolean = false;
  empty: boolean = true;
  selectedorder: any;
  selectedbinid: any;
  customer: any;
  from: any;
  to: any;
  payterm: any;
  currency: any;
  deliveryterm: any;
  shipdate: any;
  salesorderarray: any;
  nodata: boolean = true;
  bal0: boolean = false;
  stockdetails: any;
  showDialog: boolean = false;
  itemdetail: any;
  item: any;
  orderno: any;
  invno: any;
  stockentryform: any[];
  allowdecimal: boolean = false;
  stock: any
  allbins: any;
  activebins: any;
  rack: any;
  bin: any;
  type: any;
  checkeditems: any;
  totqty: any;
  filelist: any;
  Sfiles: any;
  files: any;
  filenames: any;

  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private itemservice: ItemsService,
    private userservices: UserService,
    private storeservice: StoresService,
    private completerService: CompleterService,
    private salesorder: SalesorderService,
    private router: Router,
    private binservice: BinService,
    private cdRef: ChangeDetectorRef
  ) {
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.statusarray = AppConstant.APP_CONST.status;
    this.stock = AppConstant.APP_CONST.STOCK;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.orderForm = this._fb.group({
      itemRows: this._fb.array([])
    });
  }

  ngOnInit() {
    this.getitems();
    this.getRacks();
    this.selectedbinid = [];
    this.addNewRow();
    this.type = "In"
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }
  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  onFileChange(event, input) {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    this.filelist = [];
    this.filelist = event.target.files;
    var selectedfiles = event.target.files;
    if (this.filelist[0].type != "application/pdf"
      && this.filelist[0].type != "image/jpeg"
      && this.filelist[0].type != "image/png"
      && this.filelist[0].type != "text/plain"
      && this.filelist[0].type != "image/gif") {
      this.Sfiles = [].slice.call(selectedfiles);
      this.files = this.Sfiles.map(f => f.name).join(', ');
      this.filenames = this.files;
      this.show_loader = true;
      this.storeservice.Importexcel(this.filelist, this.unitid).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.clearFile()
          this.notif.success(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.clearFile()
        this.notif.error("Error", "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', "Please Select Excel Document", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
      this.clearFile()
    }
    console.log("file", this.filelist)
  }

  clearFile() {
    this.filelist = [];
    this.fileInput.nativeElement.value = '';
    this.filenames = "";
  }

  getitems() {
    this.show_loader = true;
    this.contactlist = [];
    this.itemservice.getItemByStatus('active')
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allitems = this.completerService.local(res.data, "component_desc,component,part_no", 'part_no');
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      });
  }

  getRacks() {
    this.binservice.getBinByStatus('active').subscribe(res => {
      if (res) {
        this.activebins = res.data
        this.allbins = this.completerService.local(res.data, 'bin_name', 'bin_name');
      }
    })
  }

  selectorder(order) {
    this.orderForm.reset();
    if (order) {
      if (order.originalObject.uom.decimal_allowed == 1) {
        this.allowdecimal = true;
      } else {
        this.allowdecimal = false;
      }
      this.empty = false;
      this.show_loader = true;
      this.storeservice.getSTockQuantity(order.originalObject.id).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.itemdetail = res.data[0];
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
      console.log("item", JSON.stringify(this.itemdetail))
    }
  }
  selectrack(bin) {
    this.bin = bin.originalObject;
    this.itemdetail.bin_id = this.bin.id;
  }
  getentrydetails(id) {
    this.show_loader = true;
    this.storeservice.getSTockByID(id)
      .subscribe(res => {
        this.showDialog = true;
        this.show_loader = false;
        this.stockdetails = res.data;
      });
  }

  typeselected() {
    if (this.itemdetail) {
      this.checkqty();
    }
  }


  checkqty() {
    if (this.type == "Out") {
      if (this.itemdetail.open_qty < +this.itemdetail.quantity) {
        this.itemdetail.quantity = 0;
        this.notif.warn('Warning', "Your open qty is lesser than your entered qty", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    } else {
      return true
    }
  }

  showpop() {
    if (this.type) {
      if (this.type == "In") {
        this.showDialog = true;
      } else {

      }
    } else {
      this.notif.warn('Warning', "Please Select Stock Type", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  addNewRow() {
    this.nodata = true;
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.push(this.initItemRows());
  }

  deleteRow(index: number) {
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.removeAt(index);
  }

  initItemRows() {
    return this._fb.group({
      bin_id: "",
      bin: "",
      qty: 0,
    });
  }

  getorderform(orderForm) {
    return orderForm.get('itemRows').controls
  }

  addbins() {
    this.itemdetail.quantity = this.totqty
    this.itemdetail.stk_loc = this.orderForm.value.itemRows
    this.showDialog = false;
  }

  selectedbin(item, index) {
    var selectedbin = item.originalObject;

    let temp = this._fb.array([]);
    for (let i = 0; i < this.orderForm.value.itemRows.length; i++) {
      if (index == i) {
        temp.push(
          this._fb.group({
            bin_id: selectedbin.id,
            bin: this.orderForm.value.itemRows[i].bin,
            qty: this.orderForm.value.itemRows[i].qty,
          })
        );
      } else {
        temp.push(
          this._fb.group(this.orderForm.value.itemRows[i]));
      }
    }
    this.orderForm = this._fb.group({
      itemRows: temp
    });
  }

  calcsum() {
    this.totqty = 0
    let self = this
    _.forEach(this.orderForm.value.itemRows, function (val) {
      self.totqty += (+val.qty)
    })
    return self.totqty
  }

  saveentry() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    this.stockentryform = [];
    if (!this.type) {
      validdata = false;
      validationerrorMsg.push("Please Select Type");
    }
    if (this.itemdetail.rate_per_qty == 0) {
      validdata = false;
      validationerrorMsg.push("Please Enter Rate");
    }
    if (!this.itemdetail.stk_loc) {
      validdata = false;
      validationerrorMsg.push("Please Select Location");
    }

    if (validdata) {
      console.log("itemdetail", this.itemdetail)
      if (this.type == "In") {
        this.itemdetail.stock_type = 1
      } else this.itemdetail.stock_type = 2
      this.itemdetail.unit_id = this.unitid
      var formdata = _.omit(this.itemdetail, "id", "location")
      console.log("item", formdata)
      this.show_loader = true;
      this.storeservice.CreateStockQuantity(formdata).subscribe(res => {
        if (res.status == "success") {
          this.resetform();
          this.show_loader = false;
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.show_loader = false;
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }

  resetform() {
    this.item = ""
    this.type = "In"
    this.itemdetail = ""
    this.empty = true;
    this.orderForm.reset();
    this.orderForm = this._fb.group({
      itemRows: this._fb.array([])
    });
    this.addNewRow();
  }
  close() {
    this.showDialog = false;
  }

}
