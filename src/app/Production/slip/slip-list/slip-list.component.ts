import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { AppConstant } from '../../../app.constant';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { StoresService } from 'app/services/stores/stores.service';
class Person {
  role_name: string;
  description: string;
}


@Component({
  selector: 'app-slip-list',
  templateUrl: './slip-list.component.html',
  styleUrls: ['./slip-list.component.css']
})
export class SlipListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  showDialog: boolean = false;
  persons: Person[] = [];
  unitid: any;
  locations: any;
  show_loader: boolean = false;
  constructor(private http: Http, private storesservice: StoresService) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
  }

  ngOnInit() {
    this.LoadAngTable();
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.PACKINGSLIP.GET + this.unitid)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  viewlocation(data) {
    this.show_loader = true;
    this.storesservice.GetSlipLocation(data.id).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.showDialog = true
        this.locations = res.data
      }
    })
  }

  close() {
    this.showDialog = false
  }

}
