import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import * as __ from 'lodash';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router } from '@angular/router';
import { ItemsService } from '../../services/items/items.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { UserService } from '../../services/user/user.service';
import { StoresService } from '../../services/stores/stores.service';
import { ActivatedRoute } from "@angular/router";
import { ChangeDetectorRef } from '@angular/core';
declare let swal: any;
@Component({
  selector: 'app-slip',
  templateUrl: './slip.component.html',
  styleUrls: ['./slip.component.css']
})
export class SlipComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  contactsinvoice: any;
  toastertime: any;
  tosterminlength: any;
  statusarray: any;
  tabletimeout: any;
  userdetails: any;
  orderForm: FormGroup;
  totalquantity: any;
  total: any;
  invdate: any;
  salesdate: any
  allorders: any;
  contactlist: any;
  itemlist: any;
  allitems: any;
  payterms: any;
  selectedinvLine: any;
  selectedcon: any;
  protected dataService: CompleterData;
  show_loader: boolean = false;
  empty: boolean = true;
  selectedorder: any;
  selectedorderid: any;
  customer: any;
  from: any;
  to: any;
  payterm: any;
  currency: any;
  deliveryterm: any;
  shipdate: any;
  salesorderarray: any;
  checkeditems: any[];
  checkedids: any;
  nodata: boolean = true
  barcode: any;
  allcontacts: any;
  saleorders: any[];
  slipid: any;
  url: any;
  selectedcontact: any;
  boxform: FormGroup;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private userservices: UserService,
    private completerService: CompleterService,
    private salesorder: SalesorderService,
    private router: Router,
    private storeservices: StoresService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef) {

    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.slipid = params.id;
        console.log("urlparams", this.slipid);
      }
    });
    this.url = this.router.url;
    this.boxform = this.fb.group({
      itemProdRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.getorders();
    this.getcontacts();
    this.getBoxes()
    this.checkeditems = [];
    this.selectedorderid = [];
    this.viewpackingslip();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  allboxes: any;
  activeBoxes: any;
  getBoxes() {
    this.storeservices.GetConsumables().subscribe(res => {
      if (res.status == "success") {
        this.allboxes = res.data;
        this.activeBoxes = this.completerService.local(res.data, 'combine,component,part_no,component_desc', 'combine');
      }
    })
  }

  addNewProdRow() {
    this.nodata = true;
    const control = <FormArray>this.boxform.controls["itemProdRows"];
    control.push(this.initItemRows());
  }

  deleteProdRow(index: number) {
    const control = <FormArray>this.boxform.controls["itemProdRows"];
    control.removeAt(index);
  }

  initItemRows() {
    return this.fb.group({
      box_part_id: "",
      box_part_no: "",
      box_no: "",
      box_details: "",
      quantity: 0,
      box_weight: 0,
      length: "",
      breadth: "",
      height: ""
    });
  }
  getProdform(prodForm: any) {
    return prodForm.get('itemProdRows').controls
  }

  showProdDialog: boolean = false;
  Prodindex: any;
  prodList: any;
  producedQty: any;
  itemdetails: any;

  showProdpop(ind: number) {
    this.showProdDialog = true;
    this.Prodindex = ind
    let Prodtemp = this.fb.array([]);
    this.itemdetails = this.salesorderarray[ind]
    this.producedQty = this.salesorderarray[ind].reserved_quantity;
    if (!_.isEmpty(this.salesorderarray[ind].packingSlipBoxDetailsList)) {
      for (let i = 0; i < this.salesorderarray[ind].packingSlipBoxDetailsList.length; i++) {
        if (ind == i) {
          Prodtemp.push(
            this.fb.group({
              box_part_id: this.salesorderarray[ind].packingSlipBoxDetailsList[i].box_part_id,
              box_part_no: this.salesorderarray[ind].packingSlipBoxDetailsList[i].box_part_no,
              box_no: this.salesorderarray[ind].packingSlipBoxDetailsList[i].box_no,
              box_details: this.salesorderarray[ind].packingSlipBoxDetailsList[i].box_details,
              quantity: this.salesorderarray[ind].packingSlipBoxDetailsList[i].quantity,
              box_weight: this.salesorderarray[ind].packingSlipBoxDetailsList[i].box_weight,
              length: this.salesorderarray[ind].packingSlipBoxDetailsList[i].length,
              breadth: this.salesorderarray[ind].packingSlipBoxDetailsList[i].breadth,
              height: this.salesorderarray[ind].packingSlipBoxDetailsList[i].height,
            })
          );
        } else {
          Prodtemp.push(
            this.fb.group(this.salesorderarray[ind].packingSlipBoxDetailsList[i]));
        }
      }
      this.boxform = this.fb.group({
        itemProdRows: Prodtemp
      });
    } else {
      this.boxform = this.fb.group({
        itemProdRows: Prodtemp
      });
      this.addNewProdRow();
    }
  }

  selectedProdbin(item: any, index: any) {
    if (item) {
      var selectedprodbin = item.originalObject;
      let temp = this.fb.array([]);
      for (let i = 0; i < this.boxform.value.itemProdRows.length; i++) {
        if (index == i) {
          temp.push(
            this.fb.group({
              box_part_id: selectedprodbin.id,
              box_part_no: selectedprodbin.part_no,
              box_no: this.boxform.value.itemProdRows[i].box_no,
              box_details: this.boxform.value.itemProdRows[i].box_details,
              quantity: this.boxform.value.itemProdRows[i].quantity,
              box_weight: selectedprodbin.weight,
              length: this.boxform.value.itemProdRows[i].length,
              breadth: this.boxform.value.itemProdRows[i].breadth,
              height: this.boxform.value.itemProdRows[i].height,
            })
          );
        } else {
          temp.push(
            this.fb.group(this.boxform.value.itemProdRows[i]));
        }
      }
      this.boxform = this.fb.group({
        itemProdRows: temp
      });
    }
  }

  addboxdetails(index: any) {
    let self = this
    var tot = 0;
    var valid: boolean = true;
    var validationmsg = []
    _.forEach(this.boxform.value.itemProdRows, function (val) {
      tot += (+val.quantity)
      if (val.quantity == 0) {
        valid = false
        validationmsg.push("Please enter Qty for all Boxes ")
      }
      else if (tot > self.producedQty) {
        valid = false;
        validationmsg.push("Your Produced qty is : " + self.producedQty)
      }
    })
    if (valid) {
      this.salesorderarray[index].packingSlipBoxDetailsList = this.boxform.value.itemProdRows;
      this.salesorderarray[index].slip_quantity = tot
      this.showProdDialog = false;
      console.log("list", this.salesorderarray[index])
    } else {
      self.notif.warn('Warning', validationmsg[0], {
        timeOut: self.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: self.tosterminlength
      })
    }
  }

  restricthypens(event: any) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event: any) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getcontacts() {
    this.show_loader = true;
    this.contactlist = [];
    this.userservices.getContactsByCat("Customer")
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  getorders() {
    this.show_loader = true;
    this.salesorder.getAllsalescontract(this.unitid)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.saleorders = _.map(res.data, function (num) {
            num.contactid = num.supplier.id;
            return num
          });
          console.log("orders", this.saleorders)
        }
      });
  }

  getscno(scid: any, i: any) {
    var scno = _.findWhere(this.saleorders, { 'id': scid })
    this.salesorderarray[i].sc_no = scno.sc_no;
    return scno.sc_no
  }

  selectcont(contact: any) {
    var contactinv = [];
    this.allorders = [];
    this.salesorderarray = [];
    this.selectedorderid = [];
    this.nodata = true;
    if (contact) {
      console.log("contact", contact.originalObject)
      this.selectedcontact = contact.originalObject;
      _.filter(this.saleorders, function (num) {
        if (num.contactid == contact.originalObject.id) {
          contactinv.push(num)
        }
      });
      this.contactsinvoice = contactinv
      this.allorders = this.masterservice.formatDataforDropdown("sc_no", "id", this.contactsinvoice);
      console.log("coninv", this.allorders);
    }
  }

  slipdtls: any;
  customername: any;
  ordernumber: any;
  ids: any;
  viewpackingslip() {
    if (this.slipid) {
      this.show_loader = true;
      this.storeservices.getpackingSLipbyId(this.slipid).subscribe(res => {
        if (res.status == "success") {
          this.slipdtls = res.data;
          console.log("scidlist", this.slipdtls.packingSlipMappingList);
          this.ids = [].slice.call(this.slipdtls.packingSlipMappingList);
          this.ordernumber = this.ids.map(f => f.sc_no).join(', ');

          if (this.slipdtls.packingSlipDetailsList) {
            var alterddate = this.splitdate(this.slipdtls.slip_date);
            this.invno = this.slipdtls.slip_no;
            this.barcode = _.isEmpty(this.slipdtls.bar_code) ? '' : this.slipdtls.bar_code;
            this.invdate = alterddate;
            this.customername = this.slipdtls.contact;
            this.salesorderarray = this.slipdtls.packingSlipDetailsList;
            this.show_loader = false;
          }
        }
      })
    }
  }

  checkslipqty(slipqty: any, i: any) {
    if (slipqty > this.salesorderarray[i].reserved_quantity) {
      this.salesorderarray[i].slip_quantity = 0
    }
  }

  splitdate(d: any) {
    var newdate = d.split('-');
    var altereddate =
    {
      date: {
        year: +newdate[2],
        month: +newdate[1],
        day: +newdate[0]
      },
      "formatted": d
    }
    console.log("day", altereddate)
    return altereddate
  }

  selectorder(order: any, flag: any) {
    this.orderDetails = [];
    this.checkeditems = [];
    if (order) {
      if (flag == "selected") {
        this.empty = false;
        this.selectedorderid.push(order.value);
      } else {
        this.selectedorderid = _.reject(this.selectedorderid, function (num) {
          return num == order.value;
        });
      }
      console.log("idlist", JSON.stringify(this.selectedorderid))
      this.getorderdetails(this.selectedorderid);
    }
  }

  getorderdetails(id: any) {
    if (id.length) {
      this.show_loader = true;
      this.salesorder.getByMultipleID(id)
        .subscribe(res => {
          this.show_loader = false;
          if (res) {
            this.nodata = false;
            this.salesorderarray = res.data;
            _.map(this.salesorderarray, function (num) {
              num.XQty = 0,
                num.PQty = 0;
              num.slipqty = 0
              return num;
            });
            console.log("salesdetail", JSON.stringify(this.salesorderarray));
          }
        });
    } else {
      this.salesorderarray = [];
      this.nodata = true;
    }
  }

  proquantity: any;
  calcproqty(sqty, pqty, i) {
    var squantity = +sqty;
    var pquantity = +pqty
    this.salesorderarray[i].PQty = pquantity - squantity
    return pquantity - squantity
  }

  checkauthorized(status: any, item: any, i: any) {
    this.salesorderarray[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.salesorderarray[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  orderno: any;
  invno: any;
  orderDetails: any;
  sameport: boolean = true
  saveentry() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (!this.invno) {
      validdata = false;
      validationerrorMsg.push("Please enter Packing Slip No");
    }
    if (!this.invdate) {
      validdata = false;
      validationerrorMsg.push("Please select Packing Slip date");
    }
    if (!this.orderno) {
      validdata = false;
      validationerrorMsg.push("Please select order no");
    }
    if (_.isEmpty(this.checkeditems)) {
      validdata = false;
      validationerrorMsg.push("Please select any order");
    }

    this.orderDetails = [];
    var PaymentDetails = [];
    this.checkedids = [];
    for (let i = 0; i < this.checkeditems.length; i++) {

      if (!_.contains(this.checkedids, { "sales_order_id": this.checkeditems[i].id })) {

        if (this.checkedids.length) {
          if (this.checkedids[0].port_loading == this.checkeditems[i].port_loading && this.checkedids[0].port_discharge == this.checkeditems[i].port_discharge) {
            this.sameport = true
          } else this.sameport = false
        } else { this.sameport = true }

        var ids =
        {
          "sales_order_id": this.checkeditems[i].sales_order_id,
          "sc_no": this.checkeditems[i].sc_no,
          "port_loading": this.checkeditems[i].port_loading,
          "port_discharge": this.checkeditems[i].port_discharge
        }
        this.checkedids.push(ids)
      }
      if (!this.checkeditems[i].slip_quantity) {
        validdata = false;
        validationerrorMsg.push("Please enter slip quantity");
      }
      // else if (!this.checkeditems[i].box_no || !this.checkeditems[i].box_type) {
      //   validdata = false;
      //   validationerrorMsg.push("Please enter box details");
      // }
      // else
      //   this.orderDetails.push(this.checkeditems[i]);
    }

    if (validdata) {
      if (!this.sameport) {
        swal({
          title: 'Port Mismatch !',
          text: "Selected SC have different ports !! You Want to continue ?",
          type: 'info',
          width: 500,
          padding: 10,
          showCancelButton: true,
          confirmButtonColor: '#ffaa00',
          cancelButtonColor: '#3085d6',
          confirmButtonText: 'Yes!',
          cancelButtonText: 'No',
        }).then((result) => {
          if (result.value) {
            this.save(1)
          }
        })
      } else this.save(0)
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }

  save(flag: any) {
    var checkedidslist = __.uniqWith(this.checkedids, _.isEqual);
    console.log("checkedids", checkedidslist);

    _.forEach(this.checkeditems, function (val) {
      val.salesOrderDetails = { "id": val.id }
    })

    var formdata =
    {
      "unit_id": this.unitid,
      "addr_valid_flag": flag,
      "slip_no": this.invno,
      "bar_code": this.barcode,
      "contact_id": this.selectedcontact.id,
      "contact": this.customer,
      "packingSlipMappingList": checkedidslist,
      "slip_date": this.invdate.formatted,
      "packingSlipDetailsList": this.checkeditems,
    }
    console.log("formdata", JSON.stringify(formdata));
    this.show_loader = true;
    this.storeservices.CreatePkngslip(formdata)
      .subscribe((res) => {
        console.log("res", res.data);
        if (res.status == "success") {
          this.resetform();
          this.show_loader = false;
          this.router.navigate(['/packingslip'])
          this.notif.success('Success', res.message, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.show_loader = false;
          this.notif.error('Error', res.message, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
  }

  resetform() {
    this.invno = "";
    this.barcode = "";
    this.invdate = "";
    this.orderno = "";
    this.customer = "";
    this.salesorderarray = [];
    this.orderDetails = [];
    this.nodata = true;
  }


  close() {
    this.showProdDialog = false;
    this.boxform.reset();
    this.boxform = this.fb.group({
      itemProdRows: this.fb.array([])
    });
    this.addNewProdRow
  }

}
