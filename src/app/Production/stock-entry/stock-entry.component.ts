import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router } from '@angular/router';
import { ItemsService } from '../../services/items/items.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { UserService } from '../../services/user/user.service';
import { StoresService } from '../../services/stores/stores.service';
import { Subject } from 'rxjs/Subject';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-stock-entry',
  templateUrl: './stock-entry.component.html',
  styleUrls: ['./stock-entry.component.css']
})
export class StockEntryComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  toastertime: any;
  tosterminlength: any;
  statusarray: any;
  tabletimeout: any;
  userdetails: any;
  orderForm: FormGroup;
  totalquantity: any;
  total: any;
  invdate: any;
  salesdate: any
  allorders: any;
  contactlist: any;
  itemlist: any;
  allitems: any;
  payterms: any;
  selectedinvLine: any;
  selectedcon: any;
  protected dataService: CompleterData;
  show_loader: boolean = false;
  empty: boolean = true;
  selectedorder: any;
  selectedorderid: any;
  customer: any;
  from: any;
  to: any;
  payterm: any;
  currency: any;
  deliveryterm: any;
  shipdate: any;
  salesorderarray: any;
  nodata: boolean = true;
  bal0: boolean = false;
  stockdetails: any;
  showDialog: boolean = false;
  showQtyDialog: boolean = false;
  show_dialog_loader: boolean = false;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private itemservice: ItemsService,
    private userservices: UserService,
    private storeservice: StoresService,
    private completerService: CompleterService,
    private salesorder: SalesorderService,
    private router: Router) {
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.orderForm = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.getorders()
  }

  getorders() {
    this.show_loader = true;
    this.contactlist = [];
    this.salesorder.getAllsalescontract(this.unitid)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allorders = this.completerService.local(res.data, 'sc_no', 'sc_no');
          this.salesorderarray = [];
          this.dtTrigger.next();
        }
      });
  }

  selectorder(order) {
    if (order) {
      this.empty = false;
      this.selectedorder = order.originalObject;
      this.selectedorderid = this.selectedorder.id
      this.customer = this.selectedorder.supplier.supplier_name;
      this.from = this.selectedorder.port_loading;
      this.to = this.selectedorder.port_discharge;
      this.payterm = this.selectedorder.payment_term_id;
      this.currency = this.selectedorder.currency_id;
      this.deliveryterm = this.selectedorder.delivery_inco_term;
      this.shipdate = this.selectedorder.shipment_date;
      this.getorderdetails(this.selectedorderid);
    }
  }

  getentrydetails(id) {
    this.show_loader = true;
    this.storeservice.getSTockByID(id)
      .subscribe(res => {
        this.showDialog = true;
        this.show_loader = false;
        this.stockdetails = res.data;
      });
  }

  getorderdetails(id) {
    this.show_loader = true;
    this.storeservice.getSTockListByDC(id)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.nodata = false;
          this.salesorderarray = res.data.salesOrderDetails
          _.map(this.salesorderarray, function (num) {
            num.XQty = num.prod_quantity,
              num.PQty = 0;
            return num;
          });
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        }
      });
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  checkpqty(pqty, i) {
    if ((+this.salesorderarray[i].PQty + this.salesorderarray[i].XQty) > this.salesorderarray[i].quantity ||
      this.salesorderarray[i].PQty == "") {
      this.salesorderarray[i].PQty = 0
    }
  }

  calcbalnce(qty, xqty, pqty, stkallocqty) {
    if (!isNaN(pqty)) {
      var x = +qty;
      var y = +xqty;
      var z = +pqty;
      var val = x - stkallocqty
      return val
    }
  }

  Partstockdetails: any
  partdtl: any
  prodqty: any
  index: number
  openpop(item, i) {
    this.index = i
    this.show_loader = true;
    this.partdtl = item;
    this.prodqty = 0;
    this.storeservice.getSTockByPart(item.id, this.salesorderarray[0].id).subscribe(res => {
      this.showQtyDialog = true;
      this.show_loader = false;
      if (res.status == "success") {
        this.Partstockdetails = res.data
      }
    })
  }

  allowqty(reqqty, producedqty, prodqty) {
    var bal = reqqty - producedqty
    if (prodqty > bal) {
      this.prodqty = 0
    }
  }

  close() {
    this.showDialog = false;
    this.showQtyDialog = false;
    this.prodqty = 0
  }

  orderno: any;
  invno: any;
  stockentryform: any[];
  saveentry(i) {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    this.stockentryform = [];
    if (!this.prodqty) {
      validdata = false;
      validationerrorMsg.push("Please Enter Qty");
    }
    var Obj =
    {
      "unit_id": this.unitid,
      "sales_order_id": this.selectedorderid,
      "salesordDetails": { 'id': this.salesorderarray[i].id },
      "part_id": this.salesorderarray[i].part_id,
      "part_no": this.salesorderarray[i].part_no,
      "prod_quantity": this.prodqty
    }
    console.log("stockform", Obj)
    if (validdata) {
      // var formdata =
      // {
      //   "prodEntrylist": Obj
      // }
      this.show_dialog_loader = true;
      this.storeservice.CreateStockEntry(Obj).subscribe(res => {
        this.show_dialog_loader = false;
        if (res.status == "success") {
          this.showQtyDialog = false;
          this.resetform();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_dialog_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }

  resetform() {
    this.invno = "";
    this.invdate = "";
    this.orderno = "";
    this.customer = "";
    this.salesorderarray = [];
    if (this.dtElement.dtInstance) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      })
    } else
      this.dtTrigger.next();
    this.from = "";
    this.to = "";
    this.payterm = "";
    this.currency = "";
    this.deliveryterm = "";
    this.shipdate = "";
    this.nodata = true;
  }
}
