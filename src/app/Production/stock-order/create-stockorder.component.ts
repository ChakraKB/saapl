import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DataTableDirective } from 'angular-datatables';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemsService } from '../../services/items/items.service';
import { UserService } from '../../services/user/user.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service';

@Component({
  selector: 'app-stock-order',
  templateUrl: './create-stockorder.component.html',
  styleUrls: ['./create-stockorder.component.css']
})
export class StockOrderComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  public date = new Date();
  mydisoptionright: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true,
    disableUntil: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() - 1 }
  };

  mydisoptionleft: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    disableUntil: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() - 1 }
  };
  toastertime: any;
  tosterminlength: any;
  unitid: any;
  scorderarray: any;
  orderForm: FormGroup;
  podate: any;
  salesdate: any;
  show_loader: boolean = false;
  allitems: any;
  activeitems: any;
  nodata: boolean = false;
  selectedorderLine: any;
  so: any = {};
  currentdate: any;
  soid: any
  url: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notificate: NotificationsService,
    private http: Http,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private userservices: UserService,
    private itemservice: ItemsService,
    private completerService: CompleterService,
    private salesorder: SalesorderService,
    private acroute: ActivatedRoute,
    private router: Router
  ) {
    this.url = this.router.url
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.scorderarray = AppConstant.APP_CONST.ScOrder;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.soid = this.acroute.snapshot.queryParamMap.get('page');
    this.orderForm = this._fb.group({
      itemRows: this._fb.array([])
    });
  }

  ngOnInit() {
    this.so.scorder = "Stock Order";
    let date = new Date();
    this.alterdateformat(date);
    this.currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    this.podate = { date: this.currentdate, formatted: this.so.salesdate };
    this.addNewRow();
    this.getitems();
    if (this.soid) {
      this.viewstock();
    }
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  initItemRows() {
    return this._fb.group({
      id: "",
      line: "",
      partid: "",
      part_no: "",
      part: "",
      part_type: "",
      partdesc: "",
      component: "",
      component_desc: "",
      squantity: 0,
      material: "",
      materialrev: "",
      drawing: "",
      rev: "",
      Qty: 0,
      uom: "",
      shipmentDate: "",
      Remarks: "",
    });
  }

  getorderform(orderForm) {
    return orderForm.get('itemRows').controls
  }

  addNewRow() {
    this.nodata = true;
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.removeAt(index);
    if (this.orderForm.value.itemRows.length == 0) {
      this.nodata = false;
    }
  }

  alterdateformat(date) {
    this.so.salesdate = formatDate(date);
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }

  getitems() {
    this.show_loader = true;
    this.itemservice.getItemByStatus('active')
      .subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.allitems = res.data
          this.activeitems = this.completerService.local(this.allitems, "component,component_desc,component,part_no", 'part_no');
        }
      }, err => {
        this.notificate.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      });
  }
  stocklineitems: any;
  stockdetails: any
  viewstock() {
    this.show_loader = true;
    this.salesorder.getStockOrderBYid(this.soid).subscribe(res => {

      if (res.status == "success") {
        this.stockdetails = res.data;
        this.stocklineitems = res.data.salesOrderDetails;
        this.so.scorder = this.stockdetails.sc_order;
        this.so.scno = this.stockdetails.sc_no;
        this.so.salesdate = this.stockdetails.sc_date;

        let temp = this.fb.array([]);
        for (let i = 0; i < this.stocklineitems.length; i++) {
          if (this.stocklineitems[i].pconfirm_date) {
            var alterddate = this.splitdate(this.stocklineitems[i].pconfirm_date);
            console.log("alterdate", alterddate)
          }
          temp.push(
            this.fb.group({
              id: parseInt(this.stocklineitems[i].id),
              line: this.stocklineitems[i].line_no,
              partid: this.stocklineitems[i].part_id,
              part_no: this.stocklineitems[i].part_no,
              part_type: this.stocklineitems[i].part_type,
              part: this.stocklineitems[i].component,
              partdesc: this.stocklineitems[i].component_desc,
              material: this.stocklineitems[i].material_spec,
              materialrev: this.stocklineitems[i].material_spec_rev,
              component: this.stocklineitems[i].component,
              component_desc: this.stocklineitems[i].component_desc,
              squantity: this.stocklineitems[i].squantity,
              drawing: this.stocklineitems[i].dwg_no,
              rev: this.stocklineitems[i].revesion,
              uom: this.stocklineitems[i].uom,
              Qty: parseInt(this.stocklineitems[i].quantity),
              shipmentDate: alterddate,
              Remarks: this.stocklineitems[i].remarks,
            })
          );
        }
        this.orderForm = this.fb.group({
          itemRows: temp
        });
      }
    })
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
      {
        date: {
          year: +newdate[2],
          month: +newdate[1],
          day: +newdate[0]
        },
        "formatted": d
      }
    console.log("day", altereddate)
    return altereddate
  }

  selectedline(item, index) {
    this.selectedorderLine = item.originalObject;
    console.log("item====>", this.selectedorderLine)
    let temp = this._fb.array([]);
    for (let i = 0; i < this.orderForm.value.itemRows.length; i++) {
      if (index == i) {
        temp.push(
          this._fb.group({
            id: this.orderForm.value.itemRows[i].id,
            sales_order_id: this.orderForm.value.itemRows[i].sales_order_id,
            line: this.orderForm.value.itemRows[i].line,
            partid: this.selectedorderLine.id,
            part_no: this.selectedorderLine.part_no,
            part_type: this.selectedorderLine.part_type,
            part: this.selectedorderLine.component,
            partdesc: this.selectedorderLine.component_desc,
            material: this.selectedorderLine.material.material,
            materialrev: this.selectedorderLine.material_spec_rev,
            component: this.selectedorderLine.component,
            component_desc: this.selectedorderLine.component_desc,
            squantity: this.orderForm.value.itemRows[i].squantity,
            drawing: this.selectedorderLine.dwg_no,
            rev: this.selectedorderLine.rev,
            Qty: 0,
            shipmentDate: this.orderForm.value.itemRows[i].shipmentDate,
            uom: this.selectedorderLine.uom.uom_name,
            Remarks: ""
          })
        );
      } else {
        temp.push(
          this._fb.group(this.orderForm.value.itemRows[i]));
      }
    }
    this.orderForm = this._fb.group({
      itemRows: temp
    });
    console.log(this.orderForm.value.itemRows);
  }
  formdata: any
  saveOrder() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    if (this.so.scorder == 0) {
      validdata = false;
      validationerrorMsg.push("Please select sc type");
    }
    if (_.isEmpty(this.so.scno)) {
      validdata = false;
      validationerrorMsg.push("Please enter sc no");
    }

    var orderDetails = [];
    var PaymentDetails = [];
    _.forEach(this.orderForm.value.itemRows, function (item, key) {
      var shipmentdate = ""
      if (item.shipmentDate) {
        shipmentdate = item.shipmentDate.formatted
      } else {
        shipmentdate = ""
      }
      var orderlineitem =
        {
          "line_no": item.line,
          "part_id": item.partid,
          "part_no": item.part_no,
          "part_type": item.part_type,
          "material_spec": item.material,
          "material_spec_rev": item.materialrev,
          "component": item.component,
          "component_desc": item.component_desc,
          "squantity": item.squantity,
          "prod_quantity": 0,
          "revesion": item.rev,
          "dwg_no": item.drawing,
          "quantity": item.Qty,
          "uom": item.uom,
          "shipment_date": shipmentdate,
          "remarks": item.Remarks
        }
      if (_.isEmpty(item.part_no)) {
        validdata = false;
        validationerrorMsg.push("Please select Part_no");
      }
      else if (item.Qty == 0) {
        validdata = false;
        validationerrorMsg.push("Please enter qty");
      }
      else if (_.isEmpty(item.shipmentDate)) {
        validdata = false;
        validationerrorMsg.push("Please select ship date");
      }
      else {
        orderDetails.push(orderlineitem);
      }
      console.log("orderdetails", orderDetails)
    });

    if (validdata) {
      this.formdata =
        {
          "unit_id": this.unitid,
          "sc_no": this.so.scno,
          "sc_order": this.so.scorder,
          "sc_date": this.so.salesdate,
          "salesOrderDetails": orderDetails,
        }
      console.log("formdata", this.formdata)
      this.show_loader = true;
      this.salesorder.CreateStockOrder(this.formdata)
        .subscribe(res => {
          this.show_loader = false;
          if (res.status == "success") {
            this.resetform();
            this.router.navigate(['/stockorder'])
            this.notificate.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.notificate.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        });
    } else {
      this.notificate.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  resetform() {
    this.orderForm = this._fb.group({
      itemRows: this._fb.array([])
    });
    this.addNewRow();
    this.so.scorder = "Stock Order";
    this.so.scno = "";
    this.podate = { date: this.currentdate, formatted: this.salesdate }
  }


}
