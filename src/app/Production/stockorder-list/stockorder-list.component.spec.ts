import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockorderListComponent } from './stockorder-list.component';

describe('StockorderListComponent', () => {
  let component: StockorderListComponent;
  let fixture: ComponentFixture<StockorderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockorderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockorderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
