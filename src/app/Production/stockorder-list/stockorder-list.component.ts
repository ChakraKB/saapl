import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { Router, NavigationExtras } from '@angular/router';
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
class Person { }
@Component({
  selector: 'app-stockorder-list',
  templateUrl: './stockorder-list.component.html',
  styleUrls: ['./stockorder-list.component.css']
})
export class StockorderListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  show_loader: boolean = false;
  scrolltime: any;
  tabletimeout: any;
  unitid: any;
  showtab: boolean = false
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private salesservice: SalesorderService,
    private http: Http,
    private router: Router,
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
  }

  ngOnInit() {
    this.getStockList();
  }

  getStockList() {
    this.show_loader = true;
    this.salesservice.getAllStockOrders(this.unitid).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.persons = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  viewstock(id) {
    this.router.navigate(['/viewstockorder'], { queryParams: { "page": id } })
  }
  stockorder: any;
  salesorderdetails: any;
  groupedconfirmdate: any;

  getorderdetails(id) {
    this.show_loader = true
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    });
    this.salesservice.getStockOrderBYid(id)
      .subscribe(res => {
        this.stockorder = res.data;
        this.salesorderdetails = this.stockorder.salesOrderDetails;
        this.groupedconfirmdate = _.groupBy(this.salesorderdetails, 'pconfirm_date');
        this.show_loader = false;
      });
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Order Confirmation</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  cancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

}
