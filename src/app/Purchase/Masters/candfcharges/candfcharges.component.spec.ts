import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandfchargesComponent } from './candfcharges.component';

describe('CandfchargesComponent', () => {
  let component: CandfchargesComponent;
  let fixture: ComponentFixture<CandfchargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandfchargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandfchargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
