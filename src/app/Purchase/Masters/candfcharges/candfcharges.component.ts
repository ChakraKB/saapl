import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../../services/master.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
declare let swal: any;
import { DataTableDirective } from 'angular-datatables';
import { PurchaseMastersService } from '../../../services/Purchase/purchase-masters.service';
class Person {
  role_name: string;
  description: string;
}

@Component({
  selector: 'app-candfcharges',
  templateUrl: './candfcharges.component.html',
  styleUrls: ['./candfcharges.component.css']
})
export class CandfchargesComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  role_name: any;
  description: any;
  toastertime: number;
  tosterminlength: number;
  candfchargeform: FormGroup;
  id: any;
  formdata: any;
  buttonname: string;
  active_status: any;
  statusarray: any;
  response: any;
  allrole: any;
  candf: any = {};
  data: any;
  tabletimeout: any;
  sampleform: FormGroup;
  show_loader: boolean = false;
  scrolltime: number;
  message: any = {};
  created_date: any;
  modearray: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private purchasemasterservice: PurchaseMastersService,
    private http: Http) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.modearray = AppConstant.APP_CONST.Mode;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.candfchargeform = fb.group({
      'id': [null],
      'mode': [null, Validators.required],
      'charge': [null, Validators.required],
      'active_status': [null, Validators.required]
    })
  }
  ngOnInit() {
    this.buttonname = "Save";
    this.candf.active_status = 1;
    this.LoadAngTable();
  }

  getroles() {
    this.show_loader = true
    this.persons = [];
    this.purchasemasterservice.getallCFCharge()
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.CF_CHARGE.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }


  updatecfcharge(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.candf.id = data.id
    this.candf.mode = data.transport_mode;
    this.candf.charge = data.charge;
    this.candf.active_status = data.active_status;
    this.created_date = data.created_date
  }

  deletecfcharge(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.purchasemasterservice.DeleteCFCharge(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response)
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getroles();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }


  formObj: any = {
    mode:
      {
        required: "Transport mode required",
      },
    charge:
      {
        required: "Charge required",
      },
    active_status:
      {
        required: "Status required",
      }
  }

  addeditcfcharge(data) {
    if (this.candfchargeform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.candfchargeform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {
      if (data.id) {
        this.show_loader = true
        this.formdata =
          {
            "id": data.id,
            "transport_mode": data.mode,
            "charge": data.charge,
            "active_status": data.active_status,
            'created_date': this.created_date,
          }
        this.purchasemasterservice.UpdateCFCharge(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getroles();
                this.show_loader = false;
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getroles();
                this.show_loader = false;
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true
        this.formdata =
          {
            "transport_mode": data.mode,
            "charge": data.charge,
            "active_status": data.active_status
          }
        console.log("addformdata ==> ", this.formdata)
        this.purchasemasterservice.CreateCFCharge(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getroles();
              });
              this.resetform();
              this.show_loader = false;
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getroles();
              });
              this.show_loader = false;
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })

            }
          });
      }
    }
  }

  resetform() {
    this.candfchargeform.reset();
    this.buttonname = "Save";
    this.candfchargeform.controls['active_status'].setValue(1);
  }

}
