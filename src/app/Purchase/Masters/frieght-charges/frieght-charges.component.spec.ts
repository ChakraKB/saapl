import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrieghtChargesComponent } from './frieght-charges.component';

describe('FrieghtChargesComponent', () => {
  let component: FrieghtChargesComponent;
  let fixture: ComponentFixture<FrieghtChargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrieghtChargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrieghtChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
