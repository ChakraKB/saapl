import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceBasisComponent } from './price-basis.component';

describe('PriceBasisComponent', () => {
  let component: PriceBasisComponent;
  let fixture: ComponentFixture<PriceBasisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceBasisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceBasisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
