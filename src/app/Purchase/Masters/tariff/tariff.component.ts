import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../../services/master.service'
import { RolesService } from '../../../services/Roles/roles.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
declare let swal: any;
import { DataTableDirective } from 'angular-datatables';
import { PurchaseMastersService } from '../../../services/Purchase/purchase-masters.service';
class Person {
  role_name: string;
  description: string;
}
@Component({
  selector: 'app-tariff',
  templateUrl: './tariff.component.html',
  styleUrls: ['./tariff.component.css']
})
export class TariffComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  role_name: any;
  description: any;
  toastertime: number;
  tosterminlength: number;
  tariffform: FormGroup;
  id: any;
  formdata: any;
  buttonname: string;
  active_status: any;
  statusarray: any;
  response: any;
  allrole: any;
  tariff: any = {};
  data: any;
  tabletimeout: any;
  sampleform: FormGroup;
  show_loader: boolean = false;
  scrolltime: number;
  message: any = {};
  created_date: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private purchasemasterservice: PurchaseMastersService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.tariffform = fb.group({
      'id': [null],
      'tariffcode': [null, Validators.required],
      'bcd': [null, Validators.required],
      'cvd': [null, Validators.required],
      'cesscvd': [null, Validators.required],
      'sechighcvd': [null, Validators.required],
      'cecess': [null, Validators.required],
      'cusseccess': [null, Validators.required],
      'addduty': [null, Validators.required],
      'epcg': [null, Validators.required],
      'total': [null, Validators.required],
      'active_status': [null, Validators.required],
    })
  }
  ngOnInit() {
    this.buttonname = "Save";
    this.active_status = 1;
    this.LoadAngTable();
  }

  gettariff() {
    this.show_loader = true
    this.persons = [];
    this.purchasemasterservice.getallTariff()
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.TARIFF.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  updatetariff(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.id = data.id
    this.tariff.tariffcode = data.tt_code;
    this.tariff.bcd = data.bcd;
    this.tariff.cvd = data.cvd;
    this.tariff.cesscvd = data.edu_cess_cvd;
    this.tariff.sechighcvd = data.sec_hc_cvd;
    this.tariff.cecess = data.custom_edu_cess;
    this.tariff.cusseccess = data.cs_edu_cess;
    this.tariff.addduty = data.add_duty;
    this.tariff.epcg = data.epcg;
    this.tariff.total = data.total;
    this.active_status = data.active_status;
    this.created_date = data.crt_dt
  }

  deletetariff(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.purchasemasterservice.DeleteTariff(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response)
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettariff();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  formObj: any = {
    tariffcode:
      {
        required: "Tariff code required",
      },
    bcd:
      {
        required: "BCD(%) required",
      },
    cvd:
      {
        required: "CVD(%) required",
      },
    cesscvd:
      {
        required: "Edu.cess on CVD(%) required",
      },
    sechighcvd:
      {
        required: "Sec.High cess on CVD(%)",
      },
    cecess:
      {
        required: "Custom educational cess(%)",
      },
    cusseccess:
      {
        required: "Customs Sec& High Edu.Cess(%)",
      },
    addduty:
      {
        required: "Additional Duty(%)",
      },
    epcg:
      {
        required: "EPCG(%)",
      },
    total:
      {
        required: "Total(%)",
      },
    active_status:
      {
        required: "Status Required",
      }

  }

  addedittariff(data) {
    if (this.tariffform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.tariffform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {
      if (data.id) {
        this.formdata =
          {
            "id": data.id,
            "tt_code": data.tariffcode,
            "bcd": data.bcd,
            "cvd": data.cvd,
            "edu_cess_cvd": data.cesscvd,
            "sec_hc_cvd": data.sechighcvd,
            "custom_edu_cess": data.cecess,
            "cs_edu_cess": data.cusseccess,
            "add_duty": data.addduty,
            "epcg": data.epcg,
            "total": data.total,
            "active_status": data.active_status,
            'created_date': this.created_date,
          }
        this.show_loader = true
        this.purchasemasterservice.UpdateTariff(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettariff();
                this.show_loader = false;
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettariff();
                this.show_loader = false;
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          }, (err) => {
            this.notif.error('Error', "Something Wrong", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          });
      }
      else {
        this.show_loader = true
        this.formdata =
          {
            "tt_code": data.tariffcode,
            "bcd": data.bcd,
            "cvd": data.cvd,
            "edu_cess_cvd": data.cesscvd,
            "sec_hc_cvd": data.sechighcvd,
            "custom_edu_cess": data.cecess,
            "cs_edu_cess": data.cusseccess,
            "add_duty": data.addduty,
            "epcg": data.epcg,
            "total": data.total,
            "active_status": data.active_status,
          }
        console.log("addformdata ==> ", this.formdata)
        this.purchasemasterservice.CreateTariff(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettariff();
                this.show_loader = false;
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettariff();
                this.show_loader = false;
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })

            }
          }, (err) => {
            this.notif.error('Error', "Something Wrong", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          });
      }
    }

  }

  resetform() {
    this.tariffform.reset();
    this.buttonname = "Save";
    this.tariffform.controls['active_status'].setValue(1);
  }

}
