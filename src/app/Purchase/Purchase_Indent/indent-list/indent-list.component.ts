import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../../services/master.service';
import { RolesService } from '../../../services/Roles/roles.service';
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { PurchaseTransactionService } from '../../../services/Purchase/transactions/purchase-transaction.service';
class Person {
  id: any;
  role_name: string;
  description: string;
  checkauthorize: boolean = false;
}

@Component({
  selector: 'app-indent-list',
  templateUrl: './indent-list.component.html',
  styleUrls: ['./indent-list.component.css']
})
export class IndentListComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  files: any;
  indentstatus: any;
  showDialog: boolean = false;
  showtab: boolean = false;
  nofiles: boolean = false;
  filter: any = {};
  purDetailsList: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private purchaseindentservice: PurchaseTransactionService,
    private http: Http
  ) {
    this.indentstatus = AppConstant.APP_CONST.IndentStatus
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.filter.status = 0;
    this.searchindent();
  }

  searchindent() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
        {
          "auth_sts": this.filter.status,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
          "unit_id": this.unitid
        }
      this.show_loader = true;
      this.purchaseindentservice.GetFilteredIndent(obj).subscribe(res => {
        this.showtab = false;
        setTimeout(() => {
          jQuery('#list')[0].click();
        });
        if (res.status == "success") {
          this.show_loader = false;
          this.persons = res.data;
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        }
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  getfiles(data) {
    this.show_loader = true;
    this.purchaseindentservice.getFilesByid(data).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        console.log(res.data);
        this.files = res.data;
        if (!this.files.length) this.nofiles = true;
        else this.nofiles = false;
        this.showDialog = true;
      }
    })
  }
  viewindent(data) {
    this.purDetailsList = data;
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    });
  }

  notifycancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

  downloadfile(file) {
    let byteCharacters = atob(file.filecontent);
    let byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++)
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    let byteArray = new Uint8Array(byteNumbers);
    let blob = new Blob([byteArray], { type: file.filetype });
    importedSaveAs(blob, file.filename)
  }

}
