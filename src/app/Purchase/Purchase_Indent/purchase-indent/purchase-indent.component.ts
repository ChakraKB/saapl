import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../../services/master.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { DepartmentsService } from '../../../services/department/departments.service'
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { isUndefined } from 'util';
import { ItemsService } from '../../../services/items/items.service';
import { PurchaseTransactionService } from '../../../services/Purchase/transactions/purchase-transaction.service';
import { UserService } from '../../../services/user/user.service';
import { PlanningService } from '../../../services/Planning/planning.service';

@Component({
  selector: 'app-purchase-indent',
  templateUrl: './purchase-indent.component.html',
  styleUrls: ['./purchase-indent.component.css']
})
export class PurchaseIndentComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  public date = new Date();
  mydisoption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    disableUntil: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() - 1 }
  };
  purmaster: any = {};
  purchaseindent: any = {};
  toastertime: any;
  tosterminlength: any;
  statusarray: any;
  tabletimeout: any;
  userdetails: any;
  purchaseindentform: FormGroup;
  purmasterform: FormGroup;
  nodata: boolean = false;
  scorderarray: any;
  buybackarray: any;
  alldpts: any;
  show_dialog_loader : boolean = false;
  show_loader: boolean = false;
  itemlist: any;
  selectedpart: any;
  remarks: any = {};
  others: any = {};
  filelist: any;
  showDialog: boolean = false;
  categoryarray: any;
  protected allitems: CompleterData;
  departs: any;
  indentid: any;
  contactlist: any;
  allsuppliers: any;
  suppliers: any;
  salesdate: any;
  showprev: boolean = false;
  prevdetails: any
  showPrevDialog: boolean = false
  prevsupplier: any;
  planids: any
  workorderlist: any;
  planid: any;
  items: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private completerService: CompleterService,
    private dptservice: DepartmentsService,
    private itemservice: ItemsService,
    private userservices: UserService,
    private purchaseindentservice: PurchaseTransactionService,
    private router: Router,
    private acroute: ActivatedRoute,
    private planservice: PlanningService,
  ) {
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    console.log("userdetails", this.userdetails.username);
    this.scorderarray = AppConstant.APP_CONST.IndentTypes;
    this.categoryarray = AppConstant.APP_CONST.PurchaseCategory;
    this.buybackarray = AppConstant.APP_CONST.BuyBackTypes;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.planids = this.acroute.snapshot.queryParamMap.get('page');
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.indentid = params.id;
      }
    });
    this.purchaseindentform = this._fb.group({
      itemRows: this._fb.array([])
    });
    this.purmasterform = fb.group({
      'id': [null],
      'dpt': [null, Validators.required],
      'deldate': [null, Validators.required],
      'inddate': [null, Validators.required]
    })
  }

  ngOnInit() {
    this.getpurchase()
    this.addNewRow();
    this.getdepartments();
    this.getitems();
    this.getsuppliers();
    this.filelist = [];
    let date = new Date();
    this.alterdateformat(date);
    var currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    // this.purmaster.inddate = { date: currentdate, formatted: this.salesdate }
    if (this.indentid) {
      this.viewIndent()
    }
  }

  getpurchase() {
    if (this.planids != null) {
      var ids = atob(this.planids);
      this.planid = ids.split(',');
      console.log("planids", this.planid);
      this.setpurchase()
    } else {
      this.planid = []
    }
  }
  purchaselist: any;
  purchase: any;

  setpurchase() {
    this.show_loader = true;
    this.planservice.GetAllocatedList().subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        let self = this
        this.purchaselist = res.data.purchaseList;
        this.purchase = []
        this.planid.forEach(function (object, index) {
          var data = _.findWhere(self.purchaselist, { 'id': (+object) });
          self.purchase.push(data)
        })
        console.log("items", JSON.stringify(this.purchase));
        this.setlist()
      }
    })
  }

  setlist() {
    let temp = this.fb.array([]);
    for (let i = 0; i < this.purchase.length; i++) {
      this.show_loader = true;
      this.purchaseindentservice.getPrevPodetails(this.purchase[i].part_id).subscribe(res => {
        if (res.status == "success") {
          this.prevdetails = res.data
          console.log("prevpo", this.prevdetails);
          temp.push(
            this._fb.group({
              id: this.purchase[i].part_id,
              partno: this.purchase[i].part_no,
              partdesc: this.purchase[i].part_desc,
              uom: this.purchase[i].uom,
              qty: this.purchase[i].qty,
              prevrate: _.isEmpty(this.prevdetails.poitem) ? 0 : this.prevdetails.poitem[0].rate,
              value: 0,
              tax: "",
              category: "",
              buyback: "",
              type: "",
              preffersupp: "",
              make: "",
              addspec: "",
              intremarks: "",
              purpose: "",
              prev_po: "",
              prevsupid: 0,
              prevsupname: "",
              prevvalidity: null,
              prev: _.isEmpty(this.prevdetails) ? false : true
            })
          );
        } else {
          temp.push(
            this._fb.group({
              id: this.purchase[i].part_id,
              partno: this.purchase[i].part_no,
              partdesc: this.purchase[i].part_desc,
              uom: this.purchase[i].uom,
              qty: this.purchase[i].qty,
              prevrate: _.isEmpty(this.prevdetails.poitem) ? 0 : this.prevdetails.poitem[0].rate,
              value: 0,
              tax: "",
              category: "",
              buyback: "",
              type: "",
              preffersupp: "",
              make: "",
              addspec: "",
              intremarks: "",
              purpose: "",
              prev_po: "",
              prevsupid: 0,
              prevsupname: "",
              prevvalidity: null,
              prev: _.isEmpty(this.prevdetails) ? false : true
            })
          );
        }
      });
    }
    this.purchaseindentform = this.fb.group({
      itemRows: temp
    });
    this.show_loader = false;
  }

  alterdateformat(date) {
    this.purmaster.inddate = formatDate(date);
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getdepartments() {
    this.dptservice.getByStatus("active").subscribe(res => {
      if (res.status == "success") {
        this.departs = res.data;
        this.alldpts = this.completerService.local(res.data, 'department_name', 'department_name');
      }
    })
  }

  getitems() {
    this.show_loader = true
    this.itemlist = [];
    this.itemservice.getItemByStatus("active")
      .subscribe(res => {
        if (res) {
          this.show_loader = false
          this.allitems = this.completerService.local(res.data, "component_desc,component,part_no", 'part_no');
        }
      });
  }

  getsuppliers() {
    this.contactlist = [];
    this.userservices.getContactsByCat("Supplier")
      .subscribe(res => {
        if (res) {
          this.suppliers = res.data;
          this.allsuppliers = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  getpurchaseindentform(purchaseindentform) {
    return purchaseindentform.get('itemRows').controls
  }

  initItemRows() {
    return this._fb.group({
      id: "",
      partno: "",
      part: "",
      partdesc: "",
      material: "",
      uom: "",
      qty: 0,
      prevrate: "",
      value: 0,
      tax: "",
      category: "",
      buyback: "",
      type: "",
      preffersupp: "",
      make: "",
      addspec: "",
      intremarks: "",
      purpose: "",
      prev: false,
      prevsupid: 0,
      prevsupname: "",
      weight: 0,
      prevvalidity: null,
    });
  }

  addNewRow() {
    this.nodata = true;
    const control = <FormArray>this.purchaseindentform.controls["itemRows"];
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    const control = <FormArray>this.purchaseindentform.controls["itemRows"];
    control.removeAt(index);
  }

  selectedline(item, index) {
    this.selectedpart = item.originalObject;
    console.log("item====>", this.selectedpart);
    let self = this
    var a = _.filter(self.purchaseindentform.value.itemRows, function (num) {
      return num.partno == self.selectedpart.part_no;
    });
    if (a.length > 1) {
      let temp = this._fb.array([]);
      for (let i = 0; i < this.purchaseindentform.value.itemRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              id: 0,
              partno: "",
              part: "",
              partdesc: "",
              material: "",
              uom: "",
              qty: 0,
              prevrate: "",
              value: 0,
              tax: "",
              category: "",
              buyback: "",
              type: "",
              preffersupp: "",
              make: "",
              addspec: "",
              intremarks: "",
              purpose: "",
              prev_po: "",
              prevsupid: 0,
              prevsupname: "",
              prevvalidity: null,
              weight: 0,
              prev: false
            })
          );
        } else {
          temp.push(
            this._fb.group(this.purchaseindentform.value.itemRows[i]));
        }
      }
      this.purchaseindentform = this._fb.group({
        itemRows: temp
      });

      this.notif.warn('Warning', this.selectedpart.part_no + " is already added", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false
    }

    this.show_loader = true;
    this.purchaseindentservice.getPrevPodetails(this.selectedpart.id).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.prevdetails = res.data
        console.log("prevpo", this.prevdetails);
        let temp = this._fb.array([]);
        if (this.prevdetails) {
          this.showprev = true
          for (let i = 0; i < this.purchaseindentform.value.itemRows.length; i++) {
            if (index == i) {
              temp.push(
                this._fb.group({
                  id: this.selectedpart.id,
                  partno: this.selectedpart.part_no,
                  part: this.selectedpart.component,
                  partdesc: this.selectedpart.component_desc,
                  material: this.selectedpart.material.material,
                  uom: this.selectedpart.uom.short_name,
                  qty: this.purchaseindentform.value.itemRows[i].qty,
                  prevrate: _.isEmpty(this.prevdetails.poitem) ? 0 : this.prevdetails.poitem[0].rate,
                  value: this.purchaseindentform.value.itemRows[i].value,
                  tax: this.purchaseindentform.value.itemRows[i].tax,
                  category: this.purchaseindentform.value.itemRows[i].category,
                  buyback: this.purchaseindentform.value.itemRows[i].buyback,
                  type: this.purchaseindentform.value.itemRows[i].type,
                  preffersupp: this.purchaseindentform.value.itemRows[i].preffersupp,
                  make: this.purchaseindentform.value.itemRows[i].make,
                  addspec: this.purchaseindentform.value.itemRows[i].addspec,
                  intremarks: this.purchaseindentform.value.itemRows[i].intremarks,
                  purpose: this.purchaseindentform.value.itemRows[i].purpose,
                  prev_po: this.prevdetails.po_no,
                  prevsupid: this.prevdetails.supplier_id,
                  weight: this.selectedpart.weight,
                  prevsupname: _.isEmpty(this.prevdetails.supplier) ? 0 : this.prevdetails.supplier,
                  prevvalidity: _.isEmpty(this.prevdetails.validity) ? null : this.prevdetails.validity,
                  prev: true
                })
              );
            } else {
              temp.push(
                this._fb.group(this.purchaseindentform.value.itemRows[i]));
            }
          }
          this.purchaseindentform = this._fb.group({
            itemRows: temp
          });
        } else {
          this.showprev = false
          for (let i = 0; i < this.purchaseindentform.value.itemRows.length; i++) {
            if (index == i) {
              temp.push(
                this._fb.group({
                  id: this.selectedpart.id,
                  partno: this.selectedpart.part_no,
                  part: this.selectedpart.component,
                  partdesc: this.selectedpart.component_desc,
                  material: this.selectedpart.material.material,
                  uom: this.selectedpart.uom.short_name,
                  qty: this.purchaseindentform.value.itemRows[i].qty,
                  prevrate: _.isEmpty(this.prevdetails.poitem) ? 0 : this.prevdetails.poitem[0].rate,
                  value: this.purchaseindentform.value.itemRows[i].value,
                  tax: this.purchaseindentform.value.itemRows[i].tax,
                  category: this.purchaseindentform.value.itemRows[i].category,
                  buyback: this.purchaseindentform.value.itemRows[i].buyback,
                  type: this.purchaseindentform.value.itemRows[i].type,
                  preffersupp: this.purchaseindentform.value.itemRows[i].preffersupp,
                  make: this.purchaseindentform.value.itemRows[i].make,
                  addspec: this.purchaseindentform.value.itemRows[i].addspec,
                  intremarks: this.purchaseindentform.value.itemRows[i].intremarks,
                  purpose: this.purchaseindentform.value.itemRows[i].purpose,
                  weight: this.selectedpart.weight,
                  prev_po: "",
                  prevsupid: 0,
                  prevsupname: "",
                  prevvalidity: null,
                  prev: false
                })
              );
            } else {
              temp.push(
                this._fb.group(this.purchaseindentform.value.itemRows[i]));
            }
          }
          this.purchaseindentform = this._fb.group({
            itemRows: temp
          });
        }
      }
    })
  }

  getprevdetails(data, i) {
    console.log("data", data)
    this.show_loader = true
    this.purchaseindentservice.getPrevPodetails(data.value.id).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.prevdetails = res.data.poitem[0];
        this.prevsupplier = res.data;
        console.log("prevsupplier", res.data)
        this.showPrevDialog = true;
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    })
  }

  calcvalue(qty, rate, i) {
    var a = (+qty) * (+rate)
    this.purchaseindentform.controls.itemRows.value[i].value = a;
    return a
  }

  onFileChange(event) {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.filelist.push(event.target.files[i]);
    }
    console.log("files", this.filelist);
  }

  clearfile(file, i) {
    this.filelist.forEach(function (index, object) {
      if (index === i) {
        object.splice(index, 1);
      }
    });
    console.log(this.filelist)
  }

  clearallfile() {
    this.filelist = []
  }

  remarkspop(data, i) {
    this.remarks.index = i
    this.remarks.addspec = data.value.addspec;
    this.remarks.intremarks = data.value.intremarks;
    if (!data.value.purpose) {
      this.remarks.purpose = this.purmaster.compurchase;
    } else {
      this.remarks.purpose = data.value.purpose;
    }

    this.showDialog = true;
  }
  close() {
    this.showDialog = false;
    this.showPrevDialog = false
  }

  saveremarks(data, i) {
    this.purchaseindentform.controls.itemRows.value[i].addspec = data.addspec;
    this.purchaseindentform.controls.itemRows.value[i].intremarks = data.intremarks;
    this.purchaseindentform.controls.itemRows.value[i].purpose = data.purpose;
    this.showDialog = false;
  }

  indentdetails: any;
  PurDetailsList: any;

  viewIndent() {
    this.purchaseindentservice.getIndentByid(this.indentid).subscribe(res => {
      if (res.status == "success") {
        this.indentdetails = res.data;
        this.PurDetailsList = this.indentdetails.purDetailsList;

        this.purmaster.id = this.indentdetails.id;
        this.purmaster.deldate = this.splitdate(this.indentdetails.delivery_date);
        this.purmaster.inddate = this.splitdate(this.indentdetails.intent_date);
        this.purmaster.indno = this.indentdetails.id;
        this.purmaster.compurchase = this.indentdetails.com_pur;
        this.others.capbudgetref = this.indentdetails.cap_bud_ref;
        this.others.justification = this.indentdetails.justification;
        this.others.comments = this.indentdetails.comments;
        this.filelist = this.indentdetails.attachmentList

        let temp = this.fb.array([]);
        for (let i = 0; i < this.PurDetailsList.length; i++) {
          if (this.PurDetailsList[i].shipment_date) {
            var alterddate = this.splitdate(this.PurDetailsList[i].shipment_date);
            console.log("alterdate", alterddate)
          }
          temp.push(
            this.fb.group({
              id: this.PurDetailsList[i].indent_id,
              partno: this.PurDetailsList[i].part_no,
              part: this.PurDetailsList[i].part,
              partdesc: this.PurDetailsList[i].part_desc,
              material: this.PurDetailsList[i].material,
              uom: this.PurDetailsList[i].uom,
              qty: this.PurDetailsList[i].quantity,
              prevrate: this.PurDetailsList[i].prev_rate,
              value: this.PurDetailsList[i].value,
              tax: this.PurDetailsList[i].tax,
              category: this.PurDetailsList[i].category,
              buyback: this.PurDetailsList[i].value,
              type: this.PurDetailsList[i].type,
              weight: this.PurDetailsList[i].weight,
              preffersupp: _.isEmpty(this.PurDetailsList[i].pre_sup_id) ? 0 : this.PurDetailsList[i].pre_sup_id,
              addspec: this.PurDetailsList[i].add_spec_remarks,
              intremarks: this.PurDetailsList[i].internal_remarks,
              purpose: this.PurDetailsList[i].purpose_remarks,
            })
          );
        }
        this.purchaseindentform = this.fb.group({
          itemRows: temp
        });
      }
    })
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
    {
      date: {
        year: +newdate[2],
        month: +newdate[1],
        day: +newdate[0]
      },
      "formatted": d
    }
    console.log("day", altereddate)
    return altereddate
  }

  formObj: any = {
    dpt:
    {
      required: "Department required",
    },
    deldate:
    {
      required: "Delivery Date required",
    },
    indno:
    {
      required: "Indent No required",
    },
    compurchase:
    {
      required: "Common Purchase required",
    },
    inddate:
    {
      required: "Indent Date required",
    }
  }

  saveentry(data) {
    if (this.purmasterform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.purmasterform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else {

      var dpt = _.findWhere(this.departs, { "department_name": data.dpt })
      var validdata: Boolean = true;
      var validationerrorMsg = [];
      var indentlineitems = []
      var self = this;

      this.purchaseindentform.value.itemRows.forEach(function (item, index) {
        var prefsuppid = _.findWhere(self.suppliers, { "supplier_name": item.preffersupp });
        var obj =
        {
          "item": item.id,
          "part_no": item.partno,
          "part": item.part,
          "part_desc": item.partdesc,
          "material": item.material,
          "uom": item.uom,
          "quantity": item.qty,
          "prev_rate": item.prevrate,
          "delivery_date": data.deldate.formatted,
          "value": item.value,
          "tax": item.tax,
          "category": item.category,
          "buyback_disposal": item.buyback,
          "type": item.type,
          "pre_sup_id": _.isEmpty(prefsuppid) ? 0 : prefsuppid.id,
          "add_spec_remarks": item.addspec,
          "internal_remarks": item.intremarks,
          "purpose_remarks": item.purpose,
          "prev_po": item.prev_po,
          "prev_supplier_id": item.prevsupid,
          "prev_supplier_name": item.prevsupname,
          "prev_validity": item.prevvalidity,
          "weight": item.weight
        }
        if (!item.partno) {
          validdata = false;
          validationerrorMsg.push("Please select part");
        } else if (!item.qty) {
          validdata = false;
          validationerrorMsg.push("Please enter quantity");
        } else if (!item.tax) {
          validdata = false;
          validationerrorMsg.push("Please select tax");
        } else if (!item.category) {
          validdata = false;
          validationerrorMsg.push("Please select category");
        } else if (!item.type) {
          validdata = false;
          validationerrorMsg.push("Please select type");
        } else if (!item.intremarks) {
          validdata = false;
          validationerrorMsg.push("Please enter internal remarks");
          self.remarkspop(item, index)
        } else {
          indentlineitems.push(obj);
        }
      });

      if (validdata) {
        var formdata =
        {
          "unit_id": this.unitid,
          "dept": { "id": dpt.id },
          "delivery_date": data.deldate.formatted,
          "indent_no": data.indno,
          "com_pur": data.compurchase,
          "indent_date": data.inddate,
          "cap_bud_ref": _.isEmpty(this.others.capbudgetref) ? "" : this.others.capbudgetref,
          "justification": _.isEmpty(this.others.justification) ? "" : this.others.justification,
          "comments": _.isEmpty(this.others.comments) ? "" : this.others.comments,
          "purDetailsList": indentlineitems,
          "authorized_sts": 0
        }

        console.log("formdata", formdata)
        this.show_loader = true;
        this.purchaseindentservice.AddNewIndent(formdata, this.filelist).subscribe(res => {
          if (res.status == "success") {
            this.show_loader = false;
            this.router.navigate(['/indentlist'])
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength,
            })
          } else {
            this.show_loader = false;
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength,
            })
          }
        }, (err) => {
          this.show_loader = false;
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength,
          })
        })

      } else {
        this.notif.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength,
        })
      }
    }
  }
}
