import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { MasterService } from '../../../services/master.service';
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";

@Component({
  selector: 'app-view-indent',
  templateUrl: './view-indent.component.html',
  styleUrls: ['./view-indent.component.css']
})

export class ViewIndentComponent implements OnInit {
  @Input() purDetailsList: any;
  @Output() notifycancel: EventEmitter<any> = new EventEmitter();

  purDetailslineitems: any;
  constructor() {
  }

  ngOnInit() {
    this.viewdetails()
  }

  viewdetails() {
    if (!_.isEmpty(this.purDetailsList)) {
      this.purDetailslineitems = this.purDetailsList.purDetailsList;
      console.log("purDetailsList", JSON.stringify(this.purDetailsList))
    }
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Order Confirmation</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  cancel() {
    this.notifycancel.next();
  }

}
