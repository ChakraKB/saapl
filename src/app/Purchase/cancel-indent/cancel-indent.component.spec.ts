import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelIndentComponent } from './cancel-indent.component';

describe('CancelIndentComponent', () => {
  let component: CancelIndentComponent;
  let fixture: ComponentFixture<CancelIndentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelIndentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelIndentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
