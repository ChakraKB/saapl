import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelindentAuthorizeComponent } from './cancelindent-authorize.component';

describe('CancelindentAuthorizeComponent', () => {
  let component: CancelindentAuthorizeComponent;
  let fixture: ComponentFixture<CancelindentAuthorizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelindentAuthorizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelindentAuthorizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
