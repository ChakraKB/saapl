import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { PurchaseTransactionService } from '../../services/Purchase/transactions/purchase-transaction.service';

@Component({
  selector: 'app-cancelindent-authorize',
  templateUrl: './cancelindent-authorize.component.html',
  styleUrls: ['./cancelindent-authorize.component.css']
})
export class CancelindentAuthorizeComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  checkedindent: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private purchaseindentservice: PurchaseTransactionService,
    private http: Http
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.getcancelindentlist();
    this.checkedindent = [];
  }

  getcancelindentlist() {
    this.show_loader = true;
    this.purchaseindentservice.getIndentCacnceled(this.unitid).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.persons = res.data;
        this.checkedindent = [];
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      }
    }, (err) => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  checklist(status, item, i) {
    if (status == true) {
      this.checkedindent.push(item);
    } else {
      this.checkedindent.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log("checkedindent", this.checkedindent)
  }

  clearchecked() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].check = false;
    }
    this.checkedindent = [];
  }

  checkcqty(Cqty, i) {
    if (this.persons[i].quantity < Cqty) {
      this.persons[i].cancel_quantity = 0
    }
  }

  saveentry() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    if (!this.checkedindent.length) {
      validdata = false;
      validationerrorMsg.push("Please select any indent");
    } else {
      for (var i = 0; i < this.checkedindent.length; i++) {
        this.checkedindent[i].is_canceled = 2
      }
      console.log(this.checkedindent)
    }
    if (validdata) {
      this.show_loader = true;
      this.purchaseindentservice.UpdateCancelAuth(this.checkedindent).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.getcancelindentlist();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false;
        this.getcancelindentlist();
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
