import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparissionComponent } from './comparission.component';

describe('ComparissionComponent', () => {
  let component: ComparissionComponent;
  let fixture: ComponentFixture<ComparissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
