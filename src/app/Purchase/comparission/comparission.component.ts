import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { RolesService } from '../../services/Roles/roles.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { CompleterService } from 'ng2-completer';
import { ItemsService } from '../../services/items/items.service';
import { PurchaseMastersService } from '../../services/Purchase/purchase-masters.service';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { QuotationService } from '../../services/Purchase/quotation/quotation.service';
import { HsnService } from '../../services/hsn/hsn.service';
import { ChangeDetectorRef } from '@angular/core';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { UserService } from '../../services/user/user.service';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';

@Component({
  selector: 'app-comparission',
  templateUrl: './comparission.component.html',
  styleUrls: ['./comparission.component.css']
})
export class ComparissionComponent implements OnInit {
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  freightarray: any;
  modeofdispatch: any;
  scrolltime: any;
  quotationdetails: any;
  totals: any;
  selected: any;
  enqid: any;
  selecteditems: any;
  paymentForm: FormGroup;
  compareform: FormGroup;
  Paytype: any;
  nodata: boolean = false;
  nopaymentdata: any;
  allpayterms: any;
  payterms: any;
  paytermsLC: any;
  filelist: any;
  comp: any = {};
  pricebasis: any;
  allpricebasis: any;
  yesorno: any;
  formdata: any;
  selecteditemnsupplier: any;
  unitid: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private http: Http,
    private completerService: CompleterService,
    private purchasemasters: PurchaseMastersService,
    private qutationservice: QuotationService,
    private hsnservice: HsnService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private userservices: UserService,
    private router: Router,
    private _fb: FormBuilder,
    private poservice: PurchaseorderService
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.freightarray = AppConstant.APP_CONST.FREIGHT;
    this.modeofdispatch = AppConstant.APP_CONST.DISPATCHMODE;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.Paytype = AppConstant.APP_CONST.PaymentTypes;
    this.yesorno = AppConstant.APP_CONST.YesOrNo

    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.enqid = params.id;
        console.log("urlparams", this.enqid);
      }
    });

    this.compareform = fb.group({
      'freight': [null, Validators.required],
      'dispatchmode': [null, Validators.required],
      'pricebasis': [null],
      'license': [null],
      'epcg': [null],
      'destination': [null],
      'freightamnt': [null],
      'gstamnt': [null],
      'spclinst': [null],
      'addinst' : [null],
      'packingfrwding':[null]
    })

    this.paymentForm = this._fb.group({
      itemPayRows: this._fb.array([])
    });
  }

  ngOnInit() {
    this.getcompare();
    this.getpaymentterms();
    this.getpricebasis();
    this.addNewpaymentRow();
    this.selected = [];
    this.filelist = [];
    this.comp.epcg = "NO";
    this.comp.destination = "Coimbatore"
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  initPayItemRows() {
    return this._fb.group({
      paymenttype: "",
      paymentterm: "",
      paymentpercentage: 0,
      credit: 0
    });
  }

  getpaymentform(paymentform) {
    return paymentform.get('itemPayRows').controls
  }

  getpricebasis() {
    this.show_loader = true
    this.purchasemasters.GetPriceBasisByStatus("active")
      .subscribe(res => {
        if (res) {
          this.show_loader = false;
          this.pricebasis = res.data;
          this.allpricebasis = this.completerService.local(this.pricebasis, 'price_basis', 'price_basis');
        }
      });
  }

  getpaymentterms() {
    this.show_loader = true;
    this.userservices.getallpayterms()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allpayterms = res.data;
          this.payterms = this.completerService.local(res.data, 'term', 'term');
        }
      });
  }

  addNewpaymentRow() {
    var totalpercentage = 0
    _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
      var sum = +item.paymentpercentage
      totalpercentage += sum;
    });
    if (totalpercentage) {
      if (totalpercentage < 100) {
        this.nopaymentdata = true;
        const control = <FormArray>this.paymentForm.controls["itemPayRows"];
        control.push(this.initPayItemRows());
      }
      else {
        this.notif.warn('Warning', "you already entered 100%", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        });
      }
    }
    else {
      this.nopaymentdata = true;
      const control = <FormArray>this.paymentForm.controls["itemPayRows"];
      control.push(this.initPayItemRows());
    }
  }
  deletePayRow(index: number) {
    const control = <FormArray>this.paymentForm.controls["itemPayRows"];
    control.removeAt(index);
    if (this.paymentForm.value.itemPayRows.length == 0) {
      this.nopaymentdata = false;
    }
  }

  paytypeselected(paytype, index) {
    var pay = []
    if (paytype == "LC") {
      pay = _.filter(this.allpayterms, function (num) {
        return num.type == "LC";
      })
      this.paytermsLC = this.completerService.local(pay, 'term', 'term');
      let temp = this._fb.array([]);
      for (let i = 0; i < this.paymentForm.value.itemPayRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              paymenttype: this.paymentForm.value.itemPayRows[i].paymenttype,
              paymentterm: "",
              paymentpercentage: this.paymentForm.value.itemPayRows[i].paymentpercentage,
              credit: this.paymentForm.value.itemPayRows[i].credit
            })
          );
        } else {
          temp.push(
            this._fb.group(this.paymentForm.value.itemPayRows[i]));
        }
      }
      this.paymentForm = this._fb.group({
        itemPayRows: temp
      });
    } else {
      pay = _.filter(this.allpayterms, function (num) {
        return num.type == "NONLC";
      })
      this.payterms = this.completerService.local(pay, 'term', 'term');

      let temp = this._fb.array([]);
      for (let i = 0; i < this.paymentForm.value.itemPayRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              paymenttype: this.paymentForm.value.itemPayRows[i].paymenttype,
              paymentterm: "",
              paymentpercentage: this.paymentForm.value.itemPayRows[i].paymentpercentage,
              credit: this.paymentForm.value.itemPayRows[i].credit
            })
          );
        } else {
          temp.push(
            this._fb.group(this.paymentForm.value.itemPayRows[i]));
        }
      }
      this.paymentForm = this._fb.group({
        itemPayRows: temp
      });
    }

    console.log("pay", pay)
  }

  clacPercentage(index) {
    var totalpercentage = 0;
    _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
      var sum = +item.paymentpercentage
      totalpercentage += sum;
    });
    if (totalpercentage > 100) {
      this.notif.warn('Warning', "you can't enter more than 100%", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  onFileChange(event) {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.filelist.push(event.target.files[i]);
    }
    console.log("files", this.filelist);
  }

  clearfile(file, i) {
    this.filelist.forEach(function (data, index, object) {
      if (index === i) {
        object.splice(index, 1);
      }
    });
    console.log(this.filelist)
  }

  clearallfile() {
    this.filelist = []
  }

  getcompare() {
    this.show_loader = true;
    this.qutationservice.GetCompareList(this.enqid).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false
        this.quotationdetails = res.data;
        this.totals = res.data[0].srview
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  checkauthorized(status, item, i) {
    if (status == true) {
      this.quotationdetails[i].check = true;
      this.selected.push(item)
    } else {
      this.quotationdetails[i].check = false;
      this.selected.forEach(function (data, index, object) {
        if (data.item_code === item.item_code) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.selected);
  }

  selectedcost(parent, data, i) {
    parent.sdview.forEach(function (val, index) {
      if (index == i) {
        val.selected = true;
        console.log("costselected",val)
        return val
      } else {
        val.selected = false;
        return val
      }
    })
  }


  formObj: any = {
    freight: {
      required: "Freight Required",
    },
    dispatchmode: {
      required: "Dispatchmode Required",
    }
  }

  createpo() {
    if (!this.selected.length) {
      this.notif.warn('Warning', "Select item to Create PO", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
    else if (this.compareform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.compareform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
      return false;
    }
    else {
      var validdata: Boolean = true;
      var validationerrorMsg = [];
      var totalpercentage = 0
      var PaymentDetails = [];
      _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
        var sum = +item.paymentpercentage
        totalpercentage += sum
        console.log("totalpercentage", totalpercentage);
        var paymentlineitem =
          {
            "payment_type": item.paymenttype,
            "payment_terms": item.paymentterm,
            "payment_percentage": +item.paymentpercentage,
            "credict": +item.credit,
          }
        if (_.isEmpty(item.paymenttype && item.paymentterm)) {
          validdata = false;
          validationerrorMsg.push("Please select payment details");
        }
        if (item.paymentpercentage != 0) {
          PaymentDetails.push(paymentlineitem);
        }
        else {
          validdata = false;
          validationerrorMsg.push("Please enter percentage");
        }
        console.log("PaymentDetails", PaymentDetails)
      });
      if (totalpercentage > 100 || totalpercentage < 100) {
        validdata = false;
        validationerrorMsg.push("Please Enter 100% Payment");
      }

      this.selecteditemnsupplier = [];
      var poval = 0
      for (var i = 0; i < this.selected.length; i++) {
        this.formdata =
          {
            "part_no": this.selected[i].part_no,
            "enquiry_id": this.selected[i].enquiry_id,
            "validity": this.selected[i].validity,
          }
        for (var j = 0; j < this.selected[i].sdview.length; j++) {
          if (this.selected[i].sdview[j].selected == true) {
            this.formdata.supplier_id = this.selected[i].sdview[j].supplier_id;
            this.formdata.supplier = this.selected[i].sdview[j].supplier;
            this.formdata.quotation_id = this.selected[i].sdview[j].quotation_id;
            this.formdata.quotation_line_id = this.selected[i].sdview[j].quotation_line_id;
            var a = this.selected[i].sdview[j].landing_cost
            poval += a
          }
        }
        this.selecteditemnsupplier.push(this.formdata)
      }
      console.log("sample", poval)

      for (var i = 0; i < this.selecteditemnsupplier.length; i++) {
        if (this.selecteditemnsupplier[i].supplier_id != this.selecteditemnsupplier[0].supplier_id) {
          this.selected = [];
          for (var j = 0; j < this.quotationdetails.length; j++) {
            this.quotationdetails[i].check = false
          }
          this.checkauthorized(true, this.quotationdetails[0], 0);
          $('html, body').animate({
            scrollTop: 0
          }, this.scrolltime);
          validdata = false;
          validationerrorMsg.push("Please select same Supplier");
        }
      }

      if (validdata) {

        var pricebasis = _.findWhere(this.pricebasis, { 'price_basis': this.comp.pricebasis })
        var formdata =
          {
            "unit_id": this.unitid,
            "freight": this.comp.freight,
            "mode_of_despatch": this.comp.dispatchmode,
            // "price_basis_id": pricebasis.id,
            // "price_basis_name": pricebasis.price_basis,
            // "epcg_applicable": this.comp.epcg,
            // "license_no": _.isEmpty(this.comp.license) ? "" : this.comp.license,
            // "destination": _.isEmpty(this.comp.destination) ? "" : this.comp.destination,
            // "freight_amount": _.isEmpty(this.comp.freightamnt) ? 0 : this.comp.freightamnt,
            "gst_amount": _.isEmpty(this.comp.gstamnt) ? 0 : this.comp.gstamnt,
            "special_inst": _.isEmpty(this.comp.spclinst) ? "" : this.comp.spclinst,
            "additional_inst": _.isEmpty(this.comp.addinst) ? "" : this.comp.addinst,
            "packing_and_forwarding":_.isEmpty(this.comp.packingfrwding) ? "" : this.comp.packingfrwding,
            "po_value": poval,
            "podata": this.selecteditemnsupplier,
            "popayment": PaymentDetails,
          }
        this.show_loader = true
        this.poservice.CreatePO(formdata, this.filelist).subscribe(res => {
          if (res.status == "success") {
            this.router.navigate(['/indentpending'])
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.show_loader = false
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          this.show_loader = false
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        })
      } else {
        this.notif.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }

      console.log("selected", this.selected)
      console.log("quotationdetails", this.quotationdetails)
    }
  }

}
