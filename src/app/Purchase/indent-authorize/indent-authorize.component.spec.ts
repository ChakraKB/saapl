import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndentAuthorizeComponent } from './indent-authorize.component';

describe('IndentAuthorizeComponent', () => {
  let component: IndentAuthorizeComponent;
  let fixture: ComponentFixture<IndentAuthorizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndentAuthorizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndentAuthorizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
