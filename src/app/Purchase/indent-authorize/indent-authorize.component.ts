import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { CompleterService } from 'ng2-completer';
import { PurchaseTransactionService } from '../../services/Purchase/transactions/purchase-transaction.service';
class Person {
  id: any;
  role_name: string;
  description: string;
  checkplan: boolean = false;
  selectpart: boolean = false;
}

@Component({
  selector: 'app-indent-authorize',
  templateUrl: './indent-authorize.component.html',
  styleUrls: ['./indent-authorize.component.css']
})
export class IndentAuthorizeComponent implements OnInit {
  toastertime: number;
  tosterminlength: number;
  show_loader: boolean = false;
  authlist: any;
  checkeditems: any;
  showDialog: any;
  showRemDialog: any;
  files: any;
  nodata: boolean = false;
  nofiles: boolean = false;
  checkallapp: boolean = false;
  checkallrej: boolean = false;
  checkalldisc: boolean = false;
  categoryarray: any;

  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private purchaseindentservice: PurchaseTransactionService,
    private completerService: CompleterService,
    private http: Http) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.categoryarray = AppConstant.APP_CONST.PurchaseCategory;
  }

  ngOnInit() {
    this.getauthlist();
    this.checkeditems = []
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getauthlist() {
    this.show_loader = true;
    this.purchaseindentservice.GetAuthList(this.unitid).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.authlist = res.data;
        if (!res.data.length) this.nodata = true;
        else this.nodata = false
        _.map(this.authlist, function (num) {
          num.crt_quantity = num.quantity;
          num.approve = false;
          num.reject = false;
          num.discussion = false;
          num.checked = false;
          return num;
        });
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, (err) => {
      this.show_loader = false;
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  checkapp(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].authorized_sts = 1;
      this.authlist[i].approve = true;
      this.authlist[i].reject = false;
      this.authlist[i].discussion = false;
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].authorized_sts = 0;
    }
  }

  checkrej(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].authorized_sts = 2;
      this.authlist[i].approve = false;
      this.authlist[i].reject = true;
      this.authlist[i].discussion = false;
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].authorized_sts = 0;
    }
  }

  checkdisc(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].authorized_sts = 3;
      this.authlist[i].approve = false;
      this.authlist[i].reject = false;
      this.authlist[i].discussion = true;
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].authorized_sts = 0;
    }
  }

  checkallapplist(status) {
    this.checkallrej = false;
    this.checkalldisc = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].authorized_sts = 1;
        this.authlist[i].approve = true;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].authorized_sts = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    }
  }

  checkallrejlist(status) {
    this.checkallapp = false;
    this.checkalldisc = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].authorized_sts = 2;
        this.authlist[i].approve = false;
        this.authlist[i].reject = true;
        this.authlist[i].discussion = false;
      }
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].authorized_sts = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    }
  }

  checkalldisclist(status) {
    this.checkallapp = false;
    this.checkallrej = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].authorized_sts = 3;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = true;
      }
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].authorized_sts = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    }
  }
  remarks: any;
  indentdtls: any;
  getremarks(data) {
    this.remarks = data;
    this.showRemDialog = true;
  }

  getfiles(data) {
    this.show_loader = true;
    this.purchaseindentservice.getFilesByid(data.pindent.id).subscribe(res => {
      this.showDialog = true;
      this.show_loader = false;
      if (res.status == "success") {
        console.log(res.data);
        this.files = res.data;
        if (!this.files.length) this.nofiles = true;
        else this.nofiles = false;
      }
    })
  }

  downloadfile(file) {
    let byteCharacters = atob(file.filecontent);
    let byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++)
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    let byteArray = new Uint8Array(byteNumbers);
    let blob = new Blob([byteArray], { type: file.filetype });
    importedSaveAs(blob, file.filename)
  }


  authorizelist() {
    var newlist = [];
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    var selectedauthlists = _.filter(this.authlist, function (num) { return num.checked == true; });

    if (!selectedauthlists.length) {
      validdata = false;
      validationerrorMsg.push("Nothing to authorize");
    }
    if (validdata) {
      var selectedauthlists = selectedauthlists.forEach(element => {
        var list = _.omit(element, ['approve', 'reject', 'discussion', 'checked']);
        newlist.push(list)
      });
      console.log("selectedauthlists", JSON.stringify(newlist))
      this.show_loader = true;
      this.purchaseindentservice.AuthorizeIndent(newlist).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.getauthlist();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  close() {
    this.showDialog = false;
    this.showRemDialog = false;
  }
}
