import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingIndentComponent } from './pending-indent.component';

describe('PendingIndentComponent', () => {
  let component: PendingIndentComponent;
  let fixture: ComponentFixture<PendingIndentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingIndentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingIndentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
