import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, ChangeDetectorRef, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { DepartmentsService } from '../../services/department/departments.service'
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { isUndefined } from 'util';
import { ItemsService } from '../../services/items/items.service';
import { PurchaseTransactionService } from '../../services/Purchase/transactions/purchase-transaction.service';
import { UserService } from '../../services/user/user.service';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';

@Component({
  selector: 'app-pending-indent',
  templateUrl: './pending-indent.component.html',
  styleUrls: ['./pending-indent.component.css']
})
export class PendingIndentComponent implements OnInit {

  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  checkedindent: any;
  userdetails: any;
  departs: any;
  alldpts: any;
  pendingind: any = {};
  filter: any = {};
  indenttype: any;
  pendingindents: any;
  checkeditems: any;
  items: any;
  itemnos: any;
  showDialog: boolean = false;
  enq: any = {};
  contactlist: any;
  suppliers: any;
  allsuppliers: any;
  selectedsupp: any;
  suppemails: any;
  supp: any;
  supplierslist: any;
  currentdate: any;
  repeatpolist: any;
  showSuppDialog: boolean = false;
  show_dialog_loader: boolean = false;
  itemsuppliers: any;

  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private completerService: CompleterService,
    private dptservice: DepartmentsService,
    private itemservice: ItemsService,
    private userservices: UserService,
    private purchaseindentservice: PurchaseTransactionService,
    private router: Router,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private poservice: PurchaseorderService
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.indenttype = AppConstant.APP_CONST.IndentTypes;
  }

  ngOnInit() {
    let date = new Date();
    this.currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    this.filter.type = "Local";
    this.enq.delivery = "Coimbatore"
    this.selectedsupp = [];
    this.supplierslist = [];
    this.checkeditems = [];
    this.getdepartments();
    this.getsuppliers();
    this.getPendingindentlists();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  index: number;

  showsuppop(item, i) {
    this.show_loader = true;
    this.index = i
    this.purchaseindentservice.getItemSuppliers(item.item_id).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.itemsuppliers = res.data;
        this.showSuppDialog = true;
        if (item.supplier_rate) {
          this.radioSelected = item.supplier_rate[0].supplier_id
        } else {
          this.radioSelected = ""
        }
      }
    })
  }

  getdepartments() {
    this.show_loader = true
    this.dptservice.getByStatus("active").subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.departs = res.data;
        this.alldpts = this.completerService.local(res.data, 'department_name', 'department_name');
      }
    })
  }

  getsuppliers() {
    this.show_loader = true;
    this.contactlist = [];
    this.userservices.getContactsByCat("Supplier")
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.suppliers = res.data;
          this.allsuppliers = this.masterservice.formatDataforDropdown("supplier_name", "id", this.suppliers);
        }
      });
  }
  radioSelected: any;
  addsupplier(data, i) {
    this.pendingindents[i].supplier_rate = [];
    if (isUndefined(data) || data == "") {
      this.notif.warn('Warning', "Please select any rate", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      console.log("list", data)
      var list = _.findWhere(this.itemsuppliers, { 'supplier_id': data })
      this.pendingindents[i].rate = list.supplier_name + '-' + list.rate;
      this.pendingindents[i].supplier_rate.push(list);
      this.showSuppDialog = false;
    }
    console.log("pendingindent", this.pendingindents)
  }

  getPendingindentlists() {
    if (!_.isEmpty(this.filter.dpt)) {
      var dpt = _.findWhere(this.departs, { "department_name": this.filter.dpt })
      console.log("dpt", dpt)
    }
    this.show_loader = true
    this.purchaseindentservice.getPendingIndentList(this.unitid, this.filter.type, _.isEmpty(dpt) ? 0 : dpt.id).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.pendingindents = res.data
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false;
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }


  checkselected(data) {
    this.selectedsupp.push(data)
    this.addbcc();
  }

  checkdeselected(item) {
    this.selectedsupp.forEach(function (data, index, object) {
      if (data.item_id === item.item_id) {
        object.splice(index, 1);
      }
    });
    console.log("des", this.selectedsupp)
    this.addbcc();
  }

  addbcc() {
    let self = this
    this.supplierslist = []
    _.forEach(this.selectedsupp, function (obj) {
      var item = _.findWhere(self.suppliers, { "id": obj.value })
      self.supplierslist.push(item)
    })
    this.supp = [].slice.call(this.supplierslist);
    this.enq.bcc = this.supp.map(f => f.email).join(', ');
  }

  checklist(status, item, i) {
    this.pendingindents[i].check = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.pendingindents[i].check = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.indent_no === item.indent_no) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  showenqpop() {
    this.items = [].slice.call(this.checkeditems);
    this.itemnos = this.items.map(f => f.item_code).join(', ');
    console.log("itemnos", this.itemnos)

    if (!this.checkeditems.length) {
      this.notif.warn('Warning', "Please select any item", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      this.showDialog = true;
    }
  }

  close() {
    this.showDialog = false;
    this.showSuppDialog = false;
    this.selectedsupp = [];
    this.enq = {};
    this.enq.delivery = "Coimbatore"
  }

  sendenquery() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    if (_.isEmpty(this.enq.suppliers)) {
      validdata = false;
      validationerrorMsg.push("Please select Suppliers");
    }

    if (validdata) {
      var selsuppliers = [];
      for (var i = 0; i < this.supplierslist.length; i++) {
        var item =
          {
            "supplier_id": this.supplierslist[i].id,
            "supplier": this.supplierslist[i].supplier_name
          }
        selsuppliers.push(item)
      }

      var formdata =
        {
          "unit_id": this.unitid,
          "eidetails": this.checkeditems,
          "delivery_at": this.enq.delivery,
          "esupplier": selsuppliers,
          "cc": _.isEmpty(this.enq.cc) ? "" : this.enq.cc,
          "bcc": this.enq.bcc,
          "remarks": _.isEmpty(this.enq.remarks) ? "" : this.enq.remarks
        }
      this.showDialog = false;
      this.show_loader = true;
      this.purchaseindentservice.CreateEnquery(formdata).subscribe(res => {
        if (res.status == "success") {
          this.checkeditems = [];
          this.close()
          this.show_loader = false;
          this.getPendingindentlists();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  repeatpo() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (!this.checkeditems.length) {
      this.notif.warn('Warning', "Please select any item", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      this.repeatpolist = []
      for (var i = 0; i < this.checkeditems.length; i++) {
        if (this.checkeditems[i].prev_validity) {
          var checkdate = this.splitdate(this.checkeditems[i].prev_validity)
          var date = new Date(checkdate);
          var curdate = new Date();
          if (date < curdate) {
            validdata = false;
            validationerrorMsg.push("Prev PO validity expired for " + this.checkeditems[i].item_code);
            break;
          } else {
            this.repeatpolist.push(this.checkeditems[i])
          }
        } else {
          validdata = false;
          validationerrorMsg.push("Previous PO details not found for " + this.checkeditems[i].item_code);
        }
      }
      console.log("formdata", this.repeatpolist);
      if (validdata) {
        var polist = _.groupBy(this.repeatpolist, function (num) {
          return num.prev_po_supplier;
        });
        var formdata = []
        var poarray = _.toArray(polist);
        for (var i = 0; i < poarray.length; i++) {
          var data =
            {
              "prev_supplier_name": poarray[i][0].prev_po_supplier,
              "prev_supplier_id": poarray[i][0].prev_supplier_id,
              "prev_po": poarray[i][0].prev_po,
              "rpoview": poarray[i]
            }
          formdata.push(data)
        }
        console.log("formdata", JSON.stringify(formdata));
        this.show_loader = true;
        this.poservice.RepeatPO(formdata).subscribe(res => {
          this.checkeditems = [];
          this.close()
          this.show_loader = false;
          this.getPendingindentlists();
          if (res.status == "success") {
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.notif.warn('Warning', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          this.show_loader = false;
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        })
      } else {
        this.notif.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }
  }

  directPo() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (!this.checkeditems.length) {
      validdata = false;
      validationerrorMsg.push("Please select any item")
    }
    _.forEach(this.checkeditems, function (val) {
      if (!val.supplier_rate) {
        validdata = false;
        validationerrorMsg.push("Please select Supplier Rate details for " + val.item_code)
      }
    })

    if (validdata) {
      console.log("formdata", this.checkeditems)
      this.show_loader = true;
      this.poservice.DirectPO(this.checkeditems, this.unitid).subscribe(res => {
        this.checkeditems = [];
        this.show_loader = false;
        if (res.status == "success") {
          this.getPendingindentlists();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  setdate(d) {
    var newdate = d.split('-');
    var altereddate =
      {
        date: {
          year: +newdate[2],
          month: +newdate[1],
          day: +newdate[0]
        },
        "formatted": d
      }
    console.log("day", altereddate)
    return altereddate
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
      {
        date: {
          year: +newdate[2],
          month: +newdate[1],
          day: +newdate[0]
        },
      }
    console.log("day", [altereddate.date[1], altereddate.date[0], altereddate.date[2]].join('-'))
    // return altereddate
    return [altereddate.date.month, altereddate.date.day, altereddate.date.year].join('-');
  }

}
