import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoAmendmentComponent } from './po-amendment.component';

describe('PoAmendmentComponent', () => {
  let component: PoAmendmentComponent;
  let fixture: ComponentFixture<PoAmendmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoAmendmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoAmendmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
