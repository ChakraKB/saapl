import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { CompleterService } from 'ng2-completer';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { Location } from '@angular/common';
import * as $ from 'jquery';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';
declare let swal: any;
@Component({
  selector: 'app-po-amendment',
  templateUrl: './po-amendment.component.html',
  styleUrls: ['./po-amendment.component.css']
})
export class PoAmendmentComponent implements OnInit {
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  unitid: any;
  pono: any;
  supplier: any
  quotationdetails: any;
  others: any;
  comp: any = {};
  polist: any;
  allpo: any;
  quotationdetailscpy: any;
  quoteItemDetails: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private completerService: CompleterService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private _fb: FormBuilder,
    private location: Location,
    private poservice: PurchaseorderService
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.getAllPO()
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  getAllPO() {
    this.show_loader = true;
    this.poservice.GetAmendPOList(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.allpo = res.data;
        this.polist = this.completerService.local(this.allpo, 'po_no', 'po_no');
        console.log("polist", this.polist)
      }
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  getpodetails(data) {
    if (data) {
      var po = data.originalObject
      this.show_loader = true;
      this.poservice.GetPOByID(po.id).subscribe(res => {
        if (res.status == "success") {
          this.pono = res.data.po_no;
          this.supplier = res.data.supplier
          this.quotationdetails = res.data.poitem;
          this.quotationdetailscpy  =_.clone(this.quotationdetails);
          this.others = res.data
          this.comp.freight = this.others.freight;
          this.comp.dispatchmode = this.others.mode_of_desp;
          this.comp.pricebasis = this.others.price_basis_name;
          this.comp.epcg = this.others.epcg_applicable;
          this.comp.license = this.others.license_no;
          this.comp.destination = this.others.destination;
          this.comp.freightamnt = this.others.freight_amount;
          this.comp.gstamnt = this.others.gst_amount;
          this.show_loader = false
        }
      })
    }
  }

  calcitemcost(i) {
    if (!this.quotationdetails[i].req_qty) {
      this.quotationdetails[i].req_qty = 0;
      this.quotationdetails[i].itemcost = 0
      return 0
    } else {
      var a = this.quotationdetails[i].totalamount / (+this.quotationdetails[i].req_qty);
      this.quotationdetails[i].itemcost = a;
      return a
    }
  }



  calcsubtotal(i) {
    var tot = this.quotationdetails[i].req_qty * (+this.quotationdetails[i].rate)
    if (this.quotationdetails[i].discount_type == "%") {
      if (isNaN(this.quotationdetails[i].discount)) {
        this.quotationdetails[i].discount = 0
      } else if (this.quotationdetails[i].discount > 100) {
        this.quotationdetails[i].discount = 0
      } else {
        var disc = (tot * +this.quotationdetails[i].discount) / 100
        var itemdisc = ((this.quotationdetails[i].rate - disc) * +this.quotationdetails[i].pf) / 100
        this.quotationdetails[i].discountval = disc
      }
    } else {
      if (isNaN(this.quotationdetails[i].discount)) {
        this.quotationdetails[i].discount = 0
      } else if (this.quotationdetails[i].discount > (this.quotationdetails[i].req_qty * (+this.quotationdetails[i].rate))) {
        this.quotationdetails[i].discount = 0
      } else var disc = (+this.quotationdetails[i].discount) * this.quotationdetails[i].req_qty
      this.quotationdetails[i].discountval = disc
    }

    if (this.quotationdetails[i].pf_type == "%") {
      if (isNaN(this.quotationdetails[i].pf)) {
        this.quotationdetails[i].pf = 0
      } else if (this.quotationdetails[i].pf > 100) {
        this.quotationdetails[i].pf = 0
      } else {
        var pftot = ((tot - disc) * +this.quotationdetails[i].pf) / 100
        this.quotationdetails[i].pfval = pftot
      }
    } else {
      if (isNaN(this.quotationdetails[i].pf)) {
        this.quotationdetails[i].pf = 0
      } else var pftot = (+this.quotationdetails[i].pf) * this.quotationdetails[i].req_qty;
      this.quotationdetails[i].pfval = pftot;
    }
    this.quotationdetails[i].rateval = tot
    this.quotationdetails[i].totalamount = (tot - disc) + pftot;
    return (tot - disc) + pftot
  }


  calctrate() {
    var totalrate = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.rateval;
      totalrate += a
    })
    return +totalrate
  }

  calctdisc() {
    var totaldisc = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.discountval;
      totaldisc += a
    })
    return +totaldisc
  }

  calctpf() {
    var totalpf = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.pfval;
      totalpf += a
    })
    return +totalpf
  }

  calctotal() {
    var amounttot = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.totalamount;
      amounttot += a
    })
    return +amounttot
  }

  updatepo() {

    var validdata: Boolean = true;
    var validationerrorMsg = [];
    this.quoteItemDetails = []

    if (!this.quotationdetails) {
      validdata = false;
      validationerrorMsg.push("Select any item")
    } else {
      for (var i = 0; i < this.quotationdetails.length; i++) {
        this.quotationdetails[i].item_cost = this.quotationdetails[i].itemcost,
          this.quotationdetails[i].landing_cost = this.quotationdetails[i].totalamount,
          this.quotationdetails[i].schd_date = this.quotationdetails[i].delivery_date,
          this.quotationdetails[i].discount_value = this.quotationdetails[i].discountval,
          this.quotationdetails[i].pf_value = this.quotationdetails[i].pfval,
          this.quotationdetails[i].amount = this.quotationdetails[i].rateval
        if (!this.quotationdetails[i].rate) {
          validdata = false;
          validationerrorMsg.push("Please enter Rate/qty");
        } else if (!this.quotationdetails[i].req_qty) {
          validdata = false;
          validationerrorMsg.push("Please enter Qty");
        }
      }
    }

    if (validdata) {
      swal({
        title: 'Are you sure?',
        text: "You want Amend PO!",
        type: 'question',
        width: 500,
        padding: 10,
        showCancelButton: true,
        confirmButtonColor: '#ffaa00',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes!',
        cancelButtonText: 'No',
      }).then((result) => {
        if (result.value) {
          this.show_loader = true;
          this.poservice.UpdatePOAmend(this.others).subscribe(res => {
            if (res.status == "success") {
              this.show_loader = false;
              this.router.navigate(['/posearch'])
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.show_loader = false
              this.notif.error(res.status, res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          }, err => {
            this.show_loader = false;
            this.notif.error('Error', "Something Wrong", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          })
        }
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  resetform() {
    this.quotationdetails = [];
    this.pono = ""
  }

}
