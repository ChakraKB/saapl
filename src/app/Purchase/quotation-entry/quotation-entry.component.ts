import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { RolesService } from '../../services/Roles/roles.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { CompleterService } from 'ng2-completer';
import { ItemsService } from '../../services/items/items.service';
import { PurchaseMastersService } from '../../services/Purchase/purchase-masters.service';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { QuotationService } from '../../services/Purchase/quotation/quotation.service';
import { HsnService } from '../../services/hsn/hsn.service';
import { ChangeDetectorRef } from '@angular/core';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
@Component({
  selector: 'app-quotation-entry',
  templateUrl: './quotation-entry.component.html',
  styleUrls: ['./quotation-entry.component.css']
})
export class QuotationEntryComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  show_loader: boolean = false;
  itemlist: any;
  allitems: any;
  pricebasis: any;
  quotationform: FormGroup;
  allpricebasis: any;
  freights: any;
  allfreights: any;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  scrolltime: number;
  quote: any = {};
  freightarray: any;
  modeofdispatch: any;
  enqno: any;
  quotationdetails: any;
  supplierlist: any;
  allhsn: any;
  Sfiles: any;
  files: any;
  attchment: any;
  enqid: any;
  suppliers: any;
  quoteItemDetails: any;
  quotation: any;
  formdata: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private http: Http,
    private completerService: CompleterService,
    private purchasemasters: PurchaseMastersService,
    private qutationservice: QuotationService,
    private hsnservice: HsnService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private router: Router
  ) {
    this.freightarray = AppConstant.APP_CONST.FREIGHT;
    this.modeofdispatch = AppConstant.APP_CONST.DISPATCHMODE;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;

    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.enqno = params.enqno;
        this.enqid = params.id
        console.log("urlparams", this.enqno);
      }
    });

    this.quotationform = fb.group({
      'enquiryno': [null, Validators.required],
      'supplier': [null, Validators.required],
      'pricebasis': [null, Validators.required],
      'warantty': [null],
      'freight': [null, Validators.required],
      'dispatchmode': [null, Validators.required],
      'validity': [null, Validators.required],
      'selflifetime': [null],
      'qtnrefno': [null, Validators.required],
      'qtnrefdt': [null, Validators.required],
      'anexure': [null],
      'attchment': [null]
    })
  }

  ngOnInit() {
    this.quotationform.controls['enquiryno'].setValue(this.enqno);
    this.getquotationdetails();
    this.getallhsn();
    this.getpricebasis();
    this.getfreights();
  }


  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }
  quoted: boolean = true;
  selectedsupplier(data) {
    var supplier = data.originalObject;
    console.log(supplier);
    this.show_loader = true;
    this.qutationservice.GetQutationBySupplier(supplier.enquiry_id, supplier.supplier_id).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        if (res.data) {
          this.quotation = res.data;
          this.quotationdetails = res.data.quoteItemDetails;
          this.quote.enquiryno = res.data.enq_no;
          this.quote.freight = res.data.freight;
          this.quote.pricebasis = res.data.price_basis_id.price_basis;
          this.quote.warantty = res.data.warranty;
          this.quote.dispatchmode = res.data.mode_of_disp;
          this.quote.validity = this.splitdate(res.data.qtn_validity);
          this.quote.selflifetime = res.data.she_li_item;
          this.quote.qtnrefno = res.data.qtn_ref_no;
          this.quote.qtnrefdt = this.splitdate(res.data.qtn_ref_date);
          this.quote.anexure = res.data.annexure;
          if (res.data.quoteItemAttach.length) {
            this.Sfiles = [].slice.call(res.data.quoteItemAttach);
            this.quote.attchment = this.Sfiles.map(f => f.filename).join(', ');
            console.log("files", this.quote.attchment)
          } else {
            this.quote.attchment = ""
          }
          if (res.data.is_saved == 1) {
            this.quoted = true;
          } else {
            this.quoted = false;
          }

        } else {
          this.quotation = res.data1;
          this.quotationdetails = res.data1.eidetails;
          this.quote.freight = "";
          this.quote.pricebasis = "";
          this.quote.warantty = "";
          this.quote.dispatchmode = "";
          this.quote.validity = "";
          this.quote.selflifetime = "";
          this.quote.qtnrefno = "";
          this.quote.qtnrefdt = "";
          this.quote.anexure = "";
          this.quoted = true;
          _.map(this.quotationdetails, function (val) {
            val.discount_type = "%";
            val.pf_type = "%";
            val.cgst = 0;
            val.sgst = 0;
            val.igst = 0;
            val.rate = 0;
            val.rate = 0;
            val.discount = 0
            val.totalamount = 0;
            val.pf = 0
            val.enq_line_id = val.id
            return val;
          })
          console.log("enq", JSON.stringify(this.quotationdetails))
        }
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
      {
        date: {
          year: +newdate[2],
          month: +newdate[1],
          day: +newdate[0]
        },
        "formatted": d
      }
    console.log("day", altereddate)
    return altereddate
  }

  getquotationdetails() {
    this.show_loader = true;
    this.qutationservice.GetQutationByid(this.enqid).subscribe(res => {
      if (res.status == "success") {
        this.quotationdetails = res.data.eidetails;
        this.quoted = true;
        _.map(this.quotationdetails, function (val) {
          val.discount_type = "%";
          val.pf_type = "%";
          val.cgst = 0;
          val.sgst = 0;
          val.igst = 0;
          val.rate = 0;
          val.rate = 0;
          val.discount = 0
          val.totalamount = 0;
          val.pf = 0
          return val;
        })
        this.quote.enquiryno = res.data.enq_no;
        this.suppliers = res.data.esupplier
        this.supplierlist = this.completerService.local(res.data.esupplier, 'supplier', 'supplier');

        console.log("quotationdetails", this.quotationdetails)
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  getallhsn() {
    this.hsnservice.getallHsn().subscribe(res => {
      if (res.status == "success") {
        this.allhsn = this.completerService.local(res.data, 'hsn_code', 'hsn_code');
      }
    })
  }

  getpricebasis() {
    this.show_loader = true
    this.purchasemasters.GetPriceBasisByStatus("active")
      .subscribe(res => {
        if (res) {
          this.show_loader = false;
          this.pricebasis = res.data;
          this.allpricebasis = this.completerService.local(this.pricebasis, 'price_basis', 'price_basis');
          console.log("pricebasis", this.pricebasis)
        }
      });
  }

  getfreights() {
    this.show_loader = true
    this.purchasemasters.GetFreightChargesByStatus("active")
      .subscribe(res => {
        if (res) {
          this.show_loader = false;
          this.freights = res.data;
          this.allfreights = this.completerService.local(this.freights, 'id', 'charge');
          console.log("freights", this.freights)
        }
      });
  }

  onFileChange(event) {
    this.attchment = event.target.files
    this.Sfiles = [].slice.call(event.target.files);
    this.quote.attchment = this.Sfiles.map(f => f.name).join(', ');
    console.log("file", this.quote.attchment)
  }

  checkdisc(i) {
    if (this.quotationdetails[i].discount_type == "%") {
      if (isNaN(this.quotationdetails[i].discount)) {
        this.quotationdetails[i].discount = 0
      } else if (this.quotationdetails[i].discount > 100) {
        this.quotationdetails[i].discount = 0
      }
    } else {
      if (isNaN(this.quotationdetails[i].discount)) {
        this.quotationdetails[i].discount = 0
      } else if (this.quotationdetails[i].discount > (this.quotationdetails[i].req_qty * (+this.quotationdetails[i].rate))) {
        this.quotationdetails[i].discount = 0
      }
    }
  }

  checkpf(i) {
    if (this.quotationdetails[i].pf_type == "%") {
      if (isNaN(this.quotationdetails[i].pf)) {
        this.quotationdetails[i].pf = 0
      } else if (this.quotationdetails[i].pf > 100) {
        this.quotationdetails[i].pf = 0
      }
    } else {
      if (isNaN(this.quotationdetails[i].pf)) {
        this.quotationdetails[i].discount = 0
      }
    }
  }

  calcsubtotal(i) {

    var tot = this.quotationdetails[i].req_qty * (+this.quotationdetails[i].rate)
    if (this.quotationdetails[i].discount_type == "%") {
      if (isNaN(this.quotationdetails[i].discount)) {
        this.quotationdetails[i].discount = 0
      } else if (this.quotationdetails[i].discount > 100) {
        this.quotationdetails[i].discount = 0
      } else {
        var disc = (tot * +this.quotationdetails[i].discount) / 100
        var itemdisc = ((this.quotationdetails[i].rate - disc) * +this.quotationdetails[i].pf) / 100
        this.quotationdetails[i].discountval = disc
      }
    } else {
      if (isNaN(this.quotationdetails[i].discount)) {
        this.quotationdetails[i].discount = 0
      } else if (this.quotationdetails[i].discount > (this.quotationdetails[i].req_qty * (+this.quotationdetails[i].rate))) {
        this.quotationdetails[i].discount = 0
      } else var disc = (+this.quotationdetails[i].discount) * this.quotationdetails[i].req_qty
      this.quotationdetails[i].discountval = disc
    }

    if (this.quotationdetails[i].pf_type == "%") {
      if (isNaN(this.quotationdetails[i].pf)) {
        this.quotationdetails[i].pf = 0
      } else if (this.quotationdetails[i].pf > 100) {
        this.quotationdetails[i].pf = 0
      } else {
        var pftot = ((tot - disc) * +this.quotationdetails[i].pf) / 100
        this.quotationdetails[i].pfval = pftot
      }
    } else {
      if (isNaN(this.quotationdetails[i].pf)) {
        this.quotationdetails[i].pf = 0
      } else var pftot = (+this.quotationdetails[i].pf) * this.quotationdetails[i].req_qty;
      this.quotationdetails[i].pfval = pftot;
    }
    this.quotationdetails[i].rateval = tot
    this.quotationdetails[i].totalamount = (tot - disc) + pftot;
    return (tot - disc) + pftot
  }

  calcitemcost(i) {
    var a = this.quotationdetails[i].totalamount / this.quotationdetails[i].req_qty;
    this.quotationdetails[i].itemcost = a;
    return a
  }


  calctrate() {
    var totalrate = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.rateval;
      totalrate += a
    })
    return +totalrate
  }

  calctdisc() {
    var totaldisc = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.discountval;
      totaldisc += a
    })
    return +totaldisc
  }

  calctpf() {
    var totalpf = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.pfval;
      totalpf += a
    })
    return +totalpf
  }

  calctotal() {
    var amounttot = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.totalamount;
      amounttot += a
    })
    return +amounttot
  }

  formObj: any = {

    enquiryno: {
      required: "enquiry No Required",
    },
    supplier: {
      required: "Supplier Required",
    },
    pricebasis: {
      required: "Price basis Required",
    },
    freight: {
      required: "Freight Required",
    },

    dispatchmode: {
      required: "Dispatchmode Required",
    },
    validity: {
      required: "validity Date Required",
    },
    qtnrefno: {
      required: "Quotation Ref No Required",
    },
    qtnrefdt: {
      required: "Quotation Ref date Required",
    }

  }

  savequotation(issaved, flag) {
    if (this.quotationform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.quotationform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
      return false;
    }
    else {
      var validdata: Boolean = true;
      var validationerrorMsg = [];

      this.quoteItemDetails = []
      for (var i = 0; i < this.quotationdetails.length; i++) {
        var obj =
          {
            'id': this.quotationdetails[i].id,
            'enq_line_id': this.quotationdetails[i].enq_line_id,
            'qut_id': this.quotationdetails[i].qut_id,
            'item_id': this.quotationdetails[i].item_id,
            'item_code': this.quotationdetails[i].item_code,
            'item_desc': this.quotationdetails[i].item_desc,
            'hsn_id': this.quotationdetails[i].hsn,
            'req_qty': this.quotationdetails[i].req_qty,
            'qty': 1,
            'rate': this.quotationdetails[i].rate,
            'discount': this.quotationdetails[i].discount,
            'pf': this.quotationdetails[i].pf,
            'cgst': this.quotationdetails[i].cgst,
            'sgst': this.quotationdetails[i].sgst,
            'igst': this.quotationdetails[i].igst,
            'item_cost': this.quotationdetails[i].itemcost,
            'landing_cost': this.quotationdetails[i].totalamount,
            'buyback': 0,
            'spl_inst': this.quotationdetails[i].spl_inst,
            'schd_date': this.quotationdetails[i].delivery_date,
            'uom': this.quotationdetails[i].uom,
            'created_date': this.quotationdetails[i].created_date,
            'discount_type': this.quotationdetails[i].discount_type,
            'discount_value': this.quotationdetails[i].discountval,
            'pf_value': this.quotationdetails[i].pfval,
            'pf_type': this.quotationdetails[i].pf_type,
            'amount': this.quotationdetails[i].rateval,
            'type': this.quotationdetails[i].type,
          }
        if (!this.quotationdetails[i].rate) {
          validdata = false;
          validationerrorMsg.push("Please enter rate/qty");
        } else {
          console.log("lineitems", obj)
          this.quoteItemDetails.push(obj)
        }
      }
      if (validdata) {

        var pricebasis = _.findWhere(this.pricebasis, { 'price_basis': this.quote.pricebasis })
        var supplier = _.findWhere(this.suppliers, { 'supplier': this.quote.supplier })

        if (this.quotationdetails[0].qut_id) {
          this.formdata =
            {
              "id": this.quotationdetails[0].qut_id,
              'enq_id': this.enqid,
              'enq_no': this.quote.enquiryno,
              'supplier': supplier.supplier,
              'supplier_id': supplier.supplier_id,
              'mode_of_disp': this.quote.dispatchmode,
              'qtn_ref_date': this.quote.qtnrefdt.formatted,
              'price_basis_id': { "id": pricebasis.id },
              'qtn_validity': this.quote.validity.formatted,
              'warranty': this.quote.warantty,
              'she_li_item': this.quote.selflifetime,
              'annexure': this.quote.anexure,
              'freight': this.quote.freight,
              'qtn_ref_no': this.quote.qtnrefno,
              'quoteItemDetails': this.quoteItemDetails,
              'is_saved': issaved
            }
          console.log("formdata", this.formdata)
          console.log("attchment", this.attchment);
          this.show_loader = true;
          this.qutationservice.UpdateQuotation(this.formdata, this.attchment).subscribe(res => {
            if (res.status == "success") {
              this.show_loader = false;
              this.router.navigate(['/indentpending'])
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.show_loader = false
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          }, err => {
            this.show_loader = false
            this.notif.error('Error', "Something Wrong", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          })
        } else {
          this.formdata =
            {
              'enq_id': this.enqid,
              'enq_no': this.quote.enquiryno,
              'supplier': supplier.supplier,
              'supplier_id': supplier.supplier_id,
              'mode_of_disp': this.quote.dispatchmode,
              'qtn_ref_date': this.quote.qtnrefdt.formatted,
              'price_basis_id': { "id": pricebasis.id },
              'qtn_validity': this.quote.validity.formatted,
              'warranty': this.quote.warantty,
              'she_li_item': this.quote.selflifetime,
              'annexure': this.quote.anexure,
              'freight': this.quote.freight,
              'qtn_ref_no': this.quote.qtnrefno,
              'quoteItemDetails': this.quoteItemDetails,
              'is_saved': issaved
            }
          console.log("formdata", this.formdata)
          console.log("attchment", this.attchment);
          this.show_loader = true;
          this.qutationservice.CreateQuotation(this.formdata, this.attchment).subscribe(res => {
            if (res.status == "success") {
              this.show_loader = false;
              this.router.navigate(['/indentpending'])
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.show_loader = false
              this.notif.error('Error', "Something Wrong", {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          }, err => {
            this.show_loader = false;
            this.notif.error('Error', validationerrorMsg[0], {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          })
        }


      } else {
        this.notif.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }
  }
}
