import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { AppConstant } from '../../app.constant';

@Component({
  selector: 'app-comparision-view',
  templateUrl: './comparision-view.component.html',
  styleUrls: ['./comparision-view.component.css']
})
export class ComparisionViewComponent implements OnInit {
  @Input() enqid: any;
  @Output() close: EventEmitter<any> = new EventEmitter();
  show_dialog_loader: boolean = false;
  quotationdetails: any;
  totals: any;
  prevtot: any;
  toastertime: number;
  tosterminlength: number;
  constructor(
    private poservice: PurchaseorderService,
    private notif: NotificationsService,
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    console.log("enqid", this.enqid)
    if (this.enqid) {
      this.getcompare()
    }
  }

  getcompare() {
    console.log("data", this.enqid)
    this.show_dialog_loader = true;
    this.poservice.GetCompareByENQ(this.enqid).subscribe(res => {
      this.show_dialog_loader = false;
      if (res.status == "success") {
        this.quotationdetails = res.data;
        this.prevtot = res.data[0].psrview[0];
        this.totals = res.data[0].srview
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Order Confirmation</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          .print-view>tbody>tr>td,
          .print-view>tbody>tr>th,
          .print-view>tfoot>tr>td,
          .print-view>tfoot>tr>th,
          .print-view>thead>tr>td,
          .print-view>thead>tr>th {
            border: 1px solid #333 !important; padding:2px 3px !important
          }
          .print-view .print-label p { font-weight : 500 !important;}
          .print-view .print-label { font-weight : 500 !important;}
          .print-view .cs { background-color:#ccc !important;}
          .print-view .cs td { background-color:#ccc !important;}
           table.print-view td p {
           margin: 0!important;
          }
          @page {
            size: auto;
          }

          table.report-container {
          page-break-after:always;
          }
          thead.report-header {
          display:table-header-group;
          }
          tfoot.report-footer {
          display:table-footer-group;
          } 
          .print-view .bg-quoted {
            background-color: #dcffbc !important;
            font-weight : bold
        }
          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  back() {
    this.close.next()
  }

}
