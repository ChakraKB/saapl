import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { CompleterService } from 'ng2-completer';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';

@Component({
  selector: 'app-md-approval',
  templateUrl: './md-approval.component.html',
  styleUrls: ['./md-approval.component.css']
})
export class MdApprovalComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  toastertime: number;
  tosterminlength: number;
  show_loader: boolean = false;
  authlist: any;
  checkeditems: any;
  showDialog: any;
  showRemDialog: any;
  files: any;
  nodata: boolean = false;
  nofiles: boolean = false;
  checkallapp: boolean = false;
  checkallrej: boolean = false;
  checkalldisc: boolean = false;
  categoryarray: any;
  poid: any;
  showtab: boolean = false

  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private poservice: PurchaseorderService,
    private completerService: CompleterService,
    private http: Http) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.categoryarray = AppConstant.APP_CONST.PurchaseCategory;
  }


  ngOnInit() {
    this.checkeditems = [];
    this.getMdapprovalList();
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getpodetails(data) {
    console.log("data =========>>>>", JSON.stringify(data))
    this.poid = data.po_id;
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    })
  }

  notifycancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

  getMdapprovalList() {
    this.show_loader = true;
    this.poservice.GetMDAppList(this.unitid).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.authlist = res.data;
        if (!res.data.length) this.nodata = true;
        else this.nodata = false;
        _.map(this.authlist, function (num) {
          num.approve = false;
          num.reject = false;
          num.discussion = false;
          num.checked = false;
          return num;
        });
      }
    })
  }

  checkapp(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].is_md_authorized = 1;
      this.authlist[i].approve = true;
      this.authlist[i].reject = false;
      this.authlist[i].discussion = false;
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].is_md_authorized = 0;
    }
  }

  checkrej(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].is_md_authorized = 2;
      this.authlist[i].approve = false;
      this.authlist[i].reject = true;
      this.authlist[i].discussion = false;
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].is_md_authorized = 0;
    }
  }

  checkdisc(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].is_md_authorized = 3;
      this.authlist[i].approve = false;
      this.authlist[i].reject = false;
      this.authlist[i].discussion = true;
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].is_md_authorized = 0;
    }
  }

  checkallapplist(status) {
    this.checkallrej = false;
    this.checkalldisc = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].is_md_authorized = 1;
        this.authlist[i].approve = true;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].is_md_authorized = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    }
  }

  checkallrejlist(status) {
    this.checkallapp = false;
    this.checkalldisc = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].is_md_authorized = 2;
        this.authlist[i].approve = false;
        this.authlist[i].reject = true;
        this.authlist[i].discussion = false;
      }
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].is_md_authorized = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    }
  }

  checkalldisclist(status) {
    this.checkallapp = false;
    this.checkallrej = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].is_md_authorized = 3;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = true;
      }
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].is_md_authorized = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
        this.authlist[i].discussion = false;
      }
    }
  }

  authorizelist() {
    var newlist = [];
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    var selectedauthlists = _.filter(this.authlist, function (num) { return num.checked == true; });

    if (!selectedauthlists.length) {
      validdata = false;
      validationerrorMsg.push("Nothing to authorize");
    }
    if (validdata) {
      var selectedauthlists = selectedauthlists.forEach(element => {
        var list = _.omit(element, ['approve', 'reject', 'discussion', 'checked']);
        // var list =
        //   {
        //     "po_id": element.po_id
        //   }
        newlist.push(list)
      });
      console.log("selectedauthlists", JSON.stringify(newlist))
      this.show_loader = true;
      this.poservice.UpdateMdApp(newlist).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.getMdapprovalList();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }
}
