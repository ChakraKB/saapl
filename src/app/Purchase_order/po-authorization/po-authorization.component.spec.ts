import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoAuthorizationComponent } from './po-authorization.component';

describe('PoAuthorizationComponent', () => {
  let component: PoAuthorizationComponent;
  let fixture: ComponentFixture<PoAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
