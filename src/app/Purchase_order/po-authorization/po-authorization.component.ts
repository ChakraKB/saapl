import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { DepartmentsService } from '../../services/department/departments.service';
import { CompleterService } from 'ng2-completer';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';
class Person {
  id: any;
  role_name: string;
  description: string;
  checkauthorize: boolean = false;
}
@Component({
  selector: 'app-po-authorization',
  templateUrl: './po-authorization.component.html',
  styleUrls: ['./po-authorization.component.css']
})
export class PoAuthorizationComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  files: any;
  indentstatus: any;
  nofiles: boolean = false;
  filter: any = {};
  purDetailsList: any;
  departs: any;
  alldpts: any;
  checkeditems: any;
  contype: any;
  enqid: any;
  showDialog: any

  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private http: Http,
    private completerService: CompleterService,
    private poservice: PurchaseorderService
  ) {
    this.contype = AppConstant.APP_CONST.ContactType
    this.indentstatus = AppConstant.APP_CONST.IndentStatus3
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.checkeditems = [];
    this.filter.status = 0;
    this.filter.type = "All"
    this.getPOlist();
  }

  getPOlist() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
        {
          "type": this.filter.type,
          "unit_id": this.unitid,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
          "status": this.filter.status
        }
      this.show_loader = true;
      this.poservice.GetPoAuthList(obj).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item.id)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  getcompare(data) {
    console.log("data", data)
    this.enqid = data.enq_id;
    this.showDialog = true
  }
  close() {
    this.showDialog = false
  }


}
