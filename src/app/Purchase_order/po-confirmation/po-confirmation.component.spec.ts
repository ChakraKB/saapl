import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoConfirmationComponent } from './po-confirmation.component';

describe('PoConfirmationComponent', () => {
  let component: PoConfirmationComponent;
  let fixture: ComponentFixture<PoConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
