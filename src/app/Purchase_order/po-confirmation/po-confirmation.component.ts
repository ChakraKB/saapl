import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service'

@Component({
  selector: 'app-po-confirmation',
  templateUrl: './po-confirmation.component.html',
  styleUrls: ['./po-confirmation.component.css']
})
export class PoConfirmationComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  persons: any;
  tabletimeout: any;
  checkauthorize: any;
  checkeditems: any[];
  showDialog: boolean = false;
  orderdetails: any;
  statusarray: any;
  toastertime: any;
  tosterminlength: any;
  filter: any = {}
  show_loader: boolean = false;
  poid: any
  showtab: boolean = false;
  indentstatus: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private poservice: PurchaseorderService,
    private http: Http
  ) {
    this.indentstatus = AppConstant.APP_CONST.IndentStatus3;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.filter.status = 0;
    this.checkeditems = [];
    this.searchPo();
  }

  searchPo() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
        {
          // "auth_sts": this.filter.status,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
          "unit_id": this.unitid
        }
      this.show_loader = true;
      this.poservice.GetOrderConfirmList(obj).subscribe(res => {
        this.showtab = false;
        setTimeout(() => {
          jQuery('#list')[0].click();
        });
        if (res.status == "success") {
          this.show_loader = false;
          this.persons = res.data;
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.po_id === item.po_id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  uncheckall() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  notifycancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

  getpodetails(data) {
    console.log("data =========>>>>", JSON.stringify(data))
    this.poid = data.po_id;
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    })
  }

  confirmorder() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (!this.checkeditems.length) {
      validdata = false;
      validationerrorMsg.push("Please select PO to Confirm PO");
    }
    if (validdata) {
      _.forEach(this.checkeditems, function (val) {
        val.is_poconfirm = 2
        return val
      })
      console.log("formdata", JSON.stringify(this.checkeditems))
      this.show_loader = true;
      this.poservice.ConfirmPoOrder(this.checkeditems).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.searchPo();
          this.uncheckall();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.searchPo();
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      });
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
