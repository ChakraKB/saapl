import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { RolesService } from '../../services/Roles/roles.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { CompleterService } from 'ng2-completer';
import { ItemsService } from '../../services/items/items.service';
import { PurchaseMastersService } from '../../services/Purchase/purchase-masters.service';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { QuotationService } from '../../services/Purchase/quotation/quotation.service';
import { HsnService } from '../../services/hsn/hsn.service';
import { ChangeDetectorRef } from '@angular/core';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { UserService } from '../../services/user/user.service';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';
import { Location } from '@angular/common';
declare let swal: any;
import * as $ from 'jquery';
import PerfectScrollbar from 'perfect-scrollbar';

@Component({
  selector: 'app-po-edit',
  templateUrl: './po-edit.component.html',
  styleUrls: ['./po-edit.component.css']
})
export class PoEditComponent implements OnInit {
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  freightarray: any;
  modeofdispatch: any;
  scrolltime: any;
  quotationdetails: any;
  totals: any;
  selected: any;
  poid: any;
  selecteditems: any;
  paymentForm: FormGroup;
  compareform: FormGroup;
  Paytype: any;
  nodata: boolean = false;
  nopaymentdata: any;
  allpayterms: any;
  payterms: any;
  paytermsLC: any;
  filelist: any;
  comp: any = {};
  pricebasis: any;
  allpricebasis: any;
  yesorno: any;
  formdata: any;
  selecteditemnsupplier: any;
  allhsn: any;
  unitid: any;
  url: any;
  poauthurl: any;
  updateurl: any;
  payarray: any;
  others: any
  pono: any;
  supplier: any;
  postatus: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private http: Http,
    private completerService: CompleterService,
    private purchasemasters: PurchaseMastersService,
    private poservice: PurchaseorderService,
    private qutationservice: QuotationService,
    private hsnservice: HsnService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private userservices: UserService,
    private router: Router,
    private _fb: FormBuilder,
    private location: Location
  ) {
    this.url = this.router.url;
    this.postatus = AppConstant.APP_CONST.POSTATUS
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.freightarray = AppConstant.APP_CONST.FREIGHT;
    this.modeofdispatch = AppConstant.APP_CONST.DISPATCHMODE;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.Paytype = AppConstant.APP_CONST.PaymentTypes;
    this.yesorno = AppConstant.APP_CONST.YesOrNo

    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.poid = params.id;
        console.log("urlparams", this.poid);
      }
    });

    this.compareform = fb.group({
      'freight': [null, Validators.required],
      'dispatchmode': [null, Validators.required],
      'pricebasis': [null],
      'license': [null],
      'epcg': [null],
      'destination': [null],
      'freightamnt': [null],
      'gstamnt': [null],
      'spclinst': [null],
      'addinst': [null],
      'packingfrwding': [null]
    })

    this.paymentForm = this._fb.group({
      itemPayRows: this._fb.array([])
    });
  }

  ngOnInit() {
    this.getpaymentterms();
    this.getpricebasis();
    this.getallhsn();
    this.addNewpaymentRow();
    this.filelist = [];
    this.poauthurl = "/poauthorize/" + this.poid;
    this.updateurl = "/poedit/" + this.poid;
    this.getpodetails();
  }

  show_disclose: boolean = false;
  podate: any
  getpodetails() {
    this.show_loader = true;
    this.poservice.GetPOByID(this.poid).subscribe(res => {
      if (res.status == "success") {
        if (res.data.is_received == 2) this.show_disclose = true
        else this.show_disclose = false
        this.pono = res.data.po_no;
        this.supplier = res.data.supplier
        this.quotationdetails = res.data.poitem;
        this.payarray = res.data.popayment;
        this.filelist = res.data.poattach;
        this.podate = this.splitdate(res.data.created_date)
        this.others = res.data;
        this.comp.freight = this.others.freight;
        this.comp.dispatchmode = this.others.mode_of_desp;
        this.comp.pricebasis = this.others.price_basis_name;
        this.comp.epcg = this.others.epcg_applicable;
        this.comp.license = this.others.license_no;
        this.comp.destination = this.others.destination;
        this.comp.freightamnt = this.others.freight_amount;
        this.comp.gstamnt = this.others.gst_amount;
        this.comp.spclinst = this.others.special_inst;
        this.comp.addinst = this.others.additional_inst
        this.comp.packingfrwding = this.others.packing_and_forwarding

        let paytemp = this.fb.array([]);
        for (let i = 0; i < this.payarray.length; i++) {
          paytemp.push(
            this.fb.group({
              paymenttype: this.payarray[i].payment_type,
              paymentterm: this.payarray[i].payment_terms,
              paymentpercentage: parseInt(this.payarray[i].payment_percentage),
              credit: parseInt(this.payarray[i].credict)
            })
          );
        }
        this.paymentForm = this.fb.group({
          itemPayRows: paytemp
        });
        this.show_loader = false
      }
    })
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
    {
      date: {
        year: +newdate[2],
        month: +newdate[1],
        day: +newdate[0]
      },
      "formatted": d
    }
    console.log("day", altereddate)
    return altereddate
  }

  initPayItemRows() {
    return this._fb.group({
      paymenttype: "",
      paymentterm: "",
      paymentpercentage: 0,
      credit: 0
    });
  }

  getpaymentform(paymentform) {
    return paymentform.get('itemPayRows').controls
  }

  getpricebasis() {
    this.purchasemasters.GetPriceBasisByStatus("active")
      .subscribe(res => {
        if (res) {
          this.pricebasis = res.data;
          this.allpricebasis = this.completerService.local(this.pricebasis, 'price_basis', 'price_basis');
        }
      });
  }

  getallhsn() {
    this.hsnservice.getallHsn().subscribe(res => {
      if (res.status == "success") {
        this.allhsn = this.completerService.local(res.data, 'hsn_code', 'hsn_code');
      }
    })
  }

  getpaymentterms() {
    this.userservices.getallpayterms()
      .subscribe(res => {
        if (res) {
          this.allpayterms = res.data;
          this.payterms = this.completerService.local(res.data, 'term', 'term');
        }
      });
  }

  addNewpaymentRow() {
    var totalpercentage = 0
    _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
      var sum = +item.paymentpercentage
      totalpercentage += sum;
    });
    if (totalpercentage) {
      if (totalpercentage < 100) {
        this.nopaymentdata = true;
        const control = <FormArray>this.paymentForm.controls["itemPayRows"];
        control.push(this.initPayItemRows());
      }
      else {
        this.notif.warn('Warning', "you already entered 100%", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        });
      }
    }
    else {
      this.nopaymentdata = true;
      const control = <FormArray>this.paymentForm.controls["itemPayRows"];
      control.push(this.initPayItemRows());
    }
  }
  deletePayRow(index: number) {
    const control = <FormArray>this.paymentForm.controls["itemPayRows"];
    control.removeAt(index);
    if (this.paymentForm.value.itemPayRows.length == 0) {
      this.nopaymentdata = false;
    }
  }

  paytypeselected(paytype, index) {
    var pay = []
    if (paytype == "LC") {
      pay = _.filter(this.allpayterms, function (num) {
        return num.type == "LC";
      })
      this.paytermsLC = this.completerService.local(pay, 'term', 'term');
      let temp = this._fb.array([]);
      for (let i = 0; i < this.paymentForm.value.itemPayRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              paymenttype: this.paymentForm.value.itemPayRows[i].paymenttype,
              paymentterm: "",
              paymentpercentage: this.paymentForm.value.itemPayRows[i].paymentpercentage,
              credit: this.paymentForm.value.itemPayRows[i].credit
            })
          );
        } else {
          temp.push(
            this._fb.group(this.paymentForm.value.itemPayRows[i]));
        }
      }
      this.paymentForm = this._fb.group({
        itemPayRows: temp
      });
    } else {
      pay = _.filter(this.allpayterms, function (num) {
        return num.type == "NONLC";
      })
      this.payterms = this.completerService.local(pay, 'term', 'term');

      let temp = this._fb.array([]);
      for (let i = 0; i < this.paymentForm.value.itemPayRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              paymenttype: this.paymentForm.value.itemPayRows[i].paymenttype,
              paymentterm: "",
              paymentpercentage: this.paymentForm.value.itemPayRows[i].paymentpercentage,
              credit: this.paymentForm.value.itemPayRows[i].credit
            })
          );
        } else {
          temp.push(
            this._fb.group(this.paymentForm.value.itemPayRows[i]));
        }
      }
      this.paymentForm = this._fb.group({
        itemPayRows: temp
      });
    }

    console.log("pay", pay)
  }

  clacPercentage(index) {
    var totalpercentage = 0;
    _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
      var sum = +item.paymentpercentage
      totalpercentage += sum;
    });
    if (totalpercentage > 100) {
      this.notif.warn('Warning', "you can't enter more than 100%", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  onFileChange(event) {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.filelist.push(event.target.files[i]);
    }
    console.log("files", this.filelist);
  }

  clearfile(file, i) {
    this.filelist.forEach(function (data, index, object) {
      if (index === i) {
        object.splice(index, 1);
      }
    });
    console.log(this.filelist)
  }

  clearallfile() {
    this.filelist = []
  }

  calctrate() {
    var totalrate = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.amount;
      totalrate += a
    })
    return +totalrate
  }

  calctdisc() {
    var totaldisc = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.discount_value;
      totaldisc += a
    })
    return +totaldisc
  }

  calctpf() {
    var totalpf = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.pf_value;
      totalpf += a
    })
    return +totalpf
  }

  calctotal() {
    var amounttot = 0
    _.forEach(this.quotationdetails, function (val) {
      var a = +val.landing_cost;
      amounttot += a
    })
    return +amounttot
  }

  formObj: any = {
    freight: {
      required: "Freight Required",
    },
    dispatchmode: {
      required: "Dispatchmode Required",
    },
  }

  disclosepo() {
    swal({
      title: 'Are you sure?',
      text: "You Want to Disclose po " + this.pono,
      type: 'question',
      width: 500,
      padding: 10,
      showCancelButton: true,
      confirmButtonColor: '#ffaa00',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes!',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.others.is_short_close = 1
        this.show_loader = true;
        this.poservice.POShotclosure(this.others).subscribe(res => {
          if (res.status == "success") {
            this.back()
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.show_loader = false
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          this.show_loader = false
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        })
      }
    });
  }

  updatepo(stat) {
    if (this.url == this.poauthurl) {
      var text = "You want to Authorize PO"
    } else {
      var text = "You want to Update"
    }

    if (this.compareform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.compareform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
      return false;
    }
    else {
      var validdata: Boolean = true;
      var validationerrorMsg = [];
      var totalpercentage = 0
      var PaymentDetails = [];
      _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
        var sum = +item.paymentpercentage
        totalpercentage += sum
        console.log("totalpercentage", totalpercentage);
        var paymentlineitem =
        {
          "payment_type": item.paymenttype,
          "payment_terms": item.paymentterm,
          "payment_percentage": item.paymentpercentage,
          "credict": item.credit,
        }
        if (_.isEmpty(item.paymenttype && item.paymentterm)) {
          validdata = false;
          validationerrorMsg.push("Please select payment details");
        }
        if (item.paymentpercentage != 0) {
          PaymentDetails.push(paymentlineitem);
        }
        else {
          validdata = false;
          validationerrorMsg.push("Please enter percentage");
        }
        console.log("PaymentDetails", PaymentDetails)
      });
      if (totalpercentage > 100 || totalpercentage < 100) {
        validdata = false;
        validationerrorMsg.push("Please Enter 100% Payment");
      }
      this.selecteditemnsupplier = [];
      var poval = 0

      if (this.url == this.poauthurl) {
        _.forEach(this.quotationdetails, function (item, key) {
          if (item.is_po_authorized == 0) {
            validdata = false;
            validationerrorMsg.push("Please select status for : " + item.item_code);
          }
        });
      }


      if (validdata) {
        var pricebasis = _.findWhere(this.pricebasis, { 'price_basis': this.comp.pricebasis })
        this.formdata =
          {
            "id": this.others.id,
            "po_no": this.others.po_no,
            "enq_id": this.others.enq_id,
            "enq_no": this.others.enq_no,
            "qut_no": this.others.qut_no,
            "qut_id": this.others.qut_id,
            "validity": this.others.validity,
            "indent_no": this.others.indent_no,
            "unit_id": this.unitid,
            "supplier": this.others.supplier,
            "supplier_id": this.others.supplier_id,
            "freight": this.comp.freight,
            "mode_of_desp": this.comp.dispatchmode,
            // "price_basis_id": pricebasis.id,
            // "price_basis_name": pricebasis.price_basis,
            // "epcg_applicable": this.comp.epcg,
            // "license_no": _.isEmpty(this.comp.license) ? "" : this.comp.license,
            // "destination": _.isEmpty(this.comp.destination) ? "" : this.comp.destination,
            // "freight_amount": _.isEmpty(this.comp.freightamnt) ? 0 : this.comp.freightamnt,
            "gst_amount": _.isEmpty(this.comp.gstamnt) ? 0 : this.comp.gstamnt,
            "special_inst": _.isEmpty(this.comp.spclinst) ? "" : this.comp.spclinst,
            "additional_inst": _.isEmpty(this.comp.addinst) ? "" : this.comp.addinst,
            "packing_and_forwarding": _.isEmpty(this.comp.packingfrwding) ? "" : this.comp.packingfrwding,
            "po_value": this.others.po_value,
            "poitem": this.quotationdetails,
            "popayment": PaymentDetails,
            "po_type": this.others.po_type,
            // "created_date": this.others.created_date,
            "modified_date": this.others.modified_date,
            "created_date": this.podate.formatted
          }
        if (stat) {
          if (stat == "Approve") {
            this.formdata.status = 2
          } else if (stat == "Cancel") {
            this.formdata.status = 1
          } else if (stat == "Discuss") {
            this.formdata.status = 3
          }
        }
        else {
          this.formdata.status = this.others.status
        }
        console.log("formdata", this.formdata);
        swal({
          title: 'Are you sure?',
          text: text,
          type: 'question',
          width: 500,
          padding: 10,
          showCancelButton: true,
          confirmButtonColor: '#ffaa00',
          cancelButtonColor: '#3085d6',
          confirmButtonText: 'Yes!',
          cancelButtonText: 'No',
        }).then((result) => {
          if (result.value) {
            this.show_loader = true
            this.poservice.UpdatePO(this.formdata, this.filelist).subscribe(res => {
              if (res.status == "success") {
                this.back()
                this.notif.success('Success', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              } else {
                this.show_loader = false
                this.notif.error('Error', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              }
            }, err => {
              this.show_loader = false
              this.notif.error('Error', "Something Wrong", {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            })
          }
        });
      } else {
        this.notif.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }

    }
  }

  back() {
    this.location.back()
  }

}
