import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { PurchaseTransactionService } from '../../services/Purchase/transactions/purchase-transaction.service';
import { DepartmentsService } from '../../services/department/departments.service';
import { CompleterService } from 'ng2-completer';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';
declare let swal: any;
class Person {
  id: any;
  role_name: string;
  description: string;
  checkauthorize: boolean = false;
}
@Component({
  selector: 'app-po-search',
  templateUrl: './po-search.component.html',
  styleUrls: ['./po-search.component.css']
})
export class PoSearchComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  files: any;
  indentstatus: any;
  showDialog: boolean = false;
  showtab: boolean = false;
  nofiles: boolean = false;
  filter: any = {};
  purDetailsList: any;
  departs: any;
  alldpts: any;
  quotationdetails: any;
  totals: any;
  prevtot: any;
  enqid: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  contype: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private purchaseindentservice: PurchaseTransactionService,
    private dptservice: DepartmentsService,
    private http: Http,
    private completerService: CompleterService,
    private poservice: PurchaseorderService
  ) {
    this.contype = AppConstant.APP_CONST.ContactType;
    this.indentstatus = AppConstant.APP_CONST.IndentStatus3;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    // this.getdepartments();
    this.filter.status = 0;
    this.filter.type = "All"
    this.getPOlist();
  }

  // getdepartments() {
  //   this.show_loader = true
  //   this.dptservice.getByStatus("active").subscribe(res => {in
  //     if (res.status == "success") {
  //       this.show_loader = false;
  //       this.departs = res.data;
  //       this.alldpts = this.completerService.local(res.data, 'department_name', 'department_name');
  //     }
  //   })
  // }

  cancelpo(data, i) {
    swal({
      title: 'Are you sure?',
      text: "You want to Cancel PO!",
      type: 'question',
      width: 500,
      padding: 10,
      showCancelButton: true,
      confirmButtonColor: '#ffaa00',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes!',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.poservice.CancelPO(data.id).subscribe(res => {
          this.show_loader = false
          if (res.status == "success") {
            this.getPOlist();
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          this.show_loader = false
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        })
      }
    })
  }

  cancelpoQtn(data, i) {
    swal({
      title: 'Are you sure?',
      text: "You want to Cancel PO Quotation!",
      type: 'question',
      width: 500,
      padding: 10,
      showCancelButton: true,
      confirmButtonColor: '#ffaa00',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes!',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.poservice.CancelPOQtn(data.id).subscribe(res => {
          this.show_loader = false
          if (res.status == "success") {
            this.getPOlist();
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          this.show_loader = false
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        })
      }
    })
  }

  getcompare(data) {
    console.log("data", data)
    this.enqid = data.enq_id;
    this.showDialog = true
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Order Confirmation</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  getPOlist() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
        {
          "type": this.filter.type,
          "unit_id": this.unitid,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
          "status": this.filter.status
        }
      this.show_loader = true;
      this.poservice.GetPoAuthList(obj).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  poid: any

  getpodetails(data) {
    console.log("data =========>>>>", JSON.stringify(data))
    this.poid = data.id;
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    })
  }

  notifycancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }


  close() {
    this.showDialog = false
  }
}
