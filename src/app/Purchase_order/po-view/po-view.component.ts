import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { PurchaseorderService } from '../../services/purchase_order/purchaseorder.service';
import { UserService } from '../../services/user/user.service';

// import { PurchaseTransactionService } from '../../services/Purchase/transactions/purchase-transaction.service';

@Component({
  selector: 'app-po-view',
  templateUrl: './po-view.component.html',
  styleUrls: ['./po-view.component.css']
})
export class PoViewComponent implements OnInit {
  @Input() poid: any;
  @Input() excesslist: any
  @Input() supid: any
  @Output() notifycancel: EventEmitter<any> = new EventEmitter();
  purDetails: any;
  show_loader: boolean = false;
  toastertime: any;
  tosterminlength: any;
  PurLineDetails: any;
  suppliers: any;
  supplierdtls: any;
  url: any;
  purchaseid: any
  constructor(
    private poservice: PurchaseorderService,
    private notif: NotificationsService,
    private userservice: UserService,
    private router: Router,
    private acroute: ActivatedRoute,
  ) {
    this.url = this.router.url;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.purchaseid = params.id;
        console.log("urlparams", this.purchaseid);
      }
    });
  }

  ngOnInit() {
    this.getsuppliers();
    setTimeout(() => {
      this.viewdetails();
    }, 500);
    if (this.excesslist && this.supid) {
      setTimeout(() => {
        this.viewexcess();
      }, 500);
    }
  }

  getsuppliers() {
    this.userservice.getContactsByCat("Supplier").subscribe(res => {
      this.suppliers = res.data
    })
  }

  viewexcess() {
    this.PurLineDetails = this.excesslist;
    setTimeout(() => {
      this.supplierdtls = _.findWhere(this.suppliers, { id: this.supid })
      console.log("suppliers", this.supplierdtls);
    });
  }
  paymentterms: any

  staticdata =
    {
      "item_code": "",
      "part": "",
      "item_desc": "",
      "drawing_no": "",
      "material": "",
      "material_spec_no": "",
      "revision": "",
      "uom": "",
      "req_qty": 0,
      "rate": 0,
      "landing_cost": 0,
      "shipment_date": "",
      "pconfirm_date": "",
      "remarks": "",
      "empty": true
    }

  viewdetails() {
    if (this.poid) {
      this.show_loader = true;

      if (this.url == "/poconfirmation") {
        this.poservice.GetPOConfirmedByID(this.poid).subscribe(res => {
          if (res.status == "success") {
            this.show_loader = false;
            this.purDetails = res.data;
            this.PurLineDetails = res.data.poitem;

            // if (this.PurLineDetails.length < 15) {
            //   var ballength = 15 - this.PurLineDetails.length
            // }
            // if (ballength != 0) {
            //   for (var i = 0; i < ballength; i++) {
            //     this.PurLineDetails.push(this.staticdata)
            //   }
            // }
            console.log("PurLineDetails", this.PurLineDetails)

            this.paymentterms = this.purDetails.popayment.map(f => f.payment_terms).join(', ');
            setTimeout(() => {
              this.supplierdtls = _.findWhere(this.suppliers, { id: res.data.supplier_id })
              console.log("suppliers", this.supplierdtls);
            }, 1000);
          } else {
            this.show_loader = false;
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        })
      } else {
        this.poservice.GetPOByID(this.poid).subscribe(res => {
          if (res.status == "success") {
            this.show_loader = false;
            this.purDetails = res.data;
            this.PurLineDetails = res.data.poitem;
            // if (this.PurLineDetails.length < 15) {
            //   var ballength = 15 - this.PurLineDetails.length
            // }
            // if (ballength != 0) {
            //   for (var i = 0; i < ballength; i++) {
            //     this.PurLineDetails.push(this.staticdata)
            //   }
            // }
            console.log("PurLineDetails", this.PurLineDetails)
            this.paymentterms = this.purDetails.popayment.map(f => f.payment_terms).join(', ');
            setTimeout(() => {
              this.supplierdtls = _.findWhere(this.suppliers, { id: res.data.supplier_id })
              console.log("suppliers", this.supplierdtls);
            }, 500);
          } else {
            this.show_loader = false;
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        })
      }
    }
  }

  calctrate() {
    var totalrate = 0
    _.forEach(this.PurLineDetails, function (val) {
      var a = +val.rate;
      totalrate += a
    })
    return +totalrate;
  }

  calctdisc() {
    var totaldisc = 0
    _.forEach(this.PurLineDetails, function (val) {
      var a = +val.discount_value;
      totaldisc += a
    })
    return +totaldisc
  }

  calctpf() {
    var totalpf = 0
    _.forEach(this.PurLineDetails, function (val) {
      var a = +val.pf_value;
      totalpf += a
    })
    return +totalpf
  }

  calctotal() {
    var amounttot = 0
    _.forEach(this.PurLineDetails, function (val) {
      var a = +val.landing_cost;
      amounttot += a
    })
    return +amounttot
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Purchase Order</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          .print-view>tbody>tr>td,
          .print-view>tbody>tr>th,
          .print-view>tfoot>tr>td,
          .print-view>tfoot>tr>th,
          .print-view>thead>tr>td,
          .print-view>thead>tr>th {
            border: 1px solid #333 !important; padding:2px 3px !important
          }
          .print-view .print-label p { font-weight : 500 !important;}
          .print-view .print-label { font-weight : 500 !important;}
          .print-view .cs { background-color:#ccc !important;}
          .print-view .cs td { background-color:#ccc !important;}
           table.print-view td p {
           margin: 0!important;
          }
        table.report-container {
        page-break-after:always !important
        }
        .print-view .bg-quoted td{
          background-color: #dcffbc !important;
          font-weight : bold
        }
        thead.report-header {
        display:table-header-group;
        }
        tfoot.report-footer {
        display:table-footer-group;
        } 
        .print-view .pagebreak{page-break-after: always}
        </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  cancel() {
    this.notifycancel.next();
  }

}
