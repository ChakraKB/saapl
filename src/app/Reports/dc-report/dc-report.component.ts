import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { CompleterService } from 'ng2-completer';
import { ItemsService } from '../../services/items/items.service';
import { ReportsService } from '../../services/Reports/reports.service';

@Component({
  selector: 'app-dc-report',
  templateUrl: './dc-report.component.html',
  styleUrls: ['./dc-report.component.css']
})
export class DcReportComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  disablesincetoday: INgxMyDpOptions = this.masterservice.DateDisablesSinceToday();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  filter: any = {};
  from: any;
  to: any;
  show_loader: boolean = false;
  allitems: any;
  activeitems: any;
  stattype: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  toastertime: number;
  tosterminlength: number;
  grndetails: any;
  showDialog: boolean = false
  constructor(
    private masterservice: MasterService,
    private notif: NotificationsService,
    private completerService: CompleterService,
    private reportservice: ReportsService,
    private itemservice: ItemsService,
  ) {
    this.stattype = AppConstant.APP_CONST.RECPEND;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.getItems();
    let date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth() + 1, 1);
    this.alterdateformat(date, firstDay);
    var firstdate = AppConstant.APP_CONST.FIRST_CURRENTDATE.firstdate
    var currentdate = AppConstant.APP_CONST.FIRST_CURRENTDATE.currentdate
    this.filter.from = { date: firstdate, formatted: this.from, jsdate: new Date(date.getFullYear(), date.getMonth(), 1) }
    this.filter.to = { date: currentdate, formatted: this.to, jsdate: new Date() };
    this.getdcreportList();
    this.dtOptions = {
      dom: 'Bfrtip',
      buttons: [
        'copy',
        {
          extend: 'csv',
          text: 'CSV export',
          fieldSeparator: ';',
          exportOption: [1, 2, 3]
        },
        'print',
        'excel',
      ]
    };
  }

  getItems() {
    this.show_loader = true
    this.itemservice.getItemByStatus("active").subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.allitems = res.data;
        this.activeitems = this.completerService.local(this.allitems, "component_desc,component,part_no", 'part_no');
      }
    })
  }

  alterdateformat(date: any, firstdate: any) {
    this.from = formatDate(firstdate);
    this.to = formatToDate(date);
    function formatDate(date: any) {
      var d = new Date(date),
        month = '' + (d.getMonth()),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }

    function formatToDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }

  viewdetail(data) {
    this.showDialog = true;
    this.grndetails = data.grn_qty
  }

  close() {
    this.showDialog = false
  }

  getdcreportList() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        var validdate = this.masterservice.checkvalid(this.filter.from.jsdate, this.filter.to.jsdate)
        if (!validdate) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      // if (this.filter.item) {
      //   var part = _.findWhere(this.allitems, { "part_no": this.filter.item })
      //   var part_id = part.id
      // } else {
      //   var part_id = 0
      // }
      var obj =
      {
        "part_no": _.isEmpty(this.filter.item) ? "" : this.filter.item,
        "unit_id": this.unitid,
        "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
        "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
      }
      this.show_loader = true;
      this.reportservice.GetDcReportList(obj).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
