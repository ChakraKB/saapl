import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItembinReportComponent } from './itembin-report.component';

describe('ItembinReportComponent', () => {
  let component: ItembinReportComponent;
  let fixture: ComponentFixture<ItembinReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItembinReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItembinReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
