import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { CompleterService } from 'ng2-completer';
import { ReportsService } from '../../services/Reports/reports.service';
import { BinService } from '../../services/bin/bin.service';

@Component({
  selector: 'app-itembin-report',
  templateUrl: './itembin-report.component.html',
  styleUrls: ['./itembin-report.component.css']
})
export class ItembinReportComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  disablesincetoday: INgxMyDpOptions = this.masterservice.DateDisablesSinceToday();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  filter: any = {};
  from: any;
  to: any;
  show_loader: boolean = false;
  allitems: any;
  activeitems: any;
  stattype: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  toastertime: number;
  tosterminlength: number;
  allbins: any;
  activebins: any;
  bindetails: any;
  constructor(
    private masterservice: MasterService,
    private notif: NotificationsService,
    private completerService: CompleterService,
    private reportservice: ReportsService,
    private binservice: BinService
  ) {
    this.stattype = AppConstant.APP_CONST.RECPEND;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.getBins();
    this.dtOptions = {
      dom: 'Bfrtip',
      buttons: [
        'copy',
        {
          extend: 'csv',
          text: 'CSV export',
          fieldSeparator: ';',
          exportOption: [1, 2, 3],
        },
        'print',
        'excel',
      ]
    };
  }

  getBins() {
    this.binservice.getBinByStatus("active").subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.allbins = res.data;
        this.dtTrigger.next();
        this.activebins = this.completerService.local(this.allbins, "bin_name", 'bin_name');
      }
    })
  }

  selectedbin(data) {
    if (data) {
      this.bindetails = data.originalObject
      var obj =
        {
          "unit_id": this.unitid,
          "bin_id": data.originalObject.id
        }
      this.show_loader = true;
      this.reportservice.GetItemLocReportList(obj).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.bindetails = ""
      this.persons = []
      if (this.dtElement.dtInstance) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        })
      } else
        this.dtTrigger.next();
    }
  }

}
