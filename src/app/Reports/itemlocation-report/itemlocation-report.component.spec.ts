import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemlocationReportComponent } from './itemlocation-report.component';

describe('ItemlocationReportComponent', () => {
  let component: ItemlocationReportComponent;
  let fixture: ComponentFixture<ItemlocationReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemlocationReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemlocationReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
