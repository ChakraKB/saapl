import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingdcReportComponent } from './pendingdc-report.component';

describe('PendingdcReportComponent', () => {
  let component: PendingdcReportComponent;
  let fixture: ComponentFixture<PendingdcReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingdcReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingdcReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
