import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoValueReportComponent } from './po-value-report.component';

describe('PoValueReportComponent', () => {
  let component: PoValueReportComponent;
  let fixture: ComponentFixture<PoValueReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoValueReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoValueReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
