import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockClassComponent } from './stock-class.component';

describe('StockClassComponent', () => {
  let component: StockClassComponent;
  let fixture: ComponentFixture<StockClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
