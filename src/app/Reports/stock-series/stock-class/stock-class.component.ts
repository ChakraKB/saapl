import { AfterViewInit, Component, OnInit, ViewChild, QueryList, Input, Output, EventEmitter, } from '@angular/core';
import { MasterService } from '../../../services/master.service';
import { AppConstant } from '../../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { ReportsService } from '../../../services/Reports/reports.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DataTableDirective } from 'angular-datatables';
import { ChangeDetectorRef } from '@angular/core';
@Component({
  selector: 'app-stock-class',
  templateUrl: './stock-class.component.html',
  styleUrls: ['./stock-class.component.css']
})
export class StockClassComponent implements OnInit {
  filter: any = {};
  show_loader: boolean = false;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any;
  persons: any = [];
  toastertime: number;
  tosterminlength: number;
  dtTrigger: Subject<any> = new Subject();
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  class: any
  @Input() obj: any;
  @Output() notifyopen: EventEmitter<any> = new EventEmitter();
  constructor(
    private masterservice: MasterService,
    private notif: NotificationsService,
    private reportservice: ReportsService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.dtOptions = {
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'csv',
        'print',
        'excel',
      ]
    };
    console.log("obj", this.obj)
    this.openclass()
  }

  openclass() {
    this.show_loader = true;
    this.reportservice.GetClassBySeries(this.obj).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.class = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  openitem(data) {
    this.obj =
      {
        "series": _.isEmpty(data.series) ? "" : data.series,
        "cls": _.isEmpty(data.cls) ? "" : data.cls,
      }
    this.notifyopen.next(this.obj)
  }

}
