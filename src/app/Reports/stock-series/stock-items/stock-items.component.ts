import { AfterViewInit, Component, OnInit, ViewChild, QueryList, Input } from '@angular/core';
import { MasterService } from '../../../services/master.service';
import { AppConstant } from '../../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { ReportsService } from '../../../services/Reports/reports.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DataTableDirective } from 'angular-datatables';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-stock-items',
  templateUrl: './stock-items.component.html',
  styleUrls: ['./stock-items.component.css']
})
export class StockItemsComponent implements OnInit {
  filter: any = {};
  show_loader: boolean = false;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any;
  persons: any = [];
  toastertime: number;
  tosterminlength: number;
  dtTrigger: Subject<any> = new Subject();
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  items: any;
  receiptdetails: any;
  issuedetails: any;
  showDialog: boolean = false;
  showIssueDialog : boolean = false
  @Input() itemobj: any;
  constructor(
    private masterservice: MasterService,
    private notif: NotificationsService,
    private reportservice: ReportsService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.dtOptions = {
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'csv',
        'print',
        'excel',
      ]
    };
    console.log("obj", this.itemobj)
    this.openItems()
  }

  openItems() {
    this.show_loader = true;
    this.reportservice.GetItemsBySeriesClass(this.itemobj).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.items = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  viewreceipts(data) {
    this.show_loader = true;
    var obj =
    {
      receipts_id : data.receipts_id,
      part_id: data.item_id,
      from_date: this.itemobj.from_date,
      to_date: this.itemobj.to_date,
    }
    this.reportservice.GetItemsStockReceipts(obj).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.showDialog = true
        this.receiptdetails = res.data;
      }
    })
  }

  viewissues(data) {
    this.show_loader = true;
    var obj =
    {
      part_id: data.item_id,
      from_date: this.itemobj.from_date,
      to_date: this.itemobj.to_date,
    }
    this.reportservice.GetItemsStockIssues(obj).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.showIssueDialog = true
        this.issuedetails = res.data;
      }
    })
  }

  close() {
    this.showDialog = false;
    this.showIssueDialog = false
  }


}
