import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockSeriesComponent } from './stock-series.component';

describe('StockSeriesComponent', () => {
  let component: StockSeriesComponent;
  let fixture: ComponentFixture<StockSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
