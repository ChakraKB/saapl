import { AfterViewInit, Component, OnInit, ViewChild, QueryList } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { CompleterService } from 'ng2-completer';
import { ReportsService } from '../../services/Reports/reports.service';
import { ItemsService } from '../../services/items/items.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-stock-series',
  templateUrl: './stock-series.component.html',
  styleUrls: ['./stock-series.component.css']
})
export class StockSeriesComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any;
  persons: any = [];
  dtTrigger: Subject<any> = new Subject();
  dcsuppliers: any;
  alldcsuppliers: any;
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  files: any;
  indentstatus: any;
  showDialog: boolean = false;
  showtab: boolean = false;
  showitemtab: boolean = false;
  nofiles: boolean = false;
  filter: any = {};
  dcid: any;
  loclist: any;
  allitems: any;
  from: any;
  to: any;
  allseries: any;
  activeseries: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private completerService: CompleterService,
    private reportservice: ReportsService,
    private itemservice: ItemsService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    let date = new Date();
    this.getSeriesList()
    var firstDay = new Date(date.getFullYear(), date.getMonth() + 1, 1);
    this.alterdateformat(date, firstDay);
    var firstdate = AppConstant.APP_CONST.FIRST_CURRENTDATE.firstdate
    var currentdate = AppConstant.APP_CONST.FIRST_CURRENTDATE.currentdate
    this.filter.from = { date: firstdate, formatted: this.from, jsdate: new Date(date.getFullYear(), date.getMonth(), 1) }
    this.filter.to = { date: currentdate, formatted: this.to, jsdate: new Date() };
    this.getItemSeries();
    this.dtOptions = {
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'csv',
        'print',
        'excel',
      ]
    };
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  getSeriesList() {
    this.itemservice.GetSeriesByStatus('active').subscribe(res => {
      if (res.status == "success") {
        this.allseries = res.data
        this.activeseries = this.completerService.local(this.allseries, "name", 'name');
      }
    })
  }

  closetab() {
    this.showtab = false;
    this.showitemtab = false;
    setTimeout(() => {
      jQuery('#pending')[0].click();
    })
  }

  closeItemtab() {
    this.showitemtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    })
  }

  alterdateformat(date: any, firstdate: any) {
    this.from = formatDate(firstdate);
    this.to = formatToDate(date);
    function formatDate(date: any) {
      var d = new Date(date),
        month = '' + (d.getMonth()),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }

    function formatToDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }

  getItemSeries() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        var validdate = this.masterservice.checkvalid(this.filter.from.jsdate, this.filter.to.jsdate)
        if (!validdate) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      // if (this.filter.item) {
      //   var part = _.findWhere(this.allitems, { "part_no": this.filter.item })
      //   var part_id = part.id
      // } else {
      //   var part_id = 0
      // }
      var obj =
      {
        // "series": _.isEmpty(this.filter.series) ? "" : this.filter.series,
        "unit_id": this.unitid,
        "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
        "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
      }
      this.show_loader = true;
      this.reportservice.GetItemSeriesList(obj).subscribe(res => {
        this.closetab();
        if (res.status == "success") {
          this.persons = res.data;
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else { this.dtTrigger.next();}
            this.show_loader = false;
        } else {
          this.show_loader = false;
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }
  class: any
  obj: any
  itemobj: any
  openclass(data) {
    this.obj =
      {
        "series": _.isEmpty(data.series) ? "" : data.series,
        "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
        "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
      }
    this.showtab = true;
    setTimeout(() => {
      jQuery('#list')[0].click();
    })
  }

  notifyopen(data) {
    this.itemobj =
      {
        "series": _.isEmpty(data.series) ? "" : data.series,
        "cls": _.isEmpty(data.cls) ? "" : data.cls,
        "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
        "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
      }
    console.log("itemobj", this.itemobj)
    this.showitemtab = true;
    setTimeout(() => {
      jQuery('#item')[0].click();
    })
  }
}
