import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolReportComponent } from './tool-report.component';

describe('ToolReportComponent', () => {
  let component: ToolReportComponent;
  let fixture: ComponentFixture<ToolReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
