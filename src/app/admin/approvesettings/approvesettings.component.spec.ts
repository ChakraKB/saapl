import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovesettingsComponent } from './approvesettings.component';

describe('ApprovesettingsComponent', () => {
  let component: ApprovesettingsComponent;
  let fixture: ComponentFixture<ApprovesettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovesettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovesettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
