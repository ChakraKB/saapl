import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogincreationComponent } from './logincreation.component';

describe('LogincreationComponent', () => {
  let component: LogincreationComponent;
  let fixture: ComponentFixture<LogincreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogincreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogincreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
