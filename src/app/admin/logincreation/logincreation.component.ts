import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { RolesService } from '../../services/Roles/roles.service';
import { DepartmentsService } from '../../services/department/departments.service';
import { UnitService } from '../../services/unit/unit.service'
import { UserService } from '../../services/user/user.service';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { BootstrapPaginator } from 'angular2-datatable';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as _ from 'underscore';
import { ChangeDetectorRef } from '@angular/core';
declare let swal: any;
class Person {
  bin: string
  description: string;
}

@Component({
  selector: 'app-logincreation',
  templateUrl: './logincreation.component.html',
  styleUrls: ['./logincreation.component.css']
})
export class LogincreationComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  public filterQuery = "";
  message = '';
  users: any;
  ename: any;
  uname: any;
  roles: any;
  mobileno: any;
  email: any;
  unit: any;
  pwd: any;
  dpt: any;
  listofuom: any;
  allusers: any[];
  departments: any[];
  rolelists: any[];
  unitlists: any[];
  table: any;
  id: number;
  userForm: FormGroup;
  formdata: any;
  userid: any;
  data: any;
  toastertime: number;
  tosterminlength: number
  department: any;
  userroles: any;
  buttonname: string;
  statusarray: any[];
  status: any;
  selectedunit: any;
  tabletimeout: any;
  response: any;
  created_date: any;
  password: any;
  show_loader: boolean = false;
  scrolltime: number;
  deleteoptions: any;
  constructor(
    private fb: FormBuilder,
    private userservice: UserService,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private router: Router,
    private departmentservice: DepartmentsService,
    private rolesService: RolesService,
    private unitService: UnitService,
    private http: Http,
    private spinnerService: Ng4LoadingSpinnerService,
    private cdRef: ChangeDetectorRef
  ) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.statusarray = AppConstant.APP_CONST.status;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    console.log(this.statusarray)
    this.userForm = fb.group({
      'userid': [null],
      'ename': [null, Validators.required],
      'roles': [null, Validators.required],
      // 'uname': [null, Validators.required],
      'mobileno': [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(15)])],
      'email': [null, Validators.compose([Validators.required, Validators.email])],
      'dpt': [null, Validators.required],
      'unit': [null, Validators.required],
      'status': [null, Validators.required]
    })
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }


  ngOnInit() {
    this.buttonname = "Save";
    this.status = 1;
    this.LoadRoles();
    this.LoadDepartments();
    this.LoadUnits();
    this.loadAngtable();

  }


  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  loadAngtable() {
    this.show_loader = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.USERS.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  getusers() {
    this.show_loader = true
    this.persons = [];
    this.userservice.getallUser()
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons);
        }
      });
  }


  LoadRoles() {
    this.show_loader = true
    this.rolesService.getByStatus("active")
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.rolelists = this.masterservice.formatDataforDropdown("role_name", "id", res.data);
          console.log("listofactivetypes", this.rolelists);
        }
      });
  }

  LoadDepartments() {
    this.show_loader = true
    this.departmentservice.getByStatus("active")
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.departments = this.masterservice.formatDataforDropdown("department_name", "id", res.data);
          console.log("listofactivetypes", this.departments);
        }
      });
  }

  LoadUnits() {
    this.show_loader = true
    this.unitService.getUnitByStatus("active")
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.unitlists = this.masterservice.formatDataforDropdown("unit_name", "id", res.data);
          console.log("listofactivetypes", this.unitlists);
        }
      });
  }

  numberOnly(event) {
    this.masterservice.allowNumberOnly(event);
  }

  updateuser(user) {

    var unitids = _.pluck(user.unitList, 'unit_id');
    console.log("unitids", unitids);
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.created_date = user.crt_dt
    this.buttonname = "Update"
    this.userid = user.id;
    this.ename = user.name;
    // this.uname = user.username;
    this.userForm.controls['roles'].setValue(user.role.id);
    this.mobileno = user.mobile_no;
    this.email = user.email;
    this.userForm.controls['unit'].setValue(unitids);
    this.status = user.active_status;
    this.userForm.controls['dpt'].setValue(user.department.id);
    this.password = user.password;
  }

  deleteuser(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.userservice.deleteUser(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getusers();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  selecteddpt(dpt) {
    this.department = dpt;
  }

  formObj: any = {

    ename: {
      required: "Employee Name Required",
    },
    // uname: {
    //   required: "User Name Required"
    // },
    roles: {
      required: "Roles Required"
    },
    mobileno: {
      required: "Mobileno Required",
      minlength: "Minimum Mobile number should be in 8 digits",
      maxlength: "Maximum Mobile number should be in 15 digits"
    },
    email: {
      required: "Email Required",
      email: "Email id is not valid"
    },
    unit: {
      required: "Unit Required"
    },
    dpt: {
      required: "Department Required"
    },
    status: {
      required: "Status Required"
    }
  }

  addedituser(data) {
    if (this.userForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.userForm, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
      return false;
    } else {
      this.selectedunit = []
      this.department = _.findWhere(this.departments, { "value": data.dpt })
      this.userroles = _.findWhere(this.rolelists, { "value": data.roles })
      for (var i = 0; i < data.unit.length; i++) {
        var unitdtl = _.findWhere(this.unitlists, { "value": data.unit[i] })
        var unitdata =
        {
          "unit_id": unitdtl.value,
          "unit_name": unitdtl.label
        }
        this.selectedunit.push(unitdata)
      }

      if (data.userid) {
        this.show_loader = true
        this.formdata =
          {
            "id": data.userid,
            'name': data.ename,
            'role': { "id": this.userroles.value },
            // 'username': data.uname,
            'mobile_no': data.mobileno,
            'unitList': this.selectedunit,
            'email': data.email,
            'department': { "id": this.department.value },
            'active_status': data.status,
            'crt_dt': this.created_date,
            'password': this.password
          }
        this.userservice.updateUser(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getusers();
              });
              this.resetform();
              this.show_loader = false
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.show_loader = false
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getusers();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true
        this.formdata =
          {
            'name': data.ename,
            'role': { "id": this.userroles.value },
            // 'username': data.uname,
            'mobile_no': data.mobileno,
            'unitList': this.selectedunit,
            'email': data.email,
            'department': { "id": this.department.value },
            'active_status': data.status
          }
        this.userservice.addUser(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getusers();
              });
              this.resetform();
              this.show_loader = false;
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getusers();
              });
              this.show_loader = false;
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }
  }

  resetform() {
    this.userForm.reset();
    this.buttonname = "Save";
    this.userForm.controls['status'].setValue(1);
  }



}
