import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolespageassociationComponent } from './rolespageassociation.component';

describe('RolespageassociationComponent', () => {
  let component: RolespageassociationComponent;
  let fixture: ComponentFixture<RolespageassociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolespageassociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolespageassociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
