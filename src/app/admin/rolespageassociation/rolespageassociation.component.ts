import { Component, OnInit, ViewChild } from '@angular/core';
import { TreeviewItem } from '../../../../node_modules/ngx-treeview/src';
import { TreeNode } from 'primeng/primeng';
import { ITreeOptions, IActionMapping } from 'angular-tree-component';
import { RolesService } from '../../services/Roles/roles.service';
import { UserService } from '../../services/user/user.service';
import { MasterService } from '../../services/master.service';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { AppConstant } from '../../app.constant';
import * as _ from 'underscore';
import { isUndefined } from 'util';
@Component({
  selector: 'app-rolespageassociation',
  templateUrl: './rolespageassociation.component.html',
  styleUrls: ['./rolespageassociation.component.css']
})

export class RolespageassociationComponent implements OnInit {
  actionMapping: IActionMapping = {
    mouse: {
      click: (tree, node) => this.check(node, !node.data.checked)
    }
  };
  ITreeOptions = {
    useCheckbox: false,
    actionMapping: this.actionMapping
  };
  @ViewChild('expandingTree')
  items: TreeviewItem[];
  treearray: TreeNode[];
  selectedFiles: TreeNode[] = [];
  treearray2: any[];
  nodes: any[]
  selectedmodes: any[];
  snode: any;
  firststlayerobj: any[];
  activeroles: any[];
  selectedscreens: any[];
  selectedroleid: any;
  roles: any;
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  constructor(private roleservice: RolesService,
    private masterservice: MasterService,
    private userservice: UserService,
    private notif: NotificationsService, ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.selectedmodes = []
    this.getroles();
  }

  getroles() {
    this.roleservice.getByStatus("active")
      .subscribe(res => {
        this.activeroles = this.masterservice.formatDataforDropdown("role_name", "id", res.data);
        console.log("actroles", this.activeroles);
      })
  }

  onroleselect(data) {
    this.show_loader = true;
    console.log("rolesselected", data)
    this.selectedroleid = data.value
    this.roleservice.getScreenByRole(data.value)
      .subscribe(res => {
        this.show_loader = false;
        console.log("resscreen", res);
        this.nodes = res.data
      });
  }

  public check(node, $event) {
    this.updateChildNodesCheckBox(node, $event.target.checked);
    this.updateParentNodesCheckBox(node.parent);
  }

  public updateChildNodesCheckBox(node, checked) {
    node.data.checked = checked;
    if (node.children) {
      node.children.forEach((child) => this.updateChildNodesCheckBox(child, checked));
    }
  }
  public updateParentNodesCheckBox(node) {
    if (node && node.level > 0 && node.children) {
      let allChildChecked = true;
      let noChildChecked = true;

      for (let child of node.children) {
        if (!child.data.checked) {
          allChildChecked = false;
        } else if (child.data.checked) {
          noChildChecked = false;
        }
      }

      if (allChildChecked) {
        node.data.checked = true;
        node.data.indeterminate = false;
      } else if (noChildChecked) {
        node.data.checked = false;
        node.data.indeterminate = false;
      } else {
        node.data.checked = true;
        node.data.indeterminate = true;
      }
      this.updateParentNodesCheckBox(node.parent);
    }
    this.remid();
  }

  createaccess() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (isUndefined(this.roles) || this.roles == "") {
      validdata = false;
      validationerrorMsg.push("Please select role");
    }
    var screens = []
    if (validdata) {
      this.show_loader = true;
      if (_.isEmpty(this.selectedscreens)) {
        this.remid();
        screens = this.selectedscreens
      } else screens = this.selectedscreens
      this.userservice.createAccess(screens, this.selectedroleid).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.cancel();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false
        this.notif.error('Error', 'Something Wrong,Please try again', {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  remid() {
    var slectednode = []
    _.forEach(this.nodes, function (item, value) {
      // ====================== 1st layer ===============
      var firststlayerobj;
      firststlayerobj = _.omit(item, 'id', 'indeterminate');
      if (firststlayerobj.children) {
        // ====================== 2nd layer ===============
        var firstchildobj: any;
        var flchildobj: any;
        var submenu = []
        firstchildobj = _.omit(firststlayerobj, 'children');
        _.forEach(firststlayerobj.children, function (item, value) {
          flchildobj = _.omit(item, 'id', 'indeterminate');
          if (flchildobj.children) {
            // ====================== 3rd layer ===============
            var flchildobjdtls: any;
            var rejsubparid: any;
            var subsubmenu = []
            flchildobjdtls = _.omit(flchildobj, 'children');
            _.forEach(flchildobj.children, function (item, value) {
              rejsubparid = _.omit(item, 'id', 'indeterminate');
              subsubmenu.push(rejsubparid)
            })
            flchildobjdtls.children = subsubmenu;
            submenu.push(flchildobjdtls)
          } else {
            submenu.push(flchildobj)
          }
        })
        firstchildobj.children = submenu;
        slectednode.push(firstchildobj);
      } else {
        slectednode.push(firststlayerobj)
      }
    });
    this.selectedscreens = slectednode
  }


  cancel() {
    this.nodes = []
    this.roles = ""
    // window.location.reload();
  }

}
