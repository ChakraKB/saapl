import { Component, ViewContainerRef } from '@angular/core';
import { NotifyService } from './services/notify.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { UserService } from './services/user/user.service'
import { Router } from '@angular/router';
import * as _ from 'underscore';
import { TitleService } from "./services/title.service";
import { isUndefined, isNull } from 'util';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';
  auth: any;
  userdetails: any
  role: any;
  menupages: any;
  constructor(
    private notifyService: NotifyService,
    private router: Router,
    private permissionsService: NgxPermissionsService,
    private titleService: TitleService,
    private userservice: UserService) {
    this.auth = localStorage.getItem('AUTH')
    if (this.auth == 'true') this.auth = true;
    else this.auth = false;
  }

  ngOnInit() {
    this.titleService.ngOnInit();
    this.notifyLoggedIn();
    this.userdetails = localStorage.getItem("UserDetails");
    this.menupages = localStorage.getItem("LeftMenu");
    var userjson = JSON.parse(this.userdetails);
    var menujson = JSON.parse(this.menupages);
    if (!isNull(menujson)) {
      this.role = _.pluck(menujson, 'page_name');
      this.permissionsService.flushPermissions();
      this.permissionsService.loadPermissions(this.role);
    }
  }

  notifyLoggedIn() {
    this.notifyService.getLoggedInUser().subscribe((message) => {
      if (message == true) {
        console.log("authtrue")
        this.auth = true;
        this.userdetails = localStorage.getItem("UserDetails");
        this.menupages = localStorage.getItem("LeftMenu");
        var userjson = JSON.parse(this.userdetails);
        var menujson = JSON.parse(this.menupages);
        this.role = _.pluck(menujson, 'page_name');
        this.permissionsService.flushPermissions();
        this.permissionsService.loadPermissions(this.role);
      }
      else {
        this.auth = false;
      }
    });
  }
}
