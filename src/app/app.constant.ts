export const AppConstant = Object.freeze({

    // ----Local----
    // API_ENDPOINT : "http://demo3142783.mockable.io/",
   // API_ENDPOINT: "http://90.0.1.84:8085/saaplwebservices/webapi/",
    // API_ENDPOINT: "http://90.0.1.83:8090/saaplwebservices/webapi/",
    API_ENDPOINT: "http://90.0.1.87:8080/saaplwebservices/webapi/",
    // 

    // ----demo----
    // API_ENDPOINT: "http://tom.24x7a2z.com:8080/saaplwebservices/webapi/",
    //

    //----- LIVE 1 -------
    //API_ENDPOINT : "http://192.168.0.174:8080/saaplwebservices/webapi/",
    //

     //----- LIVE 2 -------
    // API_ENDPOINT : "http://192.168.0.96:8080/saaplwebservices/webapi/",
    //

    // ----QA----
    // API_ENDPOINT: "http://tom.24x7a2z.com:8080/qasaaplwebservices/webapi/",

    APP_CONST:
    {
        COMPANY_STATE:
        {
            id: 1
        },
        Table_Trigger_timeout:
        {
            timeout: 300
        },
        toaster:
        {
            toaster_time: 3000,
            minlength: 50
        },
        scrolltime:
        {
            scrollt: 200,
        },
        FIRST_CURRENTDATE:
        {
            "firstdate": { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: 1 },
            "currentdate": { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() }
        },
        DateformatOptions:
        {
            dateFormat: 'dd-mm-yyyy',
            firstDayOfWeek: 'su',
            appendSelectorToBody: true,
        },
        DELETESWAL:
        {
            title: 'Are you sure?',
            text: "You want to Delete!",
            type: 'info',
            width: 500,
            padding: 10,
            showCancelButton: true,
            confirmButtonColor: '#ffaa00',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No',
        },
        PTYPE:
            [
                {
                    value: 0,
                    name: "Spare"
                },
                {
                    value: 1,
                    name: "Sub-Assembly"
                },
                {
                    value: 2,
                    name: "Product"
                }
            ],
        status:
            [
                {
                    value: 1,
                    name: "Active"
                },
                {
                    value: 0,
                    name: "InActive"
                }
            ],
        ContactTypes:
            [
                {
                    value: 1,
                    name: "Supplier"
                },
                {
                    value: 0,
                    name: "Sub-contract"
                }
            ],
        PARTTYPES:
            [
                {
                    value: 1,
                    name: "Name"
                },
                {
                    value: 0,
                    name: "Description"
                }
            ],
        ContactCategory:
            [{
                value: 2,
                name: "Sub-contract"
            },
            {
                value: 1,
                name: "Customer"
            },
            {
                value: 0,
                name: "Supplier"
            }],
        SuppType:
            [{
                value: 0,
                name: "Local"
            },
            {
                value: 1,
                name: "Import"
            }],
        ContactType:
            [{
                value: 0,
                name: "All"
            },
            {
                value: 1,
                name: "ExcessPO"
            },
            {
                value: 2,
                name: "RepeatedPO"
            },
            {
                value: 3,
                name: "QuotedPO"
            },
            {
                value: 4,
                name: "DirectPO"
            }],
        PaymentTypes:
            [{
                value: 1,
                name: "NONLC"
            },
            {
                value: 0,
                name: "LC"
            }],
        ScOrder:
            [{
                value: 1,
                name: "Domestic"
            },
            {
                value: 2,
                name: "Export"
            }],
        ScType:
            [{
                value: 1,
                name: "In House"
            },
            {
                value: 2,
                name: "Out Source"
            }],
        Mode:
            [{
                value: 2,
                name: "Sea"
            },
            {
                value: 1,
                name: "Road"
            },
            {
                value: 0,
                name: "Air"
            }],
        Carryby:
            [{
                value: 2,
                name: "Lorry"
            },
            {
                value: 1,
                name: "Truck"
            },
            {
                value: 0,
                name: "Van"
            }],
        AREThree:
            [{
                value: 1,
                name: "Without availing facility"
            },
            {
                value: 0,
                name: "Availing Facility"
            }],
        YesOrNo:
            [{
                value: 0,
                name: "Yes"
            },
            {
                value: 1,
                name: "No"
            }
            ],
        FrieghtType:
            [{
                value: 0,
                name: "Freight Charge"
            },
            {
                value: 1,
                name: "Destination Charge"
            }
            ],
        BuyBackTypes:
            [{
                value: 0,
                name: "Disposal"
            },
            {
                value: 1,
                name: "BuyBack"
            },
            {
                value: 2,
                name: "Others"
            }
            ],
        PurchaseCategory:
            [{
                value: 0,
                name: "Capital"
            },
            {
                value: 1,
                name: "Expense"
            },
            {
                value: 2,
                name: "Stores"
            }
            ],
        IndentStatus:
            [{
                value: 0,
                name: "All"
            },
            {
                value: 1,
                name: "Approved"
            },
            {
                value: 2,
                name: "Rejected"
            },
            {
                value: 3,
                name: "Discussion"
            },
            {
                value: 4,
                name: "Pending"
            }
            ],
        IndentStatus2:
            [
                {
                    value: 0,
                    name: "Pending"
                },
                {
                    value: 1,
                    name: "Approved"
                },
                {
                    value: 2,
                    name: "Rejected"
                },
            ],
        IndentStatus3:
            [
                {
                    value: 4,
                    name: "All"
                },
                {
                    value: 2,
                    name: "Authorized"
                },
                {
                    value: 0,
                    name: "Pending"
                }
            ],
        IndentTypes:
            [{
                value: 0,
                name: "Local"
            },
            {
                value: 1,
                name: "Import"
            }
            ],
        Types_GRN:
            [{
                value: 0,
                name: "Local"
            },
            {
                value: 1,
                name: "Import"
            },
            {
                value: 1,
                name: "Manual"
            }
            ],
        WorkTypes:
            [{
                value: 0,
                name: "Internal"
            },
            {
                value: 1,
                name: "External"
            }
            ],
        FREIGHT:
            [{
                value: 0,
                name: "To Pay"
            },
            {
                value: 1,
                name: "Paid"
            },
            {
                value: 2,
                name: "Nil"
            }
            ],
        DISPATCHMODE:
            [{
                value: 0,
                name: "Courier"
            },
            {
                value: 1,
                name: "Ex-works(Courier)"
            },
            {
                value: 2,
                name: "others"
            }
            ],
        STOCK:
            [{
                value: 0,
                name: "In"
            },
            {
                value: 1,
                name: "Out"
            }
            ],
        POTypes:
            [{
                value: 0,
                name: "DC"
            },
            {
                value: 1,
                name: "PO"
            }
            ],
        DCType:
            [{
                value: 0,
                name: "Returnable"
            },
            {
                value: 1,
                name: "Non-Returnable"
            }
            ],
        POSTATUS:
            [
                {
                    value: 2,
                    name: "Approve"
                },
                {
                    value: 1,
                    name: "Reject"
                },
                {
                    value: 3,
                    name: "Discuss"
                },
            ],

        TOOLS:
        {
            "COREBOX": [
                {
                    value: 0,
                    name: "No"
                },
                {
                    value: 1,
                    name: "One"
                },
                {
                    value: 2,
                    name: "Two"
                },
                {
                    value: 3,
                    name: "Three"
                }
            ],
            "MATCHPLATES": [
                {
                    value: 0,
                    name: "Wood"
                },
                {
                    value: 1,
                    name: "Caste Iron"
                },
                {
                    value: 2,
                    name: "Aluminium"
                }
            ]
        },
        "RECPEND": [
            {
                value: 0,
                name: "Received"
            },
            {
                value: 1,
                name: "Pending"
            },
        ]
    },

    API_URL: {
        LOGIN:
        {
            CHECK_LOGIN: "users/login"
        },
        COMPANY_DETAILS:
        {
            GET: "companydetails/"
        },
        FORGETPASSWORD:
        {
            FORGET: "users/forgetpassword"
        },
        LEFTMENU:
        {
            MENU: "pages/leftmenu",
            ROLE: "rolepageassociation/",
            CREATE: "rolepageassociation/create",
            LEFTMENUPAGES: "rolepageassociation/pages/",
        },
        CONTACT:
        {
            GET: "suppliers",
            GETBYCAT: "suppliers/category/",
            GETBYID: "suppliers/",
            CREATE: "suppliers/create",
            UPDATE: "suppliers/update",
            DELETE: "suppliers/delete"
        },
        COUNTRY:
        {
            GET: "country",
            GETBYID: "country/",
            CREATE: "country/create",
            UPDATE: "country/update",
            DELETE: "country/delete",
            GETBYSTATUS: "country/status/"
        },
        STATE:
        {
            GET: "state",
            GETBYID: "state/",
            CREATE: "state/create",
            UPDATE: "state/update",
            DELETE: "state/delete",
            GETBYSTATUS: "state/status/",
            GETBYCOUNTRY: "state/list/"
        },
        CITY:
        {
            GET: "city",
            GETBYID: "city/",
            CREATE: "city/create",
            UPDATE: "city/update",
            DELETE: "city/delete",
            GETBYSTATUS: "city/status/",
            GETBYSTATE: "city/list/"
        },
        PORT:
        {
            GET: "ports",
            CREATE: "ports/create",
            UPDATE: "ports/update",
            DELETE: "ports/delete",
            GETBYSTATUS: "ports/status/"

        },
        USERS:
        {
            GET: "users",
            USERBYID: "users/",
            CREATE: "users/create",
            UPDATE: "users/update",
            DELETE: "users/delete",
            CHANGEPWD: 'users/changepassword',
            UPDATEUSER: 'users/userupdate',
        },
        ROLES:
        {
            GET: "roles",
            CREATE: "roles/create",
            UPDATE: "roles/update",
            DELETE: "roles/delete",
            GETBYSTATUS: "roles/status/"
        },
        DEPARTMENTS:
        {
            GET: "departments",
            CREATE: "departments/create",
            UPDATE: "departments/update",
            DELETE: "departments/delete",
            GETBYSTATUS: "departments/status/"
        },
        UOM:
        {
            GET: "uoms",
            CREATE: "uoms/create",
            UPDATE: "uoms/update",
            DELETE: "uoms/delete",
            GETBYSTATUS: "uoms/status/"
        },
        BIN_LOCATION:
        {
            GET: "locations",
            CREATE: "locations/create",
            UPDATE: "locations/update",
            DELETE: "locations/delete",
            GETBYSTATUS: "locations/status/"
        },
        RACKS:
        {
            GET: "racks",
            CREATE: "racks/create",
            UPDATE: "racks/update",
            DELETE: "racks/delete",
            GETBYSTATUS: "racks/status/",
            GETBYLOC: "locations/rack/"
        },
        BINS:
        {
            GET: "bins",
            CREATE: "bins/create",
            UPDATE: "bins/update",
            DELETE: "bins/delete",
            GETBYSTATUS: "bins/status/"
        },
        UNITS:
        {
            GET: "units",
            CREATE: "units/create",
            UPDATE: "units/update",
            DELETE: "units/delete",
            GETBYSTATUS: "units/status/"
        },
        VALVE:
        {
            GET: "valvesizes",
            CREATE: "valvesizes/create",
            UPDATE: "valvesizes/update",
            DELETE: "valvesizes/delete",
            GETBYSTATUS: "valvesizes/status/"
        },
        STAGE:
        {
            GET: "itemstages",
            CREATE: "itemstages/create",
            UPDATE: "itemstages/update",
            DELETE: "itemstages/delete",
            GETBYSTATUS: "itemstages/status/"
        },
        TYPES:
        {
            GET: "itemtypes",
            CREATE: "itemtypes/create",
            UPDATE: "itemtypes/update",
            DELETE: "itemtypes/delete",
            GETBYSTATUS: "itemtypes/status/"
        },
        SERIES:
        {
            GET: "itemseries",
            GETBYID: "itemseries/itemseries",
            CREATE: "itemseries/create",
            UPDATE: "itemseries/update",
            DELETE: "itemseries/delete",
            GETBYSTATUS: "itemseries/status/"
        },
        ITEMCLASS:
        {
            GET: "itemclass",
            GETBYID: "itemclass/",
            CREATE: "itemclass/create",
            UPDATE: "itemclass/update",
            DELETE: "itemclass/delete",
            GETBYSTATUS: "itemclass/status/"
        },
        ITEM:
        {
            GET: "items",
            GETBYID: "items/",
            GETPARTS: "items/parts",
            CREATE: "items/create",
            CREATEMULTI: "items/createmultiple",
            UPDATE: "items/update",
            DELETE: "items/delete",
            VIEW: "itemview/",
            GETBYSTATUS: "items/status/",

            GETITEMSUPPLIER: "suppliers/mappingitems/",
            CREATESUPPLIERMAPPING: "suppliers/createmapping",
            DELETESUPPLIERMAPPING: "suppliers/deletemapping",
            GETSELLINGCOST: "sellingcost/",
            CREATESELLINGCOST: "sellingcost/create",
            GETPRODUCTVALUE: "items/productcost/",

            CANCELSTAGE: "items/resetfinalstage",
            UPDATESTAGE: "items/updatestages"
        },
        HSN:
        {
            GET: "hsn",
            GETBYID: "hsn/",
            CREATE: "hsn/create",
            UPDATE: "hsn/update",
            DELETE: "hsn/delete",
            GETBYSTATUS: "hsn/status/"
        },
        TOOL:
        {
            GET: "tools",
            GETBYID: "tools/",
            CREATE: "tools/create",
            UPDATE: "tools/update",
            DELETE: "tools/delete",
            GETBYSTATUS: "tools/status/",
        },
        DCTOOLS:
        {
            CREATEISSUE: "tools/dctools",
            GET: "tools/dctoolslist",
            GETBYID: "tools/"
        },
        MATERIAL:
        {
            GET: "itemmaterial",
            GETBYID: "itemmaterial/",
            CREATE: "itemmaterial/create",
            UPDATE: "itemmaterial/update",
            DELETE: "itemmaterial/delete",
            GETBYSTATUS: "itemmaterial/status/"
        },
        PROCESS:
        {
            GET: "itemprocess",
            GETBYID: "itemprocess/",
            CREATE: "itemprocess/create",
            UPDATE: "itemprocess/update",
            DELETE: "itemprocess/delete",
            GETBYSTATUS: "itemprocess/status/"
        },
        PAYMENTTERMS:
        {
            GET: "paymentterms",
            CREATE: "paymentterms/create",
            UPDATE: "paymentterms/update",
            DELETE: "paymentterms/delete",
        },
        DELIVERYTERMS:
        {
            GET: "deliveryterms",
            CREATE: "deliveryterms/create",
            UPDATE: "deliveryterms/update",
            DELETE: "deliveryterms/delete",
        },
        CURRENCY:
        {
            GET: "currency",
        },
        SALESORDER:
        {
            GET: "salesorder/all/",
            GETSAVED: "salesorder/saved/",
            GETBYID: "salesorder/",
            GETBYIDS: "salesorder/getbyids",
            CREATE: "salesorder/create",
            UPDATE: "salesorder/update",

            GETPDCONFIRM: "salesorder/forpdconfirm/",
            PROCONFIRM: "salesorder/updatepcd",

            GETAUTHLIST: "salesorder/forapproval/",
            AUTHORIZE: "salesorder/forauthorize",

            GETSALESCONTRACTIST: "salesorder/authorized/",
        },
        STOCKORDER:
        {
            GET: "salesorder/stock/",
            GETBYID: "salesorder/sales/",
            CREATE: "salesorder/createsales"
        },
        STOCKENTRY:
        {
            CREATE: "productionentry/create",
            GETSTOCKBYID: "productionentry/getentry/",
            GETPARTSTOCK: "productionentry/stock/",
            GETPRODUCTIONLISTBYSC: "productionentry/details/"
        },
        INVENTORY:
        {
            GET: "stockquantity/",
            CREATE: "stockquantity/create",
            IMPORTFILE: "stockquantity/importexcel/"
        },
        PACKINGSLIP:
        {
            GET: "packingslip/list/",
            GETBYID: "packingslip/",
            GETBYCUSTOMERID: "packingslip/getbycustomer/",
            GETBYORDERID: "packingslip/getbyorderid/",
            CREATE: "packingslip/create",
            DETAIL: "packingslip/pslipdetails",
            SLIPLOCATION: "packingslip/location/",
            GETCONSUMABLES: "items/consumables"
        },
        PROFORMAINVOICE:
        {
            GET: "proforma/list/",
            GETBYID: "proforma/",
            CREATE: "proforma/create",
            DETAIL: "proforma/detlist/"
        },
        POSTINVOICE: {
            GET: "postinvoice/list/",
            GETBYID: "postinvoice/",
            GETEXPORTPACKINGLISTBYID: "postinvoice/exportpackinglist/",
            GETEXPORTINVBYID: "postinvoice/exportinvoice/",
            CREATE: "postinvoice/create",
            DETAIL: "postinvoice/detlist/",
            SLIPADDRESSVIEW: "postinvoice/ship/",

            SALESDCLIST: "postinvoice/salesdc/",
            SUBMITSALESDC: "postinvoice/salesdc/"
        },
        ARE: {
            GET: "are",
            VIEWAREBYINVOICE: "postinvoice/are/",
            CREATE: "are/create",
            UPDATE: "are/update"
        },

        //Planning

        PLAN: {
            GET: "plan/pplanlist",
            GETPLAN: "plan/planallocation",

            CREATEPLANALLOC: "plan/groupbomsave",
            ALLOCATEDLIST: "plan/allocatedlist",
            GROUPBOMLIST: "plan/groupbomlist",
            UPDATEBOM: "plan/groupbomupdate",
            GETPOCANCELLIST: "salesorder/plan/",
            GETSCLIST: "plan/forallocationcancel/",
            GETPLANSALESLIST: "plan/saleslist/",
            GETCANCELLIST: "plan/cancel/",
            CANCELPLANITEM: "plan/cancellist",
            GETPLANITEMBOM: "plan/completedlist/",
            GETITEMBOM: "plan/completed/",

            CANCELPLAN: "plan/cancel"
        },

        DC:
        {
            "CREATE": "dc/create",
            "GET": "dc/list/",
            "GETDCLOCATION": "dc/location/",
            "GETBYID": "dc/",
            "DC_SUPPLIERS": "dc/supplier/",
            "GETLISTBYID": "dc/getbysupplier/",
            "CREATEDC": "dc/createdc",
            "CREATEJCDC": "jobcart/tempdc",
            "GETSTORESJOBCARTITEMS": "jobcart/item/",
            "GETSTORESJOBCARTITEMSLIST": "jobcart/itemlist/",
            "GETSTORESJOBCARTSUBITEMS": "jobcart/subitem/",
            "GETSTORESJOBCARTSUBITEMSLIST": "jobcart/bomlist/",
            "GETREWRKJOBCARTITEMS": "jobcart/stock/",
            "GETSTORESJOBCARTLIST": "jobcart/stores/",
            "CREATESAJOBCART": "jobcart/manual"
        },

        //Purchase

        PRICE_BASIS: {
            GET: "pricebasis",
            CREATE: "pricebasis/create",
            UPDATE: "pricebasis/update",
            DELETE: "pricebasis/delete",
            GETBYSTATUS: "pricebasis/status/",
        },
        COST_CENTER: {
            GET: "costcenter",
            CREATE: "costcenter/create",
            UPDATE: "costcenter/update",
            DELETE: "costcenter/delete",
            GETBYSTATUS: "costcenter/status/",
        },
        FREIGHT_CHARGE: {
            GET: "freightcharge",
            CREATE: "freightcharge/create",
            UPDATE: "freightcharge/update",
            DELETE: "freightcharge/delete",
            GETBYSTATUS: "freightcharge/status/",
        },
        CF_CHARGE: {
            GET: "cfcharge",
            CREATE: "cfcharge/create",
            UPDATE: "cfcharge/update",
            DELETE: "cfcharge/delete",
            GETBYSTATUS: "cfcharge/status/",
        },
        TARIFF: {
            GET: "tariff",
            CREATE: "tariff/create",
            UPDATE: "tariff/update",
            DELETE: "tariff/delete",
            GETBYSTATUS: "tariff/status/",
        },
        PURCHASEINDENT: {
            GET: "purchaseindent/list/",
            GETBYID: "purchaseindent/",
            GETAUTHLIST: "purchaseindent/forauthorize/",
            GETPREVDETAILS: "purchaseindent/po/",
            CREATE: "purchaseindent/create",
            GETFILES: "purchaseindent/forattachment/",
            GETAPPROVED: "purchaseindent/forcancelindent/",
            GETCANCELED: "purchaseindent/forcancelauthorize/",
            GETPURCHASEFILTER: "purchaseindent/purchasefilter",

            UPDATEAUTH: "purchaseindent/updateauthorize",
            UPDATECANCELINDENT: "purchaseindent/updatecancelindent",
            UPDATECANCELAUTH: "purchaseindent/updatecancelauthorize",

            GETPENDINGINDENTS: "purchaseindent/pendingindent/",
            CREATEENQUERY: "enquiry/createenquiry",
            PURCHASE: "purchaseindent/createauto",
            GETITEMSUPPLIERS: "purchaseindent/supplier/"
        },
        QUOTATION: {
            GETQUOTATIONBYID: "enquiry/",
            GETQUOTATIONBYSUPPLIER: "enquiry/list/",
            CREATE: "quotation/create",
            UPDATE: "quotation/update",
            GETCOMPARELIST: "quotation/compare/"
        },
        PURCHASEORDER: {
            GET: "purchaseorder/list/",
            GETBYID: "purchaseorder/",
            GETCONFIRMEDBYID: "purchaseorder/view/",
            CREATE: "purchaseorder/create",
            UPDATE: "purchaseorder/update",
            REPEATPO: "purchaseorder/repeatpo",
            GETPOAUTH: "purchaseorder/pofilter",
            CANCELPO: "purchaseorder/cancel/",
            CANCELPOQTN: "purchaseorder/quotationcancel/",
            GETMDPOAPPROVAL: "purchaseorder/poapproval/",
            MDAPPROVE: "purchaseorder/updatemd",
            GETORDERCONFIRM: "purchaseorder/poorderfilter",
            CONFIRMORDER: "purchaseorder/confirmorder",
            GETCOMPARE: "purchaseorder/compare/",

            DIRECTPO: "purchaseorder/directpo",
            GETAMENDMENTPOLIST: "purchaseorder/amend/",
            UPDATEPOAMEND: "purchaseorder/update/poamend",
            POSHOTCLOSURE: "purchaseorder/shot"
        },
        GRN:
        {
            GETPOLIST: "grn/list/",
            GETPOBYID: "grn/list/",
            GETPODETAILSBYID: "grn/entry",
            CREATEGRN: "grn/create",
            GETAUTHLIST: "grn/forauthorize/",
            GRNAUTHORIZE: "grn/update",
            GRNLOCATIONLIST: "grn/location",
            GETDCDETAILSBYID: "grn/dc",
            CREATEREQENTRY: "entry/create",
            ENTRYAUTHLIST: "entry/authorize",
            ENTRYAUTHORIZE: "entry/update",
            GETLIST: "grn/list/",
            GETBYID: "grn/",

            GETSTOCKDISPOSAL: "stockdisposal/list/",
            REJINVOICELIST: "rejection/list/",
            CREATEGRNLOC: "grn/locationupdate",
            PRODUCTIONLOCLIST: "grn/prodstock",
            PRODUCTIONLOCSAVE: "grn/prodlocation",
            CREATEREJECTTODC: "rejection/createtempdc"
        },
        ISSUE:
        {
            GET: "entry/issue",
            JOBCARTGETALL: "jobcart/list/",
            JOBCARTREQUEST: "jobcart/request",
            ISSUEENTRY: "entry/issueenter",
            GETISSUERETURN: "entry/issuereturn",
            GETISSUELIST: "entry/list",
            GETISSUELOCATION: "entry/location/"
        },
        ASSEMBLY:
        {
            GETWO: "assembly/sales/",
            GETBYWO: "assembly/list/",
            CREATE: "assembly/create",
            GETREQLIST: "assembly/stock"
        },
        REPORTS: {
            LEDGER: {
                GET: "reports/stockledger"
            },
            DCREPORT: {
                GET: "dc/dcreport"
            },
            POREPORT: "purchaseorder/reports",
            SALESREPORT: "salesorder/reports",
            REJECTLIST: "rejection/reports",
            DISPOSALREPORT: "stockdisposal/reports",
            TOOLSREPORT: "tools/dcreports",
            ITEMBINLOCATION: "stockquantity/binreports/",
            ITEMLOCATIONS: "stockquantity/itemreports/",
            GETSTOCKSERIES: "reports/stockinward",
            GETCLASSBYSERIES: "reports/stockclass",
            GETITEMBYSERIESCLASS: "reports/stockitem",
            GETITEMSTOCKRECEIPTS: "reports/stockreceipts",
            GETITEMSTOCKISSUES: "reports/stockissue",
            GETGRNREPORT: "reports/grn",
            GETINVOICEREPORT: "reports/invoice"
        }
    }
});
