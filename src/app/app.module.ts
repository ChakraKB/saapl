import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { JwttokenInterceptorService } from './services/jwttoken-interceptor.service'
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DataTablesModule } from 'angular-datatables';
import { routing } from './app.routing';
import { RouterModule } from '@angular/router';
import * as jQuery from 'jquery';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { LeftmenuComponent } from './common/leftmenu/leftmenu.component';
import { FooterComponent } from './common/footer/footer.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogincreationComponent } from './admin/logincreation/logincreation.component';
import { ApprovesettingsComponent } from './admin/approvesettings/approvesettings.component';
import { RolesComponent } from './admin/roles/roles.component';
import { RolespageassociationComponent } from './admin/rolespageassociation/rolespageassociation.component';
import { SupplierComponent } from './master/supplier/supplier.component';
import { SupplierapprovalComponent } from './master/supplierapproval/supplierapproval.component';
import { DepartmentsComponent } from './master/departments/departments.component';
import { UomComponent } from './master/uom/uom.component';
import { BinlocationComponent } from './master/binlocation/binlocation.component';
import { ItemComponent } from './master/item/item.component';
import { SalesorderComponent } from './sales/salesorder/salesorder.component';
import { SalesauthorizationComponent } from './sales/salesauthorization/salesauthorization.component';
import { OrderconfirmationComponent } from './sales/orderconfirmation/orderconfirmation.component';
import { SalescontractComponent } from './sales/salescontract/salescontract.component';
import { UserService } from './services/user/user.service';
import { CommonHttpService } from './common-http.service';
import { MasterService } from './services/master.service'
import { SimpleNotificationsModule } from 'angular2-notifications';
import { DataTableModule } from "angular2-datatable";
import { DatafilterPipe } from './services/datafilter.pipe';
import { LoginAuthGuard } from './gaurds/login-auth.guard';
import { NotifyService } from './services/notify.service'
import { SelectModule } from 'angular2-select';
import { LogoutAuthGuard } from './gaurds/logout-auth.guard'
import { TreeviewModule } from 'ngx-treeview';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment';
import { RackComponent } from './master/rack/rack.component';
import { BinComponent } from './master/bin/bin.component';
import { BinService } from './services/bin/bin.service';
import { LocationService } from './services/location/location.service';
import { RackService } from './services/Rack/rack.service';
import { RolesService } from './services/Roles/roles.service';
import { DepartmentsService } from './services/department/departments.service'
import { from } from 'rxjs/observable/from';
import { UomService } from "./services/uom/uom.service";
import { UnitService } from "./services/unit/unit.service";
import { LoginService } from './services/login/login.service'
import { ValveService } from './services/valve/valve.service';
import { StageService } from './services/stage/stage.service';
import { TypeService } from './services/types/type.service';
import { ItemsService } from './services/items/items.service';
import { UnitComponent } from './master/unit/unit.component';
import { ValveComponent } from './master/valve/valve.component';
import { StageComponent } from './master/stage/stage.component';
import { TypesComponent } from './master/types/types.component';
import { AutoCompleteModule } from 'primeng/primeng';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { Ng2CompleterModule } from "ng2-completer";
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ProductconfirmationComponent } from './sales/productconfirmation/productconfirmation.component';
import { SalesorderService } from './services/salesorder/salesorder.service';
import { ScDetailPopupComponent } from './sales/sc-detail-popup/sc-detail-popup.component';
import { SavedSalesordersComponent } from './sales/saved-salesorders/saved-salesorders.component';
import { AddEditSalesorderComponent } from './sales/add-edit-salesorder/add-edit-salesorder.component';
import { ProformainvoiceComponent } from './Logistics/proformainvoice/proformainvoice.component';
import { StockEntryComponent } from './Production/stock-entry/stock-entry.component';
import { KeyFilterModule } from 'primeng/keyfilter';
import { SlipComponent } from './Production/slip/slip.component';
import { SlipPopupComponent } from './Logistics/slip-popup/slip-popup.component';
import { TreeTableModule } from 'primeng/treetable';
import { StoresService } from './services/stores/stores.service';
import { ItemViewComponent } from './master/item/item-view/item-view.component';
import { MaterialComponent } from './master/material/material.component';
import { ProcessComponent } from './master/process/process.component';
import { ViewProformaComponent } from './Logistics/proformainvoice/view-proforma/view-proforma.component';
import { PostInvoiceComponent } from './Logistics/post-invoice/post-invoice.component';
import { NavmenuComponent } from './common/navmenu/navmenu.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ErrorComponent } from './common/error/error.component';
// import {TreeModule } from 'primeng/tree';
import { TreeModule } from 'angular-tree-component';
import { ViewSavedScComponent } from './sales/saved-salesorders/view-saved-sc/view-saved-sc.component';
import { ViewScauthComponent } from './sales/salesauthorization/view-scauth/view-scauth.component';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { PortComponent } from './master/port/port.component';
import { PaymenttermComponent } from './master/paymentterm/paymentterm.component';
import { DeliverytermComponent } from './master/deliveryterm/deliveryterm.component';
import { ProformalistComponent } from './Logistics/proformainvoice/proformalist/proformalist.component';
import { AddSupplierComponent } from './master/supplier/add-supplier/add-supplier.component';
import { LoginPopComponent } from './login/login-pop/login-pop.component';
import { PostinvoiceListComponent } from './Logistics/post-invoice/postinvoice-list/postinvoice-list.component';
import { SlipListComponent } from './Production/slip/slip-list/slip-list.component';
import { ViewPackinglistComponent } from './Logistics/proformainvoice/view-packinglist/view-packinglist.component';
import { InvoiceService } from './services/invoice/invoice.service';
import { AreFormComponent } from './Logistics/are-form/are-form.component';
import { HsnMasterComponent } from './master/hsn-master/hsn-master.component';
import { AddMultipleitemComponent } from './master/item/add-multipleitem/add-multipleitem.component';
import { CountryComponent } from './master/country/country.component';
import { StateComponent } from './master/state/state.component';
import { CityComponent } from './master/city/city.component';
import { HsnService } from './services/hsn/hsn.service'
import { CountryService } from './services/country/country.service';
import { ViewAreformComponent } from './Logistics/are-form/view-areform/view-areform.component';
import { ProfileComponent } from './common/profile/profile.component';
import { PriceBasisComponent } from './Purchase/Masters/price-basis/price-basis.component';
import { CostCenterComponent } from './Purchase/Masters/cost-center/cost-center.component';
import { FrieghtChargesComponent } from './Purchase/Masters/frieght-charges/frieght-charges.component';
import { CandfchargesComponent } from './Purchase/Masters/candfcharges/candfcharges.component';
import { TariffComponent } from './Purchase/Masters/tariff/tariff.component';
import { PurchaseMastersService } from './services/Purchase/purchase-masters.service';
import { PurchaseIndentComponent } from './Purchase/Purchase_Indent/purchase-indent/purchase-indent.component';
import { PlanningComponent } from './planning/planning/planning.component';
import { PlanningService } from './services/Planning/planning.service';
import { PendingPlanlistComponent } from './planning/pending-planlist/pending-planlist.component';
import { PurchaseTransactionService } from './services/Purchase/transactions/purchase-transaction.service';
import { IndentListComponent } from './Purchase/Purchase_Indent/indent-list/indent-list.component';
import { IndentAuthorizeComponent } from './Purchase/indent-authorize/indent-authorize.component';
import { CancelIndentComponent } from './Purchase/cancel-indent/cancel-indent.component';
import { CancelindentAuthorizeComponent } from './Purchase/cancelindent-authorize/cancelindent-authorize.component';
import { PendingIndentComponent } from './Purchase/pending-indent/pending-indent.component';
import { ViewIndentComponent } from './Purchase/Purchase_Indent/view-indent/view-indent.component';
import { CompletedPlanComponent } from './planning/completed-plan/completed-plan.component';
import { QuotationEntryComponent } from './Purchase/quotation-entry/quotation-entry.component';
import { QuotationService } from './services/Purchase/quotation/quotation.service';
import { ComparissionComponent } from './Purchase/comparission/comparission.component';
import { AllocpopComponent } from './planning/allocpop/allocpop.component';
import { InventoryComponent } from './Production/inventory/inventory.component';
import { TooltipModule } from "ngx-tooltip";
import { PoSearchComponent } from './Purchase_order/po-search/po-search.component';
import { PoEditComponent } from './Purchase_order/po-edit/po-edit.component';
import { PurchaseorderService } from './services/purchase_order/purchaseorder.service';
import { PoViewComponent } from './Purchase_order/po-view/po-view.component';
import { StockOrderComponent } from './Production/stock-order/create-stockorder.component';
import { StockorderListComponent } from './Production/stockorder-list/stockorder-list.component';
import { PoAuthorizationComponent } from './Purchase_order/po-authorization/po-authorization.component';
import { CancelScComponent } from './sales/cancel-sc/cancel-sc.component';
import { CancelConfirmComponent } from './sales/cancel-confirm/cancel-confirm.component';
import { AllocationCancelComponent } from './planning/allocation-cancel/allocation-cancel.component';
import { MdApprovalComponent } from './Purchase_order/md-approval/md-approval.component';
import { PoConfirmationComponent } from './Purchase_order/po-confirmation/po-confirmation.component';
import { ComparisionViewComponent } from './Purchase_order/comparision-view/comparision-view.component';
import { GrnComponent } from './grn/grn.component';
import { GrnService } from './services/grn/grn.service';
import { GrnAuthorizationComponent } from './grn/grn-authorization/grn-authorization.component';
import { GrnLocationComponent } from './grn/grn-location/grn-location.component';
import { DcService } from './services/dc/dc.service';
import { DeliveryChallanComponent } from './stores/delivery-challan/delivery-challan.component';
import { ViewDcComponent } from './stores/view-dc/view-dc.component';
import { CreateDcComponent } from './stores/create-dc/create-dc.component';
import { ItemSupplierMapComponent } from './master/item-supplier-map/item-supplier-map.component';
import { ReqEntryComponent } from './stores/req-entry/req-entry.component';
import { ReqEntryAuthorizationComponent } from './stores/req-entry-authorization/req-entry-authorization.component';
import { IssueComponent } from './stores/issue/issue.component';
import { IssueReturnComponent } from './stores/issue-return/issue-return.component';
import { JobCartComponent } from './Production/job-cart/job-cart.component';
import { ToolIssueComponent } from './stores/tool-issue/tool-issue.component';
import { StockDisposalComponent } from './stores/stock-disposal/stock-disposal.component';
import { ToolsComponent } from './master/tools/tools.component';
import { ToolsService } from './services/tools/tools.service';
import { DisposalListComponent } from './stores/stock-disposal/disposal-list/disposal-list.component';
import { RejInvoiceComponent } from './stores/rej-invoice/rej-invoice.component';
import { SellingCostComponent } from './master/selling-cost/selling-cost.component';
import { ToolissueListComponent } from './stores/tool-issue/toolissue-list/toolissue-list.component';
import { PoAmendmentComponent } from './Purchase/po-amendment/po-amendment.component';
import { AssemblySearchComponent } from './Assembly/assembly-search/assembly-search.component';
import { AssemblyService } from './services/assembly/assembly.service';
import { GrnListComponent } from './grn/grn-list/grn-list.component';
import { ProductValueComponent } from './master/product-value/product-value.component';
import { StockReportComponent } from './Reports/stock-report/stock-report.component';
import { ReportsService } from './services/Reports/reports.service';
import { StockLedgerComponent } from './Reports/stock-ledger/stock-ledger.component';
import { DcJobcardComponent } from './stores/dc-jobcard/dc-jobcard.component';
import { ManualJobcardComponent } from './stores/manual-jobcard/manual-jobcard.component';
import { IssueListComponent } from './stores//issue-list/issue-list.component';
import { ViewdcLocationComponent } from './stores/delivery-challan/viewdc-location/viewdc-location.component';
import { AssemblyListComponent } from './Assembly/assembly-list/assembly-list.component';
import { DcReportComponent } from './Reports/dc-report/dc-report.component';
import { PoReportComponent } from './Reports/po-report/po-report.component';
import { SalesReportComponent } from './Reports/sales-report/sales-report.component';
import { RejectReportComponent } from './Reports/reject-report/reject-report.component';
import { DisposalReportComponent } from './Reports/disposal-report/disposal-report.component';
import { PendingdcReportComponent } from './Reports/pendingdc-report/pendingdc-report.component';
import { ToolReportComponent } from './Reports/tool-report/tool-report.component';
import { ItemlocationReportComponent } from './Reports/itemlocation-report/itemlocation-report.component';
import { ItembinReportComponent } from './Reports/itembin-report/itembin-report.component';
import { ViewGrnComponent } from './grn/view-grn/view-grn.component';
import { TitleService } from './services/title.service';
import { ReworkjobcartComponent } from './stores/reworkjobcart/reworkjobcart.component';
import { SlipViewComponent } from './Logistics/slip-view/slip-view.component';
import { PoValueReportComponent } from './Reports/po-value-report/po-value-report.component';
import { CompanyTdComponent } from './common/company-td/company-td.component';
import { SalesDcComponent } from './stores/sales-dc/sales-dc.component';
import { SeriesComponent } from './master/series/series.component';
import { ItemClassComponent } from './master/item-class/item-class.component';
import { StockSeriesComponent } from './Reports/stock-series/stock-series.component';
import { StockClassComponent } from './Reports/stock-series/stock-class/stock-class.component';
import { StockItemsComponent } from './Reports/stock-series/stock-items/stock-items.component';
import { PlanItemCancelComponent } from './planning/plan-item-cancel/plan-item-cancel.component';
import { PlanCancelComponent } from './planning/plan-cancel/plan-cancel.component';
import { ItemStageMapComponent } from './master/item/item-stage-map/item-stage-map.component';
import { GrnReportComponent } from './Reports/grn-report/grn-report.component';
import { InvoiceReportComponent } from './Reports/invoice-report/invoice-report.component'
import { AddMultipleStageComponent } from './master/item/add-multiple-stage/add-multiple-stage.component';
import { ManualSajobcartComponent } from './stores/manual-jobcard/manual-sajobcart/manual-sajobcart.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftmenuComponent,
    FooterComponent,
    LoginComponent,
    DashboardComponent,
    LogincreationComponent,
    ApprovesettingsComponent,
    RolesComponent,
    RolespageassociationComponent,
    SupplierComponent,
    SupplierapprovalComponent,
    DepartmentsComponent,
    UomComponent,
    BinlocationComponent,
    ItemComponent,
    AddMultipleStageComponent,
    SalesorderComponent,
    SalesauthorizationComponent,
    OrderconfirmationComponent,
    SalescontractComponent,
    DatafilterPipe,
    RackComponent,
    BinComponent,
    UnitComponent,
    ValveComponent,
    StageComponent,
    TypesComponent,
    ProductconfirmationComponent,
    ScDetailPopupComponent,
    SavedSalesordersComponent,
    AddEditSalesorderComponent,
    ProformainvoiceComponent,
    StockEntryComponent,
    SlipComponent,
    SlipPopupComponent,
    ItemViewComponent,
    MaterialComponent,
    ProcessComponent,
    ViewProformaComponent,
    PostInvoiceComponent,
    NavmenuComponent,
    ErrorComponent,
    ViewSavedScComponent,
    ViewScauthComponent,
    PortComponent,
    PaymenttermComponent,
    DeliverytermComponent,
    ProformalistComponent,
    AddSupplierComponent,
    LoginPopComponent,
    PostinvoiceListComponent,
    SlipListComponent,
    ViewPackinglistComponent,
    AreFormComponent,
    HsnMasterComponent,
    AddMultipleitemComponent,
    CountryComponent,
    StateComponent,
    CityComponent,
    ViewAreformComponent,
    ProfileComponent,
    PriceBasisComponent,
    CostCenterComponent,
    FrieghtChargesComponent,
    CandfchargesComponent,
    TariffComponent,
    PurchaseIndentComponent,
    PlanningComponent,
    PendingPlanlistComponent,
    IndentListComponent,
    IndentAuthorizeComponent,
    CancelIndentComponent,
    CancelindentAuthorizeComponent,
    PendingIndentComponent,
    ViewIndentComponent,
    CompletedPlanComponent,
    QuotationEntryComponent,
    ComparissionComponent,
    AllocpopComponent,
    InventoryComponent,
    PoSearchComponent,
    PoEditComponent,
    PoViewComponent,
    StockOrderComponent,
    StockorderListComponent,
    PoAuthorizationComponent,
    CancelScComponent,
    CancelConfirmComponent,
    AllocationCancelComponent,
    MdApprovalComponent,
    PoConfirmationComponent,
    ComparisionViewComponent,
    GrnComponent,
    GrnAuthorizationComponent,
    GrnLocationComponent,
    DeliveryChallanComponent,
    ViewDcComponent,
    CreateDcComponent,
    ItemSupplierMapComponent,
    ReqEntryComponent,
    ReqEntryAuthorizationComponent,
    IssueComponent,
    IssueReturnComponent,
    JobCartComponent,
    ToolIssueComponent,
    StockDisposalComponent,
    ToolsComponent,
    DisposalListComponent,
    RejInvoiceComponent,
    SellingCostComponent,
    ToolissueListComponent,
    PoAmendmentComponent,
    AssemblySearchComponent,
    GrnListComponent,
    ProductValueComponent,
    StockReportComponent,
    StockLedgerComponent,
    DcJobcardComponent,
    ManualJobcardComponent,
    IssueListComponent,
    ViewdcLocationComponent,
    AssemblyListComponent,
    DcReportComponent,
    PoReportComponent,
    SalesReportComponent,
    RejectReportComponent,
    DisposalReportComponent,
    PendingdcReportComponent,
    ToolReportComponent,
    ItemlocationReportComponent,
    ItembinReportComponent,
    ViewGrnComponent,
    ReworkjobcartComponent,
    SlipViewComponent,
    PoValueReportComponent,
    CompanyTdComponent,
    SalesDcComponent,
    SeriesComponent,
    ItemClassComponent,
    StockSeriesComponent,
    StockClassComponent,
    StockItemsComponent,
    PlanItemCancelComponent,
    PlanCancelComponent,
    ItemStageMapComponent,
    GrnReportComponent,
    InvoiceReportComponent,
    ManualSajobcartComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    RouterModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    DataTablesModule,
    SelectModule,
    SimpleNotificationsModule.forRoot(),
    NgxMyDatePickerModule.forRoot(),
    TreeviewModule.forRoot(),
    MomentModule,
    AutoCompleteModule,
    NgIdleKeepaliveModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    NguiAutoCompleteModule,
    Ng2CompleterModule,
    KeyFilterModule,
    TreeTableModule,
    TreeModule,
    TooltipModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwttokenInterceptorService,
      multi: true,
    },
    UserService,
    CommonHttpService,
    MasterService,
    LoginAuthGuard,
    LogoutAuthGuard,
    NotifyService,
    BinService,
    LocationService,
    RackService,
    RolesService,
    DepartmentsService,
    UomService,
    UnitService,
    LoginService,
    ValveService,
    StageService,
    TypeService,
    ToolsService,
    ItemsService,
    SalesorderService,
    StoresService,
    CookieService,
    InvoiceService,
    HsnService,
    CountryService,
    PurchaseMastersService,
    PurchaseTransactionService,
    PlanningService,
    QuotationService,
    PurchaseorderService,
    GrnService,
    DcService,
    AssemblyService,
    ReportsService,
    TitleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
