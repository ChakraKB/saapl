import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogincreationComponent } from './admin/logincreation/logincreation.component';
import { ApprovesettingsComponent } from './admin/approvesettings/approvesettings.component';
import { RolesComponent } from './admin/roles/roles.component';
import { RolespageassociationComponent } from './admin/rolespageassociation/rolespageassociation.component';
import { SupplierComponent } from './master/supplier/supplier.component';
import { SupplierapprovalComponent } from './master/supplierapproval/supplierapproval.component';
import { DepartmentsComponent } from './master/departments/departments.component';
import { UomComponent } from './master/uom/uom.component';
import { BinlocationComponent } from './master/binlocation/binlocation.component';
import { ItemComponent } from './master/item/item.component';
import { SeriesComponent } from './master/series/series.component';
import { ItemClassComponent } from './master/item-class/item-class.component';
import { SalesorderComponent } from './sales/salesorder/salesorder.component';
import { SalesauthorizationComponent } from './sales/salesauthorization/salesauthorization.component';
import { ProductconfirmationComponent } from './sales/productconfirmation/productconfirmation.component'
import { OrderconfirmationComponent } from './sales/orderconfirmation/orderconfirmation.component';
import { SalesDcComponent } from './stores/sales-dc/sales-dc.component';
import { SavedSalesordersComponent } from './sales/saved-salesorders/saved-salesorders.component';
import { RackComponent } from './master/rack/rack.component';
import { BinComponent } from './master/bin/bin.component';
import { UnitComponent } from './master/unit/unit.component';
import { ValveComponent } from './master/valve/valve.component';
import { StageComponent } from './master/stage/stage.component';
import { LoginAuthGuard } from './gaurds/login-auth.guard';
import { LogoutAuthGuard } from './gaurds/logout-auth.guard'
import { TypesComponent } from './master/types/types.component';
import { ProformainvoiceComponent } from './Logistics/proformainvoice/proformainvoice.component';
import { ViewProformaComponent } from './Logistics/proformainvoice/view-proforma/view-proforma.component'
import { StockEntryComponent } from './Production/stock-entry/stock-entry.component';
import { SlipComponent } from './Production/slip/slip.component';
import { ItemViewComponent } from './master/item/item-view/item-view.component';
import { MaterialComponent } from './master/material/material.component';
import { ProcessComponent } from './master/process/process.component'
import { PostInvoiceComponent } from './Logistics/post-invoice/post-invoice.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ErrorComponent } from './common/error/error.component';
import { ViewSavedScComponent } from './sales/saved-salesorders/view-saved-sc/view-saved-sc.component';
import { ViewScauthComponent } from './sales/salesauthorization/view-scauth/view-scauth.component';
import { PortComponent } from './master/port/port.component';
import { PaymenttermComponent } from './master/paymentterm/paymentterm.component';
import { DeliverytermComponent } from './master/deliveryterm/deliveryterm.component'
import { ProformalistComponent } from './Logistics/proformainvoice/proformalist/proformalist.component';
import { AddSupplierComponent } from './master/supplier/add-supplier/add-supplier.component';
import { PostinvoiceListComponent } from './Logistics/post-invoice/postinvoice-list/postinvoice-list.component';
import { SlipListComponent } from './Production/slip/slip-list/slip-list.component';
import { ViewPackinglistComponent } from './Logistics/proformainvoice/view-packinglist/view-packinglist.component';
import { AreFormComponent } from './Logistics/are-form/are-form.component';
import { HsnMasterComponent } from './master/hsn-master/hsn-master.component';
import { AddMultipleitemComponent } from './master/item/add-multipleitem/add-multipleitem.component';
import { CountryComponent } from './master/country/country.component';
import { StateComponent } from './master/state/state.component';
import { CityComponent } from './master/city/city.component';
import { ViewAreformComponent } from './Logistics/are-form/view-areform/view-areform.component';
import { ProfileComponent } from './common/profile/profile.component';
import { PriceBasisComponent } from './Purchase/Masters/price-basis/price-basis.component';
import { CostCenterComponent } from './Purchase/Masters/cost-center/cost-center.component';
import { FrieghtChargesComponent } from './Purchase/Masters/frieght-charges/frieght-charges.component';
import { CandfchargesComponent } from './Purchase/Masters/candfcharges/candfcharges.component';
import { TariffComponent } from './Purchase/Masters/tariff/tariff.component';
import { PurchaseIndentComponent } from './Purchase/Purchase_Indent/purchase-indent/purchase-indent.component';
import { PlanningComponent } from './planning/planning/planning.component';
import { PendingPlanlistComponent } from './planning/pending-planlist/pending-planlist.component';
import { IndentListComponent } from './Purchase/Purchase_Indent/indent-list/indent-list.component';
import { IndentAuthorizeComponent } from './Purchase/indent-authorize/indent-authorize.component';
import { CancelIndentComponent } from './Purchase/cancel-indent/cancel-indent.component';
import { CancelindentAuthorizeComponent } from './Purchase/cancelindent-authorize/cancelindent-authorize.component';
import { PendingIndentComponent } from './Purchase/pending-indent/pending-indent.component';
import { CompletedPlanComponent } from './planning/completed-plan/completed-plan.component';
import { QuotationEntryComponent } from './Purchase/quotation-entry/quotation-entry.component';
import { ComparissionComponent } from './Purchase/comparission/comparission.component';
import { InventoryComponent } from './Production/inventory/inventory.component';
import { PoSearchComponent } from './Purchase_order/po-search/po-search.component';
import { PoEditComponent } from './Purchase_order/po-edit/po-edit.component';
import { StockOrderComponent } from './Production/stock-order/create-stockorder.component';
import { StockorderListComponent } from './Production/stockorder-list/stockorder-list.component';
import { PoAuthorizationComponent } from './Purchase_order/po-authorization/po-authorization.component';
import { CancelScComponent } from './sales/cancel-sc/cancel-sc.component';
import { CancelConfirmComponent } from './sales/cancel-confirm/cancel-confirm.component';
import { AllocationCancelComponent } from './planning/allocation-cancel/allocation-cancel.component';
import { MdApprovalComponent } from './Purchase_order/md-approval/md-approval.component';
import { PoConfirmationComponent } from './Purchase_order/po-confirmation/po-confirmation.component';
import { GrnComponent } from './grn/grn.component';
import { GrnAuthorizationComponent } from './grn/grn-authorization/grn-authorization.component';
import { GrnLocationComponent } from './grn/grn-location/grn-location.component';
import { DeliveryChallanComponent } from './stores/delivery-challan/delivery-challan.component';
import { CreateDcComponent } from './stores/create-dc/create-dc.component';
import { ItemSupplierMapComponent } from './master/item-supplier-map/item-supplier-map.component';
import { ReqEntryComponent } from './stores/req-entry/req-entry.component';
import { ReqEntryAuthorizationComponent } from './stores/req-entry-authorization/req-entry-authorization.component';
import { IssueComponent } from './stores/issue/issue.component';
import { IssueReturnComponent } from './stores/issue-return/issue-return.component';
import { JobCartComponent } from './Production/job-cart/job-cart.component';
import { ToolIssueComponent } from './stores/tool-issue/tool-issue.component';
import { StockDisposalComponent } from './stores/stock-disposal/stock-disposal.component';
import { ToolsComponent } from './master/tools/tools.component';
import { DisposalListComponent } from './stores/stock-disposal/disposal-list/disposal-list.component';
import { RejInvoiceComponent } from './stores/rej-invoice/rej-invoice.component';
import { SellingCostComponent } from './master/selling-cost/selling-cost.component';
import { ToolissueListComponent } from './stores/tool-issue/toolissue-list/toolissue-list.component';
import { PoAmendmentComponent } from './Purchase/po-amendment/po-amendment.component';
import { AssemblySearchComponent } from './Assembly/assembly-search/assembly-search.component';
import { GrnListComponent } from './grn/grn-list/grn-list.component';
import { ProductValueComponent } from './master/product-value/product-value.component';
import { StockReportComponent } from './Reports/stock-report/stock-report.component';
import { StockLedgerComponent } from './Reports/stock-ledger/stock-ledger.component';
import { StockSeriesComponent } from './Reports/stock-series/stock-series.component'
import { DcJobcardComponent } from './stores/dc-jobcard/dc-jobcard.component';
import { ManualJobcardComponent } from './stores/manual-jobcard/manual-jobcard.component';
import { IssueListComponent } from './stores/issue-list/issue-list.component';
import { ViewdcLocationComponent } from './stores/delivery-challan/viewdc-location/viewdc-location.component';
import { AssemblyListComponent } from './Assembly/assembly-list/assembly-list.component';
import { DcReportComponent } from './Reports/dc-report/dc-report.component';
import { PendingdcReportComponent } from './Reports/pendingdc-report/pendingdc-report.component';
import { PoReportComponent } from './Reports/po-report/po-report.component';
import { SalesReportComponent } from './Reports/sales-report/sales-report.component';
import { RejectReportComponent } from './Reports/reject-report/reject-report.component';
import { DisposalReportComponent } from './Reports/disposal-report/disposal-report.component';
import { ToolReportComponent } from './Reports/tool-report/tool-report.component';
import { ItemlocationReportComponent } from './Reports/itemlocation-report/itemlocation-report.component';
import { ViewGrnComponent } from './grn/view-grn/view-grn.component';
import { ReworkjobcartComponent } from './stores/reworkjobcart/reworkjobcart.component';
import { SlipViewComponent } from './Logistics/slip-view/slip-view.component';
import { PoValueReportComponent } from './Reports/po-value-report/po-value-report.component'
import { PlanItemCancelComponent } from './planning/plan-item-cancel/plan-item-cancel.component';
import { PlanCancelComponent } from './planning/plan-cancel/plan-cancel.component';
import { ItemStageMapComponent } from './master/item/item-stage-map/item-stage-map.component';
import { GrnReportComponent } from './Reports/grn-report/grn-report.component';
import { InvoiceReportComponent } from './Reports/invoice-report/invoice-report.component';
import { AddMultipleStageComponent } from './master/item/add-multiple-stage/add-multiple-stage.component';
import { PoViewComponent } from './Purchase_order/po-view/po-view.component';

const appRoutes: Routes = [
  // login
  // {path: 'login', component: LoginComponent},
  { path: '', component: LoginComponent, canActivate: [LogoutAuthGuard], data: { title: "Login", } },
  { path: 'error404', component: ErrorComponent, data: { title: "No Access", } },
  // dashboard
  { path: 'dashboard', component: DashboardComponent, canActivate: [LoginAuthGuard], data: { title: "Dashboard", } },
  { path: 'profile', component: ProfileComponent, canActivate: [LoginAuthGuard], data: { title: "Profile", } },
  // admin
  {
    path: 'logincreation', component: LogincreationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Login Creation",
      permissions: {
        only: ['Login Creation'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'approvesettings', component: ApprovesettingsComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Approve Settings",
      permissions: {
        only: ['Approve Settings'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'roles', component: RolesComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Roles",
      permissions: {
        only: ['Roles'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'rolespageassociation', component: RolespageassociationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Role Page Association",
      permissions: {
        only: ['Role Page Association'],
        redirectTo: '/error404'
      }
    }
  },
  // master
  {
    path: 'supplier', component: SupplierComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Supplier/Customer",
      permissions: {
        only: ['Supplier/Customer'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'addsupplier', component: AddSupplierComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Supplier/Customer",
      permissions: {
        only: ['Supplier/Customer'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'editsupplier/:id', component: AddSupplierComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Supplier/Customer",
      permissions: {
        only: ['Supplier/Customer'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'port', component: PortComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Port",
      permissions: {
        only: ['Port'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'paymentterms', component: PaymenttermComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Payment Terms",
      permissions: {
        only: ['Payment Terms'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'deliveryterms', component: DeliverytermComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Delivery Terms",
      permissions: {
        only: ['Delivery Terms'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'supplierapproval', component: SupplierapprovalComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Supplier/Customer Approval",
      permissions: {
        only: ['Supplier/Customer Approval'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'departments', component: DepartmentsComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Departments",
      permissions: {
        only: ['Departments'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'uom', component: UomComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "UOM",
      permissions: {
        only: ['UOM'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'unit', component: UnitComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Unit",
      permissions: {
        only: ['Unit'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'location', component: BinlocationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Location",
      permissions: {
        only: ['Location'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'valve', component: ValveComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Valve",
      permissions: {
        only: ['Valve'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'stage', component: StageComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stage",
      permissions: {
        only: ['Stage'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'type', component: TypesComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Type",
      permissions: {
        only: ['Type'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'country', component: CountryComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Country",
      permissions: {
        only: ['Country'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'state', component: StateComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "State",
      permissions: {
        only: ['State'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'city', component: CityComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "City",
      permissions: {
        only: ['City'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'hsn', component: HsnMasterComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "HSN",
      permissions: {
        only: ['HSN'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'tools', component: ToolsComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Tools",
      permissions: {
        only: ['Tools'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'sellingcost', component: SellingCostComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Selling Cost",
      permissions: {
        only: ['Selling Cost'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'item', component: ItemComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Item",
      permissions: {
        only: ['Item'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'itemstagemapping', component: ItemStageMapComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Item Stage Mapping",
      permissions: {
        only: ['Item Stage Mapping'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'multipleitem', component: AddMultipleitemComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Multiple Item",
      permissions: {
        only: ['Item'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'multiplestage', component: AddMultipleStageComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Multiple Item",
      permissions: {
        only: ['Item'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'itemseries', component: SeriesComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Item Series",
      permissions: {
        only: ['Series'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'itemclass', component: ItemClassComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Item Class",
      permissions: {
        only: ['Class'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'itemsupplier', component: ItemSupplierMapComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Item-Supplier Mapping",
      permissions: {
        only: ['Item-Supplier Mapping'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'productvalue', component: ProductValueComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Product Value",
      permissions: {
        only: ['Item-Supplier Mapping'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'material', component: MaterialComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Material",
      permissions: {
        only: ['Material'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'process', component: ProcessComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Process",
      permissions: {
        only: ['Process'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'itemview/:id', component: ItemViewComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Item",
      permissions: {
        only: ['Item'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'rack', component: RackComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Rack",
      permissions: {
        only: ['Rack'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'bin', component: BinComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Bin",
      permissions: {
        only: ['Bin'],
        redirectTo: '/error404'
      }
    }
  },
  // sales
  {
    path: 'salesorder', component: SalesorderComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Sales Order",
      permissions: {
        only: ['Sales Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'salesauthorization', component: SalesauthorizationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Sales Authorization",
      permissions: {
        only: ['Sales Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewscauthorization/:id', component: ViewScauthComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Sales Authorization",
      permissions: {
        only: ['Sales Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'productconfirmation', component: ProductconfirmationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Product Confirmation",
      permissions: {
        only: ['Product Confirmation'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'salescontract', component: OrderconfirmationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Sales Contract",
      permissions: {
        only: ['Sales Contract'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'cancelsc', component: CancelScComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Cancel SC",
      permissions: {
        only: ['Cancel SC'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'cancelconfirmation', component: CancelConfirmComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Cancel Confirmation",
      permissions: {
        only: ['Cancel Confirmation'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'savedsalesorder', component: SavedSalesordersComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Saved SC",
      permissions: {
        only: ['Saved SC'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewsavedsc/:id', component: ViewSavedScComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Saved SC",
      permissions: {
        only: ['Saved SC'],
        redirectTo: '/error404'
      }
    }
  },

  //Planning
  {
    path: 'pendingplan', component: PendingPlanlistComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Pending Planning",
      permissions: {
        only: ['Pending Planning'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'planning', component: PlanningComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Pending Planning",
      permissions: {
        only: ['Pending Planning'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'pendingallocation', component: PlanningComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Pending Allocation",
      permissions: {
        only: ['Pending Allocation'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'completedplans', component: CompletedPlanComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Completed Planning",
      permissions: {
        only: ['Completed Planning'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'allocationcancel', component: PlanItemCancelComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Allocation Cancel",
      permissions: {
        only: ['Allocation Cancel'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'plancancel', component: PlanCancelComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Plan Cancel",
      permissions: {
        only: ['Plan Cancel'],
        redirectTo: '/error404'
      }
    }
  },

  //Purchase-Master
  {
    path: 'pricebasis', component: PriceBasisComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Price Basis",
      permissions: {
        only: ['Price Basis'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'costcenter', component: CostCenterComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Cost Center",
      permissions: {
        only: ['Cost Center'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'fireghtcharge', component: FrieghtChargesComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Freight Charge",
      permissions: {
        only: ['Freight Charge'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'CandFcharge', component: CandfchargesComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "C&FCharge",
      permissions: {
        only: ['C&FCharge'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'tariff', component: TariffComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Tariff",
      permissions: {
        only: ['Tariff'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'indentlist', component: IndentListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Indent",
      permissions: {
        only: ['Purchase Indent'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'purchaseindent', component: PurchaseIndentComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Indent",
      permissions: {
        only: ['Purchase Indent'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'updateindent/:id', component: PurchaseIndentComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Indent",
      permissions: {
        only: ['Purchase Indent'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'indentauthorize', component: IndentAuthorizeComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Indent Authorization",
      permissions: {
        only: ['Indent Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'indentcancel', component: CancelIndentComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Indent Cancel",
      permissions: {
        only: ['Indent Cancel'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'indentcancelauth', component: CancelindentAuthorizeComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Indent Cancel Authorization",
      permissions: {
        only: ['Indent Cancel Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'indentpending', component: PendingIndentComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Pending Indent",
      permissions: {
        only: ['Pending Indent'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'posearch', component: PoSearchComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Order",
      permissions: {
        only: ['Purchase Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'poedit/:id', component: PoEditComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Order",
      permissions: {
        only: ['Purchase Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'poview/:id', component: PoViewComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Order",
      permissions: {
        only: ['Purchase Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'poauthorize/:id', component: PoEditComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Authorization",
      permissions: {
        only: ['Purchase Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'poauth', component: PoAuthorizationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Purchase Authorization",
      permissions: {
        only: ['Purchase Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'mdpoapproval', component: MdApprovalComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "MD PO Approval",
      permissions: {
        only: ['MD PO Approval'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'poconfirmation', component: PoConfirmationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "PO Confirmation",
      permissions: {
        only: ['PO Confirmation'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'poamendment', component: PoAmendmentComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "PO Amendment",
      permissions: {
        only: ['PO Amendment'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'quotation/:enqno/:id', component: QuotationEntryComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Pending Indent",
      permissions: {
        only: ['Pending Indent'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'comparision/:id', component: ComparissionComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Pending Indent",
      permissions: {
        only: ['Pending Indent'],
        redirectTo: '/error404'
      }
    }
  },

  //Assembly

  {
    path: 'productionstock', component: AssemblyListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Production Stock",
      permissions: {
        only: ['Production Stock'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'assemblysearch', component: AssemblySearchComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Assembly Search",
      permissions: {
        only: ['Assembly Search'],
        redirectTo: '/error404'
      }
    }
  },

  // Logistics
  {
    path: 'proformaInvoicelist', component: ProformalistComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Proforma Invoice",
      permissions: {
        only: ['Proforma Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'proformaInvoice', component: ProformainvoiceComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Proforma Invoice",
      permissions: {
        only: ['Proforma Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewproformainv/:id', component: ProformainvoiceComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Proforma Invoice",
      permissions: {
        only: ['Proforma Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewproforma/:id', component: ViewProformaComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Post Invoice",
      permissions: {
        only: ['Post Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewproformafinal/:id', component: ViewProformaComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Post Invoice",
      permissions: {
        only: ['Post Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewpackingslip/:id', component: SlipViewComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Post Invoice",
      permissions: {
        only: ['Post Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewpackinglist/:id', component: ViewPackinglistComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Post Invoice",
      permissions: {
        only: ['Post Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewpackinglistfinal/:id', component: ViewPackinglistComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Post Invoice",
      permissions: {
        only: ['Post Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'postInvoiceList', component: PostinvoiceListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Post Invoice",
      permissions: {
        only: ['Post Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'postInvoice', component: PostInvoiceComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Post Invoice",
      permissions: {
        only: ['Post Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'areform', component: AreFormComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "A.R.E",
      permissions: {
        only: ['A.R.E'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewareform/:id', component: ViewAreformComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "A.R.E",
      permissions: {
        only: ['A.R.E'],
        redirectTo: '/error404'
      }
    }
  },
  // Production
  {
    path: 'productionentry', component: StockEntryComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Entry",
      permissions: {
        only: ['Stock Entry'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'stockorder', component: StockorderListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Order",
      permissions: {
        only: ['Stock Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'addstockorder', component: StockOrderComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Order",
      permissions: {
        only: ['Stock Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewstockorder', component: StockOrderComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Order",
      permissions: {
        only: ['Stock Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'stockadjustment', component: InventoryComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Adjustment",
      permissions: {
        only: ['Inventory'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'jobcard', component: JobCartComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Job Card",
      permissions: {
        only: ['Job Card'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'deliverychallan', component: DeliveryChallanComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Delivery Challan",
      permissions: {
        only: ['Delivery Challan'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'salesdc', component: SalesDcComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Sales DC",
      permissions: {
        only: ['Sales DC'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'dclocation/:id', component: ViewdcLocationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Delivery Challan",
      permissions: {
        only: ['Delivery Challan'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'createdc', component: CreateDcComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Delivery Challan",
      permissions: {
        only: ['Delivery Challan'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'packingslip', component: SlipListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Packing slip",
      permissions: {
        only: ['Packing slip'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'slip', component: SlipComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Packing slip",
      permissions: {
        only: ['Packing slip'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'packingslipview/:id', component: SlipComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Packing slip",
      permissions: {
        only: ['Packing slip'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'grn', component: GrnListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "GRN Entry",
      permissions: {
        only: ['GRN Entry'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewgrn', component: ViewGrnComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "GRN Entry",
      permissions: {
        only: ['GRN Entry'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'grnentry', component: GrnComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "GRN Entry",
      permissions: {
        only: ['GRN Entry'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'qualitycontrol', component: GrnAuthorizationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "GRN Authorization",
      permissions: {
        only: ['GRN Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'grnlocation', component: GrnLocationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "GRN Location",
      permissions: {
        only: ['GRN Location'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'requestentry', component: ReqEntryComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Request Entry",
      permissions: {
        only: ['Request Entry'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'requestauthorization', component: ReqEntryAuthorizationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Request Authorization",
      permissions: {
        only: ['Request Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'issuelist', component: IssueListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Issue",
      permissions: {
        only: ['Issue'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'issueentry', component: IssueComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Issue",
      permissions: {
        only: ['Issue'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'issuereturn', component: IssueReturnComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Issue Return",
      permissions: {
        only: ['Issue Return'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'toolissuelist', component: ToolissueListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Tool Issue",
      permissions: {
        only: ['Tool Issue'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'toolissue', component: ToolIssueComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Tool Issue",
      permissions: {
        only: ['Tool Issue'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'disposals', component: DisposalListComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Disposal",
      permissions: {
        only: ['Stock Disposal'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'stockdisposal', component: StockDisposalComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Disposal",
      permissions: {
        only: ['Stock Disposal'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'rejectinvoice', component: RejInvoiceComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Reject Invoice",
      permissions: {
        only: ['Reject Invoice'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'storesjobcard', component: DcJobcardComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stores Job Card",
      permissions: {
        only: ['Stores Job Card'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'manualjobcard', component: ManualJobcardComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stores Job Card",
      permissions: {
        only: ['Stores Job Card'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'reworkjobcard', component: ReworkjobcartComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stores Job Card",
      permissions: {
        only: ['Stores Job Card'],
        redirectTo: '/error404'
      }
    }
  },

  //REPORTS-----------------------------

  {
    path: 'stockledger', component: StockLedgerComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Ledger",
      permissions: {
        only: ['Stock Ledger'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'stockreport', component: StockReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Report",
      permissions: {
        only: ['Stock Report'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'stockseries', component: StockSeriesComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Stock Series",
      permissions: {
        only: ['Stock Series'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'dcreport', component: DcReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "DC Report",
      permissions: {
        only: ['DC'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'pendingdc', component: PendingdcReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Pending DC Report",
      permissions: {
        only: ['Pending DC'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'poreport', component: PoReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "PO Report",
      permissions: {
        only: ['PO'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'povalue', component: PoValueReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "PO Report",
      permissions: {
        only: ['PO'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'salesreport', component: SalesReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Sales Report",
      permissions: {
        only: ['Sales Report'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'rejectreport', component: RejectReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Rejection Report",
      permissions: {
        only: ['Rejection'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'disposalreport', component: DisposalReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Disposal Report",
      permissions: {
        only: ['Disposal/Scrap'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'toolreport', component: ToolReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Tools Report",
      permissions: {
        only: ['Tools Report'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'itemlocationreport', component: ItemlocationReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Locate Stock",
      permissions: {
        only: ['Locate stock'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'grnreport', component: GrnReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "GRN Report",
      permissions: {
        only: ['Grn Report'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'invoicereport', component: InvoiceReportComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      title: "Invoice Report",
      permissions: {
        only: ['Invoice Report'],
        redirectTo: '/error404'
      }
    }
  },
];

export const routing = RouterModule.forRoot(appRoutes);
