import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import * as $ from 'jquery';

@Injectable()
export class CommonHttpService {

  constructor(private http: HttpClient) { }

  public globalPostService(url: string, data: any) {
    return this.http.post(url, data).toPromise().catch(e => {
      console.log("error happend", e);
      if (e.status == 401) {
        console.log(e.statusText);
      }
    });
  }
  public globalGetService(url: string) {
    return this.http.get(url).toPromise().
      catch(e => {
        console.log("error happend", e);
      });
  }

  
  public globalPutService(url: string, data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(url, body).toPromise().catch(e => {
      console.log("error happend", e);
      if (e.status == 401) {
        console.log(e.statusText);
      }
    });
  }

  public globalGetServiceByUrl(url: string, ) {
    return this.http.get(url).toPromise().
      catch(e => {
        console.log("error happend", e);
      });
  }

}
