import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTdComponent } from './company-td.component';

describe('CompanyTdComponent', () => {
  let component: CompanyTdComponent;
  let fixture: ComponentFixture<CompanyTdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyTdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
