import { Component, OnInit } from '@angular/core';
import { AppConstant } from '../../app.constant';
import { LoginService } from '../../services/login/login.service'
import { LocalStorage } from '@ng-idle/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-company-td',
  templateUrl: './company-td.component.html',
  styleUrls: ['./company-td.component.css']
})
export class CompanyTdComponent implements OnInit {
  company_Details: any
  url: any;
  urlfront: any
  constructor(private loginservice: LoginService, private router: Router, ) {
    this.url = this.router.url;
    this.urlfront = this.url.split("/")
    console.log("url", this.urlfront)
  }

  ngOnInit() {
    this.getcompanydetails()

  }

  getcompanydetails() {
    var user = JSON.parse(localStorage.getItem("UserDetails"))
    this.loginservice.GetCompanyDetails(user.id).subscribe(res => {
      if (res.status == "success") {
        this.company_Details = res.data
      }
    })
  }

}
