import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotifyService } from '../../services/notify.service'
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import * as $ from 'jquery';
declare let swal: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  Auth: any;
  userdetails: any;
  username: string;
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  idlemsg: any;
  constructor(
    private router: Router,
    private notifyservice: NotifyService,
    private idle: Idle,
    private keepalive: Keepalive) {
    this.userdetails = JSON.parse(localStorage.getItem('UserDetails'));
    idle.setIdle(900);
    idle.setTimeout(900);
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      localStorage.clear();
      this.showexpired();
    });

    idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    idle.onTimeoutWarning.subscribe(
      (countdown) =>
        this.idlemsg = 'You will time out in ' + countdown + ' seconds!'
    );

    keepalive.interval(15);
    keepalive.onPing.subscribe(() => this.lastPing = new Date());
    this.reset();
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }



  ngOnInit() {
    // this.notifyLoggedIn();
    let $openLeftBtn = $('.open-left');
    let $menuItem = $('#sidebar-menu a');
    let ua = navigator.userAgent,
      event = (ua.match(/iP/i)) ? 'touchstart' : 'click';
    //bind on click
    $openLeftBtn.on(event, function (e) {
      e.stopPropagation();
      openLeftBar();
    });
    function openLeftBar() {
      $('#wrapper').toggleClass('enlarged');
      $('#wrapper').addClass('forced');

      if ($('#wrapper').hasClass('enlarged') && $('body').hasClass('fixed-left')) {
        $('body').removeClass('fixed-left').addClass('fixed-left-void');
      } else if (!$('#wrapper').hasClass('enlarged') && $('body').hasClass('fixed-left-void')) {
        $('body').removeClass('fixed-left-void').addClass('fixed-left');
      }

      if ($('#wrapper').hasClass('enlarged')) {
        $('.left ul').removeAttr('style');
      } else {
        $('.subdrop').siblings('ul:first').show();
      }
      toggle_slimscroll('.slimscrollleft');
      $('body').trigger('resize');
    }
    function toggle_slimscroll(item) {
      if ($('#wrapper').hasClass('enlarged')) {
        $(item).css('overflow', 'inherit').parent().css('overflow', 'inherit');
        $(item).siblings('.slimScrollBar').css('visibility', 'hidden');
      } else {
        $(item).css('overflow', 'hidden').parent().css('overflow', 'hidden');
        $(item).siblings('.slimScrollBar').css('visibility', 'visible');
      }
    }
  }

  showexpired() {
    let self = this;
    let sts;
    swal({
      title: 'Session Expired !',
      text: "Please Login Again",
      type: 'error',
      width: 500,
      padding: 10,
      showCancelButton: false,
      confirmButtonColor: '#ffaa00',
      confirmButtonText: 'Login',
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        this.logout()
      }
    })

  }

  logoutconfirmation() {
    let self = this;
    let sts;
    swal({
      title: 'Are you sure?',
      text: "You want to logout!",
      type: 'info',
      width: 500,
      padding: 10,
      showCancelButton: true,
      confirmButtonColor: '#ffaa00',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes!',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.logout()
      }
    })
  }


  logout() {
    localStorage.clear();
    sessionStorage.clear();
    this.notifyservice.showLoggedInUser(false)
    this.router.navigate(['/']);
  }

}
