import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotifyService } from '../../services/notify.service'
import * as $ from 'jquery';
import PerfectScrollbar from 'perfect-scrollbar';

@Component({
  selector: 'app-leftmenu',
  templateUrl: './leftmenu.component.html',
  styleUrls: ['./leftmenu.component.css']
})
export class LeftmenuComponent implements OnInit {

  href: any;
  auth: any;
  userdetails: any;
  roledetails: any;
  samplemenu: any[];

  constructor(private router: Router, private notifyservice: NotifyService) {
    this.userdetails = localStorage.getItem("UserDetails");
    var userjson = JSON.parse(this.userdetails);
    this.roledetails = userjson.role;
    console.log("userdtls", this.roledetails)
  }

  ngOnInit() {
    this.notifyLoggedIn();
    var ps = new PerfectScrollbar('.slimscrollleft', {
      wheelSpeed: 0.2,
      wheelPropagation: true,
      minScrollbarLength: 10
    });

  }

  ngAfterViewInit() {

    setTimeout(() => {
      let menuItemClick = function (e) {
        if (!$('#wrapper').hasClass('enlarged')) {
          if ($(this).parent().hasClass('has_sub')) {

          }
          if (!$(this).hasClass('subdrop')) {
            // hide any open menus and remove all other classes
            $('ul', $(this).parents('ul:first')).slideUp(350);
            $('a', $(this).parents('ul:first')).removeClass('subdrop');
            $('#sidebar-menu .pull-right i').removeClass('md-remove').addClass('md-add');

            // open our new menu and add the open class
            $(this).next('ul').slideDown(350);
            $(this).addClass('subdrop');
            $('.pull-right i', $(this).parents('.has_sub:last')).removeClass('md-add').addClass('md-remove');
            $('.pull-right i', $(this).siblings('ul')).removeClass('md-remove').addClass('md-add');
          } else if ($(this).hasClass('subdrop')) {
            $(this).removeClass('subdrop');
            $(this).next('ul').slideUp(350);
            $('.pull-right i', $(this).parent()).removeClass('md-remove').addClass('md-add');
          }
        }
      }

      let $menuItem = $('#sidebar-menu a');
      let ua = navigator.userAgent,
        event = (ua.match(/iP/i)) ? 'touchstart' : 'click';
      $menuItem.on(event, menuItemClick);

      // NAVIGATION HIGHLIGHT & OPEN PARENT
      $('#sidebar-menu ul li.has_sub a.active').parents('li:last').children('a:first').addClass('active').trigger('click');
    }, 500);


  }



  notifyLoggedIn() {
    this.notifyservice.getLoggedInUser().subscribe((message) => {
      if (message) {
        console.log("authtrue")
        this.auth = true;
      }
    });
  }

  // loadleftmenu() {
    // this.userservice.getMenus()
    //   .subscribe(res => {
    //     this.samplemenu = res.data;
    //     console.log("menu",this.samplemenu)
    //   })
  // }




}
