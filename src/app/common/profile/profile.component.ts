import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { UserService } from '../../services/user/user.service';
import { LocalStorage } from '@ng-idle/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  toastertime: number;
  tosterminlength: number;
  statusarray: any;
  tabletimeout: any;
  show_loader: boolean = false;
  showDialog: boolean = false;
  showMailDialog: boolean = false;
  showMblDialog: boolean = false
  scrolltime: number;
  pwdform: FormGroup;
  user: any;
  userdtls: any

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private userservice: UserService
  ) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.userdtls = JSON.parse(localStorage.getItem("UserDetails"))

    this.pwdform = fb.group({
      'oldpwd': [null, Validators.required],
      'newpwd': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.getuserdtl()
  }

  getuserdtl() {
    this.show_loader = true;
    this.userservice.getUserDetails(this.userdtls.id).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.user = res.data;
        localStorage.setItem("UserDetails", JSON.stringify(this.user));
      }
    })
  }

  changepwdpopup() {
    this.showDialog = true
  }

  changeemail() {
    this.showMailDialog = true;
  }

  changembl() {
    this.showMblDialog = true;
  }

  formObj: any = {
    oldpwd:
      {
        required: "Old Password required",
      },
    newpwd:
      {
        required: "New Password required",
      },

  }



  updatepwd(data) {
    if (this.pwdform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.pwdform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else {
      var formdata =
        {
          'oldpassword': data.oldpwd,
          'newpassword': data.newpwd,
          'userid': this.userdtls.id
        }
      console.log("formdata", formdata);
      this.show_loader = true;
      this.userservice.changepwd(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.resetform();
          this.getuserdtl();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    }
  }
  mailid: any
  mblno: any
  updatemail() {
    if (!this.mailid) {
      this.notif.warn('Warning', "Please enter Email Id", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      var formdata =
        {
          'emailid': this.mailid,
          'mobileno': "",
          'userid': this.userdtls.id
        }
      console.log("formdata", formdata);
      this.show_loader = true;
      this.userservice.UpdateUser(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.resetform();
          this.getuserdtl()
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    }
  }

  updatembl() {
    if (!this.mblno) {
      this.notif.warn('Warning', "Please enter Mobile No", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else if (this.mblno.length < 10) {
      this.notif.warn('Warning', "Please enter valid Mobile No", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      var formdata =
        {
          'emailid': "",
          'mobileno': this.mblno,
          'userid': this.userdtls.id
        }
      console.log("formdata", formdata);
      this.show_loader = true;
      this.userservice.UpdateUser(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.resetform();
          this.getuserdtl()
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    }
  }

  resetform() {
    this.showDialog = false;
    this.showMailDialog = false;
    this.showMblDialog = false;
    this.mblno = "";
    this.mailid = "";
    this.pwdform.reset();
  }

}
