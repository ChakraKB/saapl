import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class LoginAuthGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate() {
    var auth = localStorage.getItem('AUTH')
    if (auth == "true") {
      return true;
    }
    else {
      this.router.navigate(['/'])
    }
  }
}
