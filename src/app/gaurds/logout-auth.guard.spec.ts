import { TestBed, async, inject } from '@angular/core/testing';

import { LogoutAuthGuard } from './logout-auth.guard';

describe('LogoutAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogoutAuthGuard]
    });
  });

  it('should ...', inject([LogoutAuthGuard], (guard: LogoutAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
