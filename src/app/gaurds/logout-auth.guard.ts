import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { NotifyService } from '../services/notify.service'
import { Router } from '@angular/router';

@Injectable()
export class LogoutAuthGuard implements CanActivate {
  constructor(private router: Router,private notifyservice: NotifyService) {

  }
  canActivate() {
    var auth = localStorage.getItem('AUTH')

    if (auth) {
      localStorage.clear();
      this.notifyservice.showLoggedInUser(false)
      return true
    }
    else {
      return true
    }
  }
}
