import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrnAuthorizationComponent } from './grn-authorization.component';

describe('GrnAuthorizationComponent', () => {
  let component: GrnAuthorizationComponent;
  let fixture: ComponentFixture<GrnAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrnAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrnAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
