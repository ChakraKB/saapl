import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { DepartmentsService } from '../../services/department/departments.service';
import { CompleterService } from 'ng2-completer';
import { GrnService } from '../../services/grn/grn.service'
import { error } from 'util';
import { Router } from '@angular/router';
class Person {
  checkauthorize: boolean = false;
}
@Component({
  selector: 'app-grn-authorization',
  templateUrl: './grn-authorization.component.html',
  styleUrls: ['./grn-authorization.component.css']
})
export class GrnAuthorizationComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  files: any;
  indentstatus: any;
  nofiles: boolean = false;
  filter: any = {};
  purDetailsList: any;
  departs: any;
  alldpts: any;
  checkeditems: any;
  contype: any;
  enqid: any;
  showDialog: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  public user = JSON.parse(localStorage.getItem("UserDetails"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private http: Http,
    private completerService: CompleterService,
    private grnservice: GrnService,
    private router: Router,
  ) {
    this.contype = AppConstant.APP_CONST.ContactType
    this.indentstatus = AppConstant.APP_CONST.IndentStatus3
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.checkeditems = [];
    this.getgrnauthlist();
    this.filter.status = "Pending";
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  claimvalue(i) {
    var rejqty = this.persons[i].rej_qty
    var cost = this.persons[i].rate
    var claimvalue = rejqty * cost;
    this.persons[i].claim_value = claimvalue
    return claimvalue
  }

  getgrnauthlist() {
    this.show_loader = true;
    this.grnservice.GetGRAuthList(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        if (this.persons) {
          _.map(this.persons, function (val) {
            val.checked = false;
            val.scrap_qty = 0
            return val
          })
        }
        this.persons = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
        console.log("persons", this.persons)
      } else {
        this.notif.error("Error", res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      if (this.dtElement.dtInstance) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        })
      } else
        this.dtTrigger.next();
      this.notif.error("Error", "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  checkauthorized(status, item, i) {
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  checknone(status, item, i) {
    if (status == true) {
      this.persons[i].checked = true;
      this.persons[i].none = true;
      this.persons[i].riseexpo = false;
      this.persons[i].risedc = false;
    } else {
      this.persons[i].checked = false;
    }
  }

  checkexpo(status, item, i) {
    if (status == true) {
      this.persons[i].checked = true;
      this.persons[i].none = false;
      this.persons[i].riseexpo = true;
      this.persons[i].risedc = false;
      this.persons[i].excess_process = "Raise-Ex-PO"
    } else {
      this.persons[i].checked = false;
      this.persons[i].excess_process = ""
    }
  }

  checkrisedc(status, item, i) {
    if (status == true) {
      this.persons[i].checked = true;
      this.persons[i].none = false;
      this.persons[i].riseexpo = false;
      this.persons[i].risedc = true;
      this.persons[i].excess_process = "Raise DC"
    } else {
      this.persons[i].checked = false;
      this.persons[i].excess_process = ""
    }
  }

  calcacc(acc_qty, scrap_qty, rej_qty, rewrk_qty, i, field) {
    var a = (+acc_qty) + (+scrap_qty) + (+rej_qty) + (+rewrk_qty)
    if (a > this.persons[i].rec_qty) {
      if (field == 'acc_qty') this.persons[i].acc_qty = 0;
      else if (field == 'scrap_qty') this.persons[i].scrap_qty = 0;
      else if (field == 'rwrk_qty') this.persons[i].rework_qty = 0;
      else this.persons[i].rej_qty = 0;
    }
  }

  calcrej(acc_qty, rej_qty, i) {
    this.persons[i].rej_qty = +rej_qty
    if ((+this.persons[i].rej_qty) > this.persons[i].scrap_qty) {
      this.persons[i].acc_qty = this.persons[i].rec_qty - this.persons[i].rej_qty;
      this.persons[i].scrap_qty = 0;
    } else if (this.persons[i].rej_qty > this.persons[i].rec_qty) {
      this.persons[i].rej_qty = 0
    } else {
      this.persons[i].scrap_qty = 0
      this.persons[i].acc_qty = +(this.persons[i].rec_qty - (+this.persons[i].rej_qty))
    }
  }

  calcscrab(acc_qty, scrap_qty, i) {
    this.persons[i].scrap_qty = +scrap_qty
    if ((+this.persons[i].rej_qty) >= this.persons[i].scrap_qty) {
      this.persons[i].rej_qty = this.persons[i].rej_qty - this.persons[i].scrap_qty
    } else if (this.persons[i].scrap_qty > this.persons[i].rec_qty) {
      this.persons[i].scrap_qty = 0
    } else {
      this.persons[i].rej_qty = 0
      this.persons[i].acc_qty = +(this.persons[i].rec_qty - (+this.persons[i].scrap_qty))
    }
  }

  authorize() {
    var newlist = [];
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    if (!this.checkeditems.length) {
      validdata = false;
      validationerrorMsg.push("Nothing to authorize");
    } else {
      _.forEach(this.checkeditems, function (val) {
        if (val.rej_qty != 0) {
          if (_.isEmpty(val.rej_remarks)) {
            validdata = false;
            validationerrorMsg.push("Please enter reject remarks for " + val.po_no)
          }
        }
        var a = (+val.acc_qty) + (+val.scrap_qty) + (+val.rej_qty) + (+val.rework_qty)
        if (a < val.rec_qty || a > val.rec_qty) {
          validdata = false;
          validationerrorMsg.push("Your GRN Qty for " + val.po_no + " is " + val.rec_qty)
        }
      })
    }
    if (validdata) {
      this.checkeditems.forEach(element => {
        element.is_authorized = 1
        element.inspected_by = this.user.name
        var list = _.omit(element, ['none', 'riseexpo', 'risedc', 'checked']);
        newlist.push(list)
      });
      console.log("obj", newlist)
      this.show_loader = true;
      this.grnservice.AUthorizeGRN(newlist).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.notif.success(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
          this.getgrnauthlist();
          this.router.navigate(['/grnlocation'])
        } else {
          this.notif.error(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error("Error", "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
