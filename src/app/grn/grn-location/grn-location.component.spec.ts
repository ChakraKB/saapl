import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrnLocationComponent } from './grn-location.component';

describe('GrnLocationComponent', () => {
  let component: GrnLocationComponent;
  let fixture: ComponentFixture<GrnLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrnLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrnLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
