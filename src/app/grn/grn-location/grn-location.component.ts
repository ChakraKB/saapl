import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import * as __ from 'lodash';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { DepartmentsService } from '../../services/department/departments.service';
import { CompleterService } from 'ng2-completer';
import { GrnService } from '../../services/grn/grn.service';
import { RackService } from '../../services/Rack/rack.service';
import { BinService } from '../../services/bin/bin.service';

@Component({
  selector: 'app-grn-location',
  templateUrl: './grn-location.component.html',
  styleUrls: ['./grn-location.component.css']
})
export class GrnLocationComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  files: any;
  orderForm: FormGroup;
  indentstatus: any;
  nofiles: boolean = false;
  filter: any = {};
  prodfilter: any = {};
  purDetailsList: any;
  departs: any;
  alldpts: any;
  checkeditems: any;
  contype: any;
  enqid: any;
  showDialog: boolean = false;
  showProdDialog: boolean = false;
  grntypes: any;
  rack: any;
  nodata: any;
  itemdetail: any;
  allbins: any;
  index: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  allracks: any;
  rejForm: FormGroup;
  rewrkFrm: FormGroup;
  ProdStockForm: FormGroup;
  list: any;
  checkedProditems: any;
  prodList: any
  selectedorderLine: any;
  Prodindex: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private http: Http,
    private completerService: CompleterService,
    private grnservice: GrnService,
    private rackservice: RackService,
    private binservice: BinService
  ) {
    this.grntypes = AppConstant.APP_CONST.POTypes
    this.contype = AppConstant.APP_CONST.ContactType
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.orderForm = this.fb.group({
      itemRows: this.fb.array([])
    });
    this.rejForm = this.fb.group({
      itemrejRows: this.fb.array([])
    });

    this.rewrkFrm = this.fb.group({
      itemrewrkRows: this.fb.array([])
    });

    this.ProdStockForm = this.fb.group({
      itemProdRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.checkeditems = [];
    this.checkedProditems = [];
    this.filter.type = "PO";
    this.getgrnloclist();
    this.getprodlist();
    this.getactivebins();
    this.addNewRow();
    this.addNewRejRow();
    this.addNewRewrkRow();
    this.addNewProdRow();
  }

  addNewRow() {
    this.nodata = true;
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.push(this.initItemRows());
  }

  deleteRow(index: number) {
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.removeAt(index);
  }

  addNewRejRow() {
    this.nodata = true;
    const control = <FormArray>this.rejForm.controls["itemrejRows"];
    control.push(this.initItemRows());
  }

  deleteRejRow(index: number) {
    const control = <FormArray>this.rejForm.controls["itemrejRows"];
    control.removeAt(index);
  }

  addNewRewrkRow() {
    this.nodata = true;
    const control = <FormArray>this.rewrkFrm.controls["itemrewrkRows"];
    control.push(this.initItemRows());
  }

  deleteRewrkRow(index: number) {
    const control = <FormArray>this.rewrkFrm.controls["itemrewrkRows"];
    control.removeAt(index);
  }

  addNewProdRow() {
    this.nodata = true;
    const control = <FormArray>this.ProdStockForm.controls["itemProdRows"];
    control.push(this.initItemRows());
  }

  deleteProdRow(index: number) {
    const control = <FormArray>this.ProdStockForm.controls["itemProdRows"];
    control.removeAt(index);
  }

  initItemRows() {
    return this.fb.group({
      bin_id: "",
      bin: "",
      qty: 0,
    });
  }

  getorderform(orderForm) {
    return orderForm.get('itemRows').controls
  }
  getRejform(RejForm) {
    return RejForm.get('itemrejRows').controls
  }
  getrewrkform(RewrkForm) {
    return RewrkForm.get('itemrewrkRows').controls
  }
  getProdform(prodForm) {
    return prodForm.get('itemProdRows').controls
  }

  showProdpop(ind) {
    this.showProdDialog = true;
    this.Prodindex = ind
    let Prodtemp = this.fb.array([]);
    if (!_.isEmpty(this.prodList[ind].stk_loc)) {
      for (let i = 0; i < this.prodList[ind].stk_loc.length; i++) {
        if (ind == i) {
          Prodtemp.push(
            this.fb.group({
              bin_id: this.prodList[ind].stk_loc[i].bin_id,
              bin: this.prodList[ind].stk_loc[i].bin,
              qty: this.prodList[ind].stk_loc[i].qty,
            })
          );
        } else {
          Prodtemp.push(
            this.fb.group(this.prodList[ind].stk_loc[i]));
        }
      }
      this.ProdStockForm = this.fb.group({
        itemProdRows: Prodtemp
      });
    } else {
      this.ProdStockForm = this.fb.group({
        itemProdRows: Prodtemp
      });
      this.addNewProdRow();
    }
  }

  showpop(ind) {
    this.showDialog = true;
    this.index = ind
    let temp = this.fb.array([]);
    let rejtemp = this.fb.array([]);
    let rewrktemp = this.fb.array([]);
    if (!_.isEmpty(this.persons[ind].accepted_stk_loc)) {
      for (let i = 0; i < this.persons[ind].accepted_stk_loc.length; i++) {
        if (ind == i) {
          temp.push(
            this.fb.group({
              bin_id: this.persons[ind].accepted_stk_loc[i].bin_id,
              bin: this.persons[ind].accepted_stk_loc[i].bin,
              qty: this.persons[ind].accepted_stk_loc[i].qty,
            })
          );
        } else {
          temp.push(
            this.fb.group(this.persons[ind].accepted_stk_loc[i]));
        }
      }
      this.orderForm = this.fb.group({
        itemRows: temp
      });
    } else {
      this.orderForm = this.fb.group({
        itemRows: temp
      });
      this.addNewRow();
    }

    if (!_.isEmpty(this.persons[ind].rej_stk_loc)) {
      for (let i = 0; i < this.persons[ind].rej_stk_loc.length; i++) {
        if (ind == i) {
          rejtemp.push(
            this.fb.group({
              bin_id: this.persons[ind].rej_stk_loc[i].bin_id,
              bin: this.persons[ind].rej_stk_loc[i].bin,
              qty: this.persons[ind].rej_stk_loc[i].qty,
            })
          );
        } else {
          rejtemp.push(
            this.fb.group(this.persons[ind].rej_stk_loc[i]));
        }
      }
      this.rejForm = this.fb.group({
        itemrejRows: rejtemp
      });
    } else {
      this.rejForm = this.fb.group({
        itemrejRows: rejtemp
      });
      this.addNewRejRow();
    }

    if (!_.isEmpty(this.persons[ind].rework_stk_loc)) {
      for (let i = 0; i < this.persons[ind].rework_stk_loc.length; i++) {
        if (ind == i) {
          rewrktemp.push(
            this.fb.group({
              bin_id: this.persons[ind].rework_stk_loc[i].bin_id,
              bin: this.persons[ind].rework_stk_loc[i].bin,
              qty: this.persons[ind].rework_stk_loc[i].qty,
            })
          );
        } else {
          rewrktemp.push(
            this.fb.group(this.persons[ind].rework_stk_loc[i]));
        }
      }
      this.rewrkFrm = this.fb.group({
        itemrewrkRows: rewrktemp
      });
    } else {
      this.rewrkFrm = this.fb.group({
        itemrewrkRows: rewrktemp
      });
      this.addNewRewrkRow();
    }
  }

  calcreqqty(item, ind) {
    // this.selectedorderLine = item;
    // var tot = 0
    // let temp = this.fb.array([]);
    // _.forEach(this.orderForm.value.itemRows, function (val) {
    //   tot += (+val.qty)
    // })
    // console.log("total", tot);
    // if (item.qty > this.persons[this.index].acc_qty || tot > this.persons[this.index].acc_qty) {
    //   this.notif.warn('Warning', "Qty should not exceed Received Qty", {
    //     timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
    //     maxLength: this.tosterminlength
    //   })
    //   for (let i = 0; i < this.orderForm.value.itemRows.length; i++) {
    //     if (ind == i) {
    //       temp.push(
    //         this.fb.group({
    //           bin_id: this.orderForm.value.itemRows[i].bin_id,
    //           bin: this.orderForm.value.itemRows[i].bin,
    //           qty: 0,
    //         })
    //       );
    //     } else {
    //       temp.push(
    //         this.fb.group(this.orderForm.value.itemRows[i]));
    //     }
    //   }
    //   this.orderForm = this.fb.group({
    //     itemRows: temp
    //   });
    // }
  }

  addProdbins(i) {
    let self = this;
    var tot = 0;
    _.forEach(this.ProdStockForm.value.itemProdRows, function (val) {
      tot += (+val.qty)
    })
    if (tot > this.prodList[i].prod_quantity || tot < this.prodList[i].prod_quantity) {
      this.notif.warn('Warning', "Your Received Qty is : " + this.prodList[i].prod_quantity, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      this.prodList[i].stk_loc = this.ProdStockForm.value.itemProdRows;
      this.showProdDialog = false;
      console.log("list", this.prodList[i])
    }
  }

  addbins(i) {
    let self = this;
    var list = [];
    var tot = 0;
    var rejtot = 0;
    var rewrktot = 0
    _.forEach(this.orderForm.value.itemRows, function (val) {
      tot += (+val.qty)
    })
    _.forEach(this.rejForm.value.itemrejRows, function (val) {
      rejtot += (+val.qty)
    })
    _.forEach(this.rewrkFrm.value.itemrewrkRows, function (val) {
      rewrktot += (+val.qty)
    })
    if (tot > this.persons[i].acc_qty || tot < this.persons[i].acc_qty) {
      this.notif.warn('Warning', "Your Received Qty is : " + this.persons[i].acc_qty, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else if (rejtot < this.persons[i].rej_qty || rejtot > this.persons[i].rej_qty) {
      this.notif.warn('Warning', "Your Rejected Qty is : " + this.persons[i].rej_qty, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else if (rewrktot < this.persons[i].rework_qty || rewrktot > this.persons[i].rework_qty) {
      this.notif.warn('Warning', "Your Rework Qty is : " + this.persons[i].rework_qty, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      if (this.persons[i].acc_qty) this.persons[i].accepted_stk_loc = this.orderForm.value.itemRows;
      if (this.persons[i].rej_qty != 0) this.persons[i].rej_stk_loc = this.rejForm.value.itemrejRows;
      if (this.persons[i].rework_qty != 0) this.persons[i].rework_stk_loc = this.rewrkFrm.value.itemrewrkRows;
      this.showDialog = false;
      console.log("list", this.persons[i])
    }
  }

  selectedProdbin(item, index) {
    if (item) {
      var selectedprodbin = item.originalObject;
      let temp = this.fb.array([]);
      for (let i = 0; i < this.ProdStockForm.value.itemProdRows.length; i++) {
        if (index == i) {
          temp.push(
            this.fb.group({
              bin_id: selectedprodbin.id,
              bin: this.ProdStockForm.value.itemProdRows[i].bin,
              qty: this.ProdStockForm.value.itemProdRows[i].qty,
            })
          );
        } else {
          temp.push(
            this.fb.group(this.ProdStockForm.value.itemProdRows[i]));
        }
      }
      this.ProdStockForm = this.fb.group({
        itemProdRows: temp
      });
    }
  }

  selectedbin(item, index, flag) {
    if (item) {
      var selectedbin = item.originalObject;
      let temp = this.fb.array([]);
      let rejtemp = this.fb.array([]);
      let rewrktemp = this.fb.array([]);
      if (flag == "accform") {
        for (let i = 0; i < this.orderForm.value.itemRows.length; i++) {
          if (index == i) {
            temp.push(
              this.fb.group({
                bin_id: selectedbin.id,
                bin: this.orderForm.value.itemRows[i].bin,
                qty: this.orderForm.value.itemRows[i].qty,
              })
            );
          } else {
            temp.push(
              this.fb.group(this.orderForm.value.itemRows[i]));
          }
        }
        this.orderForm = this.fb.group({
          itemRows: temp
        });
      }

      if (flag == "rejform") {
        for (let i = 0; i < this.rejForm.value.itemrejRows.length; i++) {
          if (index == i) {
            rejtemp.push(
              this.fb.group({
                bin_id: selectedbin.id,
                bin: this.rejForm.value.itemrejRows[i].bin,
                qty: this.rejForm.value.itemrejRows[i].qty,
              })
            );
          } else {
            rejtemp.push(
              this.fb.group(this.rejForm.value.itemrejRows[i]));
          }
        }
        this.rejForm = this.fb.group({
          itemrejRows: rejtemp
        });
      }

      if (flag == "rewrkform") {
        for (let i = 0; i < this.rewrkFrm.value.itemrewrkRows.length; i++) {
          if (index == i) {
            rewrktemp.push(
              this.fb.group({
                bin_id: selectedbin.id,
                bin: this.rewrkFrm.value.itemrewrkRows[i].bin,
                qty: this.rewrkFrm.value.itemrewrkRows[i].qty,
              })
            );
          } else {
            rewrktemp.push(
              this.fb.group(this.rewrkFrm.value.itemrewrkRows[i]));
          }
        }
        this.rewrkFrm = this.fb.group({
          itemrewrkRows: rewrktemp
        });
      }
    }
  }

  getactivebins() {
    this.binservice.getBinByStatus('active').subscribe(res => {
      if (res) {
        this.binservice = res.data
        this.allbins = this.completerService.local(res.data, 'bin_name', 'bin_name');
      }
    })
  }

  getprodlist() {
    this.checkedProditems = []
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.prodfilter.from || this.prodfilter.to) {
      if (!(this.prodfilter.from && this.prodfilter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.prodfilter.from.jsdate <= this.prodfilter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
        {
          "unit_id": this.unitid,
          "from_date": _.isEmpty(this.prodfilter.from) ? "" : this.prodfilter.from.formatted,
          "to_date": _.isEmpty(this.prodfilter.to) ? "" : this.prodfilter.to.formatted,
        }
      this.show_loader = true;
      this.grnservice.GetProdStocList(obj).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.prodList = res.data
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }


  getgrnloclist() {
    this.checkeditems = []
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
        {
          "unit_id": this.unitid,
          "type": this.filter.type,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
        }
      this.show_loader = true;
      this.grnservice.GetGRNLocationList(obj).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  checkProdauthorized(status, item, i) {
    this.prodList[i].checkauthorize = true
    if (status == true) {
      this.checkedProditems.push(item)
    } else {
      this.prodList[i].checkauthorize = false
      this.checkedProditems.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkedProditems);
  }

  uncheckall() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  uncheckallProd() {
    for (var i = 0; i < this.prodList.length; i++) {
      this.prodList[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  selectedrack(data) {
    console.log(data.originalObject)
  }

  authorize() {
    var validdata = true;
    var validationerrorMsg = [];
    if (!this.checkeditems.length) {
      validdata = false;
      validationerrorMsg.push("Please select any item")
    }
    _.forEach(this.checkeditems, function (val) {
      if (val.acc_qty != 0 && !val.accepted_stk_loc || val.rej_qty != 0 && !val.rej_stk_loc || val.rework_qty != 0 && !val.rework_stk_loc) {
        validdata = false;
        validationerrorMsg.push("Please select Locations for GRN : " + val.grn_no)
      }
    })

    if (validdata) {
      console.log("checkeditems", this.checkeditems)
      this.show_loader = true;
      this.grnservice.CreateGrnLocation(this.checkeditems).subscribe(res => {
        if (res.status == "success") {
          this.resetform();
          this.show_loader = false;
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.show_loader = false;
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  SaveProdLocation() {
    var validdata = true;
    var validationerrorMsg = [];
    if (!this.checkedProditems.length) {
      validdata = false;
      validationerrorMsg.push("Please select any item")
    }
    _.forEach(this.checkedProditems, function (val) {
      if (!val.stk_loc) {
        validdata = false;
        validationerrorMsg.push("Please select Locations for Item : " + val.part_no)
      }
    })

    if (validdata) {
      console.log("checkedProditems", this.checkedProditems)
      this.show_loader = true;
      this.grnservice.CreateProdLocation(this.checkedProditems).subscribe(res => {
        if (res.status == "success") {
          this.resetform();
          this.show_loader = false;
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.show_loader = false;
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  resetform() {
    this.getgrnloclist();
    this.getprodlist();
  }

  close() {
    this.showDialog = false;
    this.showProdDialog = false
  }
}
