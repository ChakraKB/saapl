import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../services/master.service'
import { AppConstant } from '../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import * as __ from 'lodash';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { ChangeDetectorRef } from '@angular/core';
import { PurchaseorderService } from '../services/purchase_order/purchaseorder.service';
import { GrnService } from '../services/grn/grn.service';
import { isUndefined } from 'util';
import { UserService } from '../services/user/user.service';


@Component({
  selector: 'app-grn',
  templateUrl: './grn.component.html',
  styleUrls: ['./grn.component.css']
})

export class GrnComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  };
  toastertime: any;
  tosterminlength: any;
  tabletimeout: any;
  protected dataService: CompleterData;
  userdetails: any;
  show_loader: any;
  grn: any = {};
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  polist: any;
  orderDetails: any;
  checkeditems: any;
  empty: any;
  selectedorderid: any;
  potypes: any;
  salesorderarray: any
  pos: any;
  grndtls: any;
  excesslist: any;
  obj: any;
  allcontacts: any;
  contacts: any;
  supid: any;
  show_dialog_loader: boolean = false;
  showDialog: boolean = false
  grnid: any;
  subcontracts: any;
  allsubcontracts: any;
  formatted: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private completerService: CompleterService,
    private poservice: PurchaseorderService,
    private grnservice: GrnService,
    private router: Router,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private userservices: UserService,
  ) {
    this.potypes = AppConstant.APP_CONST.POTypes;
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.grnid = this.acroute.snapshot.queryParamMap.get('id');
  }

  ngOnInit() {
    let date = new Date();
    this.getcontacts();
    this.alterdateformat(date);
    this.grn.potype = "PO"
    var currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    this.grn.grndate = { date: currentdate, formatted: this.formatted, jsdate: new Date() }
    // this.getpolist("PO");
    this.selectedorderid = [];
    this.checkeditems = [];
    if (this.grnid) {
      // this.viewgrn()
    }
  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  alterdateformat(date) {
    this.formatted = formatDate(date);
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }

  getcontacts() {
    this.show_loader = true
    this.userservices.getContactsByCat("Supplier")
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.contacts = res.data;
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
    this.show_loader = true
    this.userservices.getContactsByCat("Sub-contract")
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.subcontracts = res.data;
          this.allsubcontracts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  selectedcontact(data) {
    this.grn.sup = data.originalObject
    this.grn.supplier_id = data.originalObject.id
    console.log("supp", data.originalObject)
    // this.show_loader = true;
  }

  calcsubtotal(i) {

    var tot = this.excesslist[i].excess_qty * (+this.excesslist[i].item_cost)
    if (this.excesslist[i].discount_type == "%") {
      if (isNaN(this.excesslist[i].discount)) {
        this.excesslist[i].discount = 0
      } else if (this.excesslist[i].discount > 100) {
        this.excesslist[i].discount = 0
      } else {
        var disc = (tot * +this.excesslist[i].discount) / 100
        var itemdisc = ((this.excesslist[i].item_cost - disc) * +this.excesslist[i].pf) / 100
        this.excesslist[i].discountval = disc
      }
    } else {
      if (isNaN(this.excesslist[i].discount)) {
        this.excesslist[i].discount = 0
      } else if (this.excesslist[i].discount > (this.excesslist[i].excess_qty * (+this.excesslist[i].item_cost))) {
        this.excesslist[i].discount = 0
      } else var disc = (+this.excesslist[i].discount) * this.excesslist[i].excess_qty
      this.excesslist[i].discountval = disc
    }

    if (this.excesslist[i].pf_type == "%") {
      if (isNaN(this.excesslist[i].pf)) {
        this.excesslist[i].pf = 0
      } else if (this.excesslist[i].pf > 100) {
        this.excesslist[i].pf = 0
      } else {
        var pftot = ((tot - disc) * +this.excesslist[i].pf) / 100
        this.excesslist[i].pfval = pftot
      }
    } else {
      if (isNaN(this.excesslist[i].pf)) {
        this.excesslist[i].pf = 0
      } else var pftot = (+this.excesslist[i].pf) * this.excesslist[i].excess_qty;
      this.excesslist[i].pfval = pftot;
    }
    this.excesslist[i].rateval = tot
    this.excesslist[i].totalamount = (tot - disc) + pftot;
    return (tot - disc) + pftot
  }

  calctotal() {
    var amounttot = 0
    _.forEach(this.excesslist, function (val) {
      var a = +val.totalamount;
      amounttot += a
    })
    return +amounttot
  }

  calctrate() {
    var totalrate = 0
    _.forEach(this.excesslist, function (val) {
      var a = +val.rate;
      totalrate += a
    })
    return +totalrate;
  }

  calctdisc() {
    var totaldisc = 0
    _.forEach(this.excesslist, function (val) {
      var a = +val.discount_value;
      totaldisc += a
    })
    return +totaldisc
  }

  calctpf() {
    var totalpf = 0
    _.forEach(this.excesslist, function (val) {
      var a = +val.pf_value;
      totalpf += a
    })
    return +totalpf
  }

  getpolist(data) {
    if (!_.isEmpty(data)) {
      this.grn.sup = data.originalObject
    } else {
      this.grn.supplier = "";
    }
    var valid: boolean = true;
    var validmsg = [];
    if (this.grn.potype == "PO") {
      var supplier = _.findWhere(this.contacts, { 'supplier_name': this.grn.supplier })
    } else {
      var supplier = _.findWhere(this.subcontracts, { 'supplier_name': this.grn.supplier })
    }
    if (!this.grn.supplier) {
      valid = false;
      validmsg.push('Please select supplier')
    }
    if (valid) {
      this.salesorderarray = [];
      this.selectedorderid = [];
      this.show_loader = true;
      this.grnservice.GetPOList(this.unitid, this.grn.potype, supplier.id).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.pos = res.data;
          if (this.grn.potype == "PO") {
            this.polist = this.masterservice.formatDataforDropdown("po_no", "supplier_id", res.data);
            console.log("polist", this.polist)
          } else {
            this.polist = this.masterservice.formatDataforDropdown("dc_no", "supplier_id", res.data);
            console.log("dclist", this.polist)
          }
        }
      })
    }
    // else {
    //   this.notif.warn('Warning', validmsg[0], {
    //     timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
    //     maxLength: this.tosterminlength
    //   })
    // }
  }

  id: any

  public compareFn(a, b): boolean {
    return a == b;
  }

  selectorder(order: any, flag) {
    var validdata: boolean = true
    if (order) {
      if (flag == "selected") {
        this.orderDetails = [];
        this.checkeditems = [];
        this.empty = false;
        if (this.selectedorderid.length) {
          // if (_.findWhere(this.selectedorderid, { "value": order.value })) {
          if (this.grn.potype == "PO") {
            this.id = _.findWhere(this.pos, { "po_no": order.label })
            order.id = this.id.id
            this.grn.supplier_id = this.id.supplier_id;
            this.selectedorderid.push(order);
          } else {
            this.id = _.findWhere(this.pos, { "dc_no": order.label })
            order.id = this.id.id
            this.grn.supplier_id = this.id.supplier_id.id;
            this.selectedorderid.push(order);
          }
          // } else {
          //   validdata = false;
          //   this.selectedorderid = _.reject(this.selectedorderid, function (num) {
          //     return num.label == order.label;
          //   });
          //   this.notif.warn('Warning', "Select Same Supplier PO", {
          //     timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          //     maxLength: this.tosterminlength
          //   })
          // }
        } else {
          if (this.grn.potype == "PO") {
            this.id = _.findWhere(this.pos, { "po_no": order.label })
            order.id = this.id.id
            this.grn.supplier_id = this.id.supplier_id;
          } else {
            this.id = _.findWhere(this.pos, { "dc_no": order.label })
            order.id = this.id.id
            this.grn.supplier_id = this.id.supplier_id.id;
          }
          this.selectedorderid.push(order);
        }

      } else {
        // if (this.selectedorderid.length == 1) {
        //   this.grn.supplier = ""
        // }
        this.selectedorderid = _.reject(this.selectedorderid, function (num) {
          return num.label == order.label;
        });
      }
      console.log("idlist", JSON.stringify(this.selectedorderid))
      if (validdata) {
        this.getpodetails(this.selectedorderid);
      }
    }
  }


  getpodetails(ids) {
    // var a = [];
    var a = _.pluck(this.selectedorderid, "id")
    console.log("a", a)
    if (ids.length) {
      this.show_loader = true;
      if (this.grn.potype == "PO") {
        this.grnservice.GetPoDetails(a).subscribe(res => {
          this.show_loader = false;
          this.salesorderarray = res.data;
          _.map(this.salesorderarray, function (val) {
            val.recqty = 0;
            return val;
          })
          console.log("res", this.salesorderarray)
        })
      } else {
        this.grnservice.GetDCDetails(a).subscribe(res => {
          this.show_loader = false;
          this.salesorderarray = res.data;
          _.map(this.salesorderarray, function (val) {
            val.recqty = 0;
            return val;
          })
          console.log("res", this.salesorderarray)
        })
      }
    } else {
      this.salesorderarray = [];
    }
  }

  calcactqty(i) {
    if (this.salesorderarray[i].recqty < this.salesorderarray[i].pending_qty) {
      this.salesorderarray[i].actqty = this.salesorderarray[i].recqty
      return this.salesorderarray[i].actqty;
    } else {
      this.salesorderarray[i].actqty = this.salesorderarray[i].pending_qty
      return this.salesorderarray[i].actqty
    }
  }

  calc(i) {
    if (isUndefined(this.salesorderarray[i].recqty)) {
      return 0
    } else {
      if (this.salesorderarray[i].recqty > this.salesorderarray[i].pending_qty) {
        var excess = this.salesorderarray[i].recqty - this.salesorderarray[i].pending_qty;
        this.salesorderarray[i].excess = excess
        return excess
      } else {
        this.salesorderarray[i].excess = 0
        return 0
      }
    }
  }

  calcrec(i) {
    if (this.salesorderarray[i].rec_qty > this.salesorderarray[i].pending_qty) {
      this.salesorderarray[i].rec_qty = 0
    }
  }

  calcdcrec(i) {
    if (this.salesorderarray[i].recqty > this.salesorderarray[i].pending_qty) {
      this.salesorderarray[i].recqty = 0
    }
  }

  saveentry() {
    var validdata = true;
    var validationmsg = []
    if (!this.grn.supplier) {
      validdata = false;
      validationmsg.push("Please Select Supplier")
    }
    else if (!this.grn.pono.length) {
      validdata = false;
      validationmsg.push("Please Select PO/DC NO")
    }
    // else if (!this.grn.grnno) {
    //   validdata = false;
    //   validationmsg.push("Please Enter GRN NO")
    // }
    else if (!this.grn.dcno) {
      validdata = false;
      validationmsg.push("Please Enter Supplier INV/DC NO")
    }
    if (!this.grn.dcdate) {
      validdata = false;
      validationmsg.push("Please Select Supplier INV/DC Date")
    }
    else if (!this.grn.vehicleno) {
      validdata = false;
      validationmsg.push("Please Enter Vehicle No")
    } else {
      this.grndtls = []
      this.excesslist = [];
      for (var i = 0; i < this.salesorderarray.length; i++) {
        if (this.grn.potype == "PO") {
          var po = _.findWhere(this.pos, { 'id': this.salesorderarray[i].po_id })
        } else {
          var po = _.findWhere(this.pos, { 'id': this.salesorderarray[i].dc_id })
        }

        if (this.grn.potype == "PO") {
          this.obj =
            {
              "id": this.salesorderarray[i].id,
              "item_id": this.salesorderarray[i].item_id,
              "item_code": this.salesorderarray[i].item_code,
              "part": this.salesorderarray[i].part,
              "dept": this.salesorderarray[i].dept,
              "po_dc_date": this.salesorderarray[i].created_date,
              "po_dc_id": this.salesorderarray[i].po_id,
              "po_dc_no": po.po_no,
              "item_desc": this.salesorderarray[i].item_desc,
              "uom": this.salesorderarray[i].uom,
              "indent_id": this.salesorderarray[i].indent_id,
              "indent_no": this.salesorderarray[i].indent_no,
              "rate": this.salesorderarray[i].rate,
              "item_cost": this.salesorderarray[i].item_cost,
              "order_qty": this.salesorderarray[i].req_qty,
              "all_rec_qty": this.salesorderarray[i].all_rec_qty,
              "pending_qty": this.salesorderarray[i].pending_qty,
              "rec_qty": +this.salesorderarray[i].recqty,
              "act_rec_qty": +this.salesorderarray[i].actqty,
              "excess_qty": +this.salesorderarray[i].excess,
              "dc_qty": +this.salesorderarray[i].recqty,
              "remarks": this.salesorderarray[i].remarks,
            }
          if (+this.salesorderarray[i].excess != 0) {
            this.excesslist.push(this.obj)
          }
          if (this.salesorderarray[i].recqty != 0) {
            this.grndtls.push(this.obj)
          }
        } else {
          this.obj =
            {
              "id": this.salesorderarray[i].id,
              "item_id": this.salesorderarray[i].next_stage_part_id.id,
              "item_code": this.salesorderarray[i].next_stage_part_no,
              "part": this.salesorderarray[i].next_stage_part_id.component,
              "dept": this.salesorderarray[i].dept,
              "po_dc_date": this.salesorderarray[i].created_date,
              "po_dc_id": this.salesorderarray[i].dc_id,
              "po_dc_no": po.dc_no,
              "item_desc": this.salesorderarray[i].next_stage_part_id.component_desc,
              "uom": this.salesorderarray[i].next_stage_part_id.uom.uom_name,
              "indent_id": this.salesorderarray[i].indent_id,
              "indent_no": this.salesorderarray[i].indent_no,
              "rate": this.salesorderarray[i].rate,
              "order_qty": this.salesorderarray[i].qty,
              "all_rec_qty": this.salesorderarray[i].all_rec_qty,
              "pending_qty": this.salesorderarray[i].pending_qty,
              "rec_qty": +this.salesorderarray[i].recqty,
              "remarks": this.salesorderarray[i].remarks,
            }
          if (this.salesorderarray[i].recqty != 0) {
            this.grndtls.push(this.obj)
          }
        }
      }
    }
    if (this.grndtls) {
      if (!this.grndtls.length) {
        validdata = false;
        validationmsg.push("Enter Recieved Quantity")
      }
    }
    if (validdata) {
      if (this.excesslist.length) {
        this.supid = this.grn.supplier_id;
        this.showDialog = true
      } else {
        this.save()
      }
    } else {
      this.notif.warn('Warning', validationmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }

  save() {
    this.showDialog = false;
    var formdata =
    {
      "unit_id": this.unitid,
      "supplier_dc_no": this.grn.dcno,
      "bill_no": this.grn.billno,
      "vehicle_no": this.grn.vehicleno,
      "grn_type": this.grn.potype,
      // "grn_no": this.grn.grnno,
      "grn_date": this.grn.grndate.formatted,
      "supplier_dc_date": this.grn.dcdate.formatted,
      "bill_date": _.isEmpty(this.grn.billdate) ? null : this.grn.billdate.formatted,
      "vehicle_name": this.grn.vehname,
      "supplier": this.grn.supplier,
      "supplier_id": this.grn.supplier_id,
      "freight_amount": this.grn.freightamount,
      "courier_no": this.grn.courierno,
      "lr_no": this.grn.lrno,
      "lr_date": _.isEmpty(this.grn.lrdate) ? null : this.grn.lrdate.formatted,
      "weight": this.grn.weight,
      "grnidet": this.grndtls
    }
    console.log("formdata", formdata);
    this.show_loader = true;
    this.grnservice.CreateGRN(formdata).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.notif.success('Success', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
        this.resetform();
        this.router.navigate(['/grn'])
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  resetform() {
    this.grn = {};
    this.grn.pono = [];
    this.grn.dcdate = ""
    this.grn.billdate = ""
    this.grn.lrdate = ""
    this.grn.potype = "PO"
    this.salesorderarray = [];
    let date = new Date();
    this.alterdateformat(date);
  }

  close() {
    this.showDialog = false;
  }

}
