import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import * as __ from 'lodash';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { ChangeDetectorRef } from '@angular/core';
import { GrnService } from '../../services/grn/grn.service';
import { UserService } from '../../services/user/user.service'

@Component({
  selector: 'app-view-grn',
  templateUrl: './view-grn.component.html',
  styleUrls: ['./view-grn.component.css']
})
export class ViewGrnComponent implements OnInit {
  @Input() grnid: any
  @Output() notifycancel: EventEmitter<any> = new EventEmitter();
  userdetails: any;
  show_loader: boolean = false;
  toastertime: any;
  tosterminlength: any;
  persons: any;
  grnitems: any;
  allcontacts: any;
  supplier: any;
  constructor(
    private grnservice: GrnService,
    private router: Router,
    private notif: NotificationsService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private userservice: UserService
  ) {
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    console.log("grnid", this.grnid)
    this.getsuppliers()
    this.viewgrn();
  }

  getsuppliers() {
    this.userservice.getallContacts().subscribe(res => {
      if (res.status == "success") {
        this.allcontacts = res.data
      }
    })
  }

  viewgrn() {
    this.show_loader = true;
    this.grnservice.GetGRNBYID(this.grnid).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.supplier = res.data.supplier_addr
        this.persons = res.data
        this.grnitems = res.data.grnidet
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
    <head>
      <title>SAAPL Work order</title>
      <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
      <style>
      .print-view>tbody>tr>td,
      .print-view>tbody>tr>th,
      .print-view>tfoot>tr>td,
      .print-view>tfoot>tr>th,
      .print-view>thead>tr>td,
      .print-view>thead>tr>th {
        border: 1px solid #333 !important; padding:2px 3px !important
      }
      .print-view .print-label p { font-weight : 500 !important;}
      .print-view .print-label { font-weight : 500 !important;}
      .print-view .cs { background-color:#ccc !important;}
      .print-view .cs td { background-color:#ccc !important;}
       table.print-view td p {
       margin: 0!important;
      }
    table.report-container {
    page-break-after:always;
    }
    thead.report-header {
    display:table-header-group;
    }
    tfoot.report-footer {
    display:table-footer-group;
    } 
    </style>
    </head>
  <body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }

  cancel() {
    this.notifycancel.next();
  }

}
