import { Component, OnInit } from '@angular/core';
import { Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { MasterService } from '../services/master.service';
import { AppConstant } from '../app.constant';
import { NotifyService } from '../services/notify.service';
import { LoginService } from '../services/login/login.service';
import { CookieService } from 'angular2-cookie/core';
import { UserService } from '../services/user/user.service';

declare let swal: any;
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  unitarray: FormGroup;
  toastertime: number;
  toasterminlength: number;
  position_top_right: any;
  userdata: any;
  screendata: any;
  username: any;
  password: any;
  rememberme: any;
  cookie_url: any;
  unitlist: any;
  unit: any
  showDialog: boolean = false
  constructor(private fb: FormBuilder,
    private router: Router,
    private renderer: Renderer2,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private notifyService: NotifyService,
    private loginService: LoginService,
    private cookieService: CookieService,
    private userservice: UserService
  ) {
    this.position_top_right = { position: ['top', 'right'] }
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.toasterminlength = AppConstant.APP_CONST.toaster.minlength
    this.login = fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required],
      'remeberme': [null]
    });

    this.unitarray = fb.group({
      'unit': [null, Validators.required],
    });

    if (cookieService.get('remember')) {
      this.rememberme = this.cookieService.get('remember');
      if (this.rememberme == "true") {
        this.username = this.cookieService.get('username');
        var password = this.cookieService.get('password');
        this.password = atob(password)
      } else {
        this.rememberme = undefined
      }
    }
  }

  ngOnInit() {
    this.renderer.addClass(document.body, 'login-bg');
    this.renderer.removeClass(document.body, 'fixed-left');
  }
  ngOnDestroy() {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.renderer.removeClass(document.body, 'login-bg');
    this.renderer.addClass(document.body, 'fixed-left');
  }

  formObj: any = {
    username: {
      required: "user Name Required",
    },
    password: {
      required: "password Required"
    },
  }

  formObj2: any = {
    unit: {
      required: "Please select unit",
    }
  }


  loginform(data) {
    if (this.login.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.login, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.toasterminlength, position: ["top", "right"]
      })
      return false;
    }

    if (data) {
      var btn = $('#loadbtn')
      btn.button('loading')
      console.log("data", data)
      var formdata =
      {
        email: data.username,
        password: data.password
      }
      this.cookieService.put('username', formdata.email, { expires: "Mon, 30 Jun 2290 00:00:00 GMT" });
      this.cookieService.put('password', btoa(formdata.password), { expires: "Mon, 30 Jun 2290 00:00:00 GMT" });
      this.cookieService.put('remember', data.remeberme, { expires: "Mon, 30 Jun 2290 00:00:00 GMT" });

      this.loginService.checkLogin(formdata)
        .subscribe(res => {
          btn.button('reset')
          if (res.status == "success") {
            console.log("response", res);
            this.userdata = res.data;
            this.screendata = res.data1;

            if (this.userdata.password_flag == 1) {
              let self = this;
              let sts;
              swal({
                title: "Hi ! " + this.userdata.name + " Enter your New Password",
                input: "password",
                inputPlaceholder: "New Password",
                html: "<input class='swal2-input' type='password' id='oldpwd' placeholder='Old password'/>",
                showCancelButton: true,
                confirmButtonText: "Submit",
                showLoaderOnConfirm: true,
                // allowOutsideClick: false

                preConfirm: (pwd) => {
                  return new Promise((resolve) => {
                    setTimeout(() => {
                      var obj =
                      {
                        "userid": this.userdata.id,
                        "newpassword": pwd,
                        "oldpassword": $('#oldpwd').val()
                      }
                      this.userservice.changepwd(obj)
                        .subscribe(resp => {
                          console.log(resp);
                          sts = resp.status;
                          if (sts == 'success') {
                            swal({
                              type: 'success',
                              title: 'Password Changed!',
                            })
                          } else {
                            swal('Oops...', 'Password Updation Failed', 'warning');
                          }
                        });
                    }, 2000)
                  })
                },
              });
            } else {
              this.loginaccess()
            }
          }
          else {
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.toasterminlength, position: ["top", "right"]
            })
          }
        },
          (err) => {
            btn.button('reset')
            console.log("err", err)
            this.notif.error('No Service', "Please Try again Later", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.toasterminlength, position: ["top", "right"]
            })
          });
    }
  }

  loginaccess() {
    if (this.userdata.unitList.length == 1) {
      var prefix = "AUTH";
      localStorage.setItem(prefix, "true");
      localStorage.setItem("UserDetails", JSON.stringify(this.userdata));
      localStorage.setItem("LeftMenu", JSON.stringify(this.screendata));
      localStorage.setItem("UnitId", JSON.stringify(this.userdata.unitList[0].unit_id));
      var auth = localStorage.getItem('AUTH');
      if (auth == "true") {
        this.notifyService.showLoggedInUser(true);
        this.router.navigate(['../dashboard']);
      }
    } else {
      this.showDialog = true;
      var unitarray = this.masterservice.formatDataforDropdown("unit_name", "unit_id", this.userdata.unitList);
      this.unitlist = unitarray
    }
  }

  logintodash(unitid) {
    if (this.unitarray.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.unitarray, this.formObj2);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.toasterminlength, position: ["bottom", "right"]
      })
      return false;
    } else {
      var prefix = "AUTH";
      localStorage.setItem(prefix, "true");
      localStorage.setItem("UserDetails", JSON.stringify(this.userdata));
      localStorage.setItem("LeftMenu", JSON.stringify(this.screendata));
      localStorage.setItem("UnitId", JSON.stringify(unitid.unit));
      var auth = localStorage.getItem('AUTH');
      if (auth == "true") {
        this.notifyService.showLoggedInUser(true);
        this.router.navigate(['../dashboard']);
      }
    }
  }

  closepopup() {
    this.unitarray.reset()
    this.showDialog = false;
  }

  onForgotPassword() {
    let self = this;
    let sts;
    swal({
      title: "Forgot Password",
      html: "Don't worry! Enter your Registered email",
      input: "email",
      showCancelButton: true,
      confirmButtonText: "Submit",
      showLoaderOnConfirm: true,

      // preConfirm: (email) => {
      //   console.log("emailid", email)
      // },
      allowOutsideClick: false,

      preConfirm: (email) => {
        return new Promise((resolve) => {
          setTimeout(() => {
            this.userservice.ForgetPassword(email)
              .subscribe(resp => {
                console.log(resp);
                sts = resp.status;
                if (sts == 'success') {
                  swal({
                    type: 'success',
                    title: 'Request Accepted!',
                    html: 'check your email: ' + email + ' for further process'
                  })
                } else {
                  swal('Oops...', 'Enter valid email', 'warning');
                }
              });
            // resolve()
          }, 2000)
        })
      },

    })
  }

}
