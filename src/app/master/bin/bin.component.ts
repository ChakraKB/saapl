import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LocationService } from '../../services/location/location.service';
import { RackService } from '../../services/Rack/rack.service';
import { BinService } from '../../services/bin/bin.service';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
declare let swal: any;
class Person {
  bin: string
  description: string;
}
@Component({
  selector: 'app-bin',
  templateUrl: './bin.component.html',
  styleUrls: ['./bin.component.css']
})
export class BinComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  binform: FormGroup;
  locid: any;
  location: any;
  formdata: any;
  buttonname: string;
  status: any;
  rack: any;
  binid: any;
  bin: any;
  statusarray: any[];
  allloctions: any;
  activelocations: any;
  listofactiveloc: any;
  rackid: any;
  selectedlocation: any;
  allracks: any;
  activeracks: any;
  listofactiverack: any;
  selectedrack: any;
  response: any;
  description: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  created_date: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private binservice: BinService,
    private locationservice: LocationService,
    private rackservice: RackService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.binform = fb.group({
      'binid': [null],
      'location': [null, Validators.required],
      'rack': [null, Validators.required],
      'bin': [null, Validators.required],
      'description': [null, Validators.required],
      'status': [null, Validators.required]
    })
  }
  ngOnInit() {
    this.buttonname = "Save";
    this.status = 1;
    this.loadAngtable();
    this.loadlocations();
  }

  // ngAfterViewInit(): void {
  //   setTimeout(() => {
  //     this.dtTrigger.next();
  //   }, this.tabletimeout);
  // }

  loadAngtable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.BINS.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  loadBins() {
    this.persons = [];
    this.binservice.getBin()
      .subscribe(res => {
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  loadlocations() {
    this.locationservice.getLocation()
      .subscribe(res => {
        if (res) {
          this.allloctions = res.data
          console.log("allloctions", this.allloctions);
          this.activelocations = _.filter(this.allloctions, function (val) { return val.active_status == 1; });
          this.listofactiveloc = this.masterservice.formatDataforDropdown("location_name", "id", this.activelocations);
          console.log("listofactiveloc", this.listofactiveloc);
        }
      });
  }

  loadracks(data) {
    console.log(data)
    this.show_loader = true
    this.rackservice.getRackByLocation(data.value)
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.allracks = res.data
          // this.activeracks = _.filter(this.allracks, function (val) { return val.active_status == 1; });
          this.listofactiverack = this.masterservice.formatDataforDropdown("rack_name", "id", this.allracks);
          console.log("listofactiverack", this.listofactiverack);
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      });
  }


  updatebin(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.binid = data.id;
    this.bin = data.bin_name;
    this.description = data.description;
    this.binform.controls['location'].setValue(data.location.id);
    this.binform.controls['rack'].setValue(data.rack.id);
    this.status = data.active_status;
    this.created_date = data.crt_dt
  }

  deleteBin(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.binservice.deleteBin(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadBins();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })

  }

  formObj: any = {
    location:
      {
        required: "Location required",
      },
    rack:
      {
        required: "Rack required",
      },
    bin:
      {
        required: "Bin required",
      },
    description:
      {
        required: "Description required",
      },
    status:
      {
        required: "Status required",
      }
  }

  addeditRack(data) {
    if (this.binform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.binform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {

      this.selectedlocation = _.findWhere(this.listofactiveloc, { "value": data.location });
      this.selectedrack = _.findWhere(this.listofactiverack, { "value": data.rack });

      if (data.binid) {
        this.show_loader = true;
        this.formdata =
          {
            "id": data.binid,
            "bin_name": data.bin,
            "rack": { "id": this.selectedrack.value },
            "location": { "id": this.selectedlocation.value },
            "description": data.description,
            "active_status": data.status,
            'crt_dt': this.created_date,
          }
        console.log("editformdata ==>", this.formdata);
        this.binservice.updateBin(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadBins();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadBins();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            "bin_name": data.bin,
            "rack": { "id": this.selectedrack.value },
            "location": { "id": this.selectedlocation.value },
            "description": data.description,
            "active_status": data.status
          }
        console.log("addformdata ==> ", this.formdata);
        this.binservice.addBin(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadBins();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadBins();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }
  }

  resetform() {
    this.binform.reset();
    this.binform.controls['status'].setValue(1);
    this.buttonname = "Save"
  }

}
