import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { CountryService } from '../../services/country/country.service';
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
declare let swal: any;
class Person {
  dptcode: string;
  dptname: string;
}
@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  cityForm: FormGroup;
  toastertime: any;
  tosterminlength: any;
  dptcode: any;
  dptid: any;
  dptname: any;
  formdata: any;
  buttonname: any;
  status: any;
  statusarray: any;
  response: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  created_date: any;
  activestates: any;
  selectedstate: any;
  city: any = {};
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private countryservice: CountryService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.cityForm = fb.group({
      'id': [null],
      'state': [null, Validators.required],
      'cityname': [null, Validators.required],
      'status': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.city.status = 1;
    this.LoadAngTable();
    this.getActivestates();
  }

  LoadAngTable() {
    this.show_loader = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.CITY.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  getActivestates() {
    this.show_loader = true;
    this.countryservice.getStateByStatus("active").subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.activestates = this.masterservice.formatDataforDropdown("state_name", "id", res.data);
      }
    })
  }

  deletecity(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.countryservice.deleteCity(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllCities();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  getAllCities() {
    this.show_loader = true;
    this.persons = [];
    this.countryservice.getallCity()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }


  updatecity(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log('dpts', data);
    this.city.id = data.id;
    this.city.cityname = data.city_name;
    this.city.status = data.active_status;
    this.created_date = data.created_date;
    this.cityForm.controls['state'].setValue(data.state_id.id);
  }

  formObj: any = {

    state: {
      required: "State Required"
    },
    cityname: {
      required: "City Required",
    },
    status: {
      required: "Status Required"
    }
  }

  addeditcity(data) {
    if (this.cityForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.cityForm, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else {
      this.selectedstate = _.findWhere(this.activestates, { "value": data.state });

      if (data.id) {
        this.show_loader = true;
        this.formdata =
          {
            'id': data.id,
            'state_id': { 'id': this.selectedstate.value },
            'city_name': data.cityname,
            'active_status': data.status,
            'crt_dt': this.created_date,
          }
        console.log("editformdata", this.formdata)
        this.countryservice.updateCity(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllCities();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllCities();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          }, (err) => {
            this.show_loader = false;
            this.notif.error('Error', "Something Wrong", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            'state_id': { 'id': this.selectedstate.value },
            'city_name': data.cityname,
            'active_status': data.status
          }
        console.log("addformdata", this.formdata)
        this.countryservice.addCity(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllCities();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllCities();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }
  }

  resetform() {
    this.cityForm.reset();
    this.buttonname = "Save";
    this.cityForm.controls['status'].setValue(1);
  }

}
