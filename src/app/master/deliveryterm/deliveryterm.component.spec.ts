import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverytermComponent } from './deliveryterm.component';

describe('DeliverytermComponent', () => {
  let component: DeliverytermComponent;
  let fixture: ComponentFixture<DeliverytermComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverytermComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverytermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
