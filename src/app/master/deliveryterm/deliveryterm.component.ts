import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { UserService } from '../../services/user/user.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
declare let swal: any;
import { DataTableDirective } from 'angular-datatables';
class Person {
  role_name: string;
  description: string;
}

@Component({
  selector: 'app-deliveryterm',
  templateUrl: './deliveryterm.component.html',
  styleUrls: ['./deliveryterm.component.css']
})
export class DeliverytermComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  role_name: any;
  description: any;
  toastertime: number;
  tosterminlength: number;
  deliverytermform: FormGroup;
  id: any;
  formdata: any;
  buttonname: string;
  active_status: any;
  statusarray: any;
  response: any;
  allrole: any;
  role: any = {};
  data: any;
  tabletimeout: any;
  sampleform: FormGroup;
  show_loader: boolean = false;
  scrolltime: number;
  message: any = {};
  created_date: any;
  Paytype: any;
  termname: any;
  type: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private userservices: UserService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.Paytype = AppConstant.APP_CONST.PaymentTypes;
    this.deliverytermform = fb.group({
      'id': [null],
      'termname': [null, Validators.required],
      'description': [null, Validators.required],
      'active_status': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.active_status = 1;
    this.LoadAngTable();
  }

  getdelieryterms() {
    this.show_loader = true
    this.persons = [];
    this.userservices.getAllDeliveryTerms()
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.DELIVERYTERMS.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  updateDelterms(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.id = data.id
    this.termname = data.term;
    this.description = data.description;
    this.active_status = data.active_status;
    this.created_date = data.crt_dt
  }

  deleteDelterms(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.userservices.deleteDeliveryTerms(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response)
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getdelieryterms();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }


  formObj: any = {
    termname:
      {
        required: "Term required",
      },
    description:
      {
        required: "Description required",
      },
    active_status:
      {
        required: "Status required",
      }
  }

  addeditdeliveryterm(data) {
    if (this.deliverytermform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.deliverytermform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {
      if (data.id) {
        this.show_loader = true
        this.formdata =
          {
            "id": data.id,
            "term": data.termname,
            "description": data.description,
            "active_status": data.active_status,
            'crt_dt': this.created_date,
          }
        this.userservices.updateDeliveryTerms(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getdelieryterms();
                this.show_loader = false;
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getdelieryterms();
                this.show_loader = false;
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true
        this.formdata =
          {
            "term": data.termname,
            "description": data.description,
            "active_status": data.active_status,
          }
        console.log("addformdata ==> ", this.formdata)
        this.userservices.addDeliveryTerms(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getdelieryterms();
              });
              this.resetform();
              this.show_loader = false;
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getdelieryterms();
              });
              this.show_loader = false;
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })

            }
          });
      }
    }
  }

  resetform() {
    this.deliverytermform.reset();
    this.buttonname = "Save";
    this.deliverytermform.controls['active_status'].setValue(1);
  }

}
