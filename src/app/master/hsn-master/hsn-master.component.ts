import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HsnService } from '../../services/hsn/hsn.service';
import { Http, Response } from '@angular/http';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
declare let swal: any;
class Person {
  dptcode: string;
  dptname: string;
}
@Component({
  selector: 'app-hsn-master',
  templateUrl: './hsn-master.component.html',
  styleUrls: ['./hsn-master.component.css']
})
export class HsnMasterComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  statusarray: any;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  hsnform: FormGroup;
  scrolltime: number;
  yesorno: any;
  formdata: any;
  buttonname: any;
  show_loader: boolean = false;
  hsn: any = {};
  response: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private hsnservice: HsnService,
    private notif: NotificationsService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.yesorno = AppConstant.APP_CONST.YesOrNo;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.hsnform = fb.group({
      'id': [null],
      'edate': [null, Validators.required],
      'hsncode': [null, Validators.required],
      'proddesc': [null, Validators.required],
      'taxrate': [null, Validators.required],
      'sgstpercentage': [null, Validators.required],
      'cgstpercentage': [null, Validators.required],
      'revcharge': [null, Validators.required],
      'sgstval': [null, Validators.required],
      'cgstval': [null, Validators.required],
      'igstval': [null, Validators.required]
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.LoadAngTable();
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  LoadAngTable() {
    this.show_loader = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.HSN.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  getAllHsn() {
    this.show_loader = true;
    this.persons = [];
    this.hsnservice.getallHsn()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  deleteHsn(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.hsnservice.deleteHsn(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllHsn();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  limit(val, key) {
    if (val > 100) {
      switch (key) {
        case 'sgstpercentage':
          this.hsn.sgstpercentage = 0;
          break;
        case 'cgstpercentage':
          this.hsn.cgstpercentage = 0;
          break;
        case 'sgstval':
          this.hsn.sgstval = 0;
          break;
        case 'cgstval':
          this.hsn.cgstval = 0;
          break;
        case 'igstval':
          this.hsn.igstval = 0;
          break;
      }
    }
  }

  updateHsn(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log('dpts', data);
    this.hsn.id = data.id;
    this.hsn.edate = this.splitdate(data.effective_from);
    this.hsn.hsncode = data.hsn_code;
    this.hsn.proddesc = data.prod_description;
    this.hsn.taxrate = data.tax_rate;
    this.hsn.sgstpercentage = data.sgst_percent;
    this.hsn.cgstpercentage = data.cgst_percent;
    this.hsn.revcharge = data.rev_charge;
    this.hsn.sgstval = data.sgst;
    this.hsn.cgstval = data.cgst;
    this.hsn.igstval = data.igst;
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
      {
        date: {
          year: +newdate[2],
          month: +newdate[1],
          day: +newdate[0]
        },
        "formatted": d
      }
    console.log("day", altereddate)
    return altereddate
  }

  formObj: any = {
    edate:
      {
        required: "Effective date required",
      },
    hsncode:
      {
        required: "Hsncode required",
      },
    proddesc:
      {
        required: "Prod Description required",
      },
    taxrate:
      {
        required: "Tax rate required",
      },
    sgstpercentage:
      {
        required: "Sgst percentage required",
      },
    cgstpercentage:
      {
        required: "Cgst percentage required",
      },
    revcharge:
      {
        required: "Revcharge required",
      },
    sgstval:
      {
        required: "Sgst value required",
      },
    cgstval:
      {
        required: "cgst value required",
      },
    igstval:
      {
        required: "Igst value required",
      }

  }

  addedithsn(data) {
    if (this.hsnform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.hsnform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else {
      console.log("data", data)
      if (data.id) {
        this.formdata =
          {
            "id": data.id,
            "effective_from": data.edate.formatted,
            "hsn_code": data.hsncode,
            "prod_description": data.proddesc,
            "tax_rate": data.taxrate,
            "sgst_percent": data.sgstpercentage,
            "cgst_percent": data.cgstpercentage,
            "rev_charge": data.revcharge,
            "sgst": data.sgstval,
            "cgst": data.cgstval,
            "igst": data.igstval,
          }
        this.show_loader = true
        this.hsnservice.updateHsn(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllHsn();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllHsn();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      } else {
        this.formdata =
          {
            "effective_from": data.edate.formatted,
            "hsn_code": data.hsncode,
            "prod_description": data.proddesc,
            "tax_rate": data.taxrate,
            "sgst_percent": data.sgstpercentage,
            "cgst_percent": data.cgstpercentage,
            "rev_charge": data.revcharge,
            "sgst": data.sgstval,
            "cgst": data.cgstval,
            "igst": data.igstval,
          }
        this.show_loader = true
        this.hsnservice.addNewHsn(this.formdata).subscribe(res => {
          this.show_loader = false;
          if (res.status == "success") {
            console.log("res", res.data)
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.getAllHsn();
            });
            this.resetform();
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        })
      }

    }
  }

  resetform() {
    this.hsnform.reset();
    this.buttonname = "Save";
  }

}
