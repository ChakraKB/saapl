import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSupplierMapComponent } from './item-supplier-map.component';

describe('ItemSupplierMapComponent', () => {
  let component: ItemSupplierMapComponent;
  let fixture: ComponentFixture<ItemSupplierMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSupplierMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSupplierMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
