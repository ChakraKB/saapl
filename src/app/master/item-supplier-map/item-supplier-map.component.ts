import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'underscore';
import { ItemsService } from '../../services/items/items.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
declare let swal: any;
@Component({
  selector: 'app-item-supplier-map',
  templateUrl: './item-supplier-map.component.html',
  styleUrls: ['./item-supplier-map.component.css']
})
export class ItemSupplierMapComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  show_loader: boolean = false;
  showDialog : boolean = false;
  contactlist: any;
  allcontacts: any;
  filter: any = {};
  persons: any;
  checkeditems: any;
  contacttype: any;
  allsuppliers: any;
  toastertime: any;
  tosterminlength: any;

  constructor(
    private userservices: UserService,
    private masterservice: MasterService,
    private completerService: CompleterService,
    private itemservice: ItemsService,
    private notif: NotificationsService,
    private router: Router,
  ) {
    this.contacttype = AppConstant.APP_CONST.ContactTypes;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.getcontacts("Supplier");
    this.filter.type = 'Supplier'
    this.checkeditems = [];
  }

  showRateDialog: boolean = false;
  rates: any
  showprevpop(items) {
    this.showRateDialog = true;
    this.rates = items
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getcontacts(type) {
    this.show_loader = true;
    this.contactlist = [];
    this.userservices.getContactsByCat(type)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allsuppliers = res.data;
          this.persons = [];
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            });
          } else {
            this.dtTrigger.next();
          }
          this.allcontacts = this.masterservice.formatDataforDropdown("supplier_name", "id", res.data);
        }
      });
  }

  selectedcontact(data) {
    console.log("supplier", data)
  }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.sl_no === item.sl_no) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  uncheckall() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  getitems() {
    if (this.persons) {
      this.uncheckall();
    }
    if (this.filter.supplier) {
      // var suppid = _.findWhere(this.allsuppliers, { 'supplier_name': this.filter.supplier })
      this.show_loader = true;
      this.itemservice.getItemSuppliers(this.filter.supplier, null, _.isEmpty(this.filter.part) ? null : this.filter.part).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.persons = res.data;
          console.log("data", this.persons)
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      })
    } else {
      this.notif.warn('Warning', "Please select supplier", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  close() {
    this.showRateDialog = false
  }

  cancelsupplier(data) {
    console.log("id", data.id)
    swal({
      title: 'Are you sure?',
      text: "You want to Cancel Supplier rate!",
      type: 'question',
      width: 500,
      padding: 10,
      showCancelButton: true,
      confirmButtonColor: '#ffaa00',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes!',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.itemservice.deleteMapping(data.id).subscribe(res => {
          this.show_loader = false
          if (res.status == "success") {
            this.getitems();
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          this.show_loader = false
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        })
      }
    })
  }

  createmapping() {
    var valid: boolean = true;
    var validationMsg = [];
    var checkeditems = []
    if (!this.checkeditems.length) {
      valid = false;
      validationMsg.push("Please Select any Item")
    }
    this.checkeditems.forEach(val => {
      var obj =
        {
          "id": val.id,
          "rownum": val.sl_no,
          "supplier_id": val.supplier_id,
          "item": { 'id': val.item.id },
          "rate": val.rate,
        }
      if (val.rate == 0 || !val.rate) {
        valid = false;
        validationMsg.push("Please enter rate for " + val.item.part_no)
      } else {
        checkeditems.push(obj)
      }
    });

    if (valid) {
      var newlist = [];
      checkeditems.forEach(element => {
        if (element.id == 0) {
          var list = _.omit(element, ['id', 'checkauthorize']);
          newlist.push(list)
        } else {
          newlist.push(element)
        }
      });
      console.log("checkeditems", newlist)
      this.show_loader = true;
      this.itemservice.CreateMapping(newlist).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.getitems();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    }
    else {
      this.show_loader = false;
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }


}
