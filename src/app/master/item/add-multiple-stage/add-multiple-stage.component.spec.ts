import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMultipleStageComponent } from './add-multiple-stage.component';

describe('AddMultipleStageComponent', () => {
  let component: AddMultipleStageComponent;
  let fixture: ComponentFixture<AddMultipleStageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMultipleStageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMultipleStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
