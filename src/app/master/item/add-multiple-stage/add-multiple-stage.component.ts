import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../../services/master.service';
import { ItemsService } from '../../../services/items/items.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { CompleterService, CompleterData } from 'ng2-completer';
import { ValveService } from '../../../services/valve/valve.service';
import { TypeService } from '../../../services/types/type.service';
import { UomService } from '../../../services/uom/uom.service';
import { StageService } from '../../../services/stage/stage.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
declare let swal: any;
import * as _ from 'underscore';

@Component({
  selector: 'app-add-multiple-stage',
  templateUrl: './add-multiple-stage.component.html',
  styleUrls: ['./add-multiple-stage.component.css']
})
export class AddMultipleStageComponent implements OnInit {
  protected dataService: CompleterData;
  statusarray: any;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  multiitemForm: FormGroup;
  partform: FormGroup;
  scrolltime: number;
  activematerials: any;
  selectedmat: any;
  mspecno: any;
  mspecrev: any;
  partnos: any
  activeprocess: any
  allvalves: any;
  listofactivevalves: any;
  alluoms: any;
  listofactiveuoms: any;
  allstages: any;
  listofactivestages: any;
  alltypes: any;
  listofactiveTypes: any;
  show_loader: boolean = false;
  allprocess: any;
  multiitem: any = {};
  allcomp: any;
  selectedstag: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private itemservice: ItemsService,
    private uomservice: UomService,
    private completerService: CompleterService,
    private typeservice: TypeService,
    private stageservice: StageService,
    private valveservice: ValveService,
    private notif: NotificationsService,
    private router: Router
  ) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.multiitemForm = fb.group({
      'part': [null, Validators.required],
      'ptype': [null],
      'material': [null, Validators.required],
      'mspecno': [null],
      'mspecrev': [null],
      'series': [null, Validators.required],
      'status': [null, Validators.required]
    })
    this.partform = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.LoadActiveitem();
    this.getparts();
    this.loadmaterials();
    this.loadValves();
    this.addNewRow();
    this.loadUoms();
    this.loadStages();
    this.loadTypes();
    this.loadprocess();
    this.loadseries();
    this.loadclass();
    this.multiitem.ptype = "Spare";
    this.multiitem.status = 1;
  }

  allclass: any;
  activeclass: any;
  allseries: any;
  activeseries: any
  loadclass() {
    this.itemservice.GetItemClassByStatus('active').subscribe(res => {
      if (res.status == "success") {
        this.allclass = res.data
        this.activeclass = this.completerService.local(this.allclass, 'name', 'name');
      }
    })
  }

  loadseries() {
    this.itemservice.GetSeriesByStatus('active').subscribe(res => {
      if (res.status == "success") {
        this.allseries = res.data
        this.activeseries = this.masterservice.formatDataforDropdown("name", "id", this.allseries);
        console.log("series", this.allseries)
      }
    })
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getparts() {
    this.itemservice.getParts().subscribe(res => {
      if (res.status == "success") {
        var allcomponents = []
        var parts = res.data;
        _.forEach(parts, function (item, key) {
          var obj =
          {
            'label': item
          }
          allcomponents.push(obj)
        });
        this.allcomp = this.completerService.local(allcomponents, 'label', 'label');
        console.log("allcomp", this.allcomp)
      }
    })
  }

  getpartform(partform) {
    return partform.get('itemRows').controls
  }

  initItemRows() {
    return this.fb.group({
      partno: "",
      partdesc: "",
      partnorev: '-',
      valve: "",
      type: "",
      stage: "",
      process: "",
      processtype: "",
      purcchaselt: 0,
      processlt: 0,
      uom: "",
      dwgline: "",
      dwgno: "",
      rev: "",
      weight: 0,
      class: "",
      prevpartid: 0,
      prevpart: "",
    });
  }

  addNewRow() {
    const control = <FormArray>this.partform.controls["itemRows"];
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    var removestage = []
    if (this.partform.value.itemRows[index].prevpartid != 0) {
      removestage.push(this.partform.value.itemRows[index].prevpartid)
    } else {
      const control = <FormArray>this.partform.controls["itemRows"];
      control.removeAt(index);
    }
  }

  LoadActiveitem() {
    this.itemservice.getItemByStatus("active")
      .subscribe(res => {
        if (res) {
          this.partnos = this.completerService.local(res.data, "component,component_desc,component,part_no", 'component');
        }
      });
  }

  loadmaterials() {
    this.itemservice.getmatByStatus("active")
      .subscribe(res => {
        if (res) {
          this.activematerials = this.masterservice.formatDataforDropdown("material", "id", res.data);
          console.log("activematerials", this.activematerials)
        }
      });
  }

  loadTypes() {
    this.typeservice.getTypeByStatus("active")
      .subscribe(res => {
        if (res) {
          this.alltypes = res.data
          this.listofactiveTypes = this.completerService.local(res.data, 'type', 'type');
          console.log("listofactivetypes", this.listofactiveTypes);
        }
      });
  }

  loadValves() {
    this.valveservice.getValveByStatus("active")
      .subscribe(res => {
        if (res) {
          this.allvalves = res.data;
          this.listofactivevalves = this.completerService.local(res.data, 'sizes', 'sizes');
          // this.listofactivevalves = this.masterservice.formatDataforDropdown("sizes", "id", res.data);
          console.log("listofactivevalves", this.listofactivevalves);
        }
      });
  }

  loadStages() {
    this.stageservice.getstageByStatus("active")
      .subscribe(res => {
        if (res) {
          this.allstages = res.data
          this.listofactivestages = this.completerService.local(res.data, 'stage', 'stage');
          console.log("listofactivestages", this.listofactivestages);
        }
      });
  }

  loadUoms() {
    this.uomservice.getUomByStatus("active")
      .subscribe(res => {
        if (res) {
          this.alluoms = res.data
          this.listofactiveuoms = this.completerService.local(res.data, 'short_name', 'short_name');
          console.log("listofactiveuoms", this.listofactiveuoms);
        }
      });
  }

  loadprocess() {
    this.itemservice.getitemproByStatus("active")
      .subscribe(res => {
        if (res) {
          this.allprocess = res.data;
          this.activeprocess = this.completerService.local(res.data, 'process', 'process');
          console.log("activeprocess", this.activeprocess)
        }
      });
  }

  onmatSelected(data) {
    this.selectedmat = _.findWhere(this.activematerials, { value: data.value });
    console.log("selectedmat", this.selectedmat);
    this.multiitem.mspecno = this.selectedmat.others.mspec_no;
    this.multiitem.mspecrev = this.selectedmat.others.mspec_rev;
  }

  stageselected(data, index) {
    this.selectedstag = data.originalObject;
    let temp = this.fb.array([]);
    for (let i = 0; i < this.partform.value.itemRows.length; i++) {
      if (index == i) {
        temp.push(
          this.fb.group({
            partno: this.partform.value.itemRows[i].partno,
            partdesc: this.partform.value.itemRows[i].partdesc,
            partnorev: this.partform.value.itemRows[i].partnorev,
            valve: this.partform.value.itemRows[i].valve,
            type: this.partform.value.itemRows[i].type,
            stage: this.partform.value.itemRows[i].stage,
            process: this.selectedstag.process,
            processtype: this.selectedstag.type,
            purcchaselt: this.partform.value.itemRows[i].purcchaselt,
            processlt: this.partform.value.itemRows[i].processlt,
            uom: this.partform.value.itemRows[i].uom,
            dwgline: this.partform.value.itemRows[i].dwgline,
            dwgno: this.partform.value.itemRows[i].dwgno,
            rev: this.partform.value.itemRows[i].rev,
            weight: this.partform.value.itemRows[i].weight,
            class: this.partform.value.itemRows[i].class,
            prevpartid: this.partform.value.itemRows[i].prevpartid,
            prevpart: this.partform.value.itemRows[i].prevpart,
            qty: 0,
          })
        );
      } else {
        temp.push(
          this.fb.group(this.partform.value.itemRows[i]));
      }
    }
    this.partform = this.fb.group({
      itemRows: temp
    });
  }

  formObj: any = {
    part:
    {
      required: "part required",
    },
    material:
    {
      required: "material required",
    },
    series:
    {
      required: "Series required",
    }
  }

  addedititems(multiitem) {
    if (this.multiitemForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.multiitemForm, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else if (!this.partform.value.itemRows.length) {
      this.notif.warn('Warning', "Please add Item Details", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
    } else {
      var validdata: Boolean = true;
      var validationerrorMsg = [];
      var multiitems = []

      for (var i = 0; i < this.partform.value.itemRows.length; i++) {
        var valve = _.findWhere(this.allvalves, { "sizes": this.partform.value.itemRows[i].valve })
        var type = _.findWhere(this.alltypes, { "type": this.partform.value.itemRows[i].type })
        var stage = _.findWhere(this.allstages, { "stage": this.partform.value.itemRows[i].stage })
        var process = _.findWhere(this.allprocess, { "process": this.partform.value.itemRows[i].process })
        var uom = _.findWhere(this.alluoms, { "short_name": this.partform.value.itemRows[i].uom })
        var itemclass = _.findWhere(this.allclass, { "name": this.partform.value.itemRows[i].class })
        var itemseries = _.findWhere(this.allseries, { "id": multiitem.series })
        console.log("series", itemseries)
        var formdata =
        {
          'component': multiitem.part,
          'part_type': multiitem.ptype,
          'material': { 'id': multiitem.material },
          'material_spec_no': multiitem.mspecno,
          'material_spec_rev': multiitem.mspecrev,
          'series_id': { "id": multiitem.series },
          'series': itemseries.name,
          'active_status': multiitem.status,
          'part_no': this.partform.value.itemRows[i].partno,
          'component_desc': this.partform.value.itemRows[i].partdesc,
          'part_no_rev': this.partform.value.itemRows[i].partnorev,
          'valvesize': _.isEmpty(valve) ? "" : { "id": valve.id },
          'type': _.isEmpty(type) ? "" : { "id": type.id },
          'stage': _.isEmpty(stage) ? "" : { "id": stage.id },
          // 'process': _.isEmpty(process) ? null : { "id": process.id },
          'item_process': this.partform.value.itemRows[i].process,
          'process_type': this.partform.value.itemRows[i].processtype,
          'uom': _.isEmpty(uom) ? null : { "id": uom.id },
          'dwg_line': this.partform.value.itemRows[i].dwgline,
          'dwg_no': this.partform.value.itemRows[i].dwgno,
          'rev': this.partform.value.itemRows[i].rev,
          'weight': +this.partform.value.itemRows[i].weight,
          'class_id': _.isEmpty(this.partform.value.itemRows[i].class) ? null : { "id": itemclass.id },
          'cls': _.isEmpty(this.partform.value.itemRows[i].class) ? "" : itemclass.name,
          'pre_stg_part_no': this.partform.value.itemRows[i].prevpart,
          'pre_stg_part_id': this.partform.value.itemRows[i].prevpartid,
          'final_stage': 1
        }
        if (!this.partform.value.itemRows[i].partno) {
          validdata = false;
          validationerrorMsg.push("Please enter Part No");
        } else if (!this.partform.value.itemRows[i].partdesc) {
          validdata = false;
          validationerrorMsg.push("Please enter Part Desc");
        } else if (!this.partform.value.itemRows[i].valve) {
          validdata = false;
          validationerrorMsg.push("Please select valve");
        } else if (!this.partform.value.itemRows[i].type) {
          validdata = false;
          validationerrorMsg.push("Please select type");
        } else if (!this.partform.value.itemRows[i].stage) {
          validdata = false;
          validationerrorMsg.push("Please select stage");
        } else if (!this.partform.value.itemRows[i].uom) {
          validdata = false;
          validationerrorMsg.push("Please select uom");
        } else if (!this.partform.value.itemRows[i].uom) {
          validdata = false;
          validationerrorMsg.push("Please select uom");
        } else if (!this.partform.value.itemRows[i].class) {
          validdata = false;
          validationerrorMsg.push("Please enter class");
        } else {
          multiitems.push(formdata)
        }
      }
      console.log("multiitems", multiitems)
      if (validdata) {
        this.show_loader = true;
        this.itemservice.addMultipleItem(multiitems).subscribe(res => {
          this.show_loader = false;
          if (res.status == "success") {
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
            this.router.navigate(['/item']);
          } else {
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, (err => {
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }))
      } else {
        this.notif.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }
  }

  cancelitem() {
    var cancelitem = []
      this.router.navigate(['/item'])
  }


  resetform() {
    this.multiitemForm.reset();
  }

}
