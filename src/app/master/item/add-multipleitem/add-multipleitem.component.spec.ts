import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMultipleitemComponent } from './add-multipleitem.component';

describe('AddMultipleitemComponent', () => {
  let component: AddMultipleitemComponent;
  let fixture: ComponentFixture<AddMultipleitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMultipleitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMultipleitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
