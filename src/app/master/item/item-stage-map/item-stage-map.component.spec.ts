import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemStageMapComponent } from './item-stage-map.component';

describe('ItemStageMapComponent', () => {
  let component: ItemStageMapComponent;
  let fixture: ComponentFixture<ItemStageMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemStageMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemStageMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
