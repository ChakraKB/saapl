import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../../services/master.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ItemsService } from '../../../services/items/items.service';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
import { ChangeDetectorRef } from '@angular/core';
declare let swal: any;

@Component({
  selector: 'app-item-stage-map',
  templateUrl: './item-stage-map.component.html',
  styleUrls: ['./item-stage-map.component.css']
})
export class ItemStageMapComponent implements OnInit {
  toastertime: number;
  tosterminlength: number;
  itemForm: FormGroup
  user: any;
  show_loader: boolean = false;
  items: any;
  allitems: any;
  casting_partid: any;
  casting_partno: any;
  nxt_stage_items: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private itemservice: ItemsService,
    private completerService: CompleterService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.user = JSON.parse(localStorage.getItem("UserDetails"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.itemForm = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.getItems();
    this.addNewRow();
  }

  initItemRows() {
    return this.fb.group({
      partid: 0,
      partno: "",
      part: "",
      partdesc: "",
      material: "",
      dwgno: ""
    });
  }

  addNewRow() {
    const control = <FormArray>this.itemForm.controls["itemRows"];
    control.push(this.initItemRows());
    console.log("control", this.itemForm)
  }
  deleteRow(index: number) {
    const control = <FormArray>this.itemForm.controls["itemRows"];
    control.removeAt(index);
  }

  getform(form) {
    return form.get('itemRows').controls
  }

  getItems() {
    this.show_loader = true;
    this.itemservice.getItemByStatus("active").subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.allitems = res.data
        this.items = this.completerService.local(res.data, "component_desc,component,part_no", 'part_no');
      }
    })
  }

  selectpart(data) {
    if (data) {
      var item = data.originalObject
      this.casting_partid = item.id
    }
  }

  selectitems(data, index) {
    var selectedpart = data.originalObject;
    let temp = this.fb.array([]);

    for (let i = 0; i < this.itemForm.value.itemRows.length; i++) {
      if (index == i) {
        temp.push(
          this.fb.group({
            partid: selectedpart.id,
            part: selectedpart.component,
            partno: selectedpart.part_no,
            partdesc: selectedpart.component_desc,
            material: selectedpart.material.material,
            dwgno: selectedpart.dwg_no,
          })
        );
      } else {
        temp.push(
          this.fb.group(this.itemForm.value.itemRows[i]));
      }
    }
    this.itemForm = this.fb.group({
      itemRows: temp
    });
  }

  mapitem() {
    var validata: boolean = true
    var validationmsg = [];
    var nxt_stage_items = []
    if (!this.casting_partno) {
      validata = false;
      validationmsg.push("Please Select Casting Part No")
    }
    _.forEach(this.itemForm.value.itemRows, function (val) {
      var obj =
      {
        "id": val.partid,
        "part_no": val.partno
      }
      if (!val.partid) {
        validata = false;
        validationmsg.push("Please Select Next Stage Part No")
      } else {
        nxt_stage_items.push(obj)
      }
    })
    console.log("nxt_stage_items", nxt_stage_items)

    if (validata) {
      var formdata =
      {
        "prev_stage_part_id": this.casting_partid,
        "prev_stage_part_no": this.casting_partno,
        "items": nxt_stage_items
      }
      console.log("formdata", formdata);
      this.show_loader = true;
      this.itemservice.UpdateItemStage(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.resetform();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      })
    } else {
      this.notif.warn('Warning', validationmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  resetform() {
    this.casting_partno = "";
    this.itemForm.reset();
    this.itemForm = this.fb.group({
      itemRows: this.fb.array([])
    });
    this.addNewRow()
  }
}
