import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ItemsService } from '../../../services/items/items.service';
import * as _ from "underscore";
@Component({
  selector: 'app-item-view',
  templateUrl: './item-view.component.html',
  styleUrls: ['./item-view.component.css']
})

export class ItemViewComponent implements OnInit {
  itemid: any;
  selecteditemdtl: any;
  prodassbom: any;
  show_loader: boolean = false;
  showDialog: boolean = false;
  @Input() rowIndex: number;
  files2: any
  constructor(
    private itemservice: ItemsService,
    private acroute: ActivatedRoute,
  ) {
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.itemid = params.id;
        console.log("urlparams", this.itemid);
      }
    });
  }

  ngOnInit() {
    this.loaditemdetails();

    this.files2 = [
      {
        "data": {
          "name": "Documents",
          "size": "75kb",
          "type": "Folder"
        },
        "children": [
          {
            "data": {
              "name": "Work",
              "size": "55kb",
              "type": "Folder"
            },
            "children": [
              {
                "data": {
                  "name": "Expenses.doc",
                  "size": "30kb",
                  "type": "Document"
                }
              },
              {
                "data": {
                  "name": "Resume.doc",
                  "size": "25kb",
                  "type": "Resume"
                }
              }
            ]
          },
          {
            "data": {
              "name": "Home",
              "size": "20kb",
              "type": "Folder"
            },
            "children": [
              {
                "data": {
                  "name": "Invoices",
                  "size": "20kb",
                  "type": "Text"
                }
              }
            ]
          }
        ]
      },
      {
        "data": {
          "name": "Pictures",
          "size": "150kb",
          "type": "Folder"
        },
        "children": [
          {
            "data": {
              "name": "barcelona.jpg",
              "size": "90kb",
              "type": "Picture"
            }
          },
          {
            "data": {
              "name": "primeui.png",
              "size": "30kb",
              "type": "Picture"
            }
          },
          {
            "data": {
              "name": "optimus.jpg",
              "size": "30kb",
              "type": "Picture"
            }
          }
        ]
      }
    ]

  }

  loaditemdetails() {
    this.show_loader = true
    this.itemservice.getByID(this.itemid)
      .subscribe(res => {
        this.show_loader = false;
        this.selecteditemdtl = res.data;
        this.loaditemtree();
        console.log("subassbom", this.selecteditemdtl);
      });
  }

  showpop() {
    this.showDialog = true;
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Item BOM</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  loaditemtree() {
    this.show_loader = true;
    this.itemservice.viewByID(this.itemid)
      .subscribe(res => {
        this.show_loader = false;
        this.prodassbom = res.data;
        console.log("prodassbom", JSON.stringify(this.prodassbom));
      });
  }

}
