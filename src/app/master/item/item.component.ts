import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ItemsService } from '../../services/items/items.service';
import { StageService } from '../../services/stage/stage.service';
import { TypeService } from '../../services/types/type.service';
import { ValveService } from '../../services/valve/valve.service';
import { UomService } from '../../services/uom/uom.service';
import { Http, Response } from '@angular/http';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
import { ChangeDetectorRef } from '@angular/core';
declare let swal: any;
class Person {
  bin: string
  description: string;
}
class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}
@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  itemForm: FormGroup;
  roleid: any;
  formdata: any;
  buttonname: string;
  status: any;
  statusarray: any;
  itemno: any;
  partno: any;
  partnorev: any;
  itype: any;
  component: any;
  valvesize: any;
  dwgline: any;
  compdesc: any;
  material: any;
  process: any;
  mspecno: any;
  mspecrev: any;
  stage: any;
  uom: any;
  dwgno: any;
  rev: any;
  weight: any;
  typelist: any;
  itemid: any;
  selectedtype: any;
  selectedcomponent: any;
  selectedvalve: any;
  selectedstage: any;
  selecteduom: any;
  response: any;
  description: any;
  alltypes: any;
  allstages: any;
  allvalves: any;
  alluoms: any;
  listofactivevalves: any;
  listofactiveuoms: any;
  listofactivestages: any
  listofactiveTypes: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  created_date: any;
  ptypes: any;
  listofPTypes: any;
  spareform: FormGroup;
  protected dataService: CompleterData;
  activematerials: any;
  activeprocess: any;
  selectedpart: any;
  selectedptype: any;
  ptype: any;
  itembomarray: any;
  itemdtl: any;
  save: boolean = true;
  prevpart: boolean = false;
  previous: any;
  spare: boolean = true;
  isSpare: boolean = true;
  showDialog: boolean = false;
  showProDialog: boolean = false;
  data: any;
  savedpart: any;
  cancelclick: boolean = true;
  subassbom: any;
  prodassbom: any;
  prodassbomdtl: any;
  selectedItem: any;
  selecteditemdtl: any;
  selectedprocess: any;
  selectedmaterial: any;
  allcomp: any;
  deleteoptions: any;
  selectedmat: any;
  purchaselt: any;
  processlt: any;
  allowdecimal: boolean = false
  role: any;
  user: any;
  series: any;
  class: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private uomservice: UomService,
    private typeservice: TypeService,
    private stageservice: StageService,
    private valveservice: ValveService,
    private itemservice: ItemsService,
    private completerService: CompleterService,
    private cdRef: ChangeDetectorRef,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.ptypes = AppConstant.APP_CONST.PTYPE;
    this.user = JSON.parse(localStorage.getItem("UserDetails"));
    this.role = this.user.role.role_name
    this.itemForm = fb.group({
      'itemid': [null],
      'partno': [null, Validators.required],
      'partnorev': [null, Validators.required],
      'component': [null, Validators.required],
      'compdesc': [null, Validators.required],
      'ptype': [null, Validators.required],
      'valvesize': [null, Validators.required],
      'itype': [null, Validators.required],
      'dwgline': [null],
      'material': [null, Validators.required],
      'mspecno': [null],
      'mspecrev': [null, Validators.required],
      'process': [null],
      'processtype': [null],
      'purchaselt': [null, Validators.required],
      'processlt': [null, Validators.required],
      'stage': [null, Validators.required],
      'uom': [null, Validators.required],
      'dwgno': [null],
      'rev': [null],
      'weight': [null, Validators.required],
      'series': [null, Validators.required],
      'class': [null, Validators.required],
      'previous': [null],
      'previous_id': [null],
      'status': [null, Validators.required],
    })
    this.spareform = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.weight = 0
    this.purchaselt = 0;
    this.processlt = 0;
    this.buttonname = "Save"
    this.LoadActiveitem();
    this.getparts();
    this.loadTypes();
    this.loadStages();
    this.loadValves();
    this.loadUoms();
    this.loadmaterials();
    this.loadprocess();
    this.loadseries();
    this.loadclass();
    this.addNewRow();
    this.loadAngtable();
    this.save = true;
    this.partnorev = "-";
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  parts: any
  getparts() {
    this.itemservice.getParts().subscribe(res => {
      if (res.status == "success") {
        var allcomponents = []
        this.parts = res.data;
        _.forEach(this.parts, function (item, key) {
          var obj =
          {
            'label': item
          }
          allcomponents.push(obj)
        });
        this.allcomp = this.completerService.local(allcomponents, 'label', 'label');
        console.log("allcomp", this.allcomp)
      }
    })
  }
  allseries: any
  activeseries: any
  allclass: any
  activeclass: any
  loadseries() {
    this.show_loader = true;
    this.itemservice.GetSeriesByStatus('active').subscribe(res => {
      if (res.status == "success") {
        this.allseries = res.data
        this.activeseries = this.masterservice.formatDataforDropdown("name", "id", res.data);
      }
    })
  }

  loadclass() {
    this.show_loader = true;
    this.itemservice.GetItemClassByStatus('active').subscribe(res => {
      if (res.status == "success") {
        this.allclass = res.data
        this.activeclass = this.masterservice.formatDataforDropdown("name", "id", res.data);
      }
    })
  }

  getform(spareform) {
    return spareform.get('itemRows').controls
  }

  onmatSelected(data) {
    this.selectedmat = _.findWhere(this.activematerials, { value: data.value });
    console.log("selectedmat", this.selectedmat);
    this.mspecno = this.selectedmat.others.mspec_no;
    this.mspecrev = this.selectedmat.others.mspec_rev;
  }
  selectedstag: any;
  processtype: any;

  onstageSelected(data) {
    this.selectedstag = _.findWhere(this.allstages, { id: data.value });
    console.log("selectedstage", this.selectedstag);
    this.process = this.selectedstag.process;
    this.processtype = this.selectedstag.type;
  }
  numberonly(event) {
    this.masterservice.allowNumberOnly(event);
  }

  initItemRows() {
    return this.fb.group({
      id: 0,
      itemid: 0,
      partno: "",
      partdesc: "",
      dwgline: "",
      parttype: "",
      partid: "",
      spare: "",
      uom: "",
      qty: 0,
    });
  }

  addNewRow() {
    const control = <FormArray>this.spareform.controls["itemRows"];
    control.push(this.initItemRows());
    console.log("control", this.spareform)
  }
  deleteRow(index: number) {
    const control = <FormArray>this.spareform.controls["itemRows"];
    control.removeAt(index);
  }

  selectprev(data) {
    if (data) {
      if (data.originalObject.id == this.itemid) {
        this.notif.warn('Warning', "you can't select parent part no", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
        this.itemForm.controls['previous'].setValue("");
        this.itemForm.value.previous = ""
      }
      else {
        this.itemForm.controls['previous_id'].setValue(data.originalObject.id);
        this.itemForm.value.previous_id = data.originalObject.id
      }
    }
  }

  selectedspare(partno, index) {
    this.selectedpart = partno.originalObject;
    console.log("part", this.selectedpart);
    if (this.selectedpart.id == this.itemid) {
      this.notif.warn('Warning', "you can't select parent part no", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
    if (this.selectedpart.uom.decimal_allowed == 1) {
      this.allowdecimal = true;
    } else {
      this.allowdecimal = false;
    }
    let temp = this.fb.array([]);
    for (let i = 0; i < this.spareform.value.itemRows.length; i++) {
      if (index == i) {
        temp.push(
          this.fb.group({
            id: this.spareform.value.itemRows[i].id,
            itemid: this.spareform.value.itemRows[i].itemid,
            partno: this.spareform.value.itemRows[i].partno,
            partdesc: this.selectedpart.component_desc,
            dwgline: this.selectedpart.dwg_line,
            parttype: this.selectedpart.part_type,
            partid: this.selectedpart.id,
            spare: this.spareform.value.itemRows[i].spare,
            uom: this.selectedpart.uom.short_name,
            qty: 0,
          })
        );
      } else {
        temp.push(
          this.fb.group(this.spareform.value.itemRows[i]));
      }
    }
    this.spareform = this.fb.group({
      itemRows: temp
    });
  }
  getsubassdetails(data, i) {
    this.itemservice.getByID(data.partid)
      .subscribe(res => {
        this.selecteditemdtl = res.data;
        console.log("subassbom", this.selecteditemdtl);
      });
    this.showDialog = true;
    this.show_loader = true;
    this.itemservice.viewByID(data.partid)
      .subscribe(res => {
        this.show_loader = false;
        this.subassbom = res.data;
        console.log("subassbom", this.subassbom);
      });
  }

  getprodetails(data) {
    this.itemservice.viewByID(data.id)
      .subscribe(res => {
        this.showProDialog = true;
        this.prodassbom = res.data;
        console.log("prodassbom", JSON.stringify(this.prodassbom));
      });
  }

  getsubassprod(data, i) {
    this.itemservice.getByID(data.child_part_id)
      .subscribe(res => {
        this.prodassbomdtl = res.data.itembom;
        console.log("prodassbomdtl", this.prodassbomdtl);
      });
  }

  showform() {
    $("#itemform").slideDown();
    $("#listtable").slideUp();
    this.itemForm.controls['weight'].setValue(0);
    this.itemForm.controls['purchaselt'].setValue(0);
    this.itemForm.controls['processlt'].setValue(0);
    this.save = true;
    this.status = 1;
    this.cancelclick = false;
    this.partnorev = "-"
  }

  fixdash() {
    if (_.isEmpty(this.partnorev)) {
      this.partnorev = "-"
    }
  }

  loadAngtable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.ITEM.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }


  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  LoadItems() {
    this.show_loader = true;
    this.persons = [];
    this.itemservice.getItem()
      .subscribe(res => {
        if (res.status == "success") {
          this.persons = res.data
          this.dtTrigger.next();
          this.show_loader = false;
        }
      });
  }

  LoadActiveitem() {
    this.itemservice.getItemByStatus("active")
      .subscribe(res => {
        if (res) {
          this.dataService = this.completerService.local(res.data, "component_desc,component,part_no", 'part_no');
        }
      });
  }

  loadmaterials() {
    this.itemservice.getmatByStatus("active")
      .subscribe(res => {
        if (res) {
          this.activematerials = this.masterservice.formatDataforDropdown("material", "id", res.data);
          console.log("activematerials", this.activematerials)
        }
      });
  }

  loadprocess() {
    this.itemservice.getitemproByStatus("active")
      .subscribe(res => {
        if (res) {
          this.activeprocess = this.masterservice.formatDataforDropdown("process", "id", res.data);
          console.log("activeprocess", this.activeprocess)
        }
      });
  }

  onpartypeselect(type) {
    this.LoadActiveitem();
    this.selectedptype = type
    if (type != "Spare") {
      if (this.spareform.value.itemRows.length == 0) {
        this.addNewRow()
      }
      $("#spareform").slideDown();
    } else {
      $("#spareform").slideUp();
      this.spareform = this.fb.group({
        itemRows: this.fb.array([])
      });
    }
  }

  loadTypes() {
    this.typeservice.getTypeByStatus("active")
      .subscribe(res => {
        if (res) {
          this.alltypes = res.data
          this.listofactiveTypes = this.masterservice.formatDataforDropdown("type", "id", this.alltypes);
          console.log("listofactivetypes", this.listofactiveTypes);
        }
      });
  }

  loadStages() {
    this.stageservice.getstageByStatus("active")
      .subscribe(res => {
        if (res) {
          this.allstages = res.data
          this.listofactivestages = this.masterservice.formatDataforDropdown("stage", "id", this.allstages);
          console.log("listofactivestages", this.listofactivestages);
        }
      });
  }

  loadValves() {
    this.valveservice.getValveByStatus("active")
      .subscribe(res => {
        if (res) {
          this.allvalves = res.data
          this.listofactivevalves = this.masterservice.formatDataforDropdown("sizes", "id", this.allvalves);
          console.log("listofactivevalves", this.listofactivevalves);
        }
      });
  }

  loadUoms() {
    this.uomservice.getUomByStatus("active")
      .subscribe(res => {
        if (res) {
          this.alluoms = res.data
          this.listofactiveuoms = this.masterservice.formatDataforDropdown("short_name", "id", this.alluoms);
          console.log("listofactiveuoms", this.listofactiveuoms);
        }
      });
  }

  copyitem(data) {
    this.save = true;
    this.show_loader = true;
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.showform();
    this.buttonname = "Save"
    console.log("item", data);
    this.partnorev = data.part_no_rev;
    this.itemForm.controls['itype'].setValue(data.type.id);
    this.ptype = data.part_type;
    this.itemForm.controls['valvesize'].setValue(data.valvesize.id);
    this.dwgline = data.dwg_line;
    this.material = data.material;
    this.purchaselt = data.purchase_lead_time;
    this.processlt = data.process_lead_time;
    this.mspecno = data.material_spec_no;
    this.mspecrev = data.material_spec_rev;
    this.process = data.item_process;
    this.processtype = data.process_type;
    this.itemForm.controls['material'].setValue(data.material.id);
    if (data.process) {
      this.itemForm.controls['process'].setValue(data.process.id);
    }
    this.itemForm.controls['stage'].setValue(data.stage.id);
    this.itemForm.controls['uom'].setValue(data.uom.id);
    this.dwgno = data.dwg_no;
    this.rev = data.rev;
    this.weight = data.weight;
    this.series = data.series;
    this.class = data.cls;
    if (data.pre_stg_part_no) {
      this.prevpart = true;
      this.itemForm.controls['previous'].setValue(data.pre_stg_part_no);
      this.itemForm.controls['previous_id'].setValue(data.pre_stg_part_id);
    } else this.prevpart = false;
    this.status = data.active_status;
    this.created_date = data.crt_dt
    let temp = this.fb.array([]);
    this.itemservice.getByID(data.id)
      .subscribe(res => {
        this.show_loader = false;
        this.itemdtl = res.data;
        console.log("itembyid", this.itemdtl);
        if (!_.isEmpty(this.itemdtl.itembom)) {
          $("#spareform").slideDown();
          this.itembomarray = this.itemdtl.itembom
          for (let i = 0; i < this.itembomarray.length; i++) {
            temp.push(
              this.fb.group({
                id: parseInt(this.itembomarray[i].id),
                itemid: parseInt(this.itembomarray[i].item_id),
                partno: this.itembomarray[i].part_no,
                partid: this.itembomarray[i].child_part_id,
                partdesc: this.itembomarray[i].child_part_desc,
                dwgline: this.itembomarray[i].child_dwg_line,
                spare: this.itembomarray[i].child_part_no,
                parttype: this.itembomarray[i].child_part_type,
                uom: this.itembomarray[i].uom,
                qty: this.itembomarray[i].quantity,
              })
            );
          }
          this.spareform = this.fb.group({
            itemRows: temp
          });
        }
      });
  }

  updateitem(data) {
    this.save = false;
    this.show_loader = true;
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.showform();
    this.buttonname = "Update"
    console.log("item", data);
    this.itemid = data.id;
    this.itemno = data.item_no;
    this.partno = data.part_no;
    this.partnorev = data.part_no_rev;
    this.itemForm.controls['itype'].setValue(data.type.id);
    this.component = data.component;
    this.ptype = data.part_type;
    this.itemForm.controls['valvesize'].setValue(data.valvesize.id);
    this.dwgline = data.dwg_line;
    this.compdesc = data.component_desc;
    this.material = data.material;
    this.purchaselt = data.purchase_lead_time;
    this.processlt = data.process_lead_time;
    this.mspecno = data.material_spec_no;
    this.mspecrev = data.material_spec_rev;
    this.process = data.item_process;
    this.processtype = data.process_type;
    this.itemForm.controls['material'].setValue(data.material.id);
    if (data.process) {
      this.itemForm.controls['process'].setValue(data.process.id);
    }
    this.itemForm.controls['stage'].setValue(data.stage.id);
    this.itemForm.controls['uom'].setValue(data.uom.id);
    this.dwgno = data.dwg_no;
    this.rev = data.rev;
    this.weight = data.weight;
    this.itemForm.controls['series'].setValue(data.series_id.id);
    this.itemForm.controls['class'].setValue(data.class_id.id);
    // this.series = data.series;
    // this.class = data.cls;
    if (data.pre_stg_part_no) {
      this.prevpart = true;
      this.itemForm.controls['previous'].setValue(data.pre_stg_part_no);
      this.itemForm.controls['previous_id'].setValue(data.pre_stg_part_id);
    } else this.prevpart = false;
    this.status = data.active_status;
    this.created_date = data.crt_dt
    let temp = this.fb.array([]);
    this.itemservice.getByID(data.id)
      .subscribe(res => {
        this.show_loader = false;
        this.itemdtl = res.data;
        console.log("itembyid", this.itemdtl);
        if (!_.isEmpty(this.itemdtl.itembom)) {
          $("#spareform").slideDown();
          this.itembomarray = this.itemdtl.itembom
          for (let i = 0; i < this.itembomarray.length; i++) {
            temp.push(
              this.fb.group({
                id: parseInt(this.itembomarray[i].id),
                itemid: parseInt(this.itembomarray[i].item_id),
                partno: this.itembomarray[i].part_no,
                partdesc: this.itembomarray[i].child_part_desc,
                dwgline: this.itembomarray[i].child_dwg_line,
                partid: this.itembomarray[i].child_part_id,
                spare: this.itembomarray[i].child_part_no,
                parttype: this.itembomarray[i].child_part_type,
                uom: this.itembomarray[i].uom,
                qty: this.itembomarray[i].quantity,
              })
            );
          }
          this.spareform = this.fb.group({
            itemRows: temp
          });
        }
      });
  }

  deleteitem(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.itemservice.deleteItem(id)
          .subscribe(res => {
            this.show_loader = false
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.LoadItems();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  formObj: any = {

    itemno: {
      required: "Item No Required",
    },
    partno: {
      required: "Part No Required"
    },
    partnorev: {
      required: "Part No Rev Required"
    },
    component: {
      required: "Part Required",
    },
    compdesc: {
      required: "Part Desc Required",
    },
    ptype: {
      required: "Part Type Required"
    },
    valvesize: {
      required: "Valve size Required"
    },
    itype: {
      required: "Type Required",
    },
    material: {
      required: "Material Required",
    },
    process: {
      required: "Process Required",
    },
    mspecrev: {
      required: "Mspecrev Required",
    },
    stage: {
      required: "Stage Required"
    },
    uom: {
      required: "Uom Required"
    },
    weight: {
      required: "Enter 0 if no weight"
    },
    series: {
      required: "Series Required"
    },
    class: {
      required: "Class Required"
    },
    purchaselt: {
      required: "Enter 0 if no purchase lead time"
    },
    processlt: {
      required: "Enter 0 if no process lead time"
    },
    status: {
      required: "Status Required"
    },
  }
  selectedseries: any;
  selectedclass: any
  addedititem(data, flag) {
    if (this.itemForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.itemForm, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
      return false;
    }
    else {
      var validata: Boolean = true;
      var validationmsg = [];
      this.selectedtype = _.findWhere(this.listofactiveTypes, { "value": data.itype });
      this.selectedvalve = _.findWhere(this.listofactivevalves, { "value": data.valvesize });
      this.selectedmaterial = _.findWhere(this.activematerials, { "value": data.material });
      this.selectedprocess = _.findWhere(this.activeprocess, { "value": data.process });
      this.selectedstage = _.findWhere(this.listofactivestages, { "value": data.stage });
      this.selecteduom = _.findWhere(this.listofactiveuoms, { "value": data.uom });
      this.selectedseries = _.findWhere(this.allseries, { "id": data.series });
      this.selectedclass = _.findWhere(this.allclass, { "id": data.class });
      var sparedetails = [];
      if (this.ptype != "Spare") {
        if (this.spareform.value.itemRows.length != 0) {
          _.forEach(this.spareform.value.itemRows, function (item, key) {
            var sparelineitem =
            {
              "child_part_id": item.partid,
              "child_part_no": item.spare,
              "child_part_desc": item.partdesc,
              "child_dwg_line": item.dwgline,
              "child_part_type": item.parttype,
              "uom": item.uom,
              "quantity": item.qty,
            }
            if (_.isEmpty(item.spare)) {
              validata = false;
              validationmsg.push("Please select Spare");
            } else if (item.qty == 0) {
              validata = false;
              validationmsg.push("Please enter qty");
            } else if (item.partno == item.spare) {
              validata = false;
              validationmsg.push("You can't select parent part no");
            }
            else {
              sparedetails.push(sparelineitem);
            }
          });
        } else {
          validata = false;
          validationmsg.push("Please add bom details");
        }

        console.log("sparedetails", sparedetails)
      }
      if (data.itemid) {
        if (data.previous_id == data.itemid) {
          validata = false;
          validationmsg.push("Your Part no  & Prev Part No was Same");
        }
        if (validata) {
          this.formdata =
            {
              'id': data.itemid,
              'item_no': data.itemno,
              'part_no': data.partno,
              'part_no_rev': data.partnorev,
              'type': { "id": this.selectedtype.value },
              'part_type': data.ptype,
              'material': { "id": this.selectedmaterial.value },
              // 'process': _.isEmpty(this.selectedprocess) ? null : { "id": this.selectedprocess.value },
              'item_process': this.process,
              'process_type': this.processtype,
              'purchase_lead_time': _.isEmpty(data.purchaselt) ? 0 : data.purchaselt,
              'process_lead_time': _.isEmpty(data.processlt) ? 0 : data.processlt,
              'stage': { "id": this.selectedstage.value },
              'uom': { "id": this.selecteduom.value },
              'valvesize': { "id": this.selectedvalve.value },
              'class_id': { "id": this.selectedclass.id },
              'series_id': { "id": this.selectedseries.id },
              'series': this.selectedseries.name,
              'cls': this.selectedclass.name,
              'component': data.component,
              'dwg_line': data.dwgline,
              'component_desc': data.compdesc,
              'material_spec_no': data.mspecno,
              'material_spec_rev': data.mspecrev,
              'dwg_no': data.dwgno,
              'rev': data.rev,
              'weight': _.isEmpty(data.weight) ? 0 : data.weight,
              'active_status': data.status,
              'crt_dt': this.created_date,
              'itembom': sparedetails,
              "final_stage": flag
            }
          if (data.previous) {
            this.formdata.pre_stg_part_id = data.previous_id,
              this.formdata.pre_stg_part_no = data.previous
          }
          console.log("editformdata", this.formdata);
          this.show_loader = true;

          this.itemservice.updateItem(this.formdata)
            .subscribe(res => {
              this.show_loader = false;
              if (res.status == "success") {
                if (flag == 1) {
                  this.response = res.data;
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                    this.LoadItems();
                  });
                  this.resetform();
                } else {
                  this.buttonname = "Save";
                  this.itemservice.getItemByStatus("active")
                    .subscribe(res => {
                      if (res) {
                        this.dataService = this.completerService.local(res.data, "component_desc,component,part_no", 'part_no');
                        this.prevpart = true;
                        this.savedpart = _.findWhere(res.data, { part_no: this.formdata.part_no });
                        this.itemForm.controls['previous'].setValue(this.savedpart.part_no);
                        this.itemForm.controls['previous_id'].setValue(this.savedpart.id);
                      }
                    });
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                    this.LoadItems();
                  });
                  this.resetfornext();
                }
                this.notif.success('Success', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              } else {
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  dtInstance.destroy();
                  this.LoadItems();
                });
                this.notif.error('Error', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              }
            });
        } else {
          this.notif.warn('Warning', validationmsg[0], {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }
      else {
        if (validata) {
          this.formdata =
            {
              'item_no': data.itemno,
              'part_no': data.partno,
              'part_no_rev': data.partnorev,
              'type': { "id": this.selectedtype.value },
              'part_type': data.ptype,
              'stage': { "id": this.selectedstage.value },
              'uom': { "id": this.selecteduom.value },
              'valvesize': { "id": this.selectedvalve.value },
              'class_id': { "id": this.selectedclass.id },
              'series_id': { "id": this.selectedseries.id },
              'series': this.selectedseries.name,
              'cls': this.selectedclass.name,
              'component': data.component,
              'dwg_line': data.dwgline,
              'component_desc': data.compdesc,
              'material': { "id": this.selectedmaterial.value },
              // 'process': _.isEmpty(this.selectedprocess) ? null : { "id": this.selectedprocess.value },
              'item_process': this.process,
              'process_type': this.processtype,
              'purchase_lead_time': _.isEmpty(data.purchaselt) ? 0 : data.purchaselt,
              'process_lead_time': _.isEmpty(data.processlt) ? 0 : data.processlt,
              'material_spec_no': data.mspecno,
              'material_spec_rev': data.mspecrev,
              'dwg_no': data.dwgno,
              'rev': data.rev,
              'weight': _.isEmpty(data.weight) ? 0 : data.weight,
              'active_status': data.status,
              'itembom': sparedetails,
              "final_stage": flag,
            }
          if (data.previous) {
            this.formdata.pre_stg_part_id = data.previous_id,
              this.formdata.pre_stg_part_no = data.previous
          }
          this.show_loader = true;
          console.log("addformdata", this.formdata)
          this.itemservice.addItem(this.formdata)
            .subscribe(res => {
              if (res.status == "success") {
                this.show_loader = false;
                if (flag == 1) {
                  this.response = res.data;
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                    this.LoadItems();
                  });
                  this.resetform();
                } else {
                  this.itemservice.getItemByStatus("active")
                    .subscribe(res => {
                      if (res) {
                        this.dataService = this.completerService.local(res.data, "component_desc,component,part_no", 'part_no');
                        this.prevpart = true;
                        this.savedpart = _.findWhere(res.data, { part_no: this.formdata.part_no });
                        this.itemForm.controls['previous'].setValue(this.savedpart.part_no);
                        this.itemForm.controls['previous_id'].setValue(this.savedpart.id);
                        console.log("parssss=>", this.savedpart);
                      }
                    });
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                    this.LoadItems();
                  });
                  this.resetfornext();
                }
                this.notif.success('Success', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              } else {
                this.notif.error('Error', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              }
            });
        } else {
          this.notif.warn('Warning', validationmsg[0], {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }
    }
  }

  cancelform(data) {
    if (this.itemForm.value.previous_id) {
      let self = this;
      let sts;
      swal({
        title: 'Are You Sure !',
        text: "You want to cancel this item ?",
        type: 'info',
        width: 500,
        padding: 10,
        showCancelButton: true,
        confirmButtonColor: '#ffaa00',
        confirmButtonText: 'Yes',
        allowOutsideClick: false
      }).then((result) => {
        if (result.value) {
          this.cancelstage(data.previous_id);
        }
      })
    }
    else {
      $("#itemform").slideUp();
      $("#listtable").slideDown();
      $("#spareform").slideUp();
      this.spareform = this.fb.group({
        itemRows: this.fb.array([])
      });
      this.itemForm.reset();
      this.prevpart = false;
      this.cancelclick = true;
      this.buttonname = "Save";
      this.weight = 0;
      this.purchaselt = 0;
      this.processlt = 0;
    }
  }

  cancelstage(id) {
    this.itemservice.CancelItemStage(id).subscribe(res => {
      console.log("status", res);
      $("#itemform").slideUp();
      $("#listtable").slideDown();
      $("#spareform").slideUp();
      this.spareform = this.fb.group({
        itemRows: this.fb.array([])
      });
      this.itemForm.reset();
      this.prevpart = false;
      this.cancelclick = true;
      this.buttonname = "Save";
      this.weight = 0;
      this.purchaselt = 0;
      this.processlt = 0;
    })

  }

  resetform() {
    $("#itemform").slideUp();
    $("#listtable").slideDown();
    $("#spareform").slideUp();
    this.spareform = this.fb.group({
      itemRows: this.fb.array([])
    });
    this.itemForm.reset();
    this.prevpart = false;
    this.cancelclick = true;
    this.buttonname = "Save";
    this.weight = 0;
    this.purchaselt = 0;
    this.processlt = 0;
  }

  resetfornext() {
    this.itemForm.controls['itemid'].setValue("");
    this.itemForm.controls['partno'].setValue("");
    this.itemForm.controls['partnorev'].setValue("-");
    this.itemForm.controls['ptype'].setValue("");
    this.itemForm.controls['valvesize'].setValue("");
    this.itemForm.controls['itype'].setValue("");
    this.itemForm.controls['stage'].setValue("");
    this.itemForm.controls['process'].setValue("");
    this.itemForm.controls['processtype'].setValue("");
    this.itemForm.controls['status'].setValue(1);
    $("#spareform").slideUp();
    this.spareform = this.fb.group({
      itemRows: this.fb.array([])
    });
  }


}
