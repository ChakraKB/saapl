import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymenttermComponent } from './paymentterm.component';

describe('PaymenttermComponent', () => {
  let component: PaymenttermComponent;
  let fixture: ComponentFixture<PaymenttermComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymenttermComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymenttermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
