import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { ItemsService } from '../../services/items/items.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
declare let swal: any;
class Person {
  location: string;
  description: string;
}
@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  Processform: FormGroup;
  Processid: any;
  Process: any;
  formdata: any;
  buttonname: string;
  status: any;
  statusarray: any[];
  response: any;
  description: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  created_date: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private itemservice: ItemsService,
    private http: Http,
    private _http: HttpClient) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.Processform = fb.group({
      'Processid': [null],
      'Process': [null, Validators.required],
      'description': [null, Validators.required],
      'status': [null, Validators.required]
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.status = 1;
    this.loadAngtable();
  }

  loadAngtable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.PROCESS.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }


  getAllProcess() {
    this.show_loader = true;
    this.persons = [];
    this.itemservice.getItempro()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }


  updateloc(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.Processid = data.id
    this.Process = data.process;
    this.description = data.description;
    this.status = data.active_status,
      this.created_date = data.crt_dt
  }

  deletelocation(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.itemservice.deleteItempro(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllProcess();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  formObj: any = {
    Process:
      {
        required: "Process required",
      },
    description:
      {
        required: "Description required",
      },
    status: {
      required: "Status Required"
    }
  }

  addeditProcess(data) {
    if (this.Processform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.Processform, this.formObj);

      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {
      if (data.Processid) {
        this.show_loader = true;
        this.formdata =
          {
            "id": data.Processid,
            "process": data.Process,
            "description": data.description,
            "active_status": data.status,
            'crt_dt': this.created_date,
          }
        console.log("editformdata ==>", this.formdata)
        this.itemservice.updateItempro(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllProcess();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllProcess();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            "process": data.Process,
            "description": data.description,
            "active_status": data.status
          }
        console.log("addformdata ==> ", this.formdata)
        this.itemservice.addItempro(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllProcess();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllProcess();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }

  }

  resetform() {
    this.Processform.reset();
    this.Processform.controls['status'].setValue(1);
    this.buttonname = "Save"
  }

}
