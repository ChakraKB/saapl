import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ItemsService } from '../../services/items/items.service';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';

@Component({
  selector: 'app-product-value',
  templateUrl: './product-value.component.html',
  styleUrls: ['./product-value.component.css']
})
export class ProductValueComponent implements OnInit {
  toastertime: any;
  tosterminlength: any;
  show_loader: boolean = false;
  allitems: any;
  items: any;
  selecteditem: any = {};
  parent: any;
  item: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private itemservice: ItemsService,
    private completerService: CompleterService,
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.getItems();
    this.selecteditem = {};
  }

  getItems() {
    this.show_loader = true;
    this.itemservice.getItem().subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.allitems = res.data;
        this.items = this.completerService.local(this.allitems, "component_desc,component,part_no", 'part_no');
      }
    })
  }
  total: any
  parentdata: any
  getitemdetails(data) {
    if (data) {
      this.selecteditem = data.originalObject;
      this.show_loader = true
      this.itemservice.getProductValue(this.selecteditem.id, this.selecteditem.part_type).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.parentdata = res.data;
          this.parent = res.data.productCostViews;
          this.total = res.data.total_product_value
        }
      })
    } else {
      this.selecteditem = {};
      this.parent = [];
      this.parentdata = {};
      this.total = 0
    }
  }

  totalcost() {
    var cost = _.sumBy(this.parent, function (o) { return o.total_cost; });
    return cost
  }

}
