import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LocationService } from '../../services/location/location.service';
import { RackService } from '../../services/Rack/rack.service';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
declare let swal: any;
class Person {
  rack: string;
  description: string;
}
@Component({
  selector: 'app-rack',
  templateUrl: './rack.component.html',
  styleUrls: ['./rack.component.css']
})
export class RackComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  rackform: FormGroup;
  locid: any;
  location: any;
  formdata: any;
  buttonname: string;
  status: any;
  rack: any;
  description: any;
  statusarray: any[];
  allloctions: any;
  activelocations: any;
  listofactiveloc: any;
  rackid: any;
  response: any;
  selectedlocation: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  created_date: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private locationservice: LocationService,
    private rackservices: RackService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.rackform = fb.group({
      'rackid': [null],
      'location': [null, Validators.required],
      'rack': [null, Validators.required],
      'description': [null, Validators.required],
      'status': [null, Validators.required]
    })
  }


  ngOnInit() {
    this.buttonname = "Save";
    this.status = 1;
    this.loadlocations();
    this.loadAngtable()
  }

  loadlocations() {
    this.show_loader = true;
    this.locationservice.getLocation()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allloctions = res.data
          console.log("allloctions", this.allloctions);
          this.activelocations = _.filter(this.allloctions, function (val) { return val.active_status == 1; });
          this.listofactiveloc = this.masterservice.formatDataforDropdown("location_name", "id", this.activelocations);
          console.log("active locations", this.listofactiveloc)
        }
      });
  }

  loadracks() {
    this.show_loader = true;
    this.persons = [];
    this.rackservices.getAllRack()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  loadAngtable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.RACKS.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  updateloc(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.rackid = data.id;
    this.rackform.controls['location'].setValue(data.location.id);
    this.rack = data.rack_name;
    this.description = data.description;
    this.status = data.active_status;
    this.created_date = data.crt_dt
  }

  deletelocation(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.rackservices.deleteRack(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadracks();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  formObj: any = {
    location:
      {
        required: "Location required",
      },
    rack:
      {
        required: "Rack required",
      },
    description:
      {
        required: "Description required",
      },
    status:
      {
        required: "Status required",
      }
  }

  addeditRack(data) {
    if (this.rackform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.rackform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {

      this.selectedlocation = _.findWhere(this.listofactiveloc, { "value": data.location });

      if (data.rackid) {
        this.show_loader = true;
        this.formdata =
          {
            "id": data.rackid,
            "rack_name": data.rack,
            "location": { "id": this.selectedlocation.value },
            "description": data.description,
            "active_status": data.status,
            'crt_dt': this.created_date,
          }
        console.log("editformdata ==>", this.formdata);
        this.rackservices.updateRack(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadracks();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadracks();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            "rack_name": data.rack,
            "location": { "id": this.selectedlocation.value },
            "description": data.description,
            "active_status": data.status
          }
        console.log("addformdata ==> ", this.formdata);
        this.rackservices.addRack(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadracks();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadracks();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }
  }

  resetform() {
    this.rackform.reset();
    this.rackform.controls['status'].setValue(1);
    this.buttonname = "Save"
  }
}
