import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingCostComponent } from './selling-cost.component';

describe('SellingCostComponent', () => {
  let component: SellingCostComponent;
  let fixture: ComponentFixture<SellingCostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingCostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
