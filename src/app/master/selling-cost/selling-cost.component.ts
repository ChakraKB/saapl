import { AfterViewInit, Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'underscore';
import { ItemsService } from '../../services/items/items.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
@Component({
  selector: 'app-selling-cost',
  templateUrl: './selling-cost.component.html',
  styleUrls: ['./selling-cost.component.css']
})
export class SellingCostComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  show_loader: boolean = false;
  showDialog:boolean = false;
  contactlist: any;
  allcontacts: any;
  filter: any = {};
  persons: any;
  checkeditems: any;
  parttype: any;
  allsuppliers: any;
  toastertime: any;
  tosterminlength: any;
  showRateDialog: boolean = false;
  rates: any
  constructor(
    private masterservice: MasterService,
    private completerService: CompleterService,
    private itemservice: ItemsService,
    private notif: NotificationsService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
  ) {
    this.parttype = AppConstant.APP_CONST.PARTTYPES;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.checkeditems = [];
  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  showprevpop(items) {
    this.showRateDialog = true;
    this.rates = items
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  selectedcontact(data) {
    console.log("supplier", data)
  }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.sl_no === item.sl_no) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  uncheckall() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  getitems() {
    if (this.persons) {
      this.uncheckall();
    }
    this.show_loader = true;
    this.itemservice.GetSellingCostList(_.isEmpty(this.filter.part) ? null : this.filter.part).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.persons = res.data;
        console.log("data", this.persons)
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    })
  }

  close() {
    this.showRateDialog = false
  }

  createmapping() {
    var valid: boolean = true;
    var validationMsg = [];
    var checkeditems = []
    if (!this.checkeditems.length) {
      valid = false;
      validationMsg.push("Please Select any Item")
    }
    this.checkeditems.forEach(val => {
      var obj =
        {
          "part_id": {'id':val.id},
          "part_no": val.part_no,
          "part_desc": val.component_desc,
          "cost": val.cost,
        }
      if (val.cost == 0 || !val.cost) {
        valid = false;
        validationMsg.push("Please enter rate for " + val.part_no)
      } else {
        checkeditems.push(obj)
      }
    });

    if (valid) {
      var newlist = [];
      // checkeditems.forEach(element => {
      //   if (element.id == 0) {
      //     // var list = _.omit(element, ['id', 'checkauthorize']);
      //     newlist.push(list)
      //   } else {
      //     newlist.push(element)
      //   }
      // });
      console.log("checkeditems", checkeditems)
      this.show_loader = true;
      this.itemservice.CreateSellingCost(checkeditems).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.getitems();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    }
    else {
      this.show_loader = false;
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }
}
