import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { CountryService } from '../../services/country/country.service';
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { ItemsService } from '../../services/items/items.service'
declare let swal: any;
class Person {
  dptcode: string;
  dptname: string;
}

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  countryForm: FormGroup;
  toastertime: any;
  tosterminlength: any;
  formdata: any;
  buttonname: any;
  status: any;
  statusarray: any;
  response: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  series: any = {};
  deleteoptions: any;
  created_date: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private itemsevice: ItemsService,
    private http: Http
  ) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.countryForm = fb.group({
      'id': [null],
      'seriesname': [null, Validators.required],
      'description': [null, Validators.required],
      'status': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.series.status = 1;
    this.LoadAngTable();
  }


  LoadAngTable() {
    this.show_loader = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.SERIES.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  deletecountry(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.itemsevice.DeleteSeries(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getCountries();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  getCountries() {
    this.show_loader = true;
    this.persons = [];
    this.itemsevice.GetAllSeries()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }


  updatecountry(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log('dpts', data);
    this.series.id = data.id;
    this.series.seriesname = data.name;
    this.series.description = data.description;
    this.series.status = data.active_status;
    this.created_date = data.created_date
  }

  formObj: any = {

    seriesname: {
      required: "Series Required"
    },
    description: {
      required: "Description Required"
    },
    status: {
      required: "Status Required"
    }
  }

  addeditcountry(data) {
    if (this.countryForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.countryForm, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else {

      if (data.id) {
        this.show_loader = true;
        this.formdata =
          {
            'id': data.id,
            'name': data.seriesname,
            'description': data.description,
            'active_status': data.status,
            'crt_dt': this.created_date,
          }
        console.log("editformdata", this.formdata)
        this.itemsevice.UpdateSeries(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getCountries();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getCountries();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            'name': data.seriesname,
            'description': data.description,
            'active_status': data.status,
          }
        console.log("addformdata", this.formdata)
        this.itemsevice.addSeries(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getCountries();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getCountries();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }
  }

  resetform() {
    this.countryForm.reset();
    this.buttonname = "Save";
    this.countryForm.controls['status'].setValue(1);
  }
}
