import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { CountryService } from '../../services/country/country.service';
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
declare let swal: any;
class Person {
  dptcode: string;
  dptname: string;
}

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  stateForm: FormGroup;
  toastertime: any;
  tosterminlength: any;
  dptcode: any;
  dptid: any;
  dptname: any;
  formdata: any;
  buttonname: any;
  status: any;
  statusarray: any;
  response: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  created_date: any;
  activecountries: any;
  selectedcountry: any;
  state: any = {};
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private countryservice: CountryService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.stateForm = fb.group({
      'id': [null],
      'country': [null, Validators.required],
      'statename': [null, Validators.required],
      'status': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.state.status = 1;
    this.LoadAngTable();
    this.getActivecountries();
  }

  LoadAngTable() {
    this.show_loader = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.STATE.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  getActivecountries() {
    this.show_loader = true;
    this.countryservice.getCountryByStatus("active").subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.activecountries = this.masterservice.formatDataforDropdown("country_name", "id", res.data);
      }
    })
  }

  deletestate(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.countryservice.deleteState(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllstates();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  getAllstates() {
    this.show_loader = true;
    this.persons = [];
    this.countryservice.getallState()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }


  updatestate(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log('dpts', data);
    this.state.id = data.id;
    this.state.statename = data.state_name;
    this.dptcode = data.department_code;
    this.state.status = data.active_status;
    this.created_date = data.created_date;
    this.stateForm.controls['country'].setValue(data.country_id.id);
  }

  formObj: any = {

    country: {
      required: "Country Required"
    },
    statename: {
      required: "State Required",
    },
    status: {
      required: "Status Required"
    }
  }

  addeditstate(data) {
    if (this.stateForm.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.stateForm, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    } else {
      this.selectedcountry = _.findWhere(this.activecountries, { "value": data.country });

      if (data.id) {
        this.show_loader = true;
        this.formdata =
          {
            'id': data.id,
            'country_id': { 'id': this.selectedcountry.value },
            'state_name': data.statename,
            'active_status': data.status,
            'crt_dt': this.created_date,
          }
        console.log("editformdata", this.formdata)
        this.countryservice.updateState(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllstates();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllstates();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          }, (err) => {
            this.show_loader = false;
            this.notif.error('Error', "Something Wrong", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            'country_id': { 'id': this.selectedcountry.value },
            'state_name': data.statename,
            'active_status': data.status
          }
        console.log("addformdata", this.formdata)
        this.countryservice.addState(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllstates();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getAllstates();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }
  }

  resetform() {
    this.stateForm.reset();
    this.buttonname = "Save";
    this.stateForm.controls['status'].setValue(1);
  }

}
