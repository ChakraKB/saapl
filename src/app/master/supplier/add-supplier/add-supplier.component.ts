import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../../services/master.service'
import { UserService } from '../../../services/user/user.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray, PatternValidator } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from "underscore";
import { ActivatedRoute } from "@angular/router";
declare let swal: any;
import { DataTableDirective } from 'angular-datatables';
import { CountryService } from '../../../services/country/country.service';
import { CompleterService } from 'ng2-completer';
class Person {
  role_name: string;
  description: string;
}

@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.css']
})
export class AddSupplierComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  role_name: any;
  description: any;
  toastertime: number;
  tosterminlength: number;
  contactform: FormGroup;
  id: any;
  formdata: any;
  buttonname: string;
  active_status: any;
  statusarray: any;
  response: any;
  allrole: any;
  role: any = {};
  data: any;
  tabletimeout: any;
  sampleform: FormGroup;
  show_loader: boolean = false;
  scrolltime: number;
  message: any = {};
  contactcat: any;
  created_date: any;
  contacttype: any;
  conid: any;
  contactdetails: any;
  conname: any;
  condisplayname: any;
  concode: any;
  category: any;
  type: any;
  mobile: any;
  phone: any;
  fax: any;
  email: any;
  website: any;
  address: any;
  citypincode: any;
  state: any;
  country: any;
  invoice_address: any;
  invcitypincode: any;
  invstate: any;
  invcountry: any;
  ship_address: any;
  shipcitypincode: any;
  shipstate: any;
  shipcountry: any;
  contact_person: any;
  crt_dt: any;
  allcountries: any;
  countries: any;
  states: any;
  cities: any;
  allstates: any;
  allcities: any;
  allinvstates: any;
  allinvcities: any;
  allshipstates: any;
  allshipcities: any;
  contactcategory: any
  showdetailtab: boolean = false
  showinvaddtab: boolean = true;
  showshipaddtab: boolean = true;
  cpform: FormGroup;
  invcity: any;
  city: any;
  shipcity: any;
  shippincode: any;
  invpincode: any;
  pincode: any;
  cp: any = {};
  gstinno: any;
  supcontperson: any
  show_update_loader: boolean = false;

  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private userservice: UserService,
    private router: Router,
    private acroute: ActivatedRoute,
    private countryservice: CountryService,
    private completerService: CompleterService,
    private http: Http) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.contactcat = AppConstant.APP_CONST.ContactCategory;
    this.contactcategory = this.masterservice.formatDataforDropdown("name", "value", this.contactcat);
    this.contacttype = AppConstant.APP_CONST.SuppType;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;

    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.conid = params.id;
        console.log("urlparams", this.conid);
      }
    });

    this.cpform = this.fb.group({
      itemRows: this.fb.array([])
    });

    this.contactform = fb.group({
      'id': [null],
      'conname': [null, Validators.required],
      'condisplayname': [null, Validators.required],
      'concode': [null],
      'category': [null, Validators.required],
      'type': [null, Validators.required],
      'phone': [null, Validators.required],
      'fax': [null],
      'email': [null, Validators.compose([Validators.required, Validators.email])],
      'website': [null],
      'gstinno': [null],
      'address': [null, Validators.required],
      'country': [null, Validators.required],
      'state': [null, Validators.required],
      'city': [null, Validators.required],
      'pincode': [null, Validators.required],
      'invoice_address': [null],
      'invcountry': [null],
      'invstate': [null],
      'invcity': [null],
      'invpincode': [null],
      'ship_address': [null],
      'shipcountry': [null],
      'shipstate': [null],
      'shipcity': [null],
      'shippincode': [null],
      'crt_dt': [null],
      'active_status': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.active_status = 1;
    this.LoadAngTable();
    this.getcountries();
    this.getstates();
    this.getcities();
    this.getcondetails(this.conid)
    this.addNewRow();
  }

  getform(cpform) {
    return cpform.get('itemRows').controls
  }

  initItemRows() {
    return this.fb.group({
      id: 0,
      supplierid: 0,
      name: "",
      mobile: "",
      email: "",
      role: "",
    });
  }

  addNewRow() {
    const control = <FormArray>this.cpform.controls["itemRows"];
    control.push(this.initItemRows());
    console.log("control", this.cpform)
  }
  deleteRow(index: number) {
    const control = <FormArray>this.cpform.controls["itemRows"];
    control.removeAt(index);
  }

  selectedcat(data) {
    console.log(data)
    if (data == "Customer") {
      this.showdetailtab = true;
      $("#showdetailtab").slideDown();
    } else {
      this.showdetailtab = false
      $("#showdetailtab").slideUp();
    }
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }


  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.ROLES.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  getcountries() {
    this.show_loader = true;
    this.countryservice.getCountryByStatus('active').subscribe(res => {
      this.show_loader = false;
      this.countries = res.data;
      this.allcountries = this.masterservice.formatDataforDropdown("country_name", "id", res.data);
    }, (err) => {
      this.show_loader = false;
      this.notif.error('Error', "countries not found,Please try again", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  getstates() {
    this.show_loader = true;
    this.countryservice.getallState().subscribe(res => {
      this.show_loader = false;
      this.states = res.data
    }, (err) => {
      this.show_loader = false;
      this.notif.error('Error', "countries not found,Please try again", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  getcities() {
    this.show_loader = true;
    this.countryservice.getallCity().subscribe(res => {
      this.show_loader = false;
      this.cities = res.data
    }, (err) => {
      this.show_loader = false;
      this.notif.error('Error', "countries not found,Please try again", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  selectedcountry(data) {
    this.show_loader = true
    this.countryservice.getStateByCountry(data.value).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false
        this.allstates = this.masterservice.formatDataforDropdown("state_name", "id", res.data);
        console.log(this.allstates)
      }
    }, (err) => {
      this.notif.error('Error', "States not found", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  selectedstate(data) {
    this.show_loader = true
    this.countryservice.getCityByState(data.value).subscribe(res => {
      if (res) {
        this.show_loader = false
        this.allcities = this.masterservice.formatDataforDropdown("city_name", "id", res.data);
      }
    }, (err) => {
      this.notif.error('Error', "Cities not found", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }
  selectedcity(data) {
    console.log("data", data.originalObject)
  }

  selectedInvcountry(data) {
    this.show_loader = true
    this.countryservice.getStateByCountry(data.value).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false
        this.allinvstates = this.masterservice.formatDataforDropdown("state_name", "id", res.data);
        console.log(this.allstates)
      }
    }, (err) => {
      this.notif.error('Error', "States not found", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  selectedinvstate(data) {
    this.show_loader = true
    this.countryservice.getCityByState(data.value).subscribe(res => {
      if (res) {
        this.show_loader = false
        this.allinvcities = this.masterservice.formatDataforDropdown("city_name", "id", res.data);
      }
    }, (err) => {
      this.notif.error('Error', "Cities not found", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  selectedInvcity(data) {
    console.log("data", data.originalObject)
  }

  selectedshipcountry(data) {
    this.show_loader = true
    this.countryservice.getStateByCountry(data.value).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false
        this.allshipstates = this.masterservice.formatDataforDropdown("state_name", "id", res.data);
        console.log(this.allstates)
      }
    }, (err) => {
      this.notif.error('Error', "States not found", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  selectedshipstate(data) {
    this.show_loader = true
    this.countryservice.getCityByState(data.value).subscribe(res => {
      if (res) {
        this.show_loader = false
        this.allshipcities = this.masterservice.formatDataforDropdown("city_name", "id", res.data);
      }
    }, (err) => {
      this.notif.error('Error', "Cities not found", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  selectedshipcities(data) {
    console.log("data", data.originalObject)
  }
  checkship: any
  checkinv: any
  changeinvaddrs(data) {
    if (data == true) {
      if (this.contactform.value.address && this.contactform.value.country && this.contactform.value.state
        && this.contactform.value.city && this.contactform.value.pincode) {
        this.showinvaddtab = true;
        this.checkinv = true
        this.allinvstates = this.allstates;
        this.allinvcities = this.allcities;
        setTimeout(() => {
          this.contactform.controls['invoice_address'].setValue(this.contactform.value.address);
          this.contactform.controls['invcountry'].setValue(this.contactform.value.country);
          this.contactform.controls['invstate'].setValue(this.contactform.value.state);
          this.contactform.controls['invcity'].setValue(this.contactform.value.city);
          this.contactform.controls['invpincode'].setValue(this.contactform.value.pincode);
          console.log("contact", this.contactform)
        });
      } else {
        this.showinvaddtab = false;
        this.checkinv = false
        this.notif.warn('Warning', "Please select address details", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength,
        })
      }
    } else {
      this.showinvaddtab = false
      // $("#showinvaddtab").slideDown();
      this.contactform.controls['invoice_address'].setValue("");
      this.contactform.controls['invcountry'].setValue("");
      this.contactform.controls['invstate'].setValue("");
      this.contactform.controls['invcity'].setValue("");
      this.contactform.controls['invpincode'].setValue("");
      console.log("contact", this.contactform)
    }
  }

  changeshipaddrs(data) {
    if (data == true) {
      if (this.contactform.value.address && this.contactform.value.country && this.contactform.value.state
        && this.contactform.value.city && this.contactform.value.pincode) {
        this.allshipstates = this.allstates;
        this.allshipcities = this.allcities;
        this.showshipaddtab = true;
        this.checkship = true
        setTimeout(() => {
          this.contactform.controls['ship_address'].setValue(this.contactform.value.address);
          this.contactform.controls['shipcountry'].setValue(this.contactform.value.country);
          this.contactform.controls['shipstate'].setValue(this.contactform.value.state);
          this.contactform.controls['shipcity'].setValue(this.contactform.value.city);
          this.contactform.controls['shippincode'].setValue(this.contactform.value.pincode);
          console.log("contact", this.contactform)
        });
      } else {
        this.showshipaddtab = false;
        this.checkship = false
        this.notif.warn('Warning', "Please select address details", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength,
        })
      }

    } else {
      this.showshipaddtab = false
      this.contactform.controls['ship_address'].setValue("");
      this.contactform.controls['shipcountry'].setValue("");
      this.contactform.controls['shipstate'].setValue("");
      this.contactform.controls['shipcity'].setValue("");
      this.contactform.controls['shippincode'].setValue("");
      console.log("contact", this.contactform)
    }
  }

  getcondetails(conid) {
    if (conid) {
      this.show_update_loader = true;
      this.userservice.getContactsByID(conid).subscribe(res => {
        if (res.status == "success") {
          this.buttonname = "Update";
          console.log("contactbyid", res.data);
          this.contactdetails = res.data;

          this.allstates = this.masterservice.formatDataforDropdown("state_name", "id", this.contactdetails.addr_stateList);
          this.allcities = this.masterservice.formatDataforDropdown("city_name", "id", this.contactdetails.addr_cityList);
          this.allinvstates = this.masterservice.formatDataforDropdown("state_name", "id", this.contactdetails.iv_addr_stateList);
          this.allinvcities = this.masterservice.formatDataforDropdown("city_name", "id", this.contactdetails.iv_addr_cityList);
          this.allshipstates = this.masterservice.formatDataforDropdown("state_name", "id", this.contactdetails.ship_addr_stateList);
          this.allshipcities = this.masterservice.formatDataforDropdown("city_name", "id", this.contactdetails.ship_addr_cityList);

          console.log("allcountries", this.allcountries),
            console.log("allstates", this.allstates),
            console.log("allcities", this.allcities)

          this.id = this.contactdetails.id;
          this.conname = this.contactdetails.supplier_name;
          this.condisplayname = this.contactdetails.display_cmpy_name;
          this.concode = this.contactdetails.supplier_code;
          this.category = this.contactdetails.category;
          this.type = this.contactdetails.type;
          this.mobile = this.contactdetails.mobile;
          this.phone = this.contactdetails.phone;
          this.fax = this.contactdetails.fax;
          this.gstinno = this.contactdetails.gstin_no;
          this.email = this.contactdetails.email;
          this.website = this.contactdetails.website;
          this.contact_person = this.contactdetails.contact_person;
          this.active_status = this.contactdetails.active_status;
          this.crt_dt = this.contactdetails.crt_dt;

          if (this.category == "Customer") {
            this.showdetailtab = true
            $("#showdetailtab").slideDown();
          } else {
            this.showdetailtab = false
            $("#showdetailtab").slideUp();
          }
          let temp = this.fb.array([]);
          this.supcontperson = this.contactdetails.sup_cont_person
          for (let i = 0; i < this.supcontperson.length; i++) {
            temp.push(
              this.fb.group({
                id: parseInt(this.supcontperson[i].id),
                supplierid: this.supcontperson[i].supplier_id,
                name: this.supcontperson[i].name,
                email: this.supcontperson[i].email,
                role: this.supcontperson[i].role,
                mobile: this.supcontperson[i].mobile_no,
              })
            );
          }
          this.cpform = this.fb.group({
            itemRows: temp
          });

          setTimeout(() => {
            this.address = this.contactdetails.address_line1;
            this.contactform.controls['country'].setValue(this.contactdetails.address_country.id);
            this.contactform.controls['state'].setValue(this.contactdetails.address_state.id);
            this.contactform.controls['city'].setValue(this.contactdetails.address_city.id);
            this.pincode = this.contactdetails.address_pincode;

            if (this.category == "Customer") {
              this.invoice_address = this.contactdetails.inv_addr_line1;
              this.contactform.controls['invcountry'].setValue(this.contactdetails.inv_addr_country.id);
              this.contactform.controls['invstate'].setValue(this.contactdetails.inv_addr_state.id);
              this.contactform.controls['invcity'].setValue(this.contactdetails.inv_addr_city.id);
              this.invpincode = this.contactdetails.inv_addr_pincode;

              this.ship_address = this.contactdetails.ship_addr_line1;
              this.contactform.controls['shipcountry'].setValue(this.contactdetails.ship_addr_country.id);
              this.contactform.controls['shipstate'].setValue(this.contactdetails.ship_addr_state.id);
              this.contactform.controls['shipcity'].setValue(this.contactdetails.ship_addr_city.id);
              this.shippincode = this.contactdetails.ship_address_pincode;
            }
            this.show_update_loader = false;
          });
        }
      })
    }
  }

  formObj: any = {
    conname:
    {
      required: "Company name required",
    },
    condisplayname:
    {
      required: "Display name required",
    },
    category:
    {
      required: "category required",
    },
    type:
    {
      required: "type required",
    },
    phone:
    {
      required: "phone required",
    },
    email:
    {
      required: "email required",
      email: "Email id is not valid"
    },
    address:
    {
      required: "address required",
    },
    country:
    {
      required: "country required",
    },
    state:
    {
      required: "state required",
    },
    city:
    {
      required: "City required",
    },
    pincode:
    {
      required: "Pincode required",
    },
    active_status:
    {
      required: "Status required",
    }

  }

  addeditcontact(data) {
    if (this.contactform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.contactform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    var cpdetails = [];
    var validata: Boolean = true;
    var validationmsg = [];

    if (this.cpform.value.itemRows.length != 0) {
      let self = this
      _.forEach(self.cpform.value.itemRows, function (item, key) {
        var cplineitems =
        {
          "id": item.id,
          "supplier_id": item.supplierid,
          "name": item.name,
          "mobile_no": item.mobile,
          "email": item.email,
          'role': item.role
        }
        // if (_.isEmpty(item.name)) {
        //   validata = false;
        //   validationmsg.push("Please Enter Name");
        // }
        //  else if (!item.mobile) {
        //   validata = false;
        //   validationmsg.push("Please enter Mobile No");
        // } else if (!item.email) {
        //   validata = false;
        //   validationmsg.push("Please enter Email Id");
        // }
        //  else if (self.cpform.status == "INVALID") {
        //   validata = false;
        //   validationmsg.push("Enter Valid Mobile No & Email Id");
        // }
        // else {
        cpdetails.push(cplineitems);
        // }
      });
    } else {
      validata = false;
      validationmsg.push("Please add contact person details");
    }
    if (this.showdetailtab == true) {
      if (!data.invcountry || !data.invstate || !data.invcity || !data.invpincode) {
        validata = false;
        validationmsg.push("Please select Invoice Address Details");
      } else if (!data.shipcountry || !data.shipstate || !data.shipcity || !data.shippincode) {
        validata = false;
        validationmsg.push("Please select Shipping Address Details");
      }
    }
    if (validata) {

      var country = _.findWhere(this.countries, { "id": data.country })
      var state = _.findWhere(this.states, { "id": data.state })
      var city = _.findWhere(this.cities, { "id": data.city })

      var invcountry = _.findWhere(this.countries, { "id": data.invcountry })
      var invstate = _.findWhere(this.states, { "id": data.invstate })
      var invcity = _.findWhere(this.cities, { "id": data.invcity })

      var shipcountry = _.findWhere(this.countries, { "id": data.shipcountry })
      var shipstate = _.findWhere(this.states, { "id": data.shipstate })
      var shipcity = _.findWhere(this.cities, { "id": data.shipcity })

      if (data.id) {
        if (this.showdetailtab == true) {
          this.formdata =
            {
              "id": data.id,
              "supplier_name": data.conname,
              "display_cmpy_name": data.condisplayname,
              'supplier_code': data.concode,
              'category': data.category,
              'type': data.type,
              // 'mobile': data.mobile,
              'phone': data.phone,
              'fax': data.fax,
              'email': data.email,
              'website': data.website,
              'gstin_no': data.gstinno,
              'address_line1': data.address,
              'address_country': { "id": country.id },
              'address_state': { "id": state.id },
              'address_city': { "id": city.id },
              'address_pincode': data.pincode,
              'inv_addr_line1': data.invoice_address,
              'inv_addr_country': { "id": invcountry.id },
              'inv_addr_state': { "id": invstate.id },
              'inv_addr_city': { "id": invcity.id },
              'inv_addr_pincode': data.invpincode,
              'ship_addr_line1': data.ship_address,
              'ship_addr_country': { "id": shipcountry.id },
              'ship_addr_state': { "id": shipstate.id },
              'ship_addr_city': { "id": shipcity.id },
              'ship_address_pincode': data.shippincode,
              "sup_cont_person": cpdetails,
              "active_status": data.active_status,
              'crt_dt': this.crt_dt,
            }
        } else {
          this.formdata =
            {
              "id": data.id,
              "supplier_name": data.conname,
              "display_cmpy_name": data.condisplayname,
              'supplier_code': data.concode,
              'category': data.category,
              'type': data.type,
              'mobile': data.mobile,
              'phone': data.phone,
              'fax': data.fax,
              'email': data.email,
              'website': data.website,
              'gstin_no': data.gstinno,
              'address_line1': data.address,
              'address_country': { "id": country.id },
              'address_state': { "id": state.id },
              'address_city': { "id": city.id },
              'address_pincode': data.pincode,
              // 'inv_addr_line1': data.invoice_address,
              // 'inv_addr_country': { "id": invcountry.id },
              // 'inv_addr_state': { "id": invstate.id },
              // 'inv_addr_city': { "id": invcity.id },
              // 'inv_addr_pincode': data.invpincode,
              // 'ship_addr_line1': data.ship_address,
              // 'ship_addr_country': { "id": shipcountry.id },
              // 'ship_addr_state': { "id": shipstate.id },
              // 'ship_addr_city': { "id": shipcity.id },
              // 'ship_address_pincode': data.shippincode,
              "sup_cont_person": cpdetails,
              "active_status": data.active_status,
              'crt_dt': this.crt_dt,
            }
        }
        this.show_loader = true;
        this.userservice.updateContacts(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.show_loader = false
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          }, err => {
            this.show_loader = false
            this.notif.error('Error', "Something Wrong", {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          });
      }
      else {
        if (this.showdetailtab == true) {
          this.formdata =
            {
              "supplier_name": data.conname,
              "display_cmpy_name": data.condisplayname,
              'supplier_code': data.concode,
              'category': data.category,
              'type': data.type,
              'mobile': data.mobile,
              'phone': data.phone,
              'fax': data.fax,
              'email': data.email,
              'website': data.website,
              'gstin_no': data.gstinno,
              'address_line1': data.address,
              'address_country': { "id": country.id },
              'address_state': { "id": state.id },
              'address_city': { "id": city.id },
              'address_pincode': data.pincode,
              'inv_addr_line1': data.invoice_address,
              'inv_addr_country': { "id": invcountry.id },
              'inv_addr_state': { "id": invstate.id },
              'inv_addr_city': { "id": invcity.id },
              'inv_addr_pincode': data.invpincode,
              'ship_addr_line1': data.ship_address,
              'ship_addr_country': { "id": shipcountry.id },
              'ship_addr_state': { "id": shipstate.id },
              'ship_addr_city': { "id": shipcity.id },
              'ship_address_pincode': data.shippincode,
              "sup_cont_person": cpdetails,
              "active_status": data.active_status,
            }
        } else {
          this.formdata =
            {
              "supplier_name": data.conname,
              "display_cmpy_name": data.condisplayname,
              'supplier_code': data.concode,
              'category': data.category,
              'type': data.type,
              'mobile': data.mobile,
              'phone': data.phone,
              'fax': data.fax,
              'email': data.email,
              'website': data.website,
              'gstin_no': data.gstinno,
              'address_line1': data.address,
              'address_country': { "id": country.id },
              'address_state': { "id": state.id },
              'address_city': { "id": city.id },
              'address_pincode': data.pincode,
              // 'inv_addr_line1': data.invoice_address,
              // 'inv_addr_country': { "id": invcountry.id },
              // 'inv_addr_state': { "id": invstate.id },
              // 'inv_addr_city': { "id": invcity.id },
              // 'inv_addr_pincode': data.invpincode,
              // 'ship_addr_line1': data.ship_address,
              // 'ship_addr_country': { "id": shipcountry.id },
              // 'ship_addr_state': { "id": shipstate.id },
              // 'ship_addr_city': { "id": shipcity.id },
              // 'ship_address_pincode': data.shippincode,
              "sup_cont_person": cpdetails,
              "active_status": data.active_status,
            }
        }
        this.show_loader = true;
        console.log("addformdata ==> ", this.formdata)
        this.userservice.addContacts(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.response = res.data;
              this.resetform();
              this.show_loader = false;
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.show_loader = false;
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })

            }
          });
      }
    } else {
      this.notif.warn('Warning', validationmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  resetform() {
    this.contactform.reset();
    this.buttonname = "Save";
    this.router.navigate(['/supplier']);
    this.contactform.controls['active_status'].setValue(1);
  }

}
