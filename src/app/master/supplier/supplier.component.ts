import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { UserService } from '../../services/user/user.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { DataTableDirective } from 'angular-datatables';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
declare let swal: any;
class Person {
  name: string;
  code: string;
  address: string;
  mobile: string;
  type: string;
}
@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  persons: Person[] = [];
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  deleteoptions: any;
  constructor(private http: Http,
    private userservice: UserService,
    private notif: NotificationsService, ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
  }

  ngOnInit() {
    this.LoadAngTable();
  }

  getcontacts() {
    this.show_loader = true
    this.persons = [];
    this.userservice.getallContacts().subscribe(res => {
      this.show_loader = false
      if (res) {
        this.persons = res.data;
        this.dtTrigger.next();
        console.log("allusers", this.persons)
      }
    })
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.CONTACT.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  deletecontact(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.userservice.deleteContacts(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.getcontacts();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }
}
