import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierapprovalComponent } from './supplierapproval.component';

describe('SupplierapprovalComponent', () => {
  let component: SupplierapprovalComponent;
  let fixture: ComponentFixture<SupplierapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
