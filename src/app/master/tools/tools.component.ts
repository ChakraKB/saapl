import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
declare let swal: any;
import { DataTableDirective } from 'angular-datatables';
import { ToolsService } from '../../services/tools/tools.service';
class Person {
  tool_name: string;
  description: string;
}

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.css']
})
export class ToolsComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  Toolsform: FormGroup;
  formdata: any;
  buttonname: string;
  statusarray: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  tool: any = {};
  created_date: any;
  deleteoptions: any;
  coreboxes: any;
  matchplates: any;
  cavities: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private toolService: ToolsService,
    private http: Http
  ) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.coreboxes = AppConstant.APP_CONST.TOOLS.COREBOX;
    this.matchplates = AppConstant.APP_CONST.TOOLS.MATCHPLATES;
    this.cavities = []
    for (var i = 1; i <= 20; i++) {
      var obj =
        {
          "value": i,
          "name": i
        }
      this.cavities.push(obj)
    }

    this.Toolsform = fb.group({
      'id': [null],
      'tool_no': [null, Validators.required],
      'tool_name': [null, Validators.required],
      'description': [null, Validators.required],
      'qty': [null, Validators.required],
      'corebox': [null, Validators.required],
      'cavitiescount': [null, Validators.required],
      'matchplate': [null, Validators.required],
      'length': [null, Validators.required],
      'breath': [null, Validators.required],
      'height': [null, Validators.required],
      'toolvalue': [null, Validators.required],
      'active_status': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.tool.active_status = 1;
    this.LoadAngTable();
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  gettools() {
    this.show_loader = true
    this.persons = [];
    this.toolService.getAllTools()
      .subscribe(res => {
        this.show_loader = false
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.TOOL.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  updaterole(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.tool.id = data.id;
    this.tool.tool_no = data.tool_no;
    this.tool.tool_name = data.tool_name;
    this.tool.description = data.tool_description;
    this.tool.qty = data.quantity;
    this.tool.corebox = data.core_box;
    this.tool.cavitiescount = data.no_of_cavities;
    this.tool.matchplate = data.match_plate_type;
    this.tool.length = data.match_plate_length;
    this.tool.breath = data.match_plate_breath;
    this.tool.height = data.match_plate_height;
    this.tool.toolvalue = data.tool_value;
    this.tool.active_status = data.active_status;
    this.tool.created_date = data.created_date
  }

  deleterole(id) {
    let self = this;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.toolService.deleteTool(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettools();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }


  formObj: any = {
    tool_no:
      {
        required: "Tool No required",
      },
    tool_name:
      {
        required: "Tool name required",
      },
    description:
      {
        required: "Description required",
      },
    qty:
      {
        required: "Quantity required",
      },
    corebox:
      {
        required: "Corebox required",
      },
    cavitiescount:
      {
        required: "No Of Cavities required",
      },
    matchplate:
      {
        required: "Match Plate required",
      },
    length:
      {
        required: "Match Plate Size required",
      },
    breath:
      {
        required: "Match Plate Size required",
      },
    height:
      {
        required: "Match Plate Size required",
      },
    toolvalue:
      {
        required: "Tool Value required",
      },
    active_status:
      {
        required: "Status required",
      }

  }

  addeditrole(data) {
    if (this.Toolsform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.Toolsform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {
      if (data.id) {
        this.show_loader = true
        this.formdata =
          {
            "id": data.id,
            "tool_no": data.tool_no,
            "tool_name": data.tool_name,
            "tool_description": data.description,
            "quantity": data.qty,
            "core_box": data.corebox,
            "no_of_cavities": data.cavitiescount,
            "match_plate_type": data.matchplate,
            "match_plate_length": data.length,
            "match_plate_breath": data.breath,
            "match_plate_height": data.height,
            "tool_value": data.toolvalue,
            "active_status": data.active_status,
            'crt_dt': this.tool.created_date,
          }
        this.toolService.updateTool(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettools();
                this.show_loader = false;
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettools();
                this.show_loader = false;
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            "tool_no": data.tool_no,
            "tool_name": data.tool_name,
            "tool_description": data.description,
            "quantity": data.qty,
            "core_box": data.corebox,
            "no_of_cavities": data.cavitiescount,
            "match_plate_type": data.matchplate,
            "match_plate_length": data.length,
            "match_plate_breath": data.breath,
            "match_plate_height": data.height,
            "tool_value": data.toolvalue,
            "active_status": data.active_status,
          }
        console.log("addformdata ==> ", this.formdata)
        this.toolService.addTool(this.formdata)
          .subscribe(res => {
            if (res.status == "success") {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettools();
              });
              this.resetform();
              this.show_loader = false;
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.gettools();
              });
              this.show_loader = false;
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })

            }
          });
      }
    }

  }

  resetform() {
    this.Toolsform.reset();
    this.buttonname = "Save";
    this.Toolsform.controls['active_status'].setValue(1);
  }


}
