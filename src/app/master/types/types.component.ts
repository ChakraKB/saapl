import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TypeService } from '../../services/types/type.service'
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
declare let swal: any;

class Person {
  type: string
  description: string;
}

@Component({
  selector: 'app-types',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.css']
})
export class TypesComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  typeform: FormGroup;
  statusarray: any;
  buttonname: any;
  response: any;
  typeid: any;
  typename: any;
  description: any;
  status: any;
  formdata: any;
  tabletimeout: any;
  show_loader: boolean = false;
  scrolltime: number;
  created_date: any;
  deleteoptions: any;
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private typeservice: TypeService,
    private http: Http) {
    this.deleteoptions = AppConstant.APP_CONST.DELETESWAL;
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    this.typeform = fb.group({
      'typeid': [null],
      'typename': [null, Validators.required],
      'description': [null, Validators.required],
      'status': [null, Validators.required]
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.status = 1;
    this.loadAngtable();
  }

  loadAngtable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.TYPES.GET)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  loadType() {
    this.show_loader = true;
    this.persons = [];
    this.typeservice.getAllType()
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("allusers", this.persons)
        }
      });
  }

  updatetype(data) {
    $('html, body').animate({
      scrollTop: 0
    }, this.scrolltime);
    this.buttonname = "Update"
    console.log("data", data)
    this.typeid = data.id;
    this.typename = data.type;
    this.description = data.description;
    this.status = data.active_status;
    this.created_date = data.crt_dt
  }

  deletetype(id) {
    let self = this;
    let sts;
    swal(this.deleteoptions).then((result) => {
      if (result.value) {
        this.show_loader = true;
        this.typeservice.deleteType(id)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res;
              console.log("responce", this.response);
              this.resetform();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadType();
              });
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    })
  }

  formObj: any = {
    typename:
      {
        required: "Type required",
      },
    description:
      {
        required: "Description required",
      },
    status:
      {
        required: "Status required",
      }
  }

  addedittype(data) {
    if (this.typeform.status == "INVALID") {
      var errorMessage = this.masterservice.getFormErrorMessage(this.typeform, this.formObj);
      this.notif.warn('Warning', errorMessage, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
      return false;
    }
    else {

      if (data.typeid) {
        this.show_loader = true;
        this.formdata =
          {
            "id": data.typeid,
            "type": data.typename,
            "description": data.description,
            "active_status": data.status,
            'crt_dt': this.created_date,
          }
        console.log("editformdata ==>", this.formdata);
        this.typeservice.updateType(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadType();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadType();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.show_loader = true;
        this.formdata =
          {
            "type": data.typename,
            "description": data.description,
            "active_status": data.status
          }
        console.log("addformdata ==> ", this.formdata);
        this.typeservice.addType(this.formdata)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.response = res.data;
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadType();
              });
              this.resetform();
              this.notif.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.loadType();
              });
              this.notif.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
    }
  }

  resetform() {
    this.typeform.reset();
    this.buttonname = "Save";
    this.typeform.controls['status'].setValue(1);
  }

}
