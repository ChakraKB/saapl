import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocationCancelComponent } from './allocation-cancel.component';

describe('AllocationCancelComponent', () => {
  let component: AllocationCancelComponent;
  let fixture: ComponentFixture<AllocationCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocationCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocationCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
