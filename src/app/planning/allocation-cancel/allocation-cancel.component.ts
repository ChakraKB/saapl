import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { PlanningService } from '../../services/Planning/planning.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service';
import { CompleterService } from 'ng2-completer';

@Component({
  selector: 'app-allocation-cancel',
  templateUrl: './allocation-cancel.component.html',
  styleUrls: ['./allocation-cancel.component.css']
})
export class AllocationCancelComponent implements OnInit {
  show_loader = false;
  toastertime: any;
  tosterminlength: any;
  parent: any;
  bomdetails: any;
  sclist: any;
  unitid: any;
  sc:any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private planservice: PlanningService,
    private salesservice: SalesorderService,
    private completerService: CompleterService,
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    }

  ngOnInit() {
    this.getsclist();
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getsclist() {
    this.show_loader = true;
    this.planservice.GetPlanCacelSC(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.sclist = this.completerService.local(res.data, 'sc_no', 'sc_no');
      }
    })
  }

  selectedsc(data) {
    var scid = data.originalObject.sales_order_id
    this.show_loader = true;
    this.planservice.GetPlanCancelList(scid).subscribe(res => {
      if (res.status == "success") {
        this.parent = res.data;
        this.bomdetails = res.data1
      }
    })
  }

  cancelalloc(item, data) {
    switch (data) {
      case 'alloc':
        if (item.cancelallocqty > item.alloc_qty) {
          item.cancelallocqty = 0;
          break;
        }
      case 'purch':
        if (item.cancelpurchcqty > item.purchase_qty) {
          item.cancelpurchcqty = 0;
          break;
        }
      case 'wippurchalloc':
        if (item.wip_purchase_cancel_qty > item.wip_purchase_alloc_qty) {
          item.wip_purchase_cancel_qty = 0;
          break;
        }
      case 'wipprocalloc':
        if (item.wip_process_cancel_qty > item.wip_process_alloc_qty) {
          item.wip_process_cancel_qty = 0;
          break;
        }
    }
  }

  checkcancel(status, item, i, data) {
    if (data == 'parent') {
      if (status == true) {
        this.parent[i].cancel = true
        this.parent[i].hold = false
      }
    } else {
      if (status == true) {
        this.bomdetails[i].cancel = true
        this.bomdetails[i].hold = false
      }
    }

  }
  checkhold(status, item, i, data) {
    if (data == 'parent') {
      if (status == true) {
        this.parent[i].cancel = false
        this.parent[i].hold = true
      }
    } else {
      if (status == true) {
        this.bomdetails[i].cancel = false
        this.bomdetails[i].hold = true
      }
    }
  }

}
