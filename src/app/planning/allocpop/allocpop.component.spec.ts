import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocpopComponent } from './allocpop.component';

describe('AllocpopComponent', () => {
  let component: AllocpopComponent;
  let fixture: ComponentFixture<AllocpopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocpopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocpopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
