import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedPlanComponent } from './completed-plan.component';

describe('CompletedPlanComponent', () => {
  let component: CompletedPlanComponent;
  let fixture: ComponentFixture<CompletedPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
