import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
declare let swal: any;
import { PlanningService } from '../../services/Planning/planning.service';
import { CompleterService } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { StageService } from '../../services/stage/stage.service';
import { UserService } from '../../services/user/user.service';
import { PurchaseTransactionService } from '../../services/Purchase/transactions/purchase-transaction.service';
import { DcService } from '../../services/dc/dc.service';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-completed-plan',
  templateUrl: './completed-plan.component.html',
  styleUrls: ['./completed-plan.component.css']
})
export class CompletedPlanComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtOptionswo: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  show_loader: boolean = false;
  alloclist: any;
  checkeditems: any;
  checkedpurchaseitems: any;
  workorderlist: any;
  worktype: any;
  allstages: any;
  listofactivestages: any;
  contactlist: any;
  allcontacts: any;
  supplierForm: FormGroup
  nopaymentdata: boolean = false;
  showDialog: boolean = false;
  reqQty: any;
  part_no: any;
  sample: any;
  nxtstages: any;
  lineindex: any;
  checkallitem: boolean = false

  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  public UserDetails = JSON.parse(localStorage.getItem("UserDetails"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private planservice: PlanningService,
    private purchaseservice: PurchaseTransactionService,
    private stageservice: StageService,
    private completerService: CompleterService,
    private userservices: UserService,
    private router: Router,
    private http: Http,
    private acroute: ActivatedRoute,
    private dcservice: DcService
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.worktype = AppConstant.APP_CONST.WorkTypes;
    this.supplierForm = this.fb.group({
      itemPayRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.getallocatedlist();
    this.loadStages();
    this.getcontacts();
    this.checkeditems = [];
    this.checkedpurchaseitems = [];
    this.checkallitem = false
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getSupplierform(paymentform) {
    return paymentform.get('itemPayRows').controls
  }
  allsuppliers: any
  getcontacts() {
    this.contactlist = [];
    this.userservices.getContactsByCat("Sub-contract")
      .subscribe(res => {
        if (res) {
          this.allsuppliers = res.data;
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  checkall(status) {
    let self = this;
    if (status == true) {
      this.checkedpurchaseitems = [];
      _.forEach(this.alloclist, function (val) {
        val.checked = true;
        self.checkedpurchaseitems.push(val)
      })
    } else {
      _.forEach(this.alloclist, function (val) {
        val.checked = false
      })
      this.checkedpurchaseitems = [];
    }
    console.log("allchecked", this.checkedpurchaseitems)
  }

  getallocatedlist() {
    this.checkedpurchaseitems = [];
    this.show_loader = true;
    this.planservice.GetAllocatedList().subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.alloclist = res.data.purchaseList;
        this.workorderlist = res.data.workorderList;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
        let self = this
        _.forEach(this.workorderlist, function (val) {
          if (val.parentPartList.length) {
            val.nxtstage = self.completerService.local(val.parentPartList, 'part_no', 'part_no');
          } else {
            val.stages = '<b>Final Stage</b>'
          }
        })
        console.log("workorderlist", JSON.stringify(this.workorderlist))
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }


  loadStages() {
    this.stageservice.getstageByStatus("active")
      .subscribe(res => {
        if (res) {
          this.allstages = res.data
          this.listofactivestages = this.completerService.local(this.allstages, 'stage', 'stage');
          console.log("listofactivestages", this.listofactivestages);
        }
      });
  }

  checkedlist(status, item, i) {
    if (status == true) {
      this.alloclist[i].checked = true
      this.checkedpurchaseitems.push(item)
    } else {
      this.alloclist[i].checked = false
      this.checkedpurchaseitems.forEach(function (data, index, object) {
        if (data.id == item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkedpurchaseitems);
  }

  clearall() {
    for (var i = 0; i < this.alloclist.length; i++) {
      this.alloclist[i].checked = false;
      this.checkedpurchaseitems = []
    }
  }

  purchaseOrder() {
    if (!this.checkedpurchaseitems.length) {
      this.notif.warn('Warning', "Please Select Any Item", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      });
    } else {
      var selecteditem = [];
      let self = this
      _.forEach(this.checkedpurchaseitems, function (item, key) {
        var obj =
        {
          "unit_id": self.unitid,
          "dept_id": self.UserDetails.department.id,
          "part_id": item.part_id,
          "part_no": item.part_no,
          "part_desc": item.part_desc,
          "base_part_id": item.base_part_id,
          "plan_id": item.plan_id,
          "plan_item_id": item.plan_item_id,
          "sales_order_id": item.sales_order_id,
          "sales_details_id": item.sales_details_id,
          "uom": item.uom,
          "qty": item.purchase_qty + item.extra_purchase_qty,
          "extra_purchase_qty": item.extra_purchase_qty,
          "delivery_date": item.pconfirm_date
        }
        selecteditem.push(obj)
      })
      console.log("po", selecteditem)
      this.show_loader = true;
      this.purchaseservice.PurchaseAutoIndent(selecteditem).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.checkallitem = false
          this.getallocatedlist();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          });
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          });
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        });
      })
    }
  }

  initPayItemRows() {
    return this.fb.group({
      id: 0,
      supplier_name: "",
      qty: 0,
    });
  }

  addNewpaymentRow() {
    var totalpercentage = 0
    let self = this

    var data: boolean = true;
    var msg = []

    _.forEach(this.supplierForm.value.itemPayRows, function (item, key) {
      if (!self.supplierForm.value.itemPayRows.length) {
        data = true
      } else {
        if (item.qty) {
          var sum = +item.qty
          totalpercentage += sum;
        } else {
          data = false
          self.notif.warn('Warning', "Please Enter Qty", {
            timeOut: self.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: self.tosterminlength
          });
          return false;
        }
      }
    });
    if (data) {
      if (totalpercentage) {
        if (totalpercentage < this.reqQty) {
          this.nopaymentdata = true;
          const control = <FormArray>this.supplierForm.controls["itemPayRows"];
          control.push(this.initPayItemRows());
        }
        else {
          this.notif.warn('Warning', "Your Req Qty is " + this.reqQty, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          });
        }
      } else {
        this.nopaymentdata = true;
        const control = <FormArray>this.supplierForm.controls["itemPayRows"];
        control.push(this.initPayItemRows());
      }
    }
  }

  deletePayRow(index: number) {
    const control = <FormArray>this.supplierForm.controls["itemPayRows"];
    control.removeAt(index);
    if (this.supplierForm.value.itemPayRows.length == 0) {
      this.nopaymentdata = false;
    }
  }

  checkauthorized(status, item, i) {
    this.workorderlist[i].checked = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.workorderlist[i].checked = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id == item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  check(data, index) {
    var total = 0
    _.forEach(this.supplierForm.value.itemPayRows, function (val) {
      var sum = +val.qty
      total += sum;
    })
    if (total > this.reqQty) {
      this.notif.warn('Warning', "Your Req Qty is " + this.reqQty, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  selectedcontact(data, i) {
    if (data) {
      var part = data.originalObject
      this.workorderlist[i].next_stage_part_id = part.part_id
      this.workorderlist[i].next_stage_part_no = part.part_no
    }
  }

  selectedsupplier(data, index) {
    var supp = data.originalObject;
    let paytemp = this.fb.array([]);
    for (let i = 0; i < this.supplierForm.value.itemPayRows.length; i++) {
      if (index == i) {
        var supplier = _.findWhere(this.allsuppliers, { 'id': supp.id })
        paytemp.push(
          this.fb.group({
            id: supplier.id,
            supplier_name: supplier.supplier_name,
            qty: this.supplierForm.value.itemPayRows[i].qty,
          })
        );
      } else {
        paytemp.push(
          this.fb.group(this.supplierForm.value.itemPayRows[i]));
      }
    }
    this.supplierForm = this.fb.group({
      itemPayRows: paytemp
    });
  }

  showsupplierpop(item, i) {
    this.showDialog = true;
    if (item.part_type == "Sub-Assembly") {
      this.reqQty = item.purchase_qty
    } else { this.reqQty = item.alloc_qty }
    this.part_no = item.part_no
    this.lineindex = i
    if (item.supplier) {
      let paytemp = this.fb.array([]);
      for (let i = 0; i < item.supplier.length; i++) {
        paytemp.push(
          this.fb.group({
            id: item.supplier[i].id,
            supplier_name: item.supplier[i].supplier_name,
            qty: item.supplier[i].qty,
          })
        );
      }
      this.supplierForm = this.fb.group({
        itemPayRows: paytemp
      });
    } else {
      const control = <FormArray>this.supplierForm.controls["itemPayRows"];
      control.push(this.initPayItemRows());
    }
  }

  allocate(ind) {
    var total = 0;
    var valdata: boolean = true
    var valmsg = []
    _.forEach(this.supplierForm.value.itemPayRows, function (val) {
      var sum = +val.qty
      total += sum;
      if (_.isEmpty(val.supplier_name)) {
        valdata = false;
        valmsg.push("Please Select Supplier")
      }
    })
    if (total > this.reqQty || total < this.reqQty) {
      valdata = false;
      valmsg.push("Your Req Qty is " + this.reqQty)
    }
    if (valdata) {
      this.workorderlist[ind].supplier = this.supplierForm.value.itemPayRows;
      this.reset()
    } else {
      this.notif.warn('Warning', valmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
    console.log("allocate", this.workorderlist)
  }

  saveOrder() {
    var validdata: boolean = true;
    var validationMsg = [];
    if (!this.checkeditems.length) {
      validdata = false
      validationMsg.push('Please select any Item')
    } else {
      this.checkeditems.forEach(function (data, index, object) {
        if (data.workorder_type) {
          if (data.workorder_type == "External" && !data.stages) {
            if (!data.supplier) {
              validdata = false;
              validationMsg.push("Please Enter Supplier Details for " + data.part_no)
            }
          }
          if (!data.next_stage && !data.stages && data.part_type != "Sub-Assembly") {
            validdata = false;
            validationMsg.push("Please select next stage for " + data.part_no)
          }
        } else {
          validdata = false;
          validationMsg.push("Please Select Work Type for " + data.part_no)
        }
      });
    }

    if (validdata) {
      var newlist = [];
      this.checkeditems.forEach(element => {
        var list = _.omit(element, ['nxtstage', 'stages', 'next_stage']);
        newlist.push(list)
      });
      var formdata =
      {
        "unit_id": this.unitid,
        "tdcview": newlist
      }
      console.log("formdata", JSON.stringify(formdata));
      this.show_loader = true;
      this.dcservice.CreateDC(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.getallocatedlist();
          this.checkeditems = [];
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  reset() {
    this.supplierForm.reset();
    this.supplierForm = this.fb.group({
      itemPayRows: this.fb.array([])
    });
    this.showDialog = false;
  }

}
