import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingPlanlistComponent } from './pending-planlist.component';

describe('PendingPlanlistComponent', () => {
  let component: PendingPlanlistComponent;
  let fixture: ComponentFixture<PendingPlanlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingPlanlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingPlanlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
