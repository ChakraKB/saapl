import { AfterViewInit, Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { PlanningService } from '../../services/Planning/planning.service';
import { UserService } from '../../services/user/user.service';
import { CompleterService } from 'ng2-completer';
import { Router } from '@angular/router';
class Person {
  id: any;
  role_name: string;
  description: string;
  checkplan: boolean = false;
  selectpart: boolean = false;
}

@Component({
  selector: 'app-pending-planlist',
  templateUrl: './pending-planlist.component.html',
  styleUrls: ['./pending-planlist.component.css']
})
export class PendingPlanlistComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  checkauthorize: any;
  checkeditems: any[];
  filter: any = {};
  show_loader: boolean = false;
  allcontacts: any;
  contactlist: any;
  selectedparts: any;
  show_dialog_loader: boolean = false;
  showDialog: boolean = false;
  showViewDialog: boolean = false;
  planDetailsList: any;
  checkallpartlist: boolean = false;
  dtTrigger: Subject<any> = new Subject();
  plandetails: any;
  @Output() notifyarray: EventEmitter<any> = new EventEmitter();

  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private planservice: PlanningService,
    private userservices: UserService,
    private completerService: CompleterService,
    private router: Router,
    private http: Http) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.checkeditems = [];
    this.selectedparts = []
    this.getcontacts();
    this.searchsc()
  }

  getcontacts() {
    this.show_loader = true;
    this.contactlist = [];
    this.userservices.getContactsByCat("Customer")
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  searchsc() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
      {
        "customer": _.isEmpty(this.filter.customer) ? "" : this.filter.customer,
        "sc_no": _.isEmpty(this.filter.scno) ? "" : this.filter.scno,
        "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
        "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
      }
      this.show_loader = true;
      this.planservice.getAllPendingPlanList(obj).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.persons = res.data;
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  viewdetails(items, i) {
    this.planDetailsList = items.planDetailsList;
    this.showViewDialog = true;
  }

  checkplan(status, item, i) {
    this.persons[i].checkplan = true;
    if (status == true) {
      var form =
      {
        "sc_no": item.sc_no,
        "sc_id": item.sales_order_id,
        "sc_details": item.planDetailsList
      }
      this.checkeditems.push(form);
    } else {
      this.persons[i].checkplan = false;
      this.checkeditems.forEach(function (data, index, object) {
        if (data.sc_id === item.sales_order_id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  showplanpop() {
    this.clearallpart();
    if (this.checkeditems.length) {
      this.showDialog = true;
    } else {
      this.notif.warn('Warning', "Please select any SC", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }
  checkedpart(status, item, ind, i) {
    this.persons[i].selectpart = true;
    if (status == true) {
      this.selectedparts.push(item.sales_detail_id);
    } else {
      this.persons[i].selectpart = false;
      this.selectedparts.forEach(function (data, index, object) {
        if (data === item.sales_detail_id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.selectedparts);
  }

  checkallpart(status) {
    this.selectedparts = []
    if (status == true) {
      for (var i = 0; i < this.checkeditems.length; i++) {
        for (var j = 0; j < this.checkeditems[i].sc_details.length; j++) {
          this.checkeditems[i].sc_details[j].selectpart = true;
          this.selectedparts.push(this.checkeditems[i].sc_details[j].sales_detail_id);
        }
      }
    } else {
      for (var i = 0; i < this.checkeditems.length; i++) {
        for (var j = 0; j < this.checkeditems[i].sc_details.length; j++) {
          this.checkeditems[i].sc_details[j].selectpart = false;
          this.selectedparts = []
        }
      }
    }
    console.log("selectedpart", this.selectedparts)
  }

  close() {
    this.showDialog = false;
    this.showViewDialog = false;
  }

  clearallsc() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkplan = false;
      this.checkeditems = [];
    }
  }

  clearallpart() {
    this.checkallpartlist = false;
    for (var i = 0; i < this.checkeditems.length; i++) {
      for (var j = 0; j < this.checkeditems[i].sc_details.length; j++) {
        this.checkeditems[i].sc_details[j].selectpart = false;
        this.selectedparts = []
      }
    }
  }

  planselectedpart() {
    if (!this.selectedparts.length) {
      this.notif.warn('Warning', "Please select any Part", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      console.log("selectedparts", this.selectedparts)
      var ids = btoa(this.selectedparts)
      this.router.navigate(['/planning'], { queryParams: { "page": ids } })
    }
  }


}
