import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { CompleterService } from 'ng2-completer';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { Location } from '@angular/common';
import * as $ from 'jquery';
import { PlanningService } from '../../services/Planning/planning.service';

import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-plan-cancel',
  templateUrl: './plan-cancel.component.html',
  styleUrls: ['./plan-cancel.component.css']
})
export class PlanCancelComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  tabletimeout: any;
  unitid: any;
  wono: any;
  others: any;
  comp: any = {};
  wolist: any;
  allwo: any;
  itembom: any;
  checkeditems: any;
  showDialog: boolean = false;
  showtab: boolean = false;
  show_dialog_loader:boolean=false;
  BomList: any;
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private completerService: CompleterService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private _fb: FormBuilder,
    private location: Location,
    private planservice: PlanningService
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.checkeditems = [];
    this.getsclist();
    this.persons = [];
    setTimeout(() => {
      this.dtTrigger.next();
    }, 500);
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getsclist() {
    this.show_loader = true;
    this.planservice.GetPlanCacelSC(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.wolist = this.completerService.local(res.data, 'sc_no', 'sc_no');
      }
    })
  }

  getwodetails(data) {
    if (data) {
      this.close()
      var obj = data.originalObject;
      this.show_loader = true
      this.planservice.GetPlanItemBom(obj.sales_order_id).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.persons = [];
      if (this.dtElement.dtInstance) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        })
      } else
        this.dtTrigger.next();
    }
  }

  cancelalloc(item, data) {
    switch (data) {
      case 'alloc':
        if (item.cancelallocqty > item.alloc_qty) {
          item.cancelallocqty = 0;
          break;
        }
      case 'purch':
        if (item.cancelpurchcqty > item.purchase_qty) {
          item.cancelpurchcqty = 0;
          break;
        }
      case 'wippurchalloc':
        if (item.wip_purchase_cancel_qty > item.wip_purchase_alloc_qty) {
          item.wip_purchase_cancel_qty = 0;
          break;
        }
      case 'wipprocalloc':
        if (item.wip_process_cancel_qty > item.wip_process_alloc_qty) {
          item.wip_process_cancel_qty = 0;
          break;
        }
    }
  }
  BomParent: any
  showbom(bom) {
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    })
    console.log("bom", bom)
    this.planservice.GetItemBom(bom.part_id, bom.sales_order_id, bom.id).subscribe(res => {
      if (res.status == "success") {
        this.BomParent = res.data1
        this.BomList = res.data2
      }
    })
  }

  cancelqty(val: any) {
    if (val.cancel_qty > val.quantity) {
      val.cancel_qty = 0
      this.recurseitem(val,"")
    }
    else {
      for (var i = 0; i < this.BomList.length; i++) {
        if (this.BomList[i].parent_plan_item_id == val.plan_item_id) {
          if (this.BomList[i].part_type == "Spare" && this.BomList[i].spare_stage_index != 1) {
            this.BomList[i].cancel_qty = val.qty_cpy * val.cancel_qty
          } else { this.BomList[i].cancel_qty = this.BomList[i].qty_cpy * val.cancel_qty }
          this.recurseitem(this.BomList[i], val)
        }
      }
    }
  }

  recurseitem(data: any, parent: any) {
    for (var i = 0; i < this.BomList.length; i++) {
      if (this.BomList[i].parent_plan_item_id == data.plan_item_id) {
        if (this.BomList[i].part_type == "Spare" && this.BomList[i].spare_stage_index != 1) {
          this.BomList[i].cancel_qty = data.qty_cpy * parent.cancel_qty
        } else {
          this.BomList[i].cancel_qty = this.BomList[i].qty_cpy * data.cancel_qty
        }
        this.recurseitem(this.BomList[i], data)
      }
    }
  }

  savecancel() {
    var validata: boolean = true;
    var validationMsg = []

    if (this.BomParent[0].cancel_qty == 0) {
      validata = false;
      validationMsg.push("Please enter Cacnel Qty")
    }
    if (validata) {
      console.log("formdata", JSON.stringify(this.BomParent))
      console.log("formdata2", JSON.stringify(this.BomList))
      this.show_loader = true;
      this.planservice.CancelPlan(this.BomParent, this.BomList).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
          this.close();
          this.reset();
        } else {
          this.show_loader = false;
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  close() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

  reset() {
    this.wono = ""
    this.persons = [];
    if (this.dtElement.dtInstance) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      })
    } else
      this.dtTrigger.next();
  }

}
