import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanItemCancelComponent } from './plan-item-cancel.component';

describe('PlanItemCancelComponent', () => {
  let component: PlanItemCancelComponent;
  let fixture: ComponentFixture<PlanItemCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanItemCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanItemCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
