import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { PlanningService } from '../../services/Planning/planning.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service';
import { CompleterService } from 'ng2-completer';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-plan-item-cancel',
  templateUrl: './plan-item-cancel.component.html',
  styleUrls: ['./plan-item-cancel.component.css']
})
export class PlanItemCancelComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  show_loader = false;
  toastertime: any;
  tosterminlength: any;
  parent: any;
  bomdetails: any;
  sclist: any;
  unitid: any;
  sc: any;
  checkeditems: any;
  alloclist: any;
  checkallitem: boolean = false

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private planservice: PlanningService,
    private salesservice: SalesorderService,
    private completerService: CompleterService,
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.getsclist();
    this.checkeditems = [];
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getsclist() {
    this.show_loader = true;
    this.planservice.GetPlanCacelSC(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.sclist = this.completerService.local(res.data, 'sc_no', 'sc_no');
        this.parent = [];
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      }
    })
  }

  selectedsc(data) {
    if (data) {
      var scid = data.originalObject.sales_order_id
      this.show_loader = true;
      this.planservice.GetCancelList(scid).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.parent = res.data;
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        }
      })
    }
  }

  checkall(status) {
    let self = this;
    if (status == true) {
      this.checkeditems = [];
      _.forEach(this.parent, function (val) {
        val.checked = true;
        self.checkeditems.push(val)
      })
    } else {
      _.forEach(this.parent, function (val) {
        val.checked = false
      })
      this.checkeditems = [];
    }
    console.log("allchecked", this.checkeditems)
  }

  checkedlist(status, item, i) {
    if (status == true) {
      this.parent[i].checked = true
      this.checkeditems.push(item)
    } else {
      this.parent[i].checked = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id == item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  submit() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    if (!this.sc) {
      validdata = false;
      validationerrorMsg.push("Please select SC No")
    } else if (!this.checkeditems.length) {
      validdata = false;
      validationerrorMsg.push("Please select any items")
    }

    if (validdata) {
      this.show_loader = true;
      this.planservice.CacnelPlanItem(this.checkeditems).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.reset();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }

  reset() {
    this.sc = "";
    this.parent = []
    this.checkeditems = []
    this.checkallitem = false
    this.getsclist();
  }

}
