import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
declare let swal: any;
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { PlanningService } from '../../services/Planning/planning.service';
import { CompleterService } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { isUndefined } from 'util';
import { ChangeDetectorRef } from '@angular/core';
class Person {
  id: any;
  role_name: string;
  description: string;
  checkplan: boolean = false;
  selectpart: boolean = false;
}
@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css']
})
export class PlanningComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  toastertime: number;
  tabletimeout: any;
  tosterminlength: number;
  dtTrigger: Subject<any> = new Subject();
  planlist: any;
  treetable: any;
  plandetails: any;
  planids: any
  show_loader: boolean = false;
  selectedparts: any;
  plan: any = {};
  treetable2: any;
  itemdetails: any;
  showDialog: boolean = false;
  checkeditems: any;
  showtab: boolean = false;
  bomdetails: any;
  parent: any;
  showAllocDialog: any;
  grpplanids: any;
  grpstrpartids: any;
  salesdetails: any;
  maritalStatus = { status: 'Nothing selected' };
  selectedwip: any;
  wiptotal: any;
  wipproctotal: any;
  show_dialog_loader: boolean = false
  statuses: string[] = [
    'Single',
    'Married',
    'Divorced',
    'Common-law',
    'Visiting'
  ];
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private planservice: PlanningService,
    private completerService: CompleterService,
    private router: Router,
    private http: Http,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.planids = this.acroute.snapshot.queryParamMap.get('page');
  }
  ngOnInit() {
    this.getpartdetails();
    this.selectedparts = [];
    this.checkeditems = [];
    this.selectedwip = []
    this.wiptotal = 0
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  radioChange(data) {
    console.log("data", data);
  }
  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getpartdetails() {
    if (this.planids != null) {
      var ids = atob(this.planids);
      var planids = ids.split(',');
    } else {
      planids = []
    }

    console.log("planids", planids)
    this.show_loader = true
    this.planservice.GetPlanDetails(planids).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.plandetails = res.data;
        this.plan.planno = this.plandetails.plan_no;
        this.treetable = this.plandetails.allocationList;
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, (err) => {
      this.show_loader = false;
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  checkauthorized(status, item, i) {
    this.plandetails[i].checkauthorize = true
    if (status == true) {
      var obj = {
        'part_id': item.part_id,
        'plan_idlist': item.plan_idlist,
        'part_no': item.part_no,
        'sales_details_idlist' : item.sales_details_idlist
      }
      this.checkeditems.push(obj)
    } else {
      this.plandetails[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.part_id == item.part_id && data.plan_idlist == item.plan_idlist && data.part_no == item.part_no) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  clearall() {
    for (var i = 0; i < this.plandetails.length; i++) {
      this.plandetails[i].checkauthorize = false;
      this.checkeditems = []
    }
  }
  parentpartno: any
  group() {
    if (!this.checkeditems.length) {
      this.notif.warn('Warning', "Please select any similar parts", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    } else {
      this.show_loader = true;
      this.planservice.GetGroupBomList(this.checkeditems).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false
          this.grpplanids = res.plan_id
          this.grpstrpartids = res.str_part_id
          this.parent = res.data;
          this.bomdetails = res.data1;
          if (this.parent.length == 1) {
            this.parentpartno = this.parent[0].part_no;
            console.log("pp", this.parentpartno)
          }
          let self = this
          _.map(this.bomdetails, function (val) {
            val.balance = 0
            return val
          })
          _.map(this.parent, function (val) {
            val.balance = 0
            return val
          })
          _.forEach(this.parent, function (val) {
            if (val.wip_purchase_list) {
              _.forEach(val.wip_purchase_list, function (val2) {
                if (val2.status == 2) {
                  _.forEach(val.detailList, function (val3) {
                    val3.wip_purchase_list.push(val2)
                  })
                }
              })

              _.forEach(val.wip_process_list, function (key) {
                if (key.status == 2) {
                  _.forEach(val.detailList, function (key2) {
                    key2.wip_purchase_list.push(key)
                  })
                }
              })
            }
          })
          _.forEach(this.bomdetails, function (val) {
            if (val.wip_purchase_list) {
              _.forEach(val.wip_purchase_list, function (val2) {
                if (val2.status == 2) {
                  _.forEach(val.detailList, function (val3) {
                    val3.wip_purchase_list.push(val2)
                  })
                }
              })

              _.forEach(val.wip_purchase_list, function (key) {
                if (key.status == 2) {
                  _.forEach(val.detailList, function (key2) {
                    key2.wip_purchase_list.push(key)
                  })
                }
              })
            }
          })
          console.log("parent ====>", this.parent);
          console.log("bom ====>", this.bomdetails);
          this.showtab = true;
          setTimeout(() => {
            jQuery('#detail')[0].click();
          });
        } else {
          this.show_loader = false
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })

    }
  }

  getitemdetail(data) {
    this.itemdetails = data;
    this.showDialog = true;
  }
  showwip() {
    if ($('#plus').hasClass('animate_plus')) {
      $('#plus').addClass('animate_cross');
      $('#plus').removeClass('animate_plus');
    } else {
      $('#plus').addClass('animate_plus')
      $('#plus').removeClass('animate_cross');
    }
    $("#wiptable").slideToggle(500);
  }
  showwiprocess() {
    if ($('#pluss').hasClass('animate_plus')) {
      $('#pluss').addClass('animate_cross');
      $('#pluss').removeClass('animate_plus');
    } else {
      $('#pluss').addClass('animate_plus')
      $('#pluss').removeClass('animate_cross');
    }
    $("#wiprocesstable").slideToggle(500);
  }
  showallocpop(salesdtls) {
    var allocatedtot = 0
    salesdtls.detailList.forEach(function (val) {
      val.alloccpy = val.alloc_qty;
      val.purchcpy = val.purchase_qty;
      val.wippurchcpy = val.wip_purchase_alloc_qty;
      val.wipproccpy = val.wip_process_alloc_qty;
      val.open_cpy = val.open_qty;
      allocatedtot += (+val.old_alloc_qty)
    })
    salesdtls.detailList[0].totopen_qty = salesdtls.detailList[0].open_cpy + (+allocatedtot)
    _.forEach(salesdtls.wip_purchase_list, function (val) {
      if (val.status == 2) {
        val.check = true;
      }
    })
    _.forEach(salesdtls.wip_process_list, function (val) {
      if (val.status == 2) {
        val.checked = true;
      }
    })
    this.showAllocDialog = true;
    this.salesdetails = salesdtls;
  }

  reset(salesdetails) {
    var b = []
    salesdetails.detailList.forEach(function (item) {
      item.alloc_qty = item.alloccpy;
      item.purchase_qty = item.purchcpy;
      item.wip_purchase_alloc_qty = item.wippurchcpy;
      item.wip_process_alloc_qty = item.wipproccpy;
      item.open_qty = item.open_cpy;
      var a = _.omit(item, 'alloccpy', 'purchcpy')
      b.push(a)
    })
    salesdetails.detailList = b
    console.log("salesdetails", salesdetails)
    this.showAllocDialog = false;
  }

  calcqty(bom) {
    var tot = 0
    for (var i = 0; i < bom.detailList.length; i++) {
      tot += +bom.detailList[i].quantity;
      bom.qty = tot.toFixed(2)
    }
    return tot
  }

  allocmain(salesdetails) {
    var totdetalloc = 0
    var totdetpurch = 0
    var totxtrapurch = 0
    var totwipproc = 0
    var totwippurch = 0
    for (var j = 0; j < salesdetails.detailList.length; j++) {
      totdetalloc += (+salesdetails.detailList[j].alloc_qty);
      totdetpurch += (+salesdetails.detailList[j].purchase_qty);
      totxtrapurch += (+salesdetails.detailList[j].extra_purchase_qty)
      totwipproc += (+salesdetails.detailList[j].wip_process_alloc_qty);
      totwippurch += (+salesdetails.detailList[j].wip_purchase_alloc_qty);
    }
    salesdetails.alloc_qty = totdetalloc;
    salesdetails.purchase_qty = totdetpurch + totxtrapurch;
    salesdetails.wip_purchase_alloc_qty = totwippurch;
    salesdetails.wip_process_alloc_qty = totwipproc;
    salesdetails.balance = (salesdetails.qty) - (salesdetails.alloc_qty + salesdetails.purchase_qty + salesdetails.wip_purchase_alloc_qty + salesdetails.wip_process_alloc_qty);
  }

  allocate(salesdetails) {
    this.show_dialog_loader = true;
    this.showAllocDialog = false;
    this.allocmain(salesdetails)
    for (var i = 0; i < salesdetails.detailList.length; i++) {
      for (var j = 0; j < this.bomdetails.length; j++) {
        for (var k = 0; k < this.bomdetails[j].detailList.length; k++) {
          if (salesdetails.part_type == "Spare") {
            this.recursespare(salesdetails)
          }
          else if (this.bomdetails[j].detailList[k].base_part_id == salesdetails.detailList[i].part_id &&
            this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[i].plan_item_id
            || this.bomdetails[j].detailList[k].parent_part_no == salesdetails.detailList[i].part_no
            && this.bomdetails[j].detailList[k].base_part_id == salesdetails.detailList[i].base_part_id
            && this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[i].plan_item_id) {
            if (salesdetails.part_type == "Spare") {
              this.recursespare(salesdetails)
            } else {
              if (this.bomdetails[j].part_type == "Spare") {
                this.alloctofirst(this.bomdetails[j], salesdetails)
              } else if (salesdetails.detailList[i].balance == 0) {
                this.bomdetails[j].detailList[k].quantity = (this.bomdetails[j].detailList[k].qtycpy) * ((+salesdetails.detailList[i].purchase_qty) + (+salesdetails.detailList[i].extra_purchase_qty))
                this.bomdetails[j].detailList[k].alloc_qty = 0;
                this.bomdetails[j].detailList[k].purchase_qty = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                this.bomdetails[j].detailList[k].wip_process_alloc_qty = 0;
                this.bomdetails[j].detailList[k].wip_purchase_alloc_qty = 0;
                this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                this.allocmain(this.bomdetails[j]);
                this.recursemain(this.bomdetails[j])
              } else {
                this.bomdetails[j].detailList[k].quantity = (this.bomdetails[j].detailList[k].qtycpy) * ((+salesdetails.detailList[i].purchase_qty) + (+salesdetails.detailList[i].extra_purchase_qty))
                this.bomdetails[j].detailList[k].alloc_qty = 0;
                this.bomdetails[j].detailList[k].purchase_qty = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                this.bomdetails[j].detailList[k].wip_process_alloc_qty = 0;
                this.bomdetails[j].detailList[k].wip_purchase_alloc_qty = 0;
                this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                console.log("childmain", this.bomdetails[j])
                this.allocmain(this.bomdetails[j])
                this.recursemain(this.bomdetails[j])
              }
            }
          } else {
            this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
            if (this.bomdetails[j].part_type == "Spare") {
              this.setuniquestage(salesdetails)
            }
          }
        }
      }
    }
    this.show_dialog_loader = false;
  }


  recursemain(salesdetails) {
    for (var i = 0; i < salesdetails.detailList.length; i++) {
      for (var j = 0; j < this.bomdetails.length; j++) {
        for (var k = 0; k < this.bomdetails[j].detailList.length; k++) {
          if (this.bomdetails[j].detailList[k].base_part_id == salesdetails.detailList[i].part_id
            || this.bomdetails[j].detailList[k].parent_part_no == salesdetails.detailList[i].part_no
            && this.bomdetails[j].detailList[k].base_part_id == salesdetails.detailList[i].base_part_id
            && this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[i].plan_item_id) {
            if (salesdetails.part_type == "Spare") {
              this.bomdetails[j].detailList[k].quantity = salesdetails.detailList[i].quantity
            } else if (this.bomdetails[j].part_type == "Spare") {
              this.alloctofirst(this.bomdetails[j], salesdetails)
            } else {
              if (salesdetails.detailList[i].balance == 0) {
                this.bomdetails[j].detailList[k].quantity = (this.bomdetails[j].detailList[k].qtycpy) * ((+salesdetails.detailList[i].purchase_qty) + (+salesdetails.detailList[i].extra_purchase_qty))
                this.bomdetails[j].detailList[k].alloc_qty = 0;
                this.bomdetails[j].detailList[k].purchase_qty = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                this.bomdetails[j].detailList[k].wip_process_alloc_qty = 0;
                this.bomdetails[j].detailList[k].wip_purchase_alloc_qty = 0;
                this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                this.allocmain(this.bomdetails[j]);
                this.recursemain(this.bomdetails[j]);
              } else {
                this.bomdetails[j].detailList[k].quantity = (this.bomdetails[j].detailList[k].qtycpy) * ((+salesdetails.detailList[i].purchase_qty) + (+salesdetails.detailList[i].extra_purchase_qty))
                this.bomdetails[j].detailList[k].alloc_qty = 0;
                this.bomdetails[j].detailList[k].purchase_qty = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                this.bomdetails[j].detailList[k].wip_process_alloc_qty = 0;
                this.bomdetails[j].detailList[k].wip_purchase_alloc_qty = 0;
                this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
                this.allocmain(this.bomdetails[j]);
                this.recursemain(this.bomdetails[j])
              }
            }
          } else {
            this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
          }
        }
      }
    }
  }

  alloctofirst(data, salesdetails) {
    for (var m = 0; m < salesdetails.detailList.length; m++) {
      for (var i = 0; i < data.detailList.length; i++) {
        for (var j = 0; j < this.bomdetails.length; j++) {
          for (var k = 0; k < this.bomdetails[j].detailList.length; k++) {
            if (this.bomdetails[j].detailList[k].parent_part_no == data.detailList[i].part_no && this.bomdetails[j].detailList[k].stage == "First" && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[m].sales_order_id ||
              this.bomdetails[j].detailList[k].stage == "No_stage" && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[m].sales_order_id && this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[m].plan_item_id) {
              this.bomdetails[j].detailList[k].quantity = ((this.bomdetails[j].detailList[k].qtycpy) * ((+salesdetails.detailList[m].purchase_qty) + (+salesdetails.detailList[m].extra_purchase_qty))).toFixed(2)
              this.bomdetails[j].detailList[k].alloc_qty = 0;
              this.bomdetails[j].detailList[k].purchase_qty = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
              this.bomdetails[j].detailList[k].wip_process_alloc_qty = 0;
              this.bomdetails[j].detailList[k].wip_purchase_alloc_qty = 0;
              this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
              this.allocmain(this.bomdetails[j]);
              if (this.bomdetails[j].detailList[k].stage != "No_stage") {
                this.Autorecursespare(this.bomdetails[j])
              }
            }
          }
        }
      }
    }
  }

  // alloctofirst(data, salesdetails) {
  //   for (var m = 0; m < salesdetails.detailList.length; m++) {
  //     for (var i = 0; i < data.detailList.length; i++) {
  //       for (var j = 0; j < this.bomdetails.length; j++) {
  //         for (var k = 0; k < this.bomdetails[j].detailList.length; k++) {
  //           if (this.bomdetails[j].detailList[k].parent_part_no == data.detailList[i].part_no && this.bomdetails[j].detailList[k].stage == "First" && this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[m].plan_item_id ||
  //             this.bomdetails[j].detailList[k].stage == "No_stage" && this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[m].plan_item_id) {
  //             this.bomdetails[j].detailList[k].quantity = (this.bomdetails[j].detailList[k].qtycpy) * ((+salesdetails.detailList[m].purchase_qty) + (+salesdetails.detailList[m].extra_purchase_qty))
  //             this.bomdetails[j].detailList[k].alloc_qty = 0;
  //             this.bomdetails[j].detailList[k].purchase_qty = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
  //             this.bomdetails[j].detailList[k].wip_process_alloc_qty = 0;
  //             this.bomdetails[j].detailList[k].wip_purchase_alloc_qty = 0;
  //             this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
  //             this.allocmain(this.bomdetails[j]);
  //             if (this.bomdetails[j].detailList[k].stage != "No_stage") {
  //               this.Autorecursespare(this.bomdetails[j])
  //             }
  //           }
  //         }
  //       }
  //     }
  //   }
  // }

  Autorecursespare(salesdetails) {
    for (var i = 0; i < salesdetails.detailList.length; i++) {
      for (var j = 0; j < this.bomdetails.length; j++) {
        for (var k = 0; k < this.bomdetails[j].detailList.length; k++) {
          if (this.bomdetails[j].detailList[k].part_type == "Spare") {
            if (this.bomdetails[j].detailList[k].parent_part_no == salesdetails.detailList[i].parent_part_no &&
              this.bomdetails[j].detailList[k].part_no != salesdetails.detailList[i].part_no && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[i].sales_order_id ||
              this.bomdetails[j].detailList[k].part_no == salesdetails.detailList[i].parent_part_no && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[i].sales_order_id ||
              this.bomdetails[j].detailList[k].parent_part_no == salesdetails.detailList[i].part_no && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[i].sales_order_id) {
              this.bomdetails[j].detailList[k].quantity = salesdetails.detailList[i].quantity
              this.bomdetails[j].detailList[k].alloc_qty = 0;
              this.bomdetails[j].detailList[k].purchase_qty = 0
              this.bomdetails[j].detailList[k].wip_process_alloc_qty = 0;
              this.bomdetails[j].detailList[k].wip_purchase_alloc_qty = 0;
              this.bomdetails[j].detailList[k].balance = (this.bomdetails[j].detailList[k].quantity) - ((+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].purchase_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty))
              this.allocmain(this.bomdetails[j]);
            }
          }
        }
      }
    }
  }

  recursespare(salesdetails) {
    for (var i = 0; i < salesdetails.detailList.length; i++) {
      var totalloc = 0
      var totqty = 0
      var totpurch = 0
      for (var j = 0; j < this.bomdetails.length; j++) {
        for (var k = 0; k < this.bomdetails[j].detailList.length; k++) {

          if (this.bomdetails[j].detailList[k].part_type == "Spare") {
            if (this.bomdetails[j].detailList[k].parent_part_no == salesdetails.detailList[i].parent_part_no && this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[i].parent_plan_item_id && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[i].sales_order_id && this.bomdetails[j].detailList[k].part_no == salesdetails.detailList[i].part_no ||
              this.bomdetails[j].detailList[k].parent_part_no == salesdetails.detailList[i].parent_part_no && this.bomdetails[j].detailList[k].plan_item_id == salesdetails.detailList[i].parent_plan_item_id && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[i].sales_order_id ||
              this.bomdetails[j].detailList[k].part_no == salesdetails.detailList[i].parent_part_no && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[i].sales_order_id ||
              this.bomdetails[j].detailList[k].parent_part_no == salesdetails.detailList[i].part_no && this.bomdetails[j].detailList[k].sales_order_id == salesdetails.detailList[i].sales_order_id && this.bomdetails[j].detailList[k].parent_plan_item_id == salesdetails.detailList[i].plan_item_id) {
              totalloc += (+this.bomdetails[j].detailList[k].alloc_qty) + (+this.bomdetails[j].detailList[k].wip_process_alloc_qty) + (+this.bomdetails[j].detailList[k].wip_purchase_alloc_qty);
              totqty = this.bomdetails[j].detailList[k].quantity;
              totpurch += this.bomdetails[j].detailList[k].purchase_qty;
              if (totalloc > totqty) {
                this.notif.warn('Warning', "Your entered qty is higher than Req Qty", {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
                salesdetails.detailList[i].alloc_qty = 0;
                this.allocmain(salesdetails);
                return false;
              }
              var allocbal = totqty - totalloc;
              if (allocbal >= 0 && totpurch != 0) {
                for (var m = 0; m < this.bomdetails.length; m++) {
                  for (var n = 0; n < this.bomdetails[m].detailList.length; n++) {
                    if (this.bomdetails[m].detailList[n].parent_part_no == salesdetails.detailList[i].parent_part_no || this.bomdetails[m].detailList[n].part_no == salesdetails.detailList[i].parent_part_no || this.bomdetails[m].detailList[n].parent_part_no == salesdetails.detailList[i].part_no) {
                      if (this.bomdetails[m].detailList[n].stage == "First" && this.bomdetails[m].detailList[n].sales_order_id == salesdetails.detailList[i].sales_order_id) {
                        this.bomdetails[m].detailList[n].purchase_qty = allocbal;
                        this.allocmain(this.bomdetails[m]);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  setuniquestage(data) {
    for (var i = 0; i < this.bomdetails.length; i++) {
      if (this.bomdetails[i].part_type == "Spare" && this.bomdetails[i].part_no != data.part_no) {
        if (data.balance == data.qty) {
          for (var k = 0; k < data.detailList.length; k++) {
            for (var j = 0; j < this.bomdetails[i].detailList.length; j++) {
              this.bomdetails[i].detailList[j].alloc_qty = data.detailList[k].alloc_qty;
              this.bomdetails[i].detailList[j].purchase_qty = data.detailList[k].purchase_qty;
              this.bomdetails[i].detailList[j].quantity = data.detailList[k].quantity;
              this.allocmain(this.bomdetails[i]);
            }
          }
        } else {
          for (var j = 0; j < this.bomdetails[i].detailList.length; j++) {
            // this.bomdetails[i].detailList[j].alloc_qty = 0;
            // this.bomdetails[i].detailList[j].purchase_qty = 0;
            // this.bomdetails[i].detailList[j].quantity = 0;
            this.allocmain(this.bomdetails[i]);
          }
        }
      }
    }
  }

  setchildempty(data) {
    for (var i = 0; i < data.detailList.length; i++) {
      for (var j = 0; j < this.bomdetails.length; j++) {
        for (var k = 0; k < this.bomdetails[j].detailList.length; k++) {
          if (this.bomdetails[j].detailList[k].parent_part_no == data.detailList[i].part_no) {
            if (data.detailList[i].quantity == data.detailList[i].purchase_qty) {
              this.bomdetails[j].detailList[k].alloc_qty = 0;
              this.bomdetails[j].detailList[k].purchase_qty = 0;
              this.bomdetails[j].detailList[k].quantity = data.detailList[i].quantity
            } else {
              this.bomdetails[j].detailList[k].quantity = data.detailList[i].quantity
            }
          }
        }
      }
    }
  }


  checkfrstwip(salesdtls, item) {
    _.forEach(salesdtls.detailList, function (val) {
      val.wip_purchase_list.push(item)
    })
    console.log("detaillist", salesdtls)
  }

  checkwip(status, item, index) {
    _.forEach(this.salesdetails.detailList, function (val) {
      val.wip_process_alloc_qty = 0;
      val.wip_purchase_alloc_qty = 0
    })
    if (status == true) {
      _.forEach(this.salesdetails.detailList, function (val) {
        val.wip_purchase_list.push(item)
      })
    } else {
      this.salesdetails.detailList.forEach(function (data, index, object) {
        data.wip_purchase_list.forEach(function (data1, indx, obj) {
          if (data1.id === item.id) {
            obj.splice(indx, 1);
          }
        })
      });
    }
    console.log("data", this.salesdetails.detailList)
  }

  checkwiprocess(status, item, index) {
    _.forEach(this.salesdetails.detailList, function (val) {
      val.wip_process_alloc_qty = 0;
      val.wip_purchase_alloc_qty = 0
    })
    if (status == true) {
      _.forEach(this.salesdetails.detailList, function (val) {
        val.wip_process_list.push(item)
      })
    } else {
      this.salesdetails.detailList.forEach(function (data, index, object) {
        data.wip_process_list.forEach(function (data1, indx, obj) {
          if (data1.id === item.id) {
            obj.splice(indx, 1);
          }
        })
      });
    }
    console.log("data", this.salesdetails.detailList)
  }

  isReadOnly(item) {
    if (item.part_type == "Spare" && item.stage == "First") {
      return true
    }
  }

  calcwiptotal() {
    this.wiptotal = 0;
    let self = this
    _.forEach(this.salesdetails.wip_purchase_list, function (val) {
      if (val.check == true) {
        self.wiptotal += val.quantity
      }
    })
    return self.wiptotal
  }

  calcwiprocesstotal() {
    this.wipproctotal = 0;
    let self = this
    _.forEach(this.salesdetails.wip_process_list, function (val2) {
      if (val2.checked == true) {
        self.wipproctotal += val2.quantity
      }
    })
    return self.wipproctotal
  }

  recursechild(item) {
    if (item.part_type == "Spare") {
      for (var l = 0; l < this.bomdetails.length; l++) {
        var parentnoschild = this.bomdetails[l].parent_part_no.split(',');
        if (_.contains(parentnoschild, item.part_no)) {
          this.bomdetails[l].qty = item.qty;
          this.bomdetails[l].balance = item.balance;
          for (var k = 0; k < this.bomdetails[l].detailList.length; k++) {
            this.bomdetails[l].detailList[k].quantity = this.bomdetails[l].detailList[k].qtycpy * item.balance
            this.bomdetails[l].detailList[k].balance = (this.bomdetails[l].detailList[k].quantity) - (this.bomdetails[l].detailList[k].alloc_qty + this.bomdetails[l].detailList[k].purchase_qty)
            this.recursechild(this.bomdetails[l])
          }
        }
      }
    } else {
      for (var l = 0; l < this.bomdetails.length; l++) {
        var parentnoschild = this.bomdetails[l].parent_part_no.split(',');
        if (_.contains(parentnoschild, item.part_no)) {
          this.bomdetails[l].qty = this.bomdetails[l].qtycpy * item.balance;
          this.bomdetails[l].balance = (this.bomdetails[l].qty) - (this.bomdetails[l].alloc_qty + this.bomdetails[l].purchase_qty);
          for (var k = 0; k < this.bomdetails[l].detailList.length; k++) {
            this.bomdetails[l].detailList[k].quantity = this.bomdetails[l].detailList[k].qtycpy * item.balance
            this.bomdetails[l].detailList[k].balance = (this.bomdetails[l].detailList[k].quantity) - (this.bomdetails[l].detailList[k].alloc_qty + this.bomdetails[l].detailList[k].purchase_qty)
            this.recursechild(this.bomdetails[l])
          }
        }
      }
    }
  }
  // ======================================== Till 5-9-2018 =======================================

  // checkbalanceqty(reqqty, allocqty, openqty, purchqty, item) {
  //   if (allocqty == null && purchqty == null) {
  //     return reqqty
  //   } else
  //     var wiptot = (+item.wip_process_alloc_qty) + (+item.wip_purchase_alloc_qty)
  //   var totstck = (+allocqty) + (+purchqty);
  //   var total = wiptot + totstck
  //   var balance = (+reqqty) - (+total);
  //   item.balance = balance
  //   return balance
  // }

  //=================================================================================================
  checkfstsparebal(reqqty, allocqty, purchqty, item) {
    if (allocqty == null && purchqty == null) {
      return reqqty
    } else
      var wiptot = (+item.wip_process_alloc_qty) + (+item.wip_purchase_alloc_qty)
    var totstck = (+allocqty)
    var total = wiptot + totstck
    var balance = (+reqqty) - (+total);
    item.balance = balance
    item.purchase_qty = balance
  }

  checkbalanceqty(reqqty, allocqty, purchqty, item) {
    if (allocqty == null && purchqty == null) {
      return reqqty
    } else
      var wiptot = (+item.wip_process_alloc_qty) + (+item.wip_purchase_alloc_qty)
    var totstck = (+allocqty)
    var total = wiptot + totstck
    var balance = (+reqqty) - (+total);
    if (item.part_type == "Spare") {
      item.balance = balance;
    } else {
      item.balance = balance
      item.purchase_qty = balance
    }
    return balance
  }

  checkmainbalanceqty(reqqty, allocqty, purchqty, item) {
    if (allocqty == null && purchqty == null) {
      return reqqty
    } else
      var wiptot = (+item.wip_process_alloc_qty) + (+item.wip_purchase_alloc_qty)
    var totstck = (+allocqty)
    var totstckqty = (+allocqty) + (+purchqty)
    var total = wiptot + totstckqty
    var balance = (+reqqty) - (+total);
    item.balance = balance
    item.purchase_qty = item.purchase_qty
    return balance
  }


  // checkallocation(item, allocat_qty, ind, purch_qty) {
  //   var opentot = 0;
  //   _.forEach(this.salesdetails.detailList, function (val) {
  //     opentot += (+val.alloc_qty)
  //   })
  //   var balopenstk = this.salesdetails.detailList[0].totopen_qty - opentot
  //   var a = (+allocat_qty) + (+purch_qty);
  //   var b = (+item.wip_process_alloc_qty) + (+item.wip_purchase_alloc_qty);
  //   var e = a + b
  //   if (a != 0) {
  //     if (e > item.quantity || +allocat_qty > item.quantity || balopenstk < 0) {
  //       item.alloc_qty = 0;
  //       item.purchase_qty = 0;
  //     }
  //   }
  //   this.checkbalanceqty(item.quantity, allocat_qty, purch_qty, item)
  // }

  // checkwipallo(item, wipproc_qty, wippurch_qty, ind) {
  //   var allocateswippurch = 0;
  //   var allocateswipproc = 0
  //   _.forEach(this.salesdetails.detailList, function (val) {
  //     allocateswippurch += (+val.wip_purchase_alloc_qty)
  //     allocateswipproc += (+val.wip_process_alloc_qty)
  //   })
  //   var balpurchasewip = this.wiptotal - allocateswippurch
  //   var balprocwip = this.wipproctotal - allocateswipproc
  //   var b = (+item.alloc_qty) + (+item.purchase_qty)
  //   var c = ((+wipproc_qty) + (+wippurch_qty));
  //   var d = b + c
  //   if (c > item.quantity || balprocwip < 0 || balpurchasewip < 0 || (+wipproc_qty) > item.quantity || wippurch_qty > item.quantity) {
  //     item.wip_process_alloc_qty = 0;
  //     item.wip_purchase_alloc_qty = 0
  //   } else if (d > item.quantity) {
  //     item.alloc_qty = 0;
  //     item.purchase_qty = 0;
  //   }
  //   this.checkbalanceqty(item.quantity, wipproc_qty, wippurch_qty, item)
  // }

  // ======================================================= New 12-9-2018 =============================================
  checkallocation(item, allocat_qty, ind, purch_qty) {
    var opentot = 0;
    _.forEach(this.salesdetails.detailList, function (val) {
      opentot += (+val.alloc_qty)
    })
    var balopenstk = this.salesdetails.detailList[0].totopen_qty - opentot
    var a = (+allocat_qty)
    var b = (+item.wip_process_alloc_qty) + (+item.wip_purchase_alloc_qty);
    var e = a + b
    if (a != 0) {
      if (e > item.quantity || +allocat_qty > item.quantity || balopenstk < 0) {
        item.alloc_qty = 0;
        item.purchase_qty = 0;
      }
    }
    if (item.part_type == "Spare" && item.stage == "First" || item.part_type == "Spare" && item.stage == "No_stage") {
      var wiptot = (+item.wip_process_alloc_qty) + (+item.wip_purchase_alloc_qty)
      var totstck = (+allocat_qty)
      var total = wiptot + totstck
      item.purchase_qty = item.quantity - total
      this.checkfstsparebal(item.quantity, item.alloc_qty, item.purchase_qty, item)
    } else {
      this.checkbalanceqty(item.quantity, item.alloc_qty, item.purchase_qty, item)
    }
  }

  checkwipallo(item, wipproc_qty, wippurch_qty, ind) {
    var allocateswippurch = 0;
    var allocateswipproc = 0
    _.forEach(this.salesdetails.detailList, function (val) {
      allocateswippurch += (+val.wip_purchase_alloc_qty)
      allocateswipproc += (+val.wip_process_alloc_qty)
    })
    var balpurchasewip = this.salesdetails.wip_purchase_qty - allocateswippurch
    var balprocwip = this.salesdetails.wip_process_qty - allocateswipproc
    var b = (+item.alloc_qty)
    var c = ((+wipproc_qty) + (+wippurch_qty));
    var d = b + c
    if (c > item.quantity || balprocwip < 0 || balpurchasewip < 0 || (+wipproc_qty) > item.quantity || wippurch_qty > item.quantity) {
      item.wip_process_alloc_qty = 0;
      item.wip_purchase_alloc_qty = 0
    } else if (d > item.quantity) {
      item.alloc_qty = 0;
      item.purchase_qty = 0;
    }
    this.checkbalanceqty(item.quantity, wipproc_qty, wippurch_qty, item)
  }
  // ======================================================= New 12-9-2018 END =============================================

  checkbal(reqqty, allocqty, openqty) {
    if (allocqty == null) {
      return reqqty
    } else
      var purchqty = (+reqqty) - (+allocqty);
    return purchqty
  }


  selectednode(item, status, data) {
    if (status == true) {
      var datachild = _.omit(item, "parent")
      this.selectedparts.push(datachild);
      console.log("selectedparts", this.selectedparts)
    } else {
      this.selectedparts.forEach(function (val, index, object) {
        if (val.data.part_no === data.part_no) {
          object.splice(index, 1);
        }
      });
      console.log("selectedparts", this.selectedparts);
    }
  }

  checkchildqty(selectedparts) {
    var filtered = _.filter(selectedparts, function (o) {
      return o.data.checked_status;
    });
    if (filtered.length != 0) {
      for (var i = 0; i < filtered.length; i++) {
        var tot = (+filtered[i].data.alloc_qty) + (+filtered[i].data.purchase_qty)
        if (tot != filtered[i].data.qty) {
          if (filtered[i].children) {
            this.checkchildqty(filtered[i].children);
          } else {
            return false
          }
        }
      }
    } else {
      return false
    }
  }

  remparent(children) {
    var self = this
    return _.map(children, function (o) {
      if (o.children) {
        o.children = self.remparent(o.children)
      } else {
        return _.omit(o, "parent", "expanded")
      }
      return _.omit(o, "parent", "expanded")
    })
  }

  checkcheckedornot(children) {
    for (var j = 0; j < children.length; j++) {
      if (children[j].data.checked_status == false && (children[j].data.alloc_qty + children[j].data.purchase_qty) != 0) {
        return children[j].data.part_no
      }
      if (children[j].children) {
        this.checkcheckedornot(children[j].children)
      }
    }
  }


  closetab() {
    this.clearall()
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

  checkchild(data) {
    for (var k = 0; k < this.bomdetails.length; k++) {
      for (var l = 0; l < this.bomdetails[k].detailList.length; l++) {
        if (this.bomdetails[k].detailList[l].base_part_id == data.part_id || this.bomdetails[k].detailList[l].parent_part_no == data.part_no) {
          if (this.bomdetails[k].balance == 0 || this.bomdetails[k].balance < 0) {
            return true
          } else {
            this.checkchild(this.bomdetails[k]);
          }
        }
      }
    }
  }

  saveentry(flag) {
    var validdata: boolean = true;
    var validationerrorMsg = []
    if (flag == 2) {
      for (var i = 0; i < this.parent.length; i++) {
        if (this.parent[i].balance != 0) {
          for (var j = 0; j < this.parent[i].detailList.length; j++) {
            if (this.bomdetails.length) {
              for (var k = 0; k < this.bomdetails.length; k++) {
                for (var l = 0; l < this.bomdetails[k].detailList.length; l++) {
                  if (this.bomdetails[k].detailList[l].base_part_id == this.parent[i].detailList[j].part_id) {
                    if (this.bomdetails[k].balance != 0) {
                      if (this.bomdetails[k].part_type != "Spare") {
                        var stat = this.checkchild(this.bomdetails[k])
                        if (isUndefined(stat)) {
                          validdata = false;
                          validationerrorMsg.push("Please enter Purch/Alloc Qty for " + this.parent[i].part_no);
                        }
                      }
                    }
                  }
                }
              }
            } else {
              validdata = false;
              validationerrorMsg.push("Please enter Purch/Alloc Qty for " + this.parent[i].part_no);
            }
          }
        }
      }
      if (this.bomdetails.length) {
        for (var i = 0; i < this.bomdetails.length; i++) {
          if (this.bomdetails[i].openqty != 0 && this.bomdetails[i].alloc_qty == 0) {
            swal({
              title: 'Are you sure?',
              text: "You Have Open Qty in " + this.bomdetails[i].part_no + " But u have not allocated",
              type: 'question',
              width: 500,
              padding: 10,
              showCancelButton: true,
              confirmButtonColor: '#ffaa00',
              cancelButtonColor: '#3085d6',
              confirmButtonText: 'Yes!',
              cancelButtonText: 'No',
            }).then((result) => {
              if (result.value) {
                this.saveplan(flag)
              }
            })
            return false
          }
        }
      }
    }
    if (validdata) {
      this.saveplan(flag)
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  saveplan(flag) {
    console.log("formdata", this.parent, this.bomdetails)
    this.show_loader = true;
    this.planservice.CreateAllocation(this.parent, this.bomdetails, this.grpplanids, this.grpstrpartids, flag).subscribe(res => {
      if (res.status == "success") {
        if (flag == 2) {
          this.show_loader = false;
          this.router.navigate(['/completedplans'])
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.show_loader = false;
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
          this.closetab()
        }
        // this.router.navigate(['/pendingallocation'])
        // this.parent = res.data;
        // this.bomdetails = res.data1;
        // console.log(this.bomdetails);
      } else {
        this.show_loader = false
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }
}
