import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditSalesorderComponent } from './add-edit-salesorder.component';

describe('AddEditSalesorderComponent', () => {
  let component: AddEditSalesorderComponent;
  let fixture: ComponentFixture<AddEditSalesorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditSalesorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditSalesorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
