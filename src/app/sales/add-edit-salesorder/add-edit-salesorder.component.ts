import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service'
import { DepartmentsService } from '../../services/department/departments.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DataTableDirective } from 'angular-datatables';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemsService } from '../../services/items/items.service';
import { UserService } from '../../services/user/user.service';
import { SalesorderService } from '../../services/salesorder/salesorder.service';
import { saveAs as importedSaveAs } from "file-saver";
import { error } from 'util';
@Component({
  selector: 'app-add-edit-salesorder',
  templateUrl: './add-edit-salesorder.component.html',
  styleUrls: ['./add-edit-salesorder.component.css']
})
export class AddEditSalesorderComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  public date = new Date();
  mydisoptionright: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true,
    disableUntil: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() - 1 }
  };

  mydisoptionleft: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    disableUntil: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() - 1 }
  };
  contactlistDropdown: any[];
  toastertime: any;
  tosterminlength: any;
  statusarray: any;
  tabletimeout: any;
  alldepts: any;
  filteredDepts: any[];
  add: boolean = true;
  view: boolean = false;
  nodata: boolean = false;
  adreseditable: boolean = true
  Shiadrsnoneditable: boolean = true;
  Invadrsnoneditable: boolean = true;
  show_update_loader: boolean = false;
  orderForm: FormGroup;
  paymentForm: FormGroup;
  salesdate: any;
  podate: any;
  shipdate: any;
  total: any;
  nopaymentdata: any;
  allcontacts: any;
  contactlist: any[];
  totalquantity: any;
  userdetails: any;
  modearray: any;
  merchandiser: any;
  salesperson: any;
  itemlist: any[];
  allitems: any;
  protected dataService: CompleterData;
  selectedpackingtype: any;
  selectedpaymenttype: any;
  selectedpaytermlist: any;
  paymenTerm: any;
  paytermsLC: any;
  payterms: any;
  allPorts: any;
  scorderarray: any;
  sctypearray: any;
  show_loader: boolean = false;
  response: any;
  url: string;
  orderarray: any;
  payarray: any;
  formdata: any = {};
  Paytype: any;
  status: any;
  scorder: any;
  contact: any;
  sctype: any;
  scno: any;
  category: any;
  pono: any;
  portloading: any;
  postdischarge: any;
  deliveryterm: any;
  mode: any;
  currency: any;
  remarks: any;
  packingtype: any;
  Bundle: any;
  addresstwo: any;
  addressthree: any;
  addressfour: any;
  Invoiceadrstwo: any;
  Invoiceadrsthree: any;
  Invoiceadrsfour: any;
  ShippingAdrstwo: any;
  ShippingAdrsthree: any;
  ShippingAdrsfour: any;
  selected: boolean = true;
  allpayterms: any;
  unitid: any;
  alldeliveryterms: any;
  allcurrencies: any;
  delterms: any;
  adrvalue: boolean = false;
  Invvalue: boolean = false;
  Shipvalue: boolean = false;
  scid: any;
  @ViewChild('fileInput') fileInput: ElementRef;
  @Input() orderdetail: any;
  @Output() notifyfunction: EventEmitter<any> = new EventEmitter();
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notificate: NotificationsService,
    private dptservice: DepartmentsService,
    private http: Http,
    private _sanitizer: DomSanitizer,
    private _fb: FormBuilder,
    private userservices: UserService,
    private itemservice: ItemsService,
    private completerService: CompleterService,
    private salesorder: SalesorderService,
    private acroute: ActivatedRoute,
    private router: Router) {
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.scid = params.id;
        console.log("urlparams", this.scid);
      }
    });

    this.url = this.router.url;
    console.log("url", this.url)
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.userdetails = JSON.parse(localStorage.getItem("UserDetails"));
    console.log("userdetails", this.userdetails.name)
    this.statusarray = AppConstant.APP_CONST.status;
    this.scorderarray = AppConstant.APP_CONST.ScOrder;
    this.sctypearray = AppConstant.APP_CONST.ScType;
    this.modearray = AppConstant.APP_CONST.Mode;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.Paytype = AppConstant.APP_CONST.PaymentTypes;
    this.orderForm = this._fb.group({
      itemRows: this._fb.array([])
    });
    this.paymentForm = this._fb.group({
      itemPayRows: this._fb.array([])
    });
  }

  ngOnInit() {
    console.log("orderdetail", JSON.stringify(this.orderdetail))
    let date = new Date();
    this.totalquantity = 0;
    this.total = 0;
    this.mode = 0;
    this.scorder = 0;
    // this.paymenttermcus = "";
    this.getdepartments();
    this.getcontacts();
    this.getcurrency();
    this.getPorts();
    this.getitems();
    this.getpaymentterms();
    this.getdeliveryterms();
    this.alterdateformat(date);
    var currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    // this.podate = { date: currentdate, formatted: this.salesdate }
    // this.shipdate = { date: currentdate, formatted: this.salesdate };
    this.addNewRow();
    this.addNewpaymentRow();
    this.merchandiser = this.userdetails.name;
    this.salesperson = this.userdetails.name;
    setTimeout(() => {
      this.viewOrderdetails();
    });
  }

  getorderform(orderForm) {
    return orderForm.get('itemRows').controls
  }

  getpaymentform(paymentform) {
    return paymentform.get('itemPayRows').controls
  }

  updateorder() {
    this.notifyfunction.next()
  }
  getcurrency() {
    this.salesorder.getAllCurrencies().subscribe(res => {
      this.allcurrencies = this.completerService.local(res.data, 'short_name', 'short_name');
    })
  }
  getcontacts() {
    this.contactlist = [];
    this.userservices.getContactsByCat("Customer")
      .subscribe(res => {
        if (res) {
          this.allcontacts = this.completerService.local(res.data, 'supplier_name,display_cmpy_name', 'supplier_name');
        }
      });
  }
  getPorts() {
    this.userservices.getPortsByStatus('active')
      .subscribe(res => {
        if (res) {
          this.allPorts = this.completerService.local(res.data, 'port_name', 'port_name');
        }
      });
  }
  getpaymentterms() {
    this.userservices.getallpayterms()
      .subscribe(res => {
        if (res) {
          this.allpayterms = res.data;
          this.payterms = this.completerService.local(res.data, 'term', 'term');
        }
      });
  }

  getdeliveryterms() {
    this.userservices.getAllDeliveryTerms()
      .subscribe(res => {
        if (res) {
          this.alldeliveryterms = res.data;
          this.delterms = this.completerService.local(res.data, 'term', 'term');
        }
      });
  }
  getitems() {
    this.show_loader = true;
    this.itemlist = [];
    this.itemservice.getItemByStatus('active')
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allitems = res.data
          this.dataService = this.completerService.local(this.allitems, "component_desc,component,part_no", 'part_no');
        }
      });
  }

  avatar: any = {};
  filelist: any[];
  Sfiles: any[];
  files: string;
  filenames: any;
  private base64string: any

  download(data) {
    var blob = new Blob([data[0]], { type: data[0].type });
    importedSaveAs(blob, data[0].name)
  }

  onFileChange(event, input) {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    this.filelist = [];
    this.filelist = event.target.files;
    var selectedfiles = event.target.files;
    this.Sfiles = [].slice.call(selectedfiles);
    this.files = this.Sfiles.map(f => f.name).join(', ');
    this.filenames = this.files;
    console.log("file", this.filelist)
  }

  clearFile() {
    this.filelist = [];
    this.fileInput.nativeElement.value = '';
    this.filenames = "";
  }

  numberonly(event) {
    this.masterservice.allowNumberOnly(event);
  }
  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }
  //================== LIST_FORMATTER====================================//
  autocompleListFormatter = (data: any): SafeHtml => {
    let html = `<span>${data.department_name}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }
  ContactListFormatter = (data: any): SafeHtml => {
    let html = `<span>${data.contactname}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }
  ItemListFormatter = (data: any): SafeHtml => {
    let html = `<span>${data.item_no}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }
  //================== LIST_FORMATTER_END====================================//
  initItemRows() {
    return this._fb.group({
      id: "",
      sales_order_id: "",
      line: "",
      partid: 0,
      part_no: "",
      part: "",
      part_type: "",
      partdesc: "",
      component: "",
      component_desc: "",
      squantity: 0,
      material: "",
      materialrev: "",
      drawing: "",
      rev: "",
      Qty: 0,
      uom: "",
      Price: 0,
      shipmentDate: "",
      Remarks: "",
    });
  }
  initPayItemRows() {
    return this._fb.group({
      paymenttype: "",
      paymentterm: "",
      paymentpercentage: 0,
      credit: 0
    });
  }
  //========================= ORDER_FORM =======================
  addNewRow() {
    this.nodata = true;
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    const control = <FormArray>this.orderForm.controls["itemRows"];
    control.removeAt(index);
    this.calculatetotal();
    if (this.orderForm.value.itemRows.length == 0) {
      this.nodata = false;
    }
  }
  //========================= ORDER_FORM_END ===========================

  //========================= PAYMENT_FORM ===========================
  addNewpaymentRow() {
    var totalpercentage = 0
    _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
      var sum = +item.paymentpercentage
      totalpercentage += sum;
    });
    if (totalpercentage) {
      if (totalpercentage < 100) {
        this.nopaymentdata = true;
        const control = <FormArray>this.paymentForm.controls["itemPayRows"];
        control.push(this.initPayItemRows());
      }
      else {

        this.notificate.warn('Warning', "you already entered 100%", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        });
      }
    }
    else {
      this.nopaymentdata = true;
      const control = <FormArray>this.paymentForm.controls["itemPayRows"];
      control.push(this.initPayItemRows());
    }
  }
  deletePayRow(index: number) {
    const control = <FormArray>this.paymentForm.controls["itemPayRows"];
    control.removeAt(index);
    if (this.paymentForm.value.itemPayRows.length == 0) {
      this.nopaymentdata = false;
    }
  }
  //========================= PAYMENT_FORM_END ===========================
  alterdateformat(date) {
    // this.salesdate = formatDate(date);
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }
  getdepartments() {
    this.contactlistDropdown = [];
    this.dptservice.getallDepartments()
      .subscribe(res => {
        if (res) {
          this.alldepts = res.data
        }
      });
  }

  subtotal(data) {
    if (data.Qty != 0 && data.Price != 0) {
      var x = +data.Qty;
      var y = +data.Price;
      var z = x * y
      return z;
    }

  }
  checkednewddress(event) {
    if (event == true) {
      this.adreseditable = false
    }
    else this.adreseditable = true
  }
  checkednewshipaddress(event) {
    if (event == true) {
      this.Shiadrsnoneditable = false
    }
    else this.Shiadrsnoneditable = true
  }
  checkednewinvddress(event) {
    if (event == true) {
      this.Invadrsnoneditable = false
    }
    else this.Invadrsnoneditable = true
  }
  //=========================== Initaites_START ===========================//
  selectedContact: any;
  selectedCurrency: any;
  selectedpload: any;
  selectedpdischarge: any;
  selectedMode: any;
  selectedorderLine: any = {};
  addressone: any;
  Invoiceadrsone: any;
  ShippingAdrsone: any;
  paymenttermcus: any;
  orderDetails: any;
  //==================================INITIATES_END =========================//
  selectedcontact(item) {
    console.log("item ==>", JSON.stringify(item))
    if (item) {
      this.selectedContact = item.originalObject;
      if (this.selectedContact) {

        var citypin = [this.selectedContact.address_city.city_name, this.selectedContact.address_pincode].join(',');
        var invcitypin = [this.selectedContact.inv_addr_city.city_name, this.selectedContact.inv_addr_pincode].join(',');
        var shipcitypin = [this.selectedContact.ship_addr_city.city_name, this.selectedContact.ship_address_pincode].join(',');

        this.addressone = this.selectedContact.address_line1;
        this.addresstwo = citypin;
        this.addressthree = this.selectedContact.address_state.state_name;
        this.addressfour = this.selectedContact.address_country.country_name;
        this.Invoiceadrsone = this.selectedContact.inv_addr_line1;
        this.Invoiceadrstwo = invcitypin;
        this.Invoiceadrsthree = this.selectedContact.inv_addr_state.state_name;
        this.Invoiceadrsfour = this.selectedContact.inv_addr_country.country_name;
        this.ShippingAdrsone = this.selectedContact.address_line1;
        this.ShippingAdrstwo = shipcitypin
        this.ShippingAdrsthree = this.selectedContact.ship_addr_state.state_name;
        this.ShippingAdrsfour = this.selectedContact.ship_addr_country.country_name;
      }
    }
  }
  selectedcurrency(item) {
    this.selectedCurrency = item.originalObject;
    console.log("item====>", this.selectedCurrency)
  }
  selectedpayterm(item) {
    if (item) {
      this.paymenTerm = item.originalObject;
      console.log("item====>", this.paymenTerm)
    }
  }
  selectedportload(item) {
    if (item) {
      this.selectedpload = item.originalObject;
      console.log("item====>", this.selectedpload)
    }
  }
  selectedportdischarge(item) {
    if (item) {
      this.selectedpdischarge = item.originalObject;
      console.log("item====>", this.selectedpdischarge);
    }
  }
  selectedmode(item) {
    if (item) {
      this.selectedMode = item.originalObject;
      console.log("item====>", this.selectedMode)
    }
  }
  selectedline(item, index) {
    this.selectedorderLine = item.originalObject;
    console.log("item====>", this.selectedorderLine)
    let temp = this._fb.array([]);
    for (let i = 0; i < this.orderForm.value.itemRows.length; i++) {
      if (index == i) {
        temp.push(
          this._fb.group({
            id: this.orderForm.value.itemRows[i].id,
            sales_order_id: this.orderForm.value.itemRows[i].sales_order_id,
            line: this.orderForm.value.itemRows[i].line,
            partid: this.selectedorderLine.id,
            part_no: this.selectedorderLine.part_no,
            part_type: this.selectedorderLine.part_type,
            part: this.selectedorderLine.component,
            partdesc: this.selectedorderLine.component_desc,
            material: this.selectedorderLine.material.material,
            materialrev: this.selectedorderLine.material_spec_rev,
            component: this.selectedorderLine.component,
            component_desc: this.selectedorderLine.component_desc,
            squantity: this.orderForm.value.itemRows[i].squantity,
            drawing: this.selectedorderLine.dwg_no,
            rev: this.selectedorderLine.rev,
            Qty: 0,
            Price: _.isEmpty(this.selectedorderLine.sellcost) ? "" : this.selectedorderLine.sellcost[0].cost,
            shipmentDate: this.orderForm.value.itemRows[i].shipmentDate,
            uom: this.selectedorderLine.uom.short_name,
            Remarks: ""
          })
        );
      } else {
        temp.push(
          this._fb.group(this.orderForm.value.itemRows[i]));
      }
    }
    this.orderForm = this._fb.group({
      itemRows: temp
    });
    this.calculatetotal();
    console.log(this.orderForm.value.itemRows);
  }

  paytypeselected(paytype, index) {
    this.selected = false
    var pay = []
    if (paytype == "LC") {
      pay = _.filter(this.allpayterms, function (num) {
        return num.type == "LC";
      })
      this.paytermsLC = this.completerService.local(pay, 'term', 'term');
      let temp = this._fb.array([]);
      for (let i = 0; i < this.paymentForm.value.itemPayRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              paymenttype: this.paymentForm.value.itemPayRows[i].paymenttype,
              paymentterm: "",
              paymentpercentage: this.paymentForm.value.itemPayRows[i].paymentpercentage,
              credit: this.paymentForm.value.itemPayRows[i].credit
            })
          );
        } else {
          temp.push(
            this._fb.group(this.paymentForm.value.itemPayRows[i]));
        }
      }
      this.paymentForm = this._fb.group({
        itemPayRows: temp
      });
    } else {
      pay = _.filter(this.allpayterms, function (num) {
        return num.type == "NONLC";
      })
      this.payterms = this.completerService.local(pay, 'term', 'term');

      let temp = this._fb.array([]);
      for (let i = 0; i < this.paymentForm.value.itemPayRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              paymenttype: this.paymentForm.value.itemPayRows[i].paymenttype,
              paymentterm: "",
              paymentpercentage: this.paymentForm.value.itemPayRows[i].paymentpercentage,
              credit: this.paymentForm.value.itemPayRows[i].credit
            })
          );
        } else {
          temp.push(
            this._fb.group(this.paymentForm.value.itemPayRows[i]));
        }
      }
      this.paymentForm = this._fb.group({
        itemPayRows: temp
      });
    }

    console.log("pay", pay)
  }

  selectedpacktype(item) {
    this.selectedpackingtype = item.originalObject;
    console.log("item====>", this.selectedpacktype);
  }
  selectedpaytype(item) {
    this.selectedpaymenttype = item.originalObject;
    console.log("item====>", this.selectedpaymenttype);
  }
  selectedpaymentterm(item) {
    this.selectedpaytermlist = item;
    console.log("item====>", this.selectedpaytermlist);
  }
  calculatetotal() {
    var calc = 0
    var totalqty = 0
    _.forEach(this.orderForm.value.itemRows, function (item, key) {
      var tqty = +item.Qty
      totalqty += tqty
      var sum = item.Qty * item.Price;
      calc += sum;
    });
    this.total = calc;
    if (isNaN(this.total)) {
      this.total = 0
    }
    this.totalquantity = totalqty
  }

  clacPercentage(index) {
    var totalpercentage = 0;
    _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
      var sum = +item.paymentpercentage
      totalpercentage += sum;
    });
    if (totalpercentage > 100) {
      this.notificate.warn('Warning', "you can't enter more than 100%", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  filelistold: any;

  viewOrderdetails() {
    this.orderarray = [];
    if (this.scid) {
      this.show_update_loader = true;
      this.salesorder.getByID(this.scid).subscribe(res => {
        this.orderdetail = res.data;
        if (!_.isEmpty(this.orderdetail)) {
          if (this.orderdetail.salesOrderFiles) {
            this.filelistold = this.orderdetail.salesOrderFiles;
            this.Sfiles = [].slice.call(this.filelistold);
            this.files = this.Sfiles.map(f => f.filename).join(', ');
            this.filenames = this.files;
            console.log("files", this.files)
          }
          var d = this.orderdetail.address;
          var addressone = d.split('##');
          var invaddress = this.orderdetail.invoice_address.split('##');
          var shipaddress = this.orderdetail.ship_address.split('##');
          console.log("splitedD", addressone);
          this.contact = this.orderdetail.supplier.supplier_name;
          this.selectedContact = this.orderdetail.supplier;
          this.addressone = addressone[0];
          this.addresstwo = addressone[1];
          this.addressthree = addressone[2];
          this.addressfour = addressone[3];

          this.Invoiceadrsone = invaddress[0];
          this.Invoiceadrstwo = invaddress[1];
          this.Invoiceadrsthree = invaddress[2];
          this.Invoiceadrsfour = invaddress[3];

          this.ShippingAdrsone = shipaddress[0];
          this.ShippingAdrstwo = shipaddress[1];
          this.ShippingAdrsthree = shipaddress[2];
          this.ShippingAdrsfour = shipaddress[3];

          this.salesdate = this.splitdate(this.orderdetail.sc_date)
          this.podate = _.isEmpty(this.orderdetail.po_date) ? "" : this.splitdate(this.orderdetail.po_date);
          this.shipdate = _.isEmpty(this.orderdetail.shipment_date) ? "" : this.splitdate(this.orderdetail.shipment_date);
          this.scorder = this.orderdetail.sc_order;
          this.sctype = this.orderdetail.sc_type;
          this.pono = this.orderdetail.po_no;
          this.scno = this.orderdetail.sc_no;
          this.paymenttermcus = this.orderdetail.payment_term_id;
          if (this.orderdetail.port_loading) {
            this.portloading = this.orderdetail.port_loading.port_name;
          }
          if (this.orderdetail.port_discharge) {
            this.postdischarge = this.orderdetail.port_discharge.port_name;
          }
          if (this.orderdetail.currency_id) {
            this.currency = this.orderdetail.currency_id.short_name
          }
          this.deliveryterm = this.orderdetail.delivery_inco_term;
          this.mode = this.orderdetail.shipment_mode;
          this.orderarray = this.orderdetail.salesOrderDetails;
          this.payarray = this.orderdetail.salesOrderCommercial;
          this.packingtype = this.orderdetail.packing_type;
          this.Bundle = this.orderdetail.bundle;
          this.totalquantity = this.orderdetail.total_order_qty;
          this.total = this.orderdetail.total_price;
          this.remarks = this.orderdetail.spl_instructions;
          this.status = this.orderdetail.is_saved;
          let temp = this.fb.array([]);
          for (let i = 0; i < this.orderarray.length; i++) {
            if (this.orderarray[i].shipment_date) {
              var alterddate = this.splitdate(this.orderarray[i].shipment_date);
              console.log("alterdate", alterddate)
            }
            temp.push(
              this.fb.group({
                id: parseInt(this.orderarray[i].id),
                sales_order_id: parseInt(this.orderarray[i].sales_order_id),
                line: this.orderarray[i].line_no,
                partid: this.orderarray[i].part_id,
                part_no: this.orderarray[i].part_no,
                part_type: this.orderarray[i].part_type,
                part: this.orderarray[i].component,
                partdesc: this.orderarray[i].component_desc,
                material: this.orderarray[i].material_spec,
                materialrev: this.orderarray[i].material_spec_rev,
                component: this.orderarray[i].component,
                component_desc: this.orderarray[i].component_desc,
                squantity: this.orderarray[i].squantity,
                drawing: this.orderarray[i].dwg_no,
                rev: this.orderarray[i].revesion,
                uom: this.orderarray[i].uom,
                Qty: this.orderarray[i].quantity,
                Price: this.orderarray[i].price,
                shipmentDate: alterddate,
                Remarks: this.orderarray[i].remarks,
              })
            );
          }
          this.orderForm = this.fb.group({
            itemRows: temp
          });

          let paytemp = this.fb.array([]);
          for (let i = 0; i < this.payarray.length; i++) {
            paytemp.push(
              this.fb.group({
                paymenttype: this.payarray[i].payment_type,
                paymentterm: this.payarray[i].payment_terms,
                paymentpercentage: this.payarray[i].payment_percentage,
                credit: this.payarray[i].credict
              })
            );
          }
          this.paymentForm = this.fb.group({
            itemPayRows: paytemp
          });
          this.nodata = true
          this.show_update_loader = false;
          console.log("table", this.orderForm.value)
        }
      })
    }
  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
    {
      date: {
        year: +newdate[2],
        month: +newdate[1],
        day: +newdate[0]
      },
      "formatted": d
    }
    console.log("day", altereddate)
    return altereddate
  }





  //========================= INITIATES ================================
  saveOrder(issaved, flag) {

    // ====================================== Submit Saleorder Portion ==========================================
    var address = [this.addressone, this.addresstwo, this.addressthree, this.addressfour].join('##');
    var Invaddress = [this.Invoiceadrsone, this.Invoiceadrstwo, this.Invoiceadrsthree, this.Invoiceadrsfour].join('##');
    var Shipaddress = [this.ShippingAdrsone, this.ShippingAdrstwo, this.ShippingAdrsthree, this.ShippingAdrsfour].join('##');
    console.log("address", address, "-->", Invaddress, "==>", Shipaddress);
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (issaved == '0') {
      if (_.isEmpty(this.contact)) {
        validdata = false;
        validationerrorMsg.push("Please select Customer");
      }
      if (this.scorder == 0) {
        validdata = false;
        validationerrorMsg.push("Please select sc type");
      }
      if (_.isEmpty(this.scno)) {
        validdata = false;
        validationerrorMsg.push("Please enter sc no");
      }
      if (!this.salesdate) {
        validdata = false;
        validationerrorMsg.push("Please select SC Date");
      }
      if (_.isEmpty(this.addressone)) {
        validdata = false;
        validationerrorMsg.push("Please select Address");
      }
      if (_.isEmpty(this.Invoiceadrsone)) {
        validdata = false;
        validationerrorMsg.push("Please select Invoice_Address");
      }
      if (_.isEmpty(this.ShippingAdrsone)) {
        validdata = false;
        validationerrorMsg.push("Please select Shipping_Address");
      }
      if (_.isEmpty(this.pono)) {
        validdata = false;
        validationerrorMsg.push("Please enter Po_Number");
      }
      if (_.isEmpty(this.paymenttermcus)) {
        validdata = false;
        validationerrorMsg.push("Please select Payment Term");
      }
      if (_.isEmpty(this.portloading)) {
        validdata = false;
        validationerrorMsg.push("Please select Port of loading");
      }
      if (_.isEmpty(this.postdischarge)) {
        validdata = false;
        validationerrorMsg.push("Please select Port of Discharge");
      }
      if (_.isEmpty(this.deliveryterm)) {
        validdata = false;
        validationerrorMsg.push("Please enter delivery term");
      }
      if (_.isEmpty(this.currency)) {
        validdata = false;
        validationerrorMsg.push("Please select currency");
      }
      if (this.mode == 0) {
        validdata = false;
        validationerrorMsg.push("Please select mode of shipment");
      }
      var orderDetails = [];
      var PaymentDetails = [];
      if (this.orderForm.value.itemRows.length) {
        _.forEach(this.orderForm.value.itemRows, function (item, key) {
          var shipmentdate = ""
          if (item.shipmentDate) {
            shipmentdate = item.shipmentDate.formatted
          } else {
            shipmentdate = ""
          }
          var orderlineitem =
          {
            "line_no": item.line,
            "part_id": item.partid,
            "part_no": item.part_no,
            "part_type": item.part_type,
            "material_spec": item.material,
            "material_spec_rev": item.materialrev,
            "component": item.component,
            "component_desc": item.component_desc,
            "squantity": item.squantity,
            "revesion": item.rev,
            "dwg_no": item.drawing,
            "quantity": item.Qty,
            "uom": item.uom,
            "price": item.Price,
            "shipment_date": shipmentdate,
            "remarks": item.Remarks
          }
          if (_.isEmpty(item.part_no)) {
            validdata = false;
            validationerrorMsg.push("Please select Part no");
          }
          if (item.Qty && item.Price != 0) {
            orderDetails.push(orderlineitem);
          }
          else {
            if (item.Qty == 0) {
              validdata = false;
              validationerrorMsg.push("Please enter qty");
            }
            else {
              validdata = false;
              validationerrorMsg.push("Please enter rate");
            }
          }
          if (_.isEmpty(item.shipmentDate)) {
            validdata = false;
            validationerrorMsg.push("Please select ship date");
          }
          console.log("orderdetails", orderDetails)
        });
      } else {
        validdata = false;
        validationerrorMsg.push("Please enter order details");
      }


      var totalpercentage = 0
      if (this.paymentForm.value.itemPayRows.length) {
        _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
          var sum = +item.paymentpercentage
          totalpercentage += sum
          console.log("totalpercentage", totalpercentage);
          var paymentlineitem =
          {
            "payment_type": item.paymenttype,
            "payment_terms": item.paymentterm,
            "payment_percentage": item.paymentpercentage,
            "credict": item.credit,
          }
          if (_.isEmpty(item.paymenttype && item.paymentterm)) {
            validdata = false;
            validationerrorMsg.push("Please select payment details");
          }
          if (item.paymentpercentage != 0) {
            PaymentDetails.push(paymentlineitem);
          }
          else {
            validdata = false;
            validationerrorMsg.push("Please enter percentage");
          }
          console.log("PaymentDetails", PaymentDetails)
        });
      } else {
        validdata = false;
        validationerrorMsg.push("Please Enter Payment Details");
      }

      if (totalpercentage > 100 || totalpercentage < 100) {
        validdata = false;
        validationerrorMsg.push("Please Enter 100% Payment");
      }
      if (this.totalquantity == 0) {
        validdata = false;
        validationerrorMsg.push("Please select order details");
      }
      if (validdata) {

        var portload = _.findWhere(this.allPorts._data, { port_name: this.portloading });
        var portdischarge = _.findWhere(this.allPorts._data, { port_name: this.postdischarge });
        var currency = _.findWhere(this.allcurrencies._data, { short_name: this.currency });

        this.show_loader = true;
        this.formdata =
          {
            "unit_id": this.unitid,
            "supplier": { "id": this.selectedContact.id },
            "sc_no": this.scno,
            "address": address,
            "invoice_address": Invaddress,
            "ship_address": Shipaddress,
            "sc_order": this.scorder,
            "sc_date": this.salesdate.formatted,
            "category": this.category,
            "po_no": this.pono,
            "po_date": _.isEmpty(this.podate) ? null : this.podate.formatted,
            "payment_term_id": this.paymenttermcus,
            "port_loading": { "id": portload.id },
            "port_discharge": { "id": portdischarge.id },
            "delivery_inco_term": this.deliveryterm,
            "shipment_mode": this.mode,
            "shipment_date": _.isEmpty(this.shipdate) ? null : this.shipdate.formatted,
            "sales_person": this.salesperson,
            "currency_id": { "id": currency.id },
            "salesOrderDetails": orderDetails,
            "salesOrderCommercial": PaymentDetails,
            "total_price": this.total,
            "total_order_qty": this.totalquantity,
            "spl_instructions": this.remarks,
            "packing_type": this.packingtype,
            "bundle": this.Bundle,
            "is_saved": 0,
          }
        console.log("formdata ===>", JSON.stringify(this.formdata))
        if (flag == "submit") {
          this.salesorder.AddNewOrder(this.formdata, this.filelist)
            .subscribe(res => {
              this.show_loader = false;
              if (res.status == "success") {
                this.resetform();
                this.notificate.success('Success', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              } else {
                this.notificate.error('Error', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              }
            });
        } else if (flag == "update") {
          this.formdata.id = this.orderdetail.id
          this.salesorder.UpdateOrder(this.formdata, this.filelist)
            .subscribe(res => {
              this.show_loader = false;
              if (res.status == "success") {
                this.resetform();
                this.notificate.success('Success', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
                this.router.navigate(['/savedsalesorder'])
                // if (this.url == "/savedsalesorder") {
                //   this.updateorder();
                // }
              } else {
                this.notificate.error('Error', res.msg, {
                  timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                  maxLength: this.tosterminlength
                })
              }
            });
        }
      }
      else {
        this.notificate.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }

    // ====================================== Submit Saleorder Portion END ==========================================


    // ====================================== Save Saleorder Portion ==========================================

    else {

      if (_.isEmpty(this.contact)) {
        validdata = false;
        validationerrorMsg.push("Please select Customer");
      }
      if (this.scorder == 0) {
        validdata = false;
        validationerrorMsg.push("Please select sc type");
      }
      // if (this.sctype == 0) {
      //   validdata = false;
      //   validationerrorMsg.push("Please select sc type");
      // }
      if (_.isEmpty(this.scno)) {
        validdata = false;
        validationerrorMsg.push("Please enter sc no");
      }
      if (!this.salesdate) {
        validdata = false;
        validationerrorMsg.push("Please select SC Date");
      }
      var orderDetails = [];
      var PaymentDetails = [];
      _.forEach(this.orderForm.value.itemRows, function (item, key) {
        var orderlineitem =
        {
          "line_no": item.line,
          "part_id": item.partid,
          "part_no": item.part_no,
          "part_type": item.part_type,
          "material_spec": item.material,
          "material_spec_rev": item.materialrev,
          "component": item.component,
          "component_desc": item.component_desc,
          "squantity": item.squantity,
          "revesion": item.rev,
          "dwg_no": item.drawing,
          "quantity": _.isEmpty(item.Qty) ? 0 : item.Qty,
          "uom": item.uom,
          "price": _.isEmpty(item.Price) ? 0 : item.Price,
          "shipment_date": item.shipmentDate.formatted,
          "remarks": item.Remarks
        }
        orderDetails.push(orderlineitem);
      });
      var totalpercentage = 0
      _.forEach(this.paymentForm.value.itemPayRows, function (item, key) {
        var sum = +item.paymentpercentage
        totalpercentage += sum
        var paymentlineitem =
        {
          "payment_type": item.paymenttype,
          "payment_terms": item.paymentterm,
          "payment_percentage": item.paymentpercentage,
          "credict": item.credit,
        }
        PaymentDetails.push(paymentlineitem);
      });

      if (validdata) {

        var portload = _.findWhere(this.allPorts._data, { port_name: this.portloading });
        var portdischarge = _.findWhere(this.allPorts._data, { port_name: this.postdischarge });
        var currency = _.findWhere(this.allcurrencies._data, { short_name: this.currency });

        this.show_loader = true;
        this.formdata =
          {
            "unit_id": this.unitid,
            "supplier": { "id": this.selectedContact.id },
            "sc_no": this.scno,
            "address": address,
            "invoice_address": Invaddress,
            "ship_address": Shipaddress,
            "sc_order": this.scorder,
            "sc_date": this.salesdate.formatted,
            "category": this.category,
            "po_no": this.pono,
            "po_date": _.isEmpty(this.podate) ? null : this.podate.formatted,
            "payment_term_id": this.paymenttermcus,
            "port_loading": _.isEmpty(portload) ? null : { "id": portload.id },
            "port_discharge": _.isEmpty(portdischarge) ? null : { "id": portdischarge.id },
            "delivery_inco_term": this.deliveryterm,
            "shipment_mode": this.mode,
            "shipment_date": _.isEmpty(this.shipdate) ? null : this.shipdate.formatted,
            "sales_person": this.salesperson,
            "currency_id": _.isEmpty(currency) ? null : { "id": currency.id },
            "merchandiser": this.merchandiser,
            "salesOrderDetails": orderDetails,
            "salesOrderCommercial": PaymentDetails,
            "total_price": this.total,
            "total_order_qty": this.totalquantity,
            "spl_instructions": this.remarks,
            "packing_type": this.packingtype,
            "bundle": this.Bundle,
            "is_saved": 1,
          }
        console.log("formdata ===>", JSON.stringify(this.formdata))
        this.salesorder.AddNewOrder(this.formdata, this.filelist)
          .subscribe(res => {
            this.show_loader = false;
            if (res.status == "success") {
              this.resetform();
              this.notificate.success('Success', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            } else {
              this.notificate.error('Error', res.msg, {
                timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
                maxLength: this.tosterminlength
              })
            }
          });
      }
      else {
        this.notificate.warn('Warning', validationerrorMsg[0], {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }

    // ====================================== Save Saleorder Portion End==========================================
  }

  resetform() {
    this.orderForm = this._fb.group({
      itemRows: this._fb.array([])
    });
    this.paymentForm = this._fb.group({
      itemPayRows: this._fb.array([])
    });
    this.addNewRow();
    this.addNewpaymentRow();
    this.contact = "";
    this.scorder = 0;
    this.scno = "";
    this.sctype = 0;
    this.addressone = "";
    this.addresstwo = "";
    this.addressthree = "";
    this.addressfour = "";
    this.Invoiceadrsone = "";
    this.Invoiceadrstwo = "";
    this.Invoiceadrsthree = "";
    this.Invoiceadrsfour = "";
    this.ShippingAdrsone = "";
    this.ShippingAdrstwo = "";
    this.ShippingAdrsthree = "";
    this.ShippingAdrsfour = "";
    this.pono = "";
    let date = new Date();
    var currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    this.podate = ""
    this.shipdate = "";
    this.paymenttermcus = "";
    this.portloading = "";
    this.postdischarge = "";
    this.deliveryterm = "";
    this.mode = 0;
    this.currency = "";
    this.packingtype = "";
    this.Bundle = "";
    this.remarks = "";
    this.total = 0;
    this.totalquantity = 0
    this.clearFile();
  }

  cancel() {
    this.resetform();
    this.updateorder();
    this.router.navigate(['../savedsalesorder']);
  }

}
