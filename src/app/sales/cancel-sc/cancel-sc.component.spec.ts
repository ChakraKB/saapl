import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelScComponent } from './cancel-sc.component';

describe('CancelScComponent', () => {
  let component: CancelScComponent;
  let fixture: ComponentFixture<CancelScComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelScComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelScComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
