import { AfterViewInit, Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { RolesService } from '../../services/Roles/roles.service'
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as jsPDF from 'jspdf';
import { autoTable } from 'jspdf-autotable';
import 'jspdf-autotable';
import * as _ from 'underscore';

class Person { }
@Component({
  selector: 'app-orderconfirmation',
  templateUrl: './orderconfirmation.component.html',
  styleUrls: ['./orderconfirmation.component.css']
})
export class OrderconfirmationComponent implements OnInit {
  @ViewChild(DataTableDirective)
  @ViewChild('printsection') printsection: ElementRef;
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  role_name: any;
  description: any;
  toastertime: number;
  tosterminlength: number;
  rolesform: FormGroup;
  id: any;
  formdata: any;
  buttonname: string;
  active_status: any;
  statusarray: any;
  response: any;
  allrole: any;
  role: any = {};
  data: any;
  tabletimeout: any;
  sampleform: FormGroup;
  ur: any;
  showtab: boolean = false;
  groupedreqdate: any;
  groupedconfirmdate: any;
  samplearray: any[];
  items: any;
  salesorder: any;
  salesorderdetails: any;
  commercialdetails: any;
  show_loader: boolean = false;
  address: any;
  supplierdetails: any;
  pincode: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private salesservice: SalesorderService,
    private http: Http) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.getsalescontract();
    this.LoadAngTable();
  }

  getsalescontract() {
    this.persons = [];
    this.salesservice.getAllsalescontract(this.unitid)
      .subscribe(res => {
        if (res) {
          this.persons = res.data
          console.log("allusers", this.persons)
        }
      });
  }

  LoadAngTable() {
    this.show_loader = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.SALESORDER.GETSALESCONTRACTIST + this.unitid)
      .map(this.extractData)
      .subscribe(persons => {
        this.persons = persons;
        this.dtTrigger.next();
        this.show_loader = false
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  getorderdetails(id) {
    this.show_loader = true
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    });
    this.samplearray = [];
    this.salesservice.getByID(id)
      .subscribe(res => {
        this.salesorder = res.data;
        this.supplierdetails = this.salesorder.supplier;
        console.log(this.supplierdetails)
        this.salesorderdetails = this.salesorder.salesOrderDetails;
        this.groupedreqdate = _.groupBy(this.salesorderdetails, 'shipment_date');
        this.groupedconfirmdate = _.groupBy(this.salesorderdetails, 'pconfirm_date');
        this.commercialdetails = this.salesorder.salesOrderCommercial;
        this.address = this.salesorder.address.split('##');
        this.pincode = this.address[1].split(',')
        console.log(this.pincode)
        console.log("address", this.address)
        this.show_loader = false;
      });
  }

  cancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

  calcqty() {
    var qty = 0;
    _.forEach(this.salesorderdetails, function (val) {
      qty += val.quantity
    })
    return qty
  }

  calcval() {
    var totval = 0
    _.forEach(this.salesorderdetails, function (val) {
      totval += (val.quantity) * (val.price)
    })
    return totval
  }


  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsections').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Order Confirmation</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          body{
            margin-top : 150px !important
          }
          .print-view>tbody>tr>td,
          .print-view>tbody>tr>th,
          .print-view>tfoot>tr>td,
          .print-view>tfoot>tr>th,
          .print-view>thead>tr>td,
          .print-view>thead>tr>th {
            border: 1px solid #333 !important; padding:2px 3px !important
          }
          .print-view .print-label p { font-weight : 500 !important;}
          .print-view .print-label { font-weight : 500 !important;}
          .print-view .cs { background-color:#ccc !important;}
          .print-view .cs td { background-color:#ccc !important;}
           table.print-view td p {
           margin: 0!important;
          }
          .m-t-70{ margin-top : 70px !important}
        table.report-container {
        page-break-after:always;
        }
        thead.report-header {
        display:table-header-group;
        }
        tfoot.report-footer {
        display:table-footer-group;
        } 
        </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  printPdf() {
    var doc = new jsPDF('p', 'pt');
    var header = function (data) {
      doc.setFontSize(18);
      doc.setTextColor(40);
      doc.setFontStyle('normal');
      // doc.text("Order Confirmation", 220, 50);
    };
    var titleoptions = {
      addPageContent: header,
      pageBreak: 'auto',
      margin: { top: 10 },
      theme: 'plain',
      columnStyles: { 0: { columnWidth: 330, overflow: 'linebreak', fontStyle: 'bold', halign: "right", fontSize: 13 } }
    };
    var title = doc.autoTableHtmlToJson(document.getElementById("title"));
    doc.autoTable(title.columns, title.data, titleoptions)

    var Refoptions = {
      addPageContent: header,
      pageBreak: 'auto',
      theme: 'plain',
      margin: { top: 60 },
      columnStyles: {
        0: { columnWidth: 90, overflow: 'linebreak', fontStyle: 'bold' }, 1: { columnWidth: 250, overflow: 'linebreak' },
        2: { columnWidth: 90, overflow: 'linebreak', fontStyle: 'bold' }
      }
    };
    var ref = doc.autoTableHtmlToJson(document.getElementById("tableref"));
    doc.autoTable(ref.columns, ref.data, Refoptions)

    var options = {
      addPageContent: header,
      pageBreak: 'auto',
      theme: 'plain',
      startY: doc.autoTableEndPosY() + 10,
      columnStyles: { 0: { columnWidth: 150, overflow: 'linebreak' } }
    };
    var to = doc.autoTableHtmlToJson(document.getElementById("tableto"));
    doc.autoTable(to.columns, to.data, options)

    var Toopt = {
      pageBreak: 'auto',
      addPageContent: header,
      theme: 'grid',
      startY: doc.autoTableEndPosY() + 10,
      columnStyles: { 0: { columnWidth: 40, overflow: 'linebreak', fontStyle: 'bold' }, 1: { overflow: 'linebreak' } }
    };
    var res = doc.autoTableHtmlToJson(document.getElementById("ordertable"));
    doc.autoTable(res.columns, res.data, Toopt)

    var footopt = {
      pageBreak: 'auto',
      addPageContent: header,
      theme: 'plain',
      startY: doc.autoTableEndPosY() + 40,
      margin: { top: 80 },
      columnStyles: { 0: { columnWidth: 250, overflow: 'linebreak' }, 1: { columnWidth: 250, overflow: 'linebreak', halign: "right", } }
    };
    var foot = doc.autoTableHtmlToJson(document.getElementById("FooterTab"));
    doc.autoTable(foot.columns, foot.data, footopt)
    // doc.save("SalesOrder.pdf");
    var string = doc.output('datauristring');
    var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
    var x = window.open();
    x.document.open();
    x.document.write(iframe);
    x.document.close();
  }


}
