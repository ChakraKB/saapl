import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductconfirmationComponent } from './productconfirmation.component';

describe('ProductconfirmationComponent', () => {
  let component: ProductconfirmationComponent;
  let fixture: ComponentFixture<ProductconfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductconfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductconfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
