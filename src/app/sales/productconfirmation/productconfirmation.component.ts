import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
class Person { }
@Component({
  selector: 'app-productconfirmation',
  templateUrl: './productconfirmation.component.html',
  styleUrls: ['./productconfirmation.component.css']
})
export class ProductconfirmationComponent implements OnInit {

  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  public date = new Date();
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true,
  };
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  rolesform: FormGroup;
  orderForm: FormGroup;
  formdata: any;
  buttonname: string;
  active_status: any;
  statusarray: any;
  response: any;
  allrole: any;
  role: any = {};
  data: any;
  tabletimeout: any;
  samplearray: any;
  nodata: any;
  showtab: boolean = false;
  show_loader: boolean = false;
  orderid: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private salesservice: SalesorderService,
    private http: Http) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.orderForm = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.LoadAngTable();
    this.nodata = false;
  }

  getorderform(orderForm) {
    return orderForm.get('itemRows').controls
  }

  getpdconfirmlist() {
    this.show_loader = true;
    var orderlist = [];
    this.salesservice.getAllPdconfirmedOrder(this.unitid)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.persons = res.data;
          this.dtTrigger.next();
          console.log("submittedarray", this.persons)
        }
      });
  }

  LoadAngTable() {
    this.show_loader = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.SALESORDER.GETPDCONFIRM + this.unitid)
      .map(this.extractData)
      .subscribe(persons => {
        console.log("totalorders", persons)
        this.show_loader = false;
        this.persons = persons
        console.log("submittedarray", this.persons)
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  download(data) {

    var byteCharacters = atob(data.salesOrderFiles[0].file);
    var byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    var blob = new Blob([byteArray], { type: 'contentType' });
    var blobUrl = URL.createObjectURL(blob);
    window.open(blobUrl);


    // var file = window.atob(data.salesOrderFiles.file);
    // var blob = new Blob([file], { type: "application/pdf" });
    // var url = window.URL.createObjectURL(blob);
    // window.open(url);
  }


  getorderdetails(id) {
    this.orderid = id;
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    });
    this.samplearray = [];
    this.show_loader = true;
    this.salesservice.getByID(this.orderid)
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.samplearray = res.data.salesOrderDetails
          console.log("salesdetail", JSON.stringify(this.samplearray));
          let temp = this.fb.array([]);
          for (let i = 0; i < this.samplearray.length; i++) {
            var alterddate = this.splitdate(this.samplearray[i].shipment_date);
            temp.push(
              this.fb.group({
                id: parseInt(this.samplearray[i].id),
                sales_order_id: parseInt(this.samplearray[i].sales_order_id),
                line: this.samplearray[i].line_no,
                partid: this.samplearray[i].part_id,
                part_no: this.samplearray[i].part_no,
                part_type: this.samplearray[i].part_type,
                part: this.samplearray[i].component,
                partdesc: this.samplearray[i].component_desc,
                material: this.samplearray[i].material_spec,
                materialrev: this.samplearray[i].material_spec_rev,
                component: this.samplearray[i].component,
                component_desc: this.samplearray[i].component_desc,
                squantity: parseInt(this.samplearray[i].squantity),
                drawing: this.samplearray[i].dwg_no,
                rev: this.samplearray[i].revesion,
                uom: this.samplearray[i].uom,
                stckqty: this.samplearray[i].open_quantity,
                Qty: parseInt(this.samplearray[i].quantity),
                Price: parseInt(this.samplearray[i].price),
                shipDate: alterddate.formatted,
                prodDate: "",
                scno: this.samplearray[i].sc_no,
                Remarks: this.samplearray[i].remarks,
                crt_dt: this.samplearray[i].crt_dt,
                modi_dt: this.samplearray[i].modi_dt
              })
            );
          }
          this.orderForm = this.fb.group({
            itemRows: temp
          });
          this.nodata = true
          console.log("table", this.orderForm.value)
        }
      });

  }

  splitdate(d) {
    var newdate = d.split('-');
    var altereddate =
      {
        date: {
          year: +newdate[2],
          month: +newdate[1],
          day: +newdate[0]
        },
        "formatted": d
      }
    console.log("day", altereddate)
    return altereddate
  }

  cancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

  saveOrder() {

    var validdata: Boolean = true;
    var validationerrorMsg = [];
    var orderDetails = [];

    for (let i = 0; i < this.orderForm.value.itemRows.length; i++) {
      var orderlineitem =
        {
          "id": this.orderForm.value.itemRows[i].id,
          "sales_order_id": this.orderForm.value.itemRows[i].sales_order_id,
          "line_no": this.orderForm.value.itemRows[i].line,
          "part_id": this.orderForm.value.itemRows[i].partid,
          "part_no": this.orderForm.value.itemRows[i].part_no,
          "part_type": this.orderForm.value.itemRows[i].part_type,
          "material_spec": this.orderForm.value.itemRows[i].material,
          "material_spec_rev": this.orderForm.value.itemRows[i].materialrev,
          "component": this.orderForm.value.itemRows[i].component,
          "component_desc": this.orderForm.value.itemRows[i].component_desc,
          "squantity": this.orderForm.value.itemRows[i].squantity,
          "dwg_no": this.orderForm.value.itemRows[i].drawing,
          "revesion": this.orderForm.value.itemRows[i].rev,
          "quantity": this.orderForm.value.itemRows[i].Qty,
          "open_quantity": this.orderForm.value.itemRows[i].stckqty,
          "price": this.orderForm.value.itemRows[i].Price,
          "uom": this.orderForm.value.itemRows[i].uom,
          "shipment_date": this.orderForm.value.itemRows[i].shipDate,
          "pconfirm_date": this.orderForm.value.itemRows[i].prodDate.formatted,
          "sc_no": this.orderForm.value.itemRows[i].scno,
          "remarks": this.orderForm.value.itemRows[i].Remarks,
          "crt_dt": this.orderForm.value.itemRows[i].crt_dt,
          "modi_dt": this.orderForm.value.itemRows[i].modi_dt
        }

      if (_.isEmpty(this.orderForm.value.itemRows[i].prodDate)) {
        validdata = false;
        validationerrorMsg.push("Production confirm date required");
      }
      else {
        orderDetails.push(orderlineitem);
      }
    };

    if (validdata) {
      this.show_loader = true;
      var formdata = {
        "id": this.orderid,
        "salesOrderDetails": orderDetails
      }
      console.log("orderdetails", formdata)
      this.salesservice.ProConfirm(formdata)
        .subscribe(res => {
          this.show_loader = false;
          if (res.status == "success") {
            console.log("success", res.data);
            this.resetform();
            this.cancel();
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.getpdconfirmlist();
              this.show_loader = false
            });
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
          else {
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        });
    }
    else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  formatdate(d) {
    var date = d
    var formated = [d.day, d.month, d.year].join('-')
    console.log("formated", formated)
    return formated
  }

  resetform() {
    this.orderForm.reset()
  }
}
