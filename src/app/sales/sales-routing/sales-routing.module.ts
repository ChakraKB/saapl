import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LoginAuthGuard } from '../../gaurds/login-auth.guard';
import { LogoutAuthGuard } from '../../gaurds/logout-auth.guard';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { SalesorderComponent } from '../salesorder/salesorder.component';
import { SalesauthorizationComponent } from '../salesauthorization/salesauthorization.component';
import { ViewScauthComponent } from '../salesauthorization/view-scauth/view-scauth.component';
import { ProductconfirmationComponent } from '../productconfirmation/productconfirmation.component';
import { OrderconfirmationComponent } from '../orderconfirmation/orderconfirmation.component';
import { SavedSalesordersComponent } from '../saved-salesorders/saved-salesorders.component';
import { ViewSavedScComponent } from '../saved-salesorders/view-saved-sc/view-saved-sc.component';

const routes: Routes = [
  {
    path: 'salesorder', component: SalesorderComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['Sales Order'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'salesauthorization', component: SalesauthorizationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['Sales Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewscauthorization/:id', component: ViewScauthComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['Sales Authorization'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'productconfirmation', component: ProductconfirmationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['Product Confirmation'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'salescontract', component: OrderconfirmationComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['Sales Contract'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'savedsalesorder', component: SavedSalesordersComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['Saved SC'],
        redirectTo: '/error404'
      }
    }
  },
  {
    path: 'viewsavedsc/:id', component: ViewSavedScComponent, canActivate: [LoginAuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['Saved SC'],
        redirectTo: '/error404'
      }
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class SalesRoutingModule { }
