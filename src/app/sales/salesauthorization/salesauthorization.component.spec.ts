import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesauthorizationComponent } from './salesauthorization.component';

describe('SalesauthorizationComponent', () => {
  let component: SalesauthorizationComponent;
  let fixture: ComponentFixture<SalesauthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesauthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesauthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
