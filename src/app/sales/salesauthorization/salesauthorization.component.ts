import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";

import { SalesorderService } from '../../services/salesorder/salesorder.service'
class Person {
  id: any;
  role_name: string;
  description: string;
  checkauthorize: boolean = false;
}
@Component({
  selector: 'app-salesauthorization',
  templateUrl: './salesauthorization.component.html',
  styleUrls: ['./salesauthorization.component.css']
})
export class SalesauthorizationComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  dtTrigger: Subject<any> = new Subject();
  role_name: any;
  description: any;
  toastertime: number;
  tosterminlength: number;
  rolesform: FormGroup;
  id: any;
  formdata: any;
  buttonname: string;
  active_status: any;
  statusarray: any;
  response: any;
  allrole: any;
  role: any = {};
  data: any;
  tabletimeout: any;
  checkauthorize: any;
  checkeditems: any[];
  showDialog: boolean = false;
  orderdetails: any;
  scno: any;
  scdate: any;
  supplierdtls: any;
  orderdtls: any;
  show_loader: Boolean = false;
  files: any;
  order: any;
  nodata: boolean = false;
  address: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private salesservice: SalesorderService,
    private http: Http) {
    this.statusarray = AppConstant.APP_CONST.status;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.rolesform = fb.group({
      'id': [null],
      'role_name': [null, Validators.required],
      'description': [null, Validators.required],
      'active_status': [null, Validators.required],
    })
  }

  ngOnInit() {
    this.buttonname = "Save";
    this.LoadAngTable();
    this.checkeditems = [];
  }

  loadauthorizedlist() {
    this.show_loader = true;
    this.salesservice.getAllauthorizedOrder(this.unitid)
      .subscribe(res => {
        this.show_loader = false;
        this.persons = res.data;
        this.dtTrigger.next();
      });
  }

  LoadAngTable() {
    this.show_loader = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.SALESORDER.GETAUTHLIST + this.unitid)
      .map(this.extractData)
      .subscribe(persons => {
        this.show_loader = false;
        this.persons = persons;
        this.dtTrigger.next();
      });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  getfiles(id) {
    this.show_loader = true;
    this.salesservice.getByID(id)
      .subscribe(res => {
        this.show_loader = false;
        this.order = res.data;
        this.showDialog = true;
        this.files = res.data.salesOrderFiles;
        if (this.files.length == 0) {
          this.nodata = true;
        } else {
          this.nodata = false;
        }
        console.log("res", this.files);
      })
  }

  downloadfile(file) {
    console.log("file", file);
    let byteCharacters = atob(file.filecontent);
    let byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++)
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    let byteArray = new Uint8Array(byteNumbers);
    let blob = new Blob([byteArray], { type: file.filetype });
    importedSaveAs(blob, file.filename)
  }

  opendetail(id) {
    this.show_loader = true;
    this.showDialog = true;
    this.salesservice.getByID(id)
      .subscribe(res => {
        this.show_loader = false;
        this.orderdetails = res.data;
        this.scno = this.orderdetails.sc_no;
        this.scdate = this.orderdetails.sc_date;
        this.supplierdtls = this.orderdetails.supplier
        this.orderdtls = this.orderdetails.salesOrderDetails
        console.log("orderdetails", JSON.stringify(this.orderdetails));
      });
  }

  orderid: any;
  orderdetail: any;
  showtab: any;

  getorderdetails(id) {
    this.show_loader = true;
    this.orderid = id;
    this.salesservice.getByID(this.orderid)
      .subscribe(res => {
        this.show_loader = false;
        this.orderdetail = res.data;
        this.showtab = true;
        setTimeout(() => {
          jQuery('#detail')[0].click();
        });
      });
  }

  notifyfunction() {
    this.cancel()
  }

  cancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.loadauthorizedlist();
    });

  }

  // checkall(status, index) {
  //   var displayedRows = this.dtOptions.pageLength;
  //   console.log("pagelength", displayedRows)
  //   this.checkeditems = [];
  //   if (status == true) {
  //     for (var i = 0; i < this.persons.length; i++) {
  //       this.persons[i].checkauthorize = true;
  //       this.checkeditems.push(this.persons[i].id)
  //     }
  //     console.log("checkedall", this.checkeditems)
  //   } else {
  //     for (var i = 0; i < this.persons.length; i++) {
  //       this.persons[i].checkauthorize = false;
  //       this.checkeditems.splice(i, 1)
  //     }
  //     console.log("Uncheckedall", this.checkeditems)
  //   }
  // }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item.id)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  uncheckall() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  authorizeSelected() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (!this.checkeditems.length) {
      validdata = false;
      validationerrorMsg.push("Please select list to Authorize");
    }

    if (validdata) {
      this.show_loader = true;
      console.log("formdata", JSON.stringify(this.checkeditems))
      this.salesservice.AuthorizeOrders(this.checkeditems)
        .subscribe(res => {
          console.log("authres", res.data)
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.loadauthorizedlist();
            this.show_loader = false;
            this.uncheckall()
          });
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        });
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
