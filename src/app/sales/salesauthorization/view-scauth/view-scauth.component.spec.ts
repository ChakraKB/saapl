import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewScauthComponent } from './view-scauth.component';

describe('ViewScauthComponent', () => {
  let component: ViewScauthComponent;
  let fixture: ComponentFixture<ViewScauthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewScauthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewScauthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
