import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { SalesorderService } from '../../../services/salesorder/salesorder.service'
import * as _ from "underscore";
import { Location } from '@angular/common';
@Component({
  selector: 'app-view-scauth',
  templateUrl: './view-scauth.component.html',
  styleUrls: ['./view-scauth.component.css']
})
export class ViewScauthComponent implements OnInit {
  scid: any;
  salesorderdetails: any
  orderdetail: any;
  show_loader: boolean = false;
  supplierdetails: any;
  totalQty: any;
  address: any;
  pincode: any;
  url: any
  savedsc: any
  constructor(
    private salesservice: SalesorderService,
    private location: Location,
    private acroute: ActivatedRoute) {
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.scid = params.id;
        console.log("urlparams", this.scid);
      }
    });
  }

  ngOnInit() {
    this.getscdetails(this.scid)
  }

  getscdetails(orderid) {
    this.show_loader = true
    this.salesservice.getByID(orderid)
      .subscribe(res => {
        this.orderdetail = res.data;
        this.supplierdetails = this.orderdetail.supplier;
        this.address = this.orderdetail.address.split('##');
        this.pincode = this.address[1].split(',')
        console.log(this.pincode)
        console.log("address", this.address)
        this.salesorderdetails = this.orderdetail.salesOrderDetails
        var totqty = 0
        _.forEach(this.salesorderdetails, function (item, key) {
          totqty += item.quantity
        })
        this.totalQty = totqty;
        this.show_loader = false;
      });
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsections').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Work order</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          .print-view>tbody>tr>td,
          .print-view>tbody>tr>th,
          .print-view>tfoot>tr>td,
          .print-view>tfoot>tr>th,
          .print-view>thead>tr>td,
          .print-view>thead>tr>th {
            border: 1px solid #333 !important; padding:2px 3px !important
          }
          .print-view .print-label p { font-weight : 500 !important;}
          .print-view .print-label { font-weight : 500 !important;}
          .print-view .cs { background-color:#ccc !important;}
          .print-view .cs td { background-color:#ccc !important;}
           table.print-view td p {
           margin: 0!important;
          }
        table.report-container {
        page-break-after:always;
        }
        thead.report-header {
        display:table-header-group;
        }
        tfoot.report-footer {
        display:table-footer-group;
        } 
        </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  back() {
    this.location.back()
  }


}
