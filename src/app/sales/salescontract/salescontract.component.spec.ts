import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalescontractComponent } from './salescontract.component';

describe('SalescontractComponent', () => {
  let component: SalescontractComponent;
  let fixture: ComponentFixture<SalescontractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalescontractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalescontractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
