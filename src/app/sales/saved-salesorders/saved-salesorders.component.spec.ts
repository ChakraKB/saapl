import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedSalesordersComponent } from './saved-salesorders.component';

describe('SavedSalesordersComponent', () => {
  let component: SavedSalesordersComponent;
  let fixture: ComponentFixture<SavedSalesordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedSalesordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedSalesordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
