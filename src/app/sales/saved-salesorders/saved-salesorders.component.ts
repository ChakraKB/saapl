import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { Router, NavigationExtras } from '@angular/router';
import { SalesorderService } from '../../services/salesorder/salesorder.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import * as _ from 'underscore';
class Person { }
@Component({
    selector: 'app-saved-salesorders',
    templateUrl: './saved-salesorders.component.html',
    styleUrls: ['./saved-salesorders.component.css']
})
export class SavedSalesordersComponent implements OnInit {
    myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtOptions: DataTables.Settings = {};
    persons: Person[] = [];
    dtTrigger: Subject<any> = new Subject();
    toastertime: number;
    tosterminlength: number;
    rolesform: FormGroup;
    orderForm: FormGroup;
    formdata: any;
    buttonname: string;
    active_status: any;
    statusarray: any;
    response: any;
    allrole: any;
    role: any = {};
    data: any;
    tabletimeout: any;
    samplearray: any;
    nodata: any;
    showtab: boolean = false;
    show_loader: boolean = false;
    orderdetail: any;
    orderid: any;
    scrolltime: any;
    scstatus: any;
    selected: any;
    scorder: any;
    public unitid = JSON.parse(localStorage.getItem("UnitId"));
    constructor(private fb: FormBuilder,
        private masterservice: MasterService,
        private notif: NotificationsService,
        private salesservice: SalesorderService,
        private http: Http,
        private router: Router, ) {
        this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
        this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
        this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
        this.scrolltime = AppConstant.APP_CONST.scrolltime.scrollt;
    }

    ngOnInit() {
        this.buttonname = "Save"
        this.LoadAngTable();
        this.nodata = false;
        this.scstatus = [{
            value: 1,
            name: "Inprogress"
        },
        {
            value: 0,
            name: "Completed"
        }]
        this.scorder = 1
    }

    getscstatdtl(data) {
        console.log(data)
        this.show_loader = true;
        this.salesservice.getAllSavedOrder(data, this.unitid).subscribe(res => {
            this.show_loader = false;
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.persons = res.data;
                this.dtTrigger.next();
            });
        })
    }

    notifyfunction() {
        this.cancel()
    }

    loadsavedlist() {
        this.salesservice.getAllSavedOrder(0, this.unitid)
            .subscribe(res => {
                this.persons = res.data;
                this.dtTrigger.next();
            });
    }

    LoadAngTable() {
        this.show_loader = true
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10
        };
        this.http.get(AppConstant.API_ENDPOINT + AppConstant.API_URL.SALESORDER.GETSAVED + this.unitid + '/' + 1)
            .map(this.extractData)
            .subscribe(persons => {
                console.log("totalorders", persons)
                this.show_loader = false;
                this.persons = persons
                console.log("submittedarray", this.persons)
                this.dtTrigger.next();
            });
    }
    private extractData(res: Response) {
        const body = res.json();
        return body.data || {};
    }

    cancel() {
        this.showtab = false;
        setTimeout(() => {
            jQuery('#list')[0].click();
        });
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.loadsavedlist();
        });

    }

}
