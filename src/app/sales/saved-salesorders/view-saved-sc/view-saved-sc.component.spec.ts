import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSavedScComponent } from './view-saved-sc.component';

describe('ViewSavedScComponent', () => {
  let component: ViewSavedScComponent;
  let fixture: ComponentFixture<ViewSavedScComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSavedScComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSavedScComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
