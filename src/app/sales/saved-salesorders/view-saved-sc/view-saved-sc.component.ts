import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { SalesorderService } from '../../../services/salesorder/salesorder.service'
import * as _ from "underscore";

@Component({
  selector: 'app-view-saved-sc',
  templateUrl: './view-saved-sc.component.html',
  styleUrls: ['./view-saved-sc.component.css']
})
export class ViewSavedScComponent implements OnInit {
  scid: any;
  orderdetail: any[]
  constructor(private salesservice: SalesorderService,
    private acroute: ActivatedRoute) {
    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.scid = params.id;
        console.log("urlparams", this.scid);
      }
    });
  }

  ngOnInit() {
    this.getscdetails(this.scid)
  }

  getscdetails(orderid) {
    this.salesservice.getByID(orderid)
      .subscribe(res => {
        this.orderdetail = res.data;
      });
  }

}
