import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScDetailPopupComponent } from './sc-detail-popup.component';

describe('ScDetailPopupComponent', () => {
  let component: ScDetailPopupComponent;
  let fixture: ComponentFixture<ScDetailPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScDetailPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScDetailPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
