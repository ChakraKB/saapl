import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class PlanningService {
  apiendpoint: string;
  getpendingplans: string;
  getplandetails: string;
  sharingData: string;
  createalloc: string;
  allocatedlist: string;
  groupbomlist: string;
  updategroupbomlist: string;
  getplancancellistbysc: string;
  getsclist: string;
  getplansaleslist: any;
  getcancellist: any;
  cancelplanitems: any;
  getplanitembom: string;
  getitembom: string;
  cancelplan: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getpendingplans = this.apiendpoint + AppConstant.API_URL.PLAN.GET;
    this.getplandetails = this.apiendpoint + AppConstant.API_URL.PLAN.GETPLAN;

    this.createalloc = this.apiendpoint + AppConstant.API_URL.PLAN.CREATEPLANALLOC;
    this.allocatedlist = this.apiendpoint + AppConstant.API_URL.PLAN.ALLOCATEDLIST;
    this.groupbomlist = this.apiendpoint + AppConstant.API_URL.PLAN.GROUPBOMLIST;
    this.updategroupbomlist = this.apiendpoint + AppConstant.API_URL.PLAN.UPDATEBOM;
    this.getsclist = this.apiendpoint + AppConstant.API_URL.PLAN.GETSCLIST;
    this.getplancancellistbysc = this.apiendpoint + AppConstant.API_URL.PLAN.GETPOCANCELLIST;

    this.getplansaleslist = this.apiendpoint + AppConstant.API_URL.PLAN.GETPLANSALESLIST;
    this.getcancellist = this.apiendpoint + AppConstant.API_URL.PLAN.GETCANCELLIST;
    this.cancelplanitems = this.apiendpoint + AppConstant.API_URL.PLAN.CANCELPLANITEM;
    this.getplanitembom = this.apiendpoint + AppConstant.API_URL.PLAN.GETPLANITEMBOM;
    this.getitembom = this.apiendpoint + AppConstant.API_URL.PLAN.GETITEMBOM;

    this.cancelplan = this.apiendpoint + AppConstant.API_URL.PLAN.CANCELPLAN;
  }

  public getAllPendingPlanList(data: any) {
    let body = new URLSearchParams();
    body.append('customer', data.customer);
    body.append('sc_no', data.sc_no);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    return this.http.put(this.getpendingplans, body)
      .map((response: Response) => response.json());
  }

  public GetPlanDetails(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.getplandetails, body)
      .map((response: Response) => response.json());
  }

  public CreateAllocation(data: any, data2: any, planid: any, strpartid: any, saved: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    body.append('obj1', JSON.stringify(data2));
    body.append('plan_id', planid);
    body.append('str_part_id', strpartid);
    body.append('is_saved', saved);
    return this.http.post(this.createalloc, body)
      .map((response: Response) => response.json());
  }

  public GetAllocatedList() {
    return this.http.get(this.allocatedlist)
      .map((response: Response) => response.json());
  }

  public GetGroupBomList(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.groupbomlist, body)
      .map((response: Response) => response.json());
  }

  public UpdateGroupBomList(data: any, data2: any, planid: any, strpartid: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    body.append('obj1', JSON.stringify(data2));
    body.append('plan_id', planid);
    body.append('str_part_id', strpartid);
    return this.http.post(this.updategroupbomlist, body)
      .map((response: Response) => response.json());
  }

  public GetPlanCancelList(id) {
    return this.http.get(this.getplancancellistbysc + id)
      .map((response: Response) => response.json())
  }

  public GetSCList(unitid: any) {
    return this.http.get(this.getsclist + unitid)
      .map((response: Response) => response.json());
  }

  public GetPlanCacelSC(unitid) {
    return this.http.get(this.getplansaleslist)
      .map((response: Response) => response.json());
  }

  public GetCancelList(scid) {
    return this.http.get(this.getcancellist + scid)
      .map((response: Response) => response.json());
  }

  public CacnelPlanItem(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.cancelplanitems, body)
      .map((response: Response) => response.json());
  }

  public GetPlanItemBom(scid) {
    return this.http.get(this.getplanitembom + scid)
      .map((response: Response) => response.json());
  }

  public GetItemBom(partid, scid, id) {
    return this.http.get(this.getitembom + partid + '/' + scid + '/' + id)
      .map((response: Response) => response.json());
  }

  public CancelPlan(data: any, data2: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    body.append('obj1', JSON.stringify(data2));
    return this.http.put(this.cancelplan, body)
      .map((response: Response) => response.json());
  }

}
