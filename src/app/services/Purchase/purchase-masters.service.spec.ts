import { TestBed, inject } from '@angular/core/testing';

import { PurchaseMastersService } from './purchase-masters.service';

describe('PurchaseMastersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PurchaseMastersService]
    });
  });

  it('should be created', inject([PurchaseMastersService], (service: PurchaseMastersService) => {
    expect(service).toBeTruthy();
  }));
});
