import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class PurchaseMastersService {
  apiendpoint: string;

  getpricebasis: string;
  getpricebasisbystatus: string;
  addpricebasis: string;
  updatepricebasis: string;
  deletepricebasis: string;

  getcostcenter: string;
  addcostcenter: string;
  updatecostcenter: string;
  deletecostcenter: string;

  getfreightcharge: string;
  getfreightchargebystatus: string
  addfreightcharge: string;
  updatefreightcharge: string;
  deletefreightcharge: string;

  getcfcharge: string;
  addcfcharge: string;
  updatecfcharge: string;
  deletecfcharge: string;

  gettariff: string;
  addtariff: string;
  updatetariff: string;
  deletetariff: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getpricebasis = this.apiendpoint + AppConstant.API_URL.PRICE_BASIS.GET;
    this.getpricebasisbystatus = this.apiendpoint + AppConstant.API_URL.PRICE_BASIS.GETBYSTATUS;
    this.addpricebasis = this.apiendpoint + AppConstant.API_URL.PRICE_BASIS.CREATE;
    this.updatepricebasis = this.apiendpoint + AppConstant.API_URL.PRICE_BASIS.UPDATE;
    this.deletepricebasis = this.apiendpoint + AppConstant.API_URL.PRICE_BASIS.DELETE;

    this.getcostcenter = this.apiendpoint + AppConstant.API_URL.COST_CENTER.GET;
    this.addcostcenter = this.apiendpoint + AppConstant.API_URL.COST_CENTER.CREATE;
    this.updatecostcenter = this.apiendpoint + AppConstant.API_URL.COST_CENTER.UPDATE;
    this.deletecostcenter = this.apiendpoint + AppConstant.API_URL.COST_CENTER.DELETE;

    this.getfreightcharge = this.apiendpoint + AppConstant.API_URL.FREIGHT_CHARGE.GET;
    this.getfreightchargebystatus = this.apiendpoint + AppConstant.API_URL.FREIGHT_CHARGE.GETBYSTATUS;
    this.addfreightcharge = this.apiendpoint + AppConstant.API_URL.FREIGHT_CHARGE.CREATE;
    this.updatefreightcharge = this.apiendpoint + AppConstant.API_URL.FREIGHT_CHARGE.UPDATE;
    this.deletefreightcharge = this.apiendpoint + AppConstant.API_URL.FREIGHT_CHARGE.DELETE;

    this.getcfcharge = this.apiendpoint + AppConstant.API_URL.CF_CHARGE.GET;
    this.addcfcharge = this.apiendpoint + AppConstant.API_URL.CF_CHARGE.CREATE;
    this.updatecfcharge = this.apiendpoint + AppConstant.API_URL.CF_CHARGE.UPDATE;
    this.deletecfcharge = this.apiendpoint + AppConstant.API_URL.CF_CHARGE.DELETE;

    this.gettariff = this.apiendpoint + AppConstant.API_URL.TARIFF.GET;
    this.addtariff = this.apiendpoint + AppConstant.API_URL.TARIFF.CREATE;
    this.updatetariff = this.apiendpoint + AppConstant.API_URL.TARIFF.UPDATE;
    this.deletetariff = this.apiendpoint + AppConstant.API_URL.TARIFF.DELETE;

  }
  // ========================= PRICE BASIS ==============================
  public getallpricebasis() {
    return this.http.get(this.getpricebasis)
      .map((response: Response) => response.json());
  }

  public GetPriceBasisByStatus(data) {
    return this.http.get(this.getpricebasisbystatus + data)
      .map((response: Response) => response.json());
  }

  public CreatePricebasis(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addpricebasis, body)
      .map((response: Response) => response.json());
  }

  public UpdatePricebasis(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatepricebasis, body)
      .map((response: Response) => response.json());
  }

  public DeletePricebasis(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletepricebasis, options)
      .map((response: Response) => response.json());
  }

  // =============================== COST CENTER ========================================

  public getallCostcenter() {
    return this.http.get(this.getcostcenter)
      .map((response: Response) => response.json());
  }

  public CreateCostcenter(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addcostcenter, body)
      .map((response: Response) => response.json());
  }

  public UpdateCostcenter(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatecostcenter, body)
      .map((response: Response) => response.json());
  }

  public DeleteCostcenter(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletecostcenter, options)
      .map((response: Response) => response.json());
  }

  // =============================== FREIGHT CHARGE ========================================

  public getallFreightCharge() {
    return this.http.get(this.getfreightcharge)
      .map((response: Response) => response.json());
  }

  public GetFreightChargesByStatus(data) {
    return this.http.get(this.getfreightchargebystatus + data)
      .map((response: Response) => response.json());
  }

  public CreateFreightCharge(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addfreightcharge, body)
      .map((response: Response) => response.json());
  }

  public UpdateFreightCharge(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatefreightcharge, body)
      .map((response: Response) => response.json());
  }

  public DeleteFreightCharge(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletefreightcharge, options)
      .map((response: Response) => response.json());
  }

  // =============================== CF CHARGE ========================================

  public getallCFCharge() {
    return this.http.get(this.getcfcharge)
      .map((response: Response) => response.json());
  }

  public CreateCFCharge(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addcfcharge, body)
      .map((response: Response) => response.json());
  }

  public UpdateCFCharge(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatecfcharge, body)
      .map((response: Response) => response.json());
  }

  public DeleteCFCharge(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletecfcharge, options)
      .map((response: Response) => response.json());
  }

  // =============================== TARIFF ========================================

  public getallTariff() {
    return this.http.get(this.gettariff)
      .map((response: Response) => response.json());
  }

  public CreateTariff(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addtariff, body)
      .map((response: Response) => response.json());
  }

  public UpdateTariff(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatetariff, body)
      .map((response: Response) => response.json());
  }

  public DeleteTariff(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletetariff, options)
      .map((response: Response) => response.json());
  }

}
