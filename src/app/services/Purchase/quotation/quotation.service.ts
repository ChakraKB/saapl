import { Injectable } from '@angular/core';
import { AppConstant } from '../../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class QuotationService {
  apiendpoint: string;
  getquotationbyid: string;
  createquotation: string;
  updatequotation: string;
  getquotationbysupplier: string;
  getcomparelist: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getquotationbyid = this.apiendpoint + AppConstant.API_URL.QUOTATION.GETQUOTATIONBYID;
    this.createquotation = this.apiendpoint + AppConstant.API_URL.QUOTATION.CREATE;
    this.updatequotation = this.apiendpoint + AppConstant.API_URL.QUOTATION.UPDATE;
    this.getquotationbysupplier = this.apiendpoint + AppConstant.API_URL.QUOTATION.GETQUOTATIONBYSUPPLIER;

    this.getcomparelist = this.apiendpoint + AppConstant.API_URL.QUOTATION.GETCOMPARELIST;
  }

  public GetQutationByid(data) {
    return this.http.get(this.getquotationbyid + data)
      .map((response: Response) => response.json());
  }

  public GetQutationBySupplier(enqid: any, suppid: any) {
    return this.http.get(this.getquotationbysupplier + enqid + '/' + suppid)
      .map((response: Response) => response.json());
  }

  public CreateQuotation(data: any, file: any) {
    let body = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        body.append("file", file[i]);
      }
    }
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.createquotation, body)
      .map((response: Response) => response.json());
  }

  public UpdateQuotation(data: any, file: any) {
    let body = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        body.append("file", file[i]);
      }
    }
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatequotation, body)
      .map((response: Response) => response.json());
  }

  public GetCompareList(enqid: any) {
    return this.http.get(this.getcomparelist + enqid)
      .map((response: Response) => response.json());
  }

}
