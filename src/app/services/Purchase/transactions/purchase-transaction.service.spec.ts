import { TestBed, inject } from '@angular/core/testing';

import { PurchaseTransactionService } from './purchase-transaction.service';

describe('PurchaseTransactionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PurchaseTransactionService]
    });
  });

  it('should be created', inject([PurchaseTransactionService], (service: PurchaseTransactionService) => {
    expect(service).toBeTruthy();
  }));
});
