import { Injectable } from '@angular/core';
import { AppConstant } from '../../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class PurchaseTransactionService {
  apiendpoint: string;
  addindent: string;
  getindent: string;
  getindentbyid: string;
  getauthlist: string;
  authorize: string
  getfiles: string;
  getapproved: string;
  getcanceled: string;
  cancelindent: string;
  cancelindentauth: string;
  getindentfiltered: string;
  getpendingindents: string;
  createenquery: string;
  getprevdetails: string;
  purchaseauto: string;
  getitemsuppliers: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getindent = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GET;
    this.getindentbyid = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETBYID;
    this.getprevdetails = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETPREVDETAILS;
    this.getauthlist = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETAUTHLIST;
    this.addindent = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.CREATE;
    this.getfiles = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETFILES;
    this.getapproved = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETAPPROVED;
    this.getcanceled = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETCANCELED;
    this.getindentfiltered = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETPURCHASEFILTER;

    this.authorize = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.UPDATEAUTH;
    this.cancelindent = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.UPDATECANCELINDENT;
    this.cancelindentauth = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.UPDATECANCELAUTH;

    this.getpendingindents = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETPENDINGINDENTS;
    this.createenquery = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.CREATEENQUERY;
    this.purchaseauto = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.PURCHASE;
    this.getitemsuppliers = this.apiendpoint + AppConstant.API_URL.PURCHASEINDENT.GETITEMSUPPLIERS
  }

  public getIndentList(id) {
    return this.http.get(this.getindent + id)
      .map((response: Response) => response.json());
  }

  public getPrevPodetails(id) {
    return this.http.get(this.getprevdetails + id)
      .map((response: Response) => response.json());
  }

  public GetAuthList(unitid) {
    return this.http.get(this.getauthlist + unitid)
      .map((response: Response) => response.json());
  }

  public getIndentByid(id) {
    return this.http.get(this.getindentbyid + id)
      .map((response: Response) => response.json());
  }

  public getIndentApproved(id) {
    return this.http.get(this.getapproved + id)
      .map((response: Response) => response.json());
  }

  public getIndentCacnceled(id) {
    return this.http.get(this.getcanceled + id)
      .map((response: Response) => response.json());
  }

  public getFilesByid(id) {
    return this.http.get(this.getfiles + id)
      .map((response: Response) => response.json());
  }

  public GetFilteredIndent(data: any) {
    let body = new URLSearchParams();
    body.append('auth_sts', data.auth_sts);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('unit_id', data.unit_id);
    return this.http.put(this.getindentfiltered, body)
      .map((response: Response) => response.json());
  }

  public AddNewIndent(model: any, file: any) {
    let data = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        data.append("file", file[i]);
      }
    }
    data.append("obj", JSON.stringify(model));
    let body = new URLSearchParams();
    body.append("obj", JSON.stringify(model));
    return this.http.post(this.addindent, data)
      .map((response: Response) => response.json());
  }

  public AuthorizeIndent(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.authorize, body)
      .map((response: Response) => response.json());
  }

  public UpdateCancelIndent(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.cancelindent, body)
      .map((response: Response) => response.json());
  }

  public UpdateCancelAuth(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.cancelindentauth, body)
      .map((response: Response) => response.json());
  }

  public getPendingIndentList(id, type, dpt) {
    return this.http.get(this.getpendingindents + id + '/' + type + '/' + dpt)
      .map((response: Response) => response.json());
  }

  public CreateEnquery(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createenquery, body)
      .map((response: Response) => response.json());
  }

  public PurchaseAutoIndent(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.purchaseauto, data)
      .map((response: Response) => response.json());
  }

  public getItemSuppliers(id) {
    return this.http.get(this.getitemsuppliers + id)
      .map((response: Response) => response.json());
  }

}
