import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class RackService {

  apiendpoint: string;
  getrack: string;
  addnewrack: string;
  deleterack: string;
  updaterack: string;
  getbystatus: string;
  getrackbylocation: string;

  constructor(private http: Http) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getrack = this.apiendpoint + AppConstant.API_URL.RACKS.GET;
    this.addnewrack = this.apiendpoint + AppConstant.API_URL.RACKS.CREATE;
    this.updaterack = this.apiendpoint + AppConstant.API_URL.RACKS.UPDATE;
    this.deleterack = this.apiendpoint + AppConstant.API_URL.RACKS.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.RACKS.GETBYSTATUS;
    this.getrackbylocation = this.apiendpoint + AppConstant.API_URL.RACKS.GETBYLOC;
  }

  public getRackByLocation(locid) {
    return this.http.get(this.getrackbylocation + locid)
      .map((response: Response) => response.json());
  }

  public getAllRack() {
    return this.http.get(this.getrack)
      .map((response: Response) => response.json());
  }

  public getRackByStatus(status) {
    return this.http.get(this.getbystatus + status)
      .map((response: Response) => response.json());
  }

  public addRack(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewrack, body)
      .map((response: Response) => response.json());
  }

  public deleteRack(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleterack, options)
      .map((response: Response) => response.json());
  }

  public updateRack(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updaterack, body)
      .map((response: Response) => response.json());
  }

}
