import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class ReportsService {
  apiendpoint: string;

  getledgerlist: string;
  getdcreportlist: string
  salesrerport: string;
  purchasererport: string;
  rejectreport: string;
  disposalreport: string;
  toolsreport: string;
  itemlocationList: string;
  itemreportlocationList: string;
  getseries: string
  getclassbyseires: string
  getitembyseiresclass: string;
  getitemstockreceipts: string;
  getitemstockissues: string;
  getgrnreport: string;
  getinvoicereport : string

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getledgerlist = this.apiendpoint + AppConstant.API_URL.REPORTS.LEDGER.GET;
    this.getdcreportlist = this.apiendpoint + AppConstant.API_URL.REPORTS.DCREPORT.GET;
    this.salesrerport = this.apiendpoint + AppConstant.API_URL.REPORTS.SALESREPORT;
    this.purchasererport = this.apiendpoint + AppConstant.API_URL.REPORTS.POREPORT;
    this.rejectreport = this.apiendpoint + AppConstant.API_URL.REPORTS.REJECTLIST;
    this.disposalreport = this.apiendpoint + AppConstant.API_URL.REPORTS.DISPOSALREPORT;
    this.itemlocationList = this.apiendpoint + AppConstant.API_URL.REPORTS.ITEMBINLOCATION;
    this.toolsreport = this.apiendpoint + AppConstant.API_URL.REPORTS.TOOLSREPORT;
    this.itemreportlocationList = this.apiendpoint + AppConstant.API_URL.REPORTS.ITEMLOCATIONS;
    this.getseries = this.apiendpoint + AppConstant.API_URL.REPORTS.GETSTOCKSERIES;
    this.getclassbyseires = this.apiendpoint + AppConstant.API_URL.REPORTS.GETCLASSBYSERIES;
    this.getitembyseiresclass = this.apiendpoint + AppConstant.API_URL.REPORTS.GETITEMBYSERIESCLASS;
    this.getitemstockreceipts = this.apiendpoint + AppConstant.API_URL.REPORTS.GETITEMSTOCKRECEIPTS;
    this.getitemstockissues = this.apiendpoint + AppConstant.API_URL.REPORTS.GETITEMSTOCKISSUES;
    this.getgrnreport = this.apiendpoint + AppConstant.API_URL.REPORTS.GETGRNREPORT;
    this.getinvoicereport = this.apiendpoint + AppConstant.API_URL.REPORTS.GETINVOICEREPORT;
  }

  // STOCK LEDGER

  public GetStockLedgerList(data: any) {
    let body = new URLSearchParams();
    // body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('part_id', data.part_id);
    body.append('series', data.series);
    return this.http.post(this.getledgerlist, body)
      .map((response: Response) => response.json());
  }

  // DC REPORT

  public GetDcReportList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('part_no', data.part_no);
    return this.http.put(this.getdcreportlist, body)
      .map((response: Response) => response.json());
  }

  // SALES REPORT

  public GetSCReportList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('part_id', data.part_id);
    body.append('country_id', data.loc_id);
    body.append('supplier_id', data.supplier_id);
    return this.http.put(this.salesrerport, body)
      .map((response: Response) => response.json());
  }

  // PO REPORT

  public GetPOReportList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('part_id', data.part_id);
    return this.http.put(this.purchasererport, body)
      .map((response: Response) => response.json());
  }

  // REJECTED REPORT

  public GetRejectedReportList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    return this.http.put(this.rejectreport, body)
      .map((response: Response) => response.json());
  }

  // DISPOSAL REPORT

  public GetdisposalReportList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    return this.http.put(this.disposalreport, body)
      .map((response: Response) => response.json());
  }

  // TOOL REPORT

  public GetToolReportList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    return this.http.put(this.toolsreport, body)
      .map((response: Response) => response.json());
  }

  // ITEM LOCATION REPORT

  public GetItemLocReportList(data: any) {
    return this.http.get(this.itemlocationList + data.unit_id + "/" + data.bin_id)
      .map((response: Response) => response.json());
  }

  // ITEM REPORT

  public GetItemReportList(data: any) {
    return this.http.get(this.itemreportlocationList + data.unit_id + "/" + data.part_id)
      .map((response: Response) => response.json());
  }

  // STCOK SERIES REPORT

  public GetItemSeriesList(data: any) {
    let body = new URLSearchParams();
    // body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    // body.append('series', data.series);
    return this.http.post(this.getseries, body)
      .map((response: Response) => response.json());
  }

  public GetClassBySeries(data: any) {
    let body = new URLSearchParams();
    // body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('series', data.series);
    return this.http.post(this.getclassbyseires, body)
      .map((response: Response) => response.json());
  }

  public GetItemsBySeriesClass(data: any) {
    let body = new URLSearchParams();
    // body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('series', data.series);
    body.append('cls', data.cls);
    return this.http.post(this.getitembyseiresclass, body)
      .map((response: Response) => response.json());
  }

  public GetItemsStockReceipts(data: any) {
    let body = new URLSearchParams();
    // body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('part_id', data.part_id);
    body.append('receipts_id', data.receipts_id);
    return this.http.post(this.getitemstockreceipts, body)
      .map((response: Response) => response.json());
  }

  public GetItemsStockIssues(data: any) {
    let body = new URLSearchParams();
    // body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('part_id', data.part_id);
    return this.http.post(this.getitemstockissues, body)
      .map((response: Response) => response.json());
  }

  public GetGrnReport(data: any) {
    let body = new URLSearchParams();
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('grn_type', data.grn_type);
    return this.http.post(this.getgrnreport, body)
      .map((response: Response) => response.json());
  }

  public GetInvoiceReport(data: any) {
    let body = new URLSearchParams();
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('status', data.status);
    return this.http.post(this.getinvoicereport, body)
      .map((response: Response) => response.json());
  }

}
