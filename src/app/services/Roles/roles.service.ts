import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";
import { AppConstant } from '../../app.constant';

@Injectable()
export class RolesService {
  apiendpoint: string;
  getroles: string;
  addnewrole: string;
  updaterole: string;
  deleterole: string;
  getbystatus: string
  screens: string
  constructor(private http: Http) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.screens = this.apiendpoint + AppConstant.API_URL.LEFTMENU.ROLE;
    this.getroles = this.apiendpoint + AppConstant.API_URL.ROLES.GET;
    this.addnewrole = this.apiendpoint + AppConstant.API_URL.ROLES.CREATE;
    this.updaterole = this.apiendpoint + AppConstant.API_URL.ROLES.UPDATE;
    this.deleterole = this.apiendpoint + AppConstant.API_URL.ROLES.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.ROLES.GETBYSTATUS;
  }

  public getScreenByRole(data: any) {
    return this.http.get(this.screens + data)
      .map((response: Response) => response.json());
  }

  public getAllRoles() {
    return this.http.get(this.getroles)
      .map((response: Response) => response.json());
  }

  public addRole(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    console.log("body", body);
    return this.http.put(this.addnewrole, body)
      .map((response: Response) => response.json());
  }

  public deleteRole(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleterole, options)
      .map((response: Response) => response.json());
  }

  public updateRole(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updaterole, body)
      .map((response: Response) => response.json());
  }

  public getByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

}
