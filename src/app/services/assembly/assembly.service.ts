import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
    Http,
    RequestMethod,
    Response,
    RequestOptions,
    URLSearchParams,
    Headers
} from "@angular/http";

@Injectable()
export class AssemblyService {
    apiendpoint: string;
    getlistbywo: string;
    createreq: string;
    getassemblelist: string

    constructor(private http: Http,) {
        this.apiendpoint = AppConstant.API_ENDPOINT;
        this.getlistbywo = this.apiendpoint + AppConstant.API_URL.ASSEMBLY.GETBYWO;
        this.createreq = this.apiendpoint + AppConstant.API_URL.ASSEMBLY.CREATE;
        this.getassemblelist = this.apiendpoint + AppConstant.API_URL.ASSEMBLY.GETREQLIST;
    }

    public GetListByWO(id) {
        return this.http.get(this.getlistbywo + id)
            .map((response: Response) => response.json());
    }

    public CreateAsmblyReq(data: any, unitid: any) {
        let body = new URLSearchParams();
        body.append('obj', JSON.stringify(data));
        body.append('unit_id', unitid);
        return this.http.post(this.createreq, body)
            .map((response: Response) => response.json());
    }

    public GetAssembleReqList(data: any) {
        let body = new URLSearchParams();
        body.append('unit_id', data.unit_id);
        body.append('part_no', data.part_no);
        body.append('from_date', data.from_date);
        body.append('to_date', data.to_date);
        body.append('sales_order_id', data.sc_id);
        return this.http.put(this.getassemblelist, body)
            .map((response: Response) => response.json());
    }

}
