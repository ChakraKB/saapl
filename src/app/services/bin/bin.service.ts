import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class BinService {

  apiendpoint: string;
  getbin: string;
  addnewbin: string;
  deletebin: string;
  updatebin: string;
  getbystatus: string;

  constructor(private http: Http) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getbin = this.apiendpoint + AppConstant.API_URL.BINS.GET;
    this.addnewbin = this.apiendpoint + AppConstant.API_URL.BINS.CREATE;
    this.updatebin = this.apiendpoint + AppConstant.API_URL.BINS.UPDATE;
    this.deletebin = this.apiendpoint + AppConstant.API_URL.BINS.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.BINS.GETBYSTATUS;
  }

  public getBin() {
    return this.http.get(this.getbin)
      .map((response: Response) => response.json());
  }
  public addBin(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewbin, body)
      .map((response: Response) => response.json());
  }

  public deleteBin(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletebin, options)
      .map((response: Response) => response.json());
  }

  public updateBin(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatebin, body)
      .map((response: Response) => response.json());
  }

  public getBinByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

}
