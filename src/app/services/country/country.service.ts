import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class CountryService {

  apiendpoint: string;
  getcountry: string;
  addnewcountry: string;
  deletecountry: string;
  updatecountry: string;
  getcountrybystatus: string;

  getstate: string;
  addnewstate: string;
  updatestate: string;
  deletestate: string;
  getstatebystatus: string;
  getstatebycountry: string;

  getcity: string;
  addnewcity: string;
  updatecity: string;
  deletecity: string;
  getcitybystatus: string
  getcitybystate: string;


  constructor(
    private http: Http) {

    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getcountry = this.apiendpoint + AppConstant.API_URL.COUNTRY.GET;
    this.addnewcountry = this.apiendpoint + AppConstant.API_URL.COUNTRY.CREATE;
    this.updatecountry = this.apiendpoint + AppConstant.API_URL.COUNTRY.UPDATE;
    this.deletecountry = this.apiendpoint + AppConstant.API_URL.COUNTRY.DELETE;
    this.getcountrybystatus = this.apiendpoint + AppConstant.API_URL.COUNTRY.GETBYSTATUS;

    this.getstate = this.apiendpoint + AppConstant.API_URL.STATE.GET;
    this.addnewstate = this.apiendpoint + AppConstant.API_URL.STATE.CREATE;
    this.updatestate = this.apiendpoint + AppConstant.API_URL.STATE.UPDATE;
    this.deletestate = this.apiendpoint + AppConstant.API_URL.STATE.DELETE;
    this.getstatebystatus = this.apiendpoint + AppConstant.API_URL.STATE.GETBYSTATUS;
    this.getstatebycountry = this.apiendpoint + AppConstant.API_URL.STATE.GETBYCOUNTRY;

    this.getcity = this.apiendpoint + AppConstant.API_URL.CITY.GET;
    this.addnewcity = this.apiendpoint + AppConstant.API_URL.CITY.CREATE;
    this.updatecity = this.apiendpoint + AppConstant.API_URL.CITY.UPDATE;
    this.deletecity = this.apiendpoint + AppConstant.API_URL.CITY.DELETE;
    this.getcitybystatus = this.apiendpoint + AppConstant.API_URL.CITY.GETBYSTATUS;
    this.getcitybystate = this.apiendpoint + AppConstant.API_URL.CITY.GETBYSTATE;
  }
  //  ============================================= COUNTRY =============================================
  public getallCountry() {
    return this.http.get(this.getcountry)
      .map((response: Response) => response.json());
  }
  public addCountry(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewcountry, body)
      .map((response: Response) => response.json());
  }

  public deleteCountry(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletecountry, options)
      .map((response: Response) => response.json());
  }

  public updateCountry(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatecountry, body)
      .map((response: Response) => response.json());
  }

  public getCountryByStatus(data: any) {
    return this.http.get(this.getcountrybystatus + data)
      .map((response: Response) => response.json());
  }



  // =============================================== STATE =============================================

  public getallState() {
    return this.http.get(this.getstate)
      .map((response: Response) => response.json());
  }
  public addState(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewstate, body)
      .map((response: Response) => response.json());
  }

  public deleteState(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletestate, options)
      .map((response: Response) => response.json());
  }

  public updateState(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatestate, body)
      .map((response: Response) => response.json());
  }

  public getStateByStatus(data: any) {
    return this.http.get(this.getstatebystatus + data)
      .map((response: Response) => response.json());
  }

  public getStateByCountry(data: any) {
    return this.http.get(this.getstatebycountry + data)
      .map((response: Response) => response.json());
  }

  // =============================================== CITY =============================================

  public getallCity() {
    return this.http.get(this.getcity)
      .map((response: Response) => response.json());
  }
  public addCity(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewcity, body)
      .map((response: Response) => response.json());
  }

  public deleteCity(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletecity, options)
      .map((response: Response) => response.json());
  }

  public updateCity(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatecity, body)
      .map((response: Response) => response.json());
  }

  public getCityByStatus(data: any) {
    return this.http.get(this.getcitybystatus + data)
      .map((response: Response) => response.json());
  }

  public getCityByState(data: any) {
    return this.http.get(this.getcitybystate + data)
      .map((response: Response) => response.json());
  }
}
