import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "underscore";

@Pipe({
  name: 'datafilter'
})
export class DatafilterPipe implements PipeTransform {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row => row.emp_name.indexOf(query) > -1);
    }
    return array;
  }

}
