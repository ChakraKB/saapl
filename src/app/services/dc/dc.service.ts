import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class DcService {
  apiendpoint: string;
  createdc: string;
  getdclist: string;
  getbyid: string;
  getdcsuppliers: string;
  getbysupplier: string;
  createnewdc: string;
  getdclocations: string;
  getstoresjobcart: string
  getjobcartitems: string;
  getjobcartitemslist: string;
  getjobcartsubitems: string;
  getjobcartsubitemslist: string;
  getrwItems: string;
  createjcdc: string;
  createsajobcart: string;
  constructor(
    private http: Http
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getdclist = this.apiendpoint + AppConstant.API_URL.DC.GET;
    this.createdc = this.apiendpoint + AppConstant.API_URL.DC.CREATE;
    this.getbyid = this.apiendpoint + AppConstant.API_URL.DC.GETBYID;
    this.getdcsuppliers = this.apiendpoint + AppConstant.API_URL.DC.DC_SUPPLIERS;
    this.getbysupplier = this.apiendpoint + AppConstant.API_URL.DC.GETLISTBYID;
    this.createnewdc = this.apiendpoint + AppConstant.API_URL.DC.CREATEDC;

    this.getdclocations = this.apiendpoint + AppConstant.API_URL.DC.GETDCLOCATION;
    this.getstoresjobcart = this.apiendpoint + AppConstant.API_URL.DC.GETSTORESJOBCARTLIST;
    this.getjobcartitems = this.apiendpoint + AppConstant.API_URL.DC.GETSTORESJOBCARTITEMS;
    this.getjobcartitemslist = this.apiendpoint + AppConstant.API_URL.DC.GETSTORESJOBCARTITEMSLIST;
    this.getjobcartsubitems = this.apiendpoint + AppConstant.API_URL.DC.GETSTORESJOBCARTSUBITEMS;
    this.getjobcartsubitemslist = this.apiendpoint + AppConstant.API_URL.DC.GETSTORESJOBCARTSUBITEMSLIST;
    this.createjcdc = this.apiendpoint + AppConstant.API_URL.DC.CREATEJCDC;
    this.getrwItems = this.apiendpoint + AppConstant.API_URL.DC.GETREWRKJOBCARTITEMS;
    this.createsajobcart = this.apiendpoint + AppConstant.API_URL.DC.CREATESAJOBCART;
  }

  public GetDCSuppliers(unitid: any) {
    return this.http.get(this.getdcsuppliers + unitid)
      .map((response: Response) => response.json());
  }
  public GetDCBySupplier(id: any, type: any) {
    return this.http.get(this.getbysupplier + id + '/' + type)
      .map((response: Response) => response.json());
  }

  public GetDCList(unitid: any) {
    return this.http.get(this.getdclist + unitid)
      .map((response: Response) => response.json());
  }

  public GetDCListByID(id: any) {
    return this.http.get(this.getbyid + id)
      .map((response: Response) => response.json());
  }

  public CreateDC(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.createdc, body)
      .map((response: Response) => response.json());
  }

  public CreateNewDC(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.createnewdc, body)
      .map((response: Response) => response.json());
  }

  public GetDCLocations(dcid: any) {
    return this.http.get(this.getdclocations + dcid)
      .map((response: Response) => response.json());
  }

  public GetStoresJCList(unitid: any) {
    return this.http.get(this.getstoresjobcart + unitid)
      .map((response: Response) => response.json());
  }

  public GetStoresJCItems(flag: string, unitid: any) {
    if (flag == "Spare") {
      return this.http.get(this.getjobcartitems + unitid)
        .map((response: Response) => response.json());
    } else {
      return this.http.get(this.getjobcartsubitems + unitid)
        .map((response: Response) => response.json());
    }
  }

  public GetJobCartItemList(flag: string, itemid: any) {
    if (flag == "Spare") {
      return this.http.get(this.getjobcartitemslist + itemid)
        .map((response: Response) => response.json());
    } else {
      return this.http.get(this.getjobcartsubitemslist + itemid)
        .map((response: Response) => response.json());
    }
  }

  public GetRewrkJCItems(unitid: any) {
    return this.http.get(this.getrwItems + unitid)
      .map((response: Response) => response.json());
  }

  public CreateJCDC(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.createjcdc, body)
      .map((response: Response) => response.json());
  }

  public CreateSubAssJobcart(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.createsajobcart, body)
      .map((response: Response) => response.json());
  }

}
