import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class DepartmentsService {

  apiendpoint: string;
  getdpts: string;
  addnewdpt: string;
  deletedpt: string;
  updatedpt: string;
  getbystatus: string;

  constructor(
    private http: Http
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getdpts = this.apiendpoint + AppConstant.API_URL.DEPARTMENTS.GET;
    this.addnewdpt = this.apiendpoint + AppConstant.API_URL.DEPARTMENTS.CREATE;
    this.updatedpt = this.apiendpoint + AppConstant.API_URL.DEPARTMENTS.UPDATE;
    this.deletedpt = this.apiendpoint + AppConstant.API_URL.DEPARTMENTS.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.DEPARTMENTS.GETBYSTATUS;
  }

  public getallDepartments() {
    return this.http.get(this.getdpts)
      .map((response: Response) => response.json());
  }
  public addDepartment(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewdpt, body)
      .map((response: Response) => response.json());
  }

  public deleteDpt(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletedpt, options)
      .map((response: Response) => response.json());
  }

  public updateDepartment(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatedpt, body)
      .map((response: Response) => response.json());
  }

  public getByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

}
