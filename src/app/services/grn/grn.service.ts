import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class GrnService {
  apiendpoint: string;
  getpolist: string;
  getpodetails: string;
  creategrn: string;
  grnauthlist: string;
  authorizegrn: string;
  getgrnlocationlist: string;
  getdcdetails: string;
  createreqentry: string;
  getentryauthlist: string;
  entryauthorize: string;
  getallstockdisposal: string;
  creategrnlocation: string;
  getgrnlist: string;
  prodstocklist: string;
  createprodlocation: string;
  getgrnbyid: string

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getpolist = this.apiendpoint + AppConstant.API_URL.GRN.GETPOLIST;
    this.getpodetails = this.apiendpoint + AppConstant.API_URL.GRN.GETPODETAILSBYID;
    this.getdcdetails = this.apiendpoint + AppConstant.API_URL.GRN.GETDCDETAILSBYID;
    this.creategrn = this.apiendpoint + AppConstant.API_URL.GRN.CREATEGRN;
    this.grnauthlist = this.apiendpoint + AppConstant.API_URL.GRN.GETAUTHLIST;
    this.authorizegrn = this.apiendpoint + AppConstant.API_URL.GRN.GRNAUTHORIZE;
    this.getgrnlocationlist = this.apiendpoint + AppConstant.API_URL.GRN.GRNLOCATIONLIST;

    this.createreqentry = this.apiendpoint + AppConstant.API_URL.GRN.CREATEREQENTRY;
    this.getentryauthlist = this.apiendpoint + AppConstant.API_URL.GRN.ENTRYAUTHLIST;
    this.entryauthorize = this.apiendpoint + AppConstant.API_URL.GRN.ENTRYAUTHORIZE;
    this.getallstockdisposal = this.apiendpoint + AppConstant.API_URL.GRN.GETSTOCKDISPOSAL;

    this.creategrnlocation = this.apiendpoint + AppConstant.API_URL.GRN.CREATEGRNLOC;
    this.getgrnlist = this.apiendpoint + AppConstant.API_URL.GRN.GETLIST;
    this.getgrnbyid = this.apiendpoint + AppConstant.API_URL.GRN.GETBYID;
    this.prodstocklist = this.apiendpoint + AppConstant.API_URL.GRN.PRODUCTIONLOCLIST;
    this.createprodlocation = this.apiendpoint + AppConstant.API_URL.GRN.PRODUCTIONLOCSAVE;
  }

  public GetPOList(unitid: any, type: any, supplier: any) {
    return this.http.get(this.getpolist + unitid + '/' + type + '/' + supplier)
      .map((response: Response) => response.json());
  }

  public GetGRNBYID(id: any) {
    return this.http.get(this.getgrnbyid + id)
      .map((response: Response) => response.json());
  }

  public GetPoDetails(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.getpodetails, data)
      .map((response: Response) => response.json());
  }

  public GetDCDetails(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.getdcdetails, data)
      .map((response: Response) => response.json());
  }

  public CreateGRN(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.creategrn, body)
      .map((response: Response) => response.json());
  }

  public GetGRAuthList(unitid: any) {
    return this.http.get(this.grnauthlist + unitid)
      .map((response: Response) => response.json());
  }

  public AUthorizeGRN(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.authorizegrn, data)
      .map((response: Response) => response.json());
  }

  public GetGRNLocationList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('type', data.type);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    console.log("body", body);
    return this.http.put(this.getgrnlocationlist, body)
      .map((response: Response) => response.json());
  }

  public CreateReqEntry(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.createreqentry, data)
      .map((response: Response) => response.json());
  }


  public GetEntryAuthList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('status', data.status)
    console.log("body", body);
    return this.http.put(this.getentryauthlist, body)
      .map((response: Response) => response.json());
  }

  public EntryAuthorize(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.entryauthorize, data)
      .map((response: Response) => response.json());
  }

  public GetDisposalList(unitid: any) {
    return this.http.get(this.getallstockdisposal + unitid)
      .map((response: Response) => response.json());
  }

  public CreateGrnLocation(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.creategrnlocation, data)
      .map((response: Response) => response.json());
  }

  public GetGRNList(unitid: any) {
    return this.http.get(this.getgrnlist + unitid)
      .map((response: Response) => response.json());
  }

  public GetProdStocList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    return this.http.put(this.prodstocklist, body)
      .map((response: Response) => response.json());
  }

  public CreateProdLocation(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.createprodlocation, data)
      .map((response: Response) => response.json());
  }

}
