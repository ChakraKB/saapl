import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";


@Injectable()
export class HsnService {
  apiendpoint: string;
  gethsn: string;
  addnewhsn: string;
  deletehsn: string;
  updatehsn: string;
  getbystatus: string;
  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.gethsn = this.apiendpoint + AppConstant.API_URL.HSN.GET;
    this.addnewhsn = this.apiendpoint + AppConstant.API_URL.HSN.CREATE;
    this.updatehsn = this.apiendpoint + AppConstant.API_URL.HSN.UPDATE;
    this.deletehsn = this.apiendpoint + AppConstant.API_URL.HSN.DELETE;
  }

  public getallHsn() {
    return this.http.get(this.gethsn)
      .map((response: Response) => response.json());
  }
  public addNewHsn(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewhsn, body)
      .map((response: Response) => response.json());
  }

  public deleteHsn(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletehsn, options)
      .map((response: Response) => response.json());
  }

  public updateHsn(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatehsn, body)
      .map((response: Response) => response.json());
  }

}
