import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";


@Injectable()
export class InvoiceService {
  apiendpoint: string;

  getallproforma: string;
  createproforma: string;
  getallproformabyid: string;

  createpostinvoice: string;
  getpostinvoice: string;
  getpostinvoiceByid: string;
  getexportbyid: any;

  getare: string;
  addare: string;
  updateare: string
  vieware: string;

  viewslipaddress: string;
  getsalesdclist: string;
  createsalesdc: string;

  getexportpackinglistbyid:string

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getallproforma = this.apiendpoint + AppConstant.API_URL.PROFORMAINVOICE.GET;
    this.getallproformabyid = this.apiendpoint + AppConstant.API_URL.PROFORMAINVOICE.GETBYID;
    this.createproforma = this.apiendpoint + AppConstant.API_URL.PROFORMAINVOICE.CREATE;

    this.createpostinvoice = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.CREATE;
    this.getpostinvoice = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.GET;
    this.getpostinvoiceByid = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.GETBYID;
    this.getexportbyid = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.GETEXPORTINVBYID;
    this.getexportpackinglistbyid = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.GETEXPORTPACKINGLISTBYID;

    this.getare = this.apiendpoint + AppConstant.API_URL.ARE.GET;
    this.addare = this.apiendpoint + AppConstant.API_URL.ARE.CREATE;
    this.updateare = this.apiendpoint + AppConstant.API_URL.ARE.UPDATE;
    this.vieware = this.apiendpoint + AppConstant.API_URL.ARE.VIEWAREBYINVOICE;

    this.viewslipaddress = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.SLIPADDRESSVIEW;
    this.getsalesdclist = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.SALESDCLIST;
    this.createsalesdc = this.apiendpoint + AppConstant.API_URL.POSTINVOICE.SUBMITSALESDC
  }

  public getPostInvoiceBy(data: any) {
    return this.http.get(this.getpostinvoice + data)
      .map((response: Response) => response.json());
  }

  public getExportInvoiceByID(data: any) {
    return this.http.get(this.getexportbyid + data)
      .map((response: Response) => response.json());
  }

  public getPostInvoiceByID(data: any) {
    return this.http.get(this.getpostinvoiceByid + data)
      .map((response: Response) => response.json());
  }

  public GetExportPslipByID(data: any) {
    return this.http.get(this.getexportpackinglistbyid + data)
      .map((response: Response) => response.json());
  }


  public CreatePostInvoice(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createpostinvoice, body)
      .map((response: Response) => response.json());
  }

  public getAre() {
    return this.http.get(this.getare)
      .map((response: Response) => response.json());
  }

  public CreateAre(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addare, body)
      .map((response: Response) => response.json());
  }

  public updateAre(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateare, body)
      .map((response: Response) => response.json());
  }

  public getareformByInvoice(data: any) {
    return this.http.get(this.vieware + data)
      .map((response: Response) => response.json());
  }

  public getAllProformaInvoice(unitid: any) {
    return this.http.get(this.getallproforma + unitid)
      .map((response: Response) => response.json());
  }

  public getProformaInvoiceByID(data: any) {
    return this.http.get(this.getallproformabyid + data)
      .map((response: Response) => response.json());
  }

  public CreateProformaInvoice(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createproforma, body)
      .map((response: Response) => response.json());
  }

  public getSlipview(unitid: any, data: any) {
    return this.http.get(this.viewslipaddress + unitid + "/" + data)
      .map((response: Response) => response.json());
  }

  public GetSalesDCList(unit_id) {
    return this.http.get(this.getsalesdclist + unit_id)
      .map((response: Response) => response.json());
  }

  public CreateSalesDC(inv_no: any, id: any, pslip_id: any) {
    return this.http.get(this.createsalesdc + inv_no + "/" + id + "/" + pslip_id)
      .map((response: Response) => response.json());
  }

}
