import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class ItemsService {

  apiendpoint: string;
  getitem: string;
  addnewitem: string;
  deleteitem: string;
  updateitem: string;
  getbystatus: string;
  getitembyid: string;
  viewbyid: string;
  getitemmaterial: string;
  getitemmaterialbyid: string;
  getitemmaterialbystatus: string;
  addnewitemmaterial: string;
  updateitemmaterial: string;
  deleteitemmaterial: string;
  getitemprocess: any;
  getitemprocessbyid: any;
  getitemprocessbystatus: string;
  addnewitemprocess: any;
  updateitemprocess: any;
  deleteitemprocess: any;
  getparts: any;
  getitemsuppliers: string;
  addmultiitem: any;
  createmapping: string;
  deletemapping: string;
  getsellingcost: string;
  createsellingcost: string;
  getproductval: string;
  cancelstage: string;
  updatestage: string

  getseries: string;
  getseriesbyid: string;
  getseriesbystatus: string;
  createseries: string;
  updateseries: string;
  deleteseries: string

  getitemclass: string;
  getitemclassbyid: string;
  getitemclassbystatus: string;
  createitemclass: string;
  updateitemclass: string;
  deleteitemclass: string;


  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getitem = this.apiendpoint + AppConstant.API_URL.ITEM.GET;
    this.getitembyid = this.apiendpoint + AppConstant.API_URL.ITEM.GETBYID;
    this.addnewitem = this.apiendpoint + AppConstant.API_URL.ITEM.CREATE;
    this.updateitem = this.apiendpoint + AppConstant.API_URL.ITEM.UPDATE;
    this.deleteitem = this.apiendpoint + AppConstant.API_URL.ITEM.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.ITEM.GETBYSTATUS;
    this.viewbyid = this.apiendpoint + AppConstant.API_URL.ITEM.VIEW;
    this.getitem = this.apiendpoint + AppConstant.API_URL.ITEM.GET;
    this.addmultiitem = this.apiendpoint + AppConstant.API_URL.ITEM.CREATEMULTI;
    this.getparts = this.apiendpoint + AppConstant.API_URL.ITEM.GETPARTS;

    this.getitemmaterial = this.apiendpoint + AppConstant.API_URL.MATERIAL.GET;
    this.getitemmaterialbyid = this.apiendpoint + AppConstant.API_URL.MATERIAL.GETBYID;
    this.getitemmaterialbystatus = this.apiendpoint + AppConstant.API_URL.MATERIAL.GETBYSTATUS;
    this.addnewitemmaterial = this.apiendpoint + AppConstant.API_URL.MATERIAL.CREATE;
    this.updateitemmaterial = this.apiendpoint + AppConstant.API_URL.MATERIAL.UPDATE;
    this.deleteitemmaterial = this.apiendpoint + AppConstant.API_URL.MATERIAL.DELETE;

    this.getitemprocess = this.apiendpoint + AppConstant.API_URL.PROCESS.GET;
    this.getitemprocessbyid = this.apiendpoint + AppConstant.API_URL.PROCESS.GETBYID;
    this.getitemprocessbystatus = this.apiendpoint + AppConstant.API_URL.PROCESS.GETBYSTATUS;
    this.addnewitemprocess = this.apiendpoint + AppConstant.API_URL.PROCESS.CREATE;
    this.updateitemprocess = this.apiendpoint + AppConstant.API_URL.PROCESS.UPDATE;
    this.deleteitemprocess = this.apiendpoint + AppConstant.API_URL.PROCESS.DELETE;

    this.getitemsuppliers = this.apiendpoint + AppConstant.API_URL.ITEM.GETITEMSUPPLIER;
    this.createmapping = this.apiendpoint + AppConstant.API_URL.ITEM.CREATESUPPLIERMAPPING;
    this.deletemapping = this.apiendpoint + AppConstant.API_URL.ITEM.DELETESUPPLIERMAPPING;

    this.getsellingcost = this.apiendpoint + AppConstant.API_URL.ITEM.GETSELLINGCOST;
    this.createsellingcost = this.apiendpoint + AppConstant.API_URL.ITEM.CREATESELLINGCOST;
    this.getproductval = this.apiendpoint + AppConstant.API_URL.ITEM.GETPRODUCTVALUE;
    this.cancelstage = this.apiendpoint + AppConstant.API_URL.ITEM.CANCELSTAGE;
    this.updatestage = this.apiendpoint + AppConstant.API_URL.ITEM.UPDATESTAGE;

    this.getseries = this.apiendpoint + AppConstant.API_URL.SERIES.GET;
    this.getseriesbyid = this.apiendpoint + AppConstant.API_URL.SERIES.GETBYID;
    this.getseriesbystatus = this.apiendpoint + AppConstant.API_URL.SERIES.GETBYSTATUS;
    this.createseries = this.apiendpoint + AppConstant.API_URL.SERIES.CREATE;
    this.updateseries = this.apiendpoint + AppConstant.API_URL.SERIES.UPDATE;
    this.deleteseries = this.apiendpoint + AppConstant.API_URL.SERIES.DELETE

    this.getitemclass = this.apiendpoint + AppConstant.API_URL.ITEMCLASS.GET;
    this.getitemclassbyid = this.apiendpoint + AppConstant.API_URL.ITEMCLASS.GETBYID;
    this.getitemclassbystatus = this.apiendpoint + AppConstant.API_URL.ITEMCLASS.GETBYSTATUS;
    this.createitemclass = this.apiendpoint + AppConstant.API_URL.ITEMCLASS.CREATE;
    this.updateitemclass = this.apiendpoint + AppConstant.API_URL.ITEMCLASS.UPDATE;
    this.deleteitemclass = this.apiendpoint + AppConstant.API_URL.ITEMCLASS.DELETE
  }

  public getItem() {
    return this.http.get(this.getitem)
      .map((response: Response) => response.json());
  }

  public getParts() {
    return this.http.get(this.getparts)
      .map((response: Response) => response.json());
  }

  public getByID(data: any) {
    return this.http.get(this.getitembyid + data)
      .map((response: Response) => response.json());
  }

  public viewByID(data: any) {
    return this.http.get(this.viewbyid + data)
      .map((response: Response) => response.json());
  }

  public addItem(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewitem, body)
      .map((response: Response) => response.json());
  }

  public addMultipleItem(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addmultiitem, body)
      .map((response: Response) => response.json());
  }

  public deleteItem(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteitem, options)
      .map((response: Response) => response.json());
  }

  public updateItem(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateitem, body)
      .map((response: Response) => response.json());
  }

  public getItemByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

  // ================ MATERIAL =============================

  public getItemmat() {
    return this.http.get(this.getitemmaterial)
      .map((response: Response) => response.json());
  }

  public getmatByID(data: any) {
    return this.http.get(this.getitemmaterialbyid + data)
      .map((response: Response) => response.json());
  }

  public getmatByStatus(data: any) {
    return this.http.get(this.getitemmaterialbystatus + data)
      .map((response: Response) => response.json());
  }

  public addItemmat(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewitemmaterial, body)
      .map((response: Response) => response.json());
  }

  public deleteItemmat(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteitemmaterial, options)
      .map((response: Response) => response.json());
  }

  public updateItemmat(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateitemmaterial, body)
      .map((response: Response) => response.json());
  }

  // ========================== PROCESS ==========================

  public getItempro() {
    return this.http.get(this.getitemprocess)
      .map((response: Response) => response.json());
  }

  public getproByID(data: any) {
    return this.http.get(this.getitemprocessbyid + data)
      .map((response: Response) => response.json());
  }

  public getitemproByStatus(data: any) {
    return this.http.get(this.getitemprocessbystatus + data)
      .map((response: Response) => response.json());
  }
  public addItempro(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewitemprocess, body)
      .map((response: Response) => response.json());
  }

  public deleteItempro(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteitemprocess, options)
      .map((response: Response) => response.json());
  }

  public updateItempro(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateitemprocess, body)
      .map((response: Response) => response.json());
  }

  public getItemSuppliers(suppid: any, type: any, cont: any) {
    return this.http.get(this.getitemsuppliers + suppid + '/' + type + '/' + cont)
      .map((response: Response) => response.json());
  }

  public CreateMapping(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createmapping, body)
      .map((response: Response) => response.json());
  }

  public deleteMapping(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletemapping, options)
      .map((response: Response) => response.json());
  }

  public GetSellingCostList(cont: any) {
    return this.http.get(this.getsellingcost + cont)
      .map((response: Response) => response.json());
  }

  public CreateSellingCost(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createsellingcost, body)
      .map((response: Response) => response.json());
  }

  public getProductValue(id: any, type: any) {
    return this.http.get(this.getproductval + id + "/" + type)
      .map((response: Response) => response.json());
  }

  public CancelItemStage(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.cancelstage, body)
      .map((response: Response) => response.json());
  }

  public UpdateItemStage(data: any) {
    let body = new URLSearchParams();
    body.append('prev_stg_part_id', data.prev_stage_part_id);
    body.append('prev_stg_part_no', data.prev_stage_part_no);
    body.append('obj', JSON.stringify(data.items));
    return this.http.post(this.updatestage, body)
      .map((response: Response) => response.json());
  }

  // SERIES

  public GetAllSeries() {
    return this.http.get(this.getseries)
      .map((response: Response) => response.json());
  }

  public GetSeriesByID(data: any) {
    return this.http.get(this.getseriesbyid + data)
      .map((response: Response) => response.json());
  }

  public GetSeriesByStatus(data: any) {
    return this.http.get(this.getseriesbystatus + data)
      .map((response: Response) => response.json());
  }

  public addSeries(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createseries, body)
      .map((response: Response) => response.json());
  }
  public UpdateSeries(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateseries, body)
      .map((response: Response) => response.json());
  }

  public DeleteSeries(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteseries, options)
      .map((response: Response) => response.json());
  }

  // ITEMCLASS

  public GetAllItemClass() {
    return this.http.get(this.getitemclass)
      .map((response: Response) => response.json());
  }

  public GetItemClassByID(data: any) {
    return this.http.get(this.getitemclassbyid + data)
      .map((response: Response) => response.json());
  }

  public GetItemClassByStatus(data: any) {
    return this.http.get(this.getitemclassbystatus + data)
      .map((response: Response) => response.json());
  }

  public addItemClass(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createitemclass, body)
      .map((response: Response) => response.json());
  }
  public UpdateItemClass(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateitemclass, body)
      .map((response: Response) => response.json());
  }

  public DeleteItemClass(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteitemclass, options)
      .map((response: Response) => response.json());
  }


}
