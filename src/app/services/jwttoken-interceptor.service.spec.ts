import { TestBed, inject } from '@angular/core/testing';

import { JwttokenInterceptorService } from './jwttoken-interceptor.service';

describe('JwttokenInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JwttokenInterceptorService]
    });
  });

  it('should be created', inject([JwttokenInterceptorService], (service: JwttokenInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
