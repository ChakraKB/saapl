import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { AppConstant } from '../app.constant';

@Injectable()
export class JwttokenInterceptorService {

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).do(event => {
      // alert("JWT")
    },
      error => {
        // alert("JWT")
        if (error instanceof HttpErrorResponse && error.status >= 400) {
          // Handle Error
          console.error("Error Happend while communicating API");
          console.error(`Info: url : ${error.url} status: ${error.status}`);
        }
      });
  }
}
