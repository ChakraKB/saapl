import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";
import { promise } from 'selenium-webdriver';
@Injectable()
export class LocationService {

  apiendpoint: string;
  getloc: string;
  addnewloc: string;
  deleteloc: string;
  updateloc: string;
  getbystatus: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getloc = this.apiendpoint + AppConstant.API_URL.BIN_LOCATION.GET;
    this.addnewloc = this.apiendpoint + AppConstant.API_URL.BIN_LOCATION.CREATE;
    this.updateloc = this.apiendpoint + AppConstant.API_URL.BIN_LOCATION.UPDATE;
    this.deleteloc = this.apiendpoint + AppConstant.API_URL.BIN_LOCATION.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.BIN_LOCATION.GETBYSTATUS;
  }
  public getLocation() {
    return this.http.get(this.getloc)
      .map((response: Response) => response.json());
  }
  public addlocation(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewloc, body)
      .map((response: Response) => response.json());
  }

  public deletelocation(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteloc, options)
      .map((response: Response) => response.json());
  }

  public updatelocation(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateloc, body)
      .map((response: Response) => response.json());
  }

  public getLocationByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

}
