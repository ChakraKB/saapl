import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginService {

  apiendpoint: string;
  login: string
  companydetails: string
  constructor(private http: Http) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.login = this.apiendpoint + AppConstant.API_URL.LOGIN.CHECK_LOGIN;

    this.companydetails = this.apiendpoint + AppConstant.API_URL.COMPANY_DETAILS.GET;
  }

  checkLogin(data) {
    let body = new URLSearchParams();
    body.append("email", data.email);
    body.append("password", data.password);
    return this.http
      .post(this.login, body)
      .map((response: Response) => response.json())
      .catch((e: any) => {
        if (e.status === 500) {
          return Observable.throw(new Error(e.status));
        }
        else if (e.status === 404) {
          return Observable.throw(new Error(e.status));
        }
        else if (e.status === 400) {
          return Observable.throw(new Error(e.status));
        }
        else if (e.status === 409) {
          return Observable.throw(new Error(e.status));
        }
        else if (e.status === 406) {
          return Observable.throw(new Error(e.status));
        }
      });
  }

  public GetCompanyDetails(userid) {
    return this.http.get(this.companydetails + userid)
      .map((response: Response) => response.json());
  }
}
