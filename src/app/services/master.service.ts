import { Injectable } from '@angular/core';
import {  FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import * as _ from "underscore";

export interface IMessage {
  name?: string,
  email?: string,
  message?: string
}

@Injectable()
export class MasterService {
  public date = new Date();
  constructor(private http: Http) { }

  getFormErrorMessage(formGroupObj: FormGroup, errorObj: any) {
    for (let i in formGroupObj.controls) {
      var formControlObj = formGroupObj.controls[i];
      if (formControlObj instanceof FormControl) {
        if (formControlObj.errors) {
          return errorObj[i][Object.keys(formControlObj.errors)[0]];
        }
      }
    }
  }

  checkvalid(from, to) {
    var fromdate = new Date(from);
    var todate = new Date(to)
    fromdate.setHours(0, 0, 0, 0);
    todate.setHours(0, 0, 0, 0);
    if (!(fromdate <= todate)) {
      return false
    }
    else {
      return true
    }
  }

  public formatDataforDropdown(label, id, data, Placeholdervalue?) {
    let formatdata = [];
    let customdata = {
      label: null,
      value: null
    };
    if (!_.isEmpty(Placeholdervalue)) {
      formatdata.push({
        label: Placeholdervalue,
        value: null
      });
    }

    _.forEach(data, function (value) {
      var shallow = _.clone(customdata);
      shallow.label = value[label];
      shallow.value = value[id];
      shallow.others = value;
      formatdata.push(shallow);
    });
    return formatdata;
  }


  allowNumberOnly(event) {
    let e = event;
    var regexStr: any = '^[0-9]*$';
    if ([8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode == 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true)) {
      // let it happen, don't do anything
      return;
    }
    let ch = String.fromCharCode(e.keyCode);
    let regEx = new RegExp(regexStr);
    if (regEx.test(ch))
      return;
    else
      e.preventDefault();

  }

  allowNumberdecimalOnly(event) {
    let e = event;
    var regexStr: any = '^[0-9]*$';
    if ([8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode == 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true)) {
      // let it happen, don't do anything
      return;
    }
    var decimal_allow = false;
    if (e.keyCode == 46) // de
    {
      if (e.target.value.indexOf(".") == -1) {
        return;
      }
      else {
        e.preventDefault();
      }
    }
    let ch = String.fromCharCode(e.keyCode);
    let regEx = new RegExp(regexStr);
    if (regEx.test(ch))
      return;
    else
      e.preventDefault();
  }

  restricthyp($event) {
    if ($event.keyCode !== 8) {
      if ($event.key === '-') {
        $event.preventDefault();
      }
    }
  }

  setTwoNumberDecimal($event) {
    if ($event.keyCode !== 8) {
      if ($event.key === '-') {
        $event.preventDefault();
      }
      // let dotlength = $event.target.value.indexOf('.');
      if ($event.target.value.indexOf('.') !== -1) {
        // let length = $event.target.value.length;
        if ($event.key === '.') {
          $event.preventDefault();
        }
        if ((($event.target.value.indexOf('.')) + 3) === $event.target.value.length) {
          $event.preventDefault();
        }

      }
    }
  }

  DateDisablesSinceToday() {
    return {
      dateFormat: 'dd-mm-yyyy',
      firstDayOfWeek: 'su',
      appendSelectorToBody: true,
      disableSince: { year: this.date.getFullYear(), month: this.date.getMonth() + 1, day: this.date.getDate() + 1 }
    }
  }

}
