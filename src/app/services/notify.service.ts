import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class NotifyService {
  private loggedIn = new Subject<any>();
  constructor() {
  }

  getLoggedInUser(): Observable<any> {
    return this.loggedIn.asObservable();
  }
  
  showLoggedInUser(data: any) {
    this.loggedIn.next(data);
  }

}
