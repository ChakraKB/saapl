import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class PurchaseorderService {
  apiendpoint: string;
  getpo: string;
  getbyid: string;
  createpo: string;
  upadatepo: string;
  repeatpo: string;
  getpoauth: string;
  cancelpo: string;
  cancelpoqtn: string;
  getmdapplist: string;
  mdapproval: string;
  getorderconfirmlist: string;
  confirmorder: string;
  getcompare: string;
  directpo: string;
  getconfirmedbyid: string;
  getamendpolist: string;
  updatepoamend: string;
  shotclosure: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.getpo = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GET;
    this.getbyid = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GETBYID;
    this.getconfirmedbyid = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GETCONFIRMEDBYID;
    this.createpo = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.CREATE;
    this.upadatepo = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.UPDATE;
    this.repeatpo = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.REPEATPO;

    this.getpoauth = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GETPOAUTH;
    this.cancelpo = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.CANCELPO;
    this.cancelpoqtn = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.CANCELPOQTN;

    this.getmdapplist = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GETMDPOAPPROVAL;
    this.mdapproval = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.MDAPPROVE;
    this.getorderconfirmlist = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GETORDERCONFIRM;
    this.confirmorder = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.CONFIRMORDER;
    this.getcompare = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GETCOMPARE;

    this.directpo = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.DIRECTPO;
    this.getamendpolist = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.GETAMENDMENTPOLIST;
    this.updatepoamend = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.UPDATEPOAMEND;
    this.shotclosure = this.apiendpoint + AppConstant.API_URL.PURCHASEORDER.POSHOTCLOSURE
  }

  public GetPOList(unitid: any) {
    return this.http.get(this.getpo + unitid)
      .map((response: Response) => response.json());
  }

  public GetPOByID(id: any) {
    return this.http.get(this.getbyid + id)
      .map((response: Response) => response.json());
  }

  public GetPOConfirmedByID(id: any) {
    return this.http.get(this.getconfirmedbyid + id)
      .map((response: Response) => response.json());
  }

  public CreatePO(model: any, file: any) {
    let data = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        data.append("file", file[i]);
      }
    }
    data.append("obj", JSON.stringify(model));
    let body = new URLSearchParams();
    body.append("obj", JSON.stringify(model));
    return this.http.post(this.createpo, data)
      .map((response: Response) => response.json());
  }

  public UpdatePO(model: any, file: any) {
    let data = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        data.append("file", file[i]);
      }
    }
    data.append("obj", JSON.stringify(model));
    let body = new URLSearchParams();
    body.append("obj", JSON.stringify(model));
    return this.http.post(this.upadatepo, data)
      .map((response: Response) => response.json());
  }

  public RepeatPO(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.repeatpo, data)
      .map((response: Response) => response.json());
  }

  public GetPoAuthList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('type', data.type);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('status', data.status);
    console.log("body", body);
    return this.http.put(this.getpoauth, body)
      .map((response: Response) => response.json());
  }

  public CancelPO(id: any) {
    return this.http.get(this.cancelpo + id)
      .map((response: Response) => response.json());
  }

  public CancelPOQtn(id: any) {
    return this.http.get(this.cancelpoqtn + id)
      .map((response: Response) => response.json());
  }

  public GetMDAppList(unitid: any) {
    return this.http.get(this.getmdapplist + unitid)
      .map((response: Response) => response.json());
  }

  public UpdateMdApp(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.mdapproval, data)
      .map((response: Response) => response.json());
  }

  public GetOrderConfirmList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    console.log("body", body);
    return this.http.put(this.getorderconfirmlist, body)
      .map((response: Response) => response.json());
  }

  public GetCompareByENQ(enqid: any) {
    return this.http.get(this.getcompare + enqid)
      .map((response: Response) => response.json());
  }

  public ConfirmPoOrder(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.confirmorder, data)
      .map((response: Response) => response.json());
  }


  public DirectPO(model: any, unitid: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    data.append("unit_id", unitid)
    return this.http.post(this.directpo, data)
      .map((response: Response) => response.json());
  }

  public GetAmendPOList(unitid: any) {
    return this.http.get(this.getamendpolist + unitid)
      .map((response: Response) => response.json());
  }

  public UpdatePOAmend(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.updatepoamend, data)
      .map((response: Response) => response.json());
  }

  public POShotclosure(model: any) {
    let data = new URLSearchParams();
    data.append("obj", JSON.stringify(model));
    return this.http.post(this.shotclosure, data)
      .map((response: Response) => response.json());
  }

}
