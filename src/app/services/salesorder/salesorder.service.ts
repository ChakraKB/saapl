import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class SalesorderService {

  apiendpoint: string;
  getsalesorder: string;
  addnewsalesorder: string;
  deletesalesorder: string;
  updatesalesorder: string;
  getbystatus: string;
  getdetailsbyid: string
  prodconfirm: string;
  getsavedorders: string;
  getpdcorders: string;
  getauthorders: string;
  AUthorizeorders: string;
  getsalescontractlist: string;
  getcurrencies: string;
  getbymultiids: string;
  createstockorder: string;
  getstockorder: string;
  getstockbyid: string;
  getassemblyworkorders: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getsalesorder = this.apiendpoint + AppConstant.API_URL.SALESORDER.GET;
    this.getsavedorders = this.apiendpoint + AppConstant.API_URL.SALESORDER.GETSAVED;
    this.addnewsalesorder = this.apiendpoint + AppConstant.API_URL.SALESORDER.CREATE;
    this.getdetailsbyid = this.apiendpoint + AppConstant.API_URL.SALESORDER.GETBYID;
    this.getbymultiids = this.apiendpoint + AppConstant.API_URL.SALESORDER.GETBYIDS;
    this.updatesalesorder = this.apiendpoint + AppConstant.API_URL.SALESORDER.UPDATE;
    this.getpdcorders = this.apiendpoint + AppConstant.API_URL.SALESORDER.GETPDCONFIRM;
    this.prodconfirm = this.apiendpoint + AppConstant.API_URL.SALESORDER.PROCONFIRM;

    this.getauthorders = this.apiendpoint + AppConstant.API_URL.SALESORDER.GETAUTHLIST;
    this.AUthorizeorders = this.apiendpoint + AppConstant.API_URL.SALESORDER.AUTHORIZE;

    this.getsalescontractlist = this.apiendpoint + AppConstant.API_URL.SALESORDER.GETSALESCONTRACTIST;
    this.getcurrencies = this.apiendpoint + AppConstant.API_URL.CURRENCY.GET;

    this.createstockorder = this.apiendpoint + AppConstant.API_URL.STOCKORDER.CREATE;
    this.getstockorder = this.apiendpoint + AppConstant.API_URL.STOCKORDER.GET;
    this.getstockbyid = this.apiendpoint + AppConstant.API_URL.STOCKORDER.GETBYID;

    this.getassemblyworkorders = this.apiendpoint + AppConstant.API_URL.ASSEMBLY.GETWO;
  }

  public getAllOrders(unitid) {
    return this.http.get(this.getsalesorder + unitid)
      .map((response: Response) => response.json());
  }

  public getAllSavedOrder(id, unitid) {
    return this.http.get(this.getsavedorders + unitid + '/' + id)
      .map((response: Response) => response.json());
  }

  public getAllPdconfirmedOrder(unitid) {
    return this.http.get(this.getpdcorders + unitid)
      .map((response: Response) => response.json());
  }

  public getAllauthorizedOrder(unitid) {
    return this.http.get(this.getauthorders + unitid)
      .map((response: Response) => response.json());
  }

  public getAllsalescontract(unitid) {
    return this.http.get(this.getsalescontractlist + unitid)
      .map((response: Response) => response.json());
  }

  public getByID(data: any) {
    return this.http.get(this.getdetailsbyid + data)
      .map((response: Response) => response.json());
  }

  public getByMultipleID(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.getbymultiids, body)
      .map((response: Response) => response.json());
  }

  public AddNewOrder(model: any, file: any) {
    let data = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        data.append("file", file[i]);
      }
    }
    data.append("obj", JSON.stringify(model));
    let body = new URLSearchParams();
    body.append("obj", JSON.stringify(model));
    return this.http.post(this.addnewsalesorder, data)
      .map((response: Response) => response.json());
  }

  public UpdateOrder(model: any, file: any) {
    let data = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        data.append("file", file[i]);
      }
    }
    data.append("obj", JSON.stringify(model));
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(model));
    return this.http.post(this.updatesalesorder, data)
      .map((response: Response) => response.json());
  }

  public ProConfirm(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.prodconfirm, body)
      .map((response: Response) => response.json());
  }

  public AuthorizeOrders(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.AUthorizeorders, body)
      .map((response: Response) => response.json());
  }

  public getAllCurrencies() {
    return this.http.get(this.getcurrencies)
      .map((response: Response) => response.json());
  }

  public getAllStockOrders(unitid) {
    return this.http.get(this.getstockorder + unitid)
      .map((response: Response) => response.json());
  }

  public getStockOrderBYid(id) {
    return this.http.get(this.getstockbyid + id)
      .map((response: Response) => response.json());
  }

  public CreateStockOrder(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.createstockorder, body)
      .map((response: Response) => response.json());
  }

  public getAssOrders(unitid) {
    return this.http.get(this.getassemblyworkorders + unitid)
      .map((response: Response) => response.json());
  }

}
