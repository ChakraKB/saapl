import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class StageService {

  apiendpoint: string;
  getstage: string;
  addnewstage: string;
  deletestage: string;
  updatestage: string;
  getbystatus: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getstage = this.apiendpoint + AppConstant.API_URL.STAGE.GET;
    this.addnewstage = this.apiendpoint + AppConstant.API_URL.STAGE.CREATE;
    this.updatestage = this.apiendpoint + AppConstant.API_URL.STAGE.UPDATE;
    this.deletestage = this.apiendpoint + AppConstant.API_URL.STAGE.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.STAGE.GETBYSTATUS;
  }

  public getAllStage() {
    return this.http.get(this.getstage)
      .map((response: Response) => response.json());
  }
  public addStage(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewstage, body)
      .map((response: Response) => response.json());
  }

  public deleteStage(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletestage, options)
      .map((response: Response) => response.json());
  }

  public updateStage(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatestage, body)
      .map((response: Response) => response.json());
  }

  public getstageByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

}


