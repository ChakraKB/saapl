import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class StoresService {
  apiendpoint: string;
  createslip: string;
  createstockentry: string;
  getstockquantity: string;
  createstockqty: string;
  getstockbyid: string;
  getslip: string;
  slipdetails: string;
  getslipbyorderid: string;
  getslipbycustmerid: string;
  getslipbyid: string;
  getissuelist: string;
  getjobcartlist: string;
  requestjobcart: string;
  issuentry: string;
  rejinvlist: string;
  getissuedlist: string
  getissuelocations: string;
  getissuereturnbyitem: string;
  createrejtodc: string;
  importexcel: string;
  partstock: string;
  sliplocation: string;
  getstockbysc: string;
  getconsumables: string

  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.createstockentry = this.apiendpoint + AppConstant.API_URL.STOCKENTRY.CREATE;
    this.getstockbyid = this.apiendpoint + AppConstant.API_URL.STOCKENTRY.GETSTOCKBYID;

    this.getstockquantity = this.apiendpoint + AppConstant.API_URL.INVENTORY.GET;
    this.createstockqty = this.apiendpoint + AppConstant.API_URL.INVENTORY.CREATE;

    this.getslip = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.GET;
    this.getslipbyorderid = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.GETBYORDERID;
    this.getslipbycustmerid = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.GETBYCUSTOMERID + this.unitid;
    this.createslip = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.CREATE;
    this.slipdetails = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.DETAIL;
    this.getslipbyid = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.GETBYID;
    this.sliplocation = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.SLIPLOCATION;

    this.getissuelist = this.apiendpoint + AppConstant.API_URL.ISSUE.GET;
    this.getjobcartlist = this.apiendpoint + AppConstant.API_URL.ISSUE.JOBCARTGETALL;
    this.requestjobcart = this.apiendpoint + AppConstant.API_URL.ISSUE.JOBCARTREQUEST;
    this.issuentry = this.apiendpoint + AppConstant.API_URL.ISSUE.ISSUEENTRY;
    this.getissuereturnbyitem = this.apiendpoint + AppConstant.API_URL.ISSUE.GETISSUERETURN;
    this.getissuedlist = this.apiendpoint + AppConstant.API_URL.ISSUE.GETISSUELIST;
    this.getissuelocations = this.apiendpoint + AppConstant.API_URL.ISSUE.GETISSUELOCATION;

    this.rejinvlist = this.apiendpoint + AppConstant.API_URL.GRN.REJINVOICELIST;
    this.createrejtodc = this.apiendpoint + AppConstant.API_URL.GRN.CREATEREJECTTODC;
    this.importexcel = this.apiendpoint + AppConstant.API_URL.INVENTORY.IMPORTFILE;
    this.partstock = this.apiendpoint + AppConstant.API_URL.STOCKENTRY.GETPARTSTOCK;

    this.getstockbysc = this.apiendpoint + AppConstant.API_URL.STOCKENTRY.GETPRODUCTIONLISTBYSC;
    this.getconsumables = this.apiendpoint + AppConstant.API_URL.PACKINGSLIP.GETCONSUMABLES;


  }

  public CreateStockEntry(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createstockentry, body)
      .map((response: Response) => response.json());
  }

  public getSTockByID(data: any) {
    return this.http.get(this.getstockbyid + data)
      .map((response: Response) => response.json());
  }

  public getSTockQuantity(data: any) {
    return this.http.get(this.getstockquantity + data)
      .map((response: Response) => response.json());
  }

  public CreateStockQuantity(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createstockqty, body)
      .map((response: Response) => response.json());
  }

  public getpackingSLipbyOrder(orderid, unitid) {
    return this.http.get(this.getslipbyorderid + unitid + '/' + orderid)
      .map((response: Response) => response.json());
  }

  public getpackingSLipbyCustomer(cusid) {
    return this.http.get(this.getslipbycustmerid + '/' + cusid)
      .map((response: Response) => response.json());
  }

  public getpackingSLipbyId(id) {
    return this.http.get(this.getslipbyid + id)
      .map((response: Response) => response.json());
  }

  public CreatePkngslip(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createslip, body)
      .map((response: Response) => response.json());
  }

  public getpackingSLipdetails(slipid) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(slipid));
    return this.http.post(this.slipdetails, body)
      .map((response: Response) => response.json());
  }

  public GetIssueList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    return this.http.put(this.getissuelist, body)
      .map((response: Response) => response.json());
  }

  public GetJobcartList(data: any) {
    return this.http.get(this.getjobcartlist + data)
      .map((response: Response) => response.json());
  }

  public RequestJobCart(slipid) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(slipid));
    return this.http.post(this.requestjobcart, body)
      .map((response: Response) => response.json());
  }

  public IssueEntry(slipid) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(slipid));
    return this.http.post(this.issuentry, body)
      .map((response: Response) => response.json());
  }

  public GetRejectedInvList(data: any) {
    return this.http.get(this.rejinvlist + data)
      .map((response: Response) => response.json());
  }

  public GetIssueReturnList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    body.append('dept', data.department_name);
    body.append('item_id', data.part_no);
    return this.http.put(this.getissuereturnbyitem, body)
      .map((response: Response) => response.json());
  }

  public getIssuedList(data: any) {
    let body = new URLSearchParams();
    body.append('unit_id', data.unit_id);
    body.append('from_date', data.from_date);
    body.append('to_date', data.to_date);
    return this.http.post(this.getissuedlist, body)
      .map((response: Response) => response.json());
  }

  public getIssuedLocation(id: any) {
    return this.http.get(this.getissuelocations + id)
      .map((response: Response) => response.json());
  }

  public CreateRejectedDC(slipid) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(slipid));
    return this.http.post(this.createrejtodc, body)
      .map((response: Response) => response.json());
  }


  public Importexcel(file: any, unitid: any) {
    let data = new FormData();
    if (file) {
      for (var i = 0; i < file.length; i++) {
        data.append("file", file[i]);
      }
    }
    return this.http.post(this.importexcel + unitid, data)
      .map((response: Response) => response.json());
  }

  public getSTockByPart(part_id: any, salesorderid: any) {
    return this.http.get(this.partstock + part_id + "/" + salesorderid)
      .map((response: Response) => response.json());
  }

  public GetSlipLocation(id: any) {
    return this.http.get(this.sliplocation + id)
      .map((response: Response) => response.json());
  }

  public getSTockListByDC(id: any) {
    return this.http.get(this.getstockbysc + id)
      .map((response: Response) => response.json());
  }

  public GetConsumables() {
    return this.http.get(this.getconsumables)
      .map((response: Response) => response.json());
  }

}
