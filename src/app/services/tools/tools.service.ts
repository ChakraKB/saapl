import { Injectable } from '@angular/core';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";
import { AppConstant } from '../../app.constant';


@Injectable()
export class ToolsService {
  apiendpoint: string;
  gettools: string;
  addnewtool: string;
  updatetool: string;
  deletetool: string;
  getbystatus: string;
  createdctools: string;
  gettoolissuelist: string;
  getissuebyid: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;

    this.gettools = this.apiendpoint + AppConstant.API_URL.TOOL.GET;
    this.addnewtool = this.apiendpoint + AppConstant.API_URL.TOOL.CREATE;
    this.updatetool = this.apiendpoint + AppConstant.API_URL.TOOL.UPDATE;
    this.deletetool = this.apiendpoint + AppConstant.API_URL.TOOL.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.TOOL.GETBYSTATUS;

    this.gettoolissuelist = this.apiendpoint + AppConstant.API_URL.DCTOOLS.GET;
    this.getissuebyid = this.apiendpoint + AppConstant.API_URL.DCTOOLS.GETBYID;
    this.createdctools = this.apiendpoint + AppConstant.API_URL.DCTOOLS.CREATEISSUE;
  }

  public getAllTools() {
    return this.http.get(this.gettools)
      .map((response: Response) => response.json());
  }

  public addTool(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    console.log("body", body);
    return this.http.put(this.addnewtool, body)
      .map((response: Response) => response.json());
  }

  public deleteTool(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletetool, options)
      .map((response: Response) => response.json());
  }

  public updateTool(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatetool, body)
      .map((response: Response) => response.json());
  }

  public getByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

  public getToolsIssued() {
    return this.http.get(this.gettoolissuelist)
      .map((response: Response) => response.json());
  }

  public getToolsIssuedByID(id: any) {
    return this.http.get(this.getissuebyid + id)
      .map((response: Response) => response.json());
  }

  public CreateDCTool(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    console.log("body", body);
    return this.http.put(this.createdctools, body)
      .map((response: Response) => response.json());
  }

}
