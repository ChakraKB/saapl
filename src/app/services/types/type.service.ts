import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class TypeService {

  apiendpoint: string;
  gettype: string;
  addnewtype: string;
  deletetype: string;
  updatetype: string;
  getbystatus: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.gettype = this.apiendpoint + AppConstant.API_URL.TYPES.GET;
    this.addnewtype = this.apiendpoint + AppConstant.API_URL.TYPES.CREATE;
    this.updatetype = this.apiendpoint + AppConstant.API_URL.TYPES.UPDATE;
    this.deletetype = this.apiendpoint + AppConstant.API_URL.TYPES.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.TYPES.GETBYSTATUS;
  }

  public getAllType() {
    return this.http.get(this.gettype)
      .map((response: Response) => response.json());
  }
  public addType(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewtype, body)
      .map((response: Response) => response.json());
  }

  public deleteType(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletetype, options)
      .map((response: Response) => response.json());
  }

  public updateType(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatetype, body)
      .map((response: Response) => response.json());
  }

  public getTypeByStatus(data: any) {
    return this.http.get(this.getbystatus + (data))
      .map((response: Response) => response.json());
  }

}
