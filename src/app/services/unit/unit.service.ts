import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class UnitService {

  apiendpoint: string;
  getunit: string;
  addnewunit: string;
  deleteunit: string;
  updateunit: string;
  getbystatus: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getunit = this.apiendpoint + AppConstant.API_URL.UNITS.GET;
    this.addnewunit = this.apiendpoint + AppConstant.API_URL.UNITS.CREATE;
    this.updateunit = this.apiendpoint + AppConstant.API_URL.UNITS.UPDATE;
    this.deleteunit = this.apiendpoint + AppConstant.API_URL.UNITS.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.UNITS.GETBYSTATUS;
  }

  public getallUnit() {
    return this.http.get(this.getunit)
      .map((response: Response) => response.json());
  }
  public addUnit(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewunit, body)
      .map((response: Response) => response.json());
  }

  public deleteUnit(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteunit, options)
      .map((response: Response) => response.json());
  }

  public updateUnit(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateunit, body)
      .map((response: Response) => response.json());
  }

  public getUnitByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

}
