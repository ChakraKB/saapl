import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class UomService {

  apiendpoint: string;
  getuoms: string;
  addnewuom: string;
  deleteuom: string;
  updateuom: string;
  getbystatus: string

  constructor(private http: Http) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getuoms = this.apiendpoint + AppConstant.API_URL.UOM.GET;
    this.addnewuom = this.apiendpoint + AppConstant.API_URL.UOM.CREATE;
    this.updateuom = this.apiendpoint + AppConstant.API_URL.UOM.UPDATE;
    this.deleteuom = this.apiendpoint + AppConstant.API_URL.UOM.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.UOM.GETBYSTATUS;
  }

  public getAllUom() {
    return this.http.get(this.getuoms)
      .map((response: Response) => response.json());
  }
  public addUom(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewuom, body)
      .map((response: Response) => response.json());
  }

  public deleteUom(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteuom, options)
      .map((response: Response) => response.json());
  }

  public updateUom(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateuom, body)
      .map((response: Response) => response.json());
  }

  public getUomByStatus(data: any) {
    return this.http.get(this.getbystatus + (data))
      .map((response: Response) => response.json());
  }

}
