import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class UserService {

  apiendpoint: string;
  getuser: string;
  getuserbyid: string;
  addnewuser: string;
  deleteuser: string;
  updateuser: string;
  getbystatus: string;
  getcontacts: string;
  getpaymentterms: string;
  getports: string;
  leftmenu: string;
  createleftmenu: string;
  getleftmenu: string;
  createpaymentterms: string;
  updatepaymentterms: string;
  deletepaymentterms: string;
  createport: string;
  updateport: string;
  deleteport: string;
  getdelterms: string;
  createdelterms: string;
  updatedelterms: string;
  deletedelterms: string;
  createcontacts: string;
  updatecontacts: string;
  deletecontacts: string;
  getcontactsByID: string;
  getcontactsbycat: string;
  getportbystatus: string;
  updatemailid: string;
  updatepassword: string;
  forgetpassword: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.leftmenu = this.apiendpoint + AppConstant.API_URL.LEFTMENU.MENU;
    this.createleftmenu = this.apiendpoint + AppConstant.API_URL.LEFTMENU.CREATE;
    this.getleftmenu = this.apiendpoint + AppConstant.API_URL.LEFTMENU.LEFTMENUPAGES;

    this.getuser = this.apiendpoint + AppConstant.API_URL.USERS.GET;
    this.getuserbyid = this.apiendpoint + AppConstant.API_URL.USERS.USERBYID;
    this.addnewuser = this.apiendpoint + AppConstant.API_URL.USERS.CREATE;
    this.updateuser = this.apiendpoint + AppConstant.API_URL.USERS.UPDATE;
    this.deleteuser = this.apiendpoint + AppConstant.API_URL.USERS.DELETE;

    this.getpaymentterms = this.apiendpoint + AppConstant.API_URL.PAYMENTTERMS.GET;
    this.createpaymentterms = this.apiendpoint + AppConstant.API_URL.PAYMENTTERMS.CREATE;
    this.updatepaymentterms = this.apiendpoint + AppConstant.API_URL.PAYMENTTERMS.UPDATE;
    this.deletepaymentterms = this.apiendpoint + AppConstant.API_URL.PAYMENTTERMS.DELETE;

    this.getports = this.apiendpoint + AppConstant.API_URL.PORT.GET;
    this.createport = this.apiendpoint + AppConstant.API_URL.PORT.CREATE;
    this.updateport = this.apiendpoint + AppConstant.API_URL.PORT.UPDATE;
    this.deleteport = this.apiendpoint + AppConstant.API_URL.PORT.DELETE;
    this.getportbystatus = this.apiendpoint + AppConstant.API_URL.PORT.GETBYSTATUS;

    this.getdelterms = this.apiendpoint + AppConstant.API_URL.DELIVERYTERMS.GET;
    this.createdelterms = this.apiendpoint + AppConstant.API_URL.DELIVERYTERMS.CREATE;
    this.updatedelterms = this.apiendpoint + AppConstant.API_URL.DELIVERYTERMS.UPDATE;
    this.deletedelterms = this.apiendpoint + AppConstant.API_URL.DELIVERYTERMS.DELETE;

    this.getcontacts = this.apiendpoint + AppConstant.API_URL.CONTACT.GET;
    this.getcontactsbycat = this.apiendpoint + AppConstant.API_URL.CONTACT.GETBYCAT;
    this.getcontactsByID = this.apiendpoint + AppConstant.API_URL.CONTACT.GETBYID;
    this.createcontacts = this.apiendpoint + AppConstant.API_URL.CONTACT.CREATE;
    this.updatecontacts = this.apiendpoint + AppConstant.API_URL.CONTACT.UPDATE;
    this.deletecontacts = this.apiendpoint + AppConstant.API_URL.CONTACT.DELETE;

    this.updatemailid = this.apiendpoint + AppConstant.API_URL.USERS.UPDATEUSER;
    this.updatepassword = this.apiendpoint + AppConstant.API_URL.USERS.CHANGEPWD;
    this.forgetpassword = this.apiendpoint + AppConstant.API_URL.FORGETPASSWORD.FORGET;

  }

  public getMenus() {
    return this.http.get(this.leftmenu)
      .map((response: Response) => response.json());
  }

  public getUserDetails(id) {
    return this.http.get(this.getuserbyid + id)
      .map((response: Response) => response.json());
  }

  public getLeftmenuByID(data: any) {
    return this.http.get(this.getleftmenu + data)
      .map((response: Response) => response.json());
  }

  public createAccess(data: any, roleid: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    body.append('role_id', roleid)
    return this.http.put(this.createleftmenu, body)
      .map((response: Response) => response.json());
  }

  public getallUser() {
    return this.http.get(this.getuser)
      .map((response: Response) => response.json());
  }

  public addUser(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewuser, body)
      .map((response: Response) => response.json());
  }

  public deleteUser(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteuser, options)
      .map((response: Response) => response.json());
  }

  public updateUser(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateuser, body)
      .map((response: Response) => response.json());
  }

  public UpdateUser(data: any) {
    let body = new URLSearchParams();
    body.append('email_id', data.emailid);
    body.append('mob_no', data.mobileno);
    body.append('userid', data.userid);
    return this.http.post(this.updatemailid, body)
      .map((response: Response) => response.json());
  }

  public changepwd(data: any) {
    let body = new URLSearchParams();
    body.append('oldpassword', data.oldpassword);
    body.append('newpassword', data.newpassword);
    body.append('userid', data.userid);
    return this.http.post(this.updatepassword, body)
      .map((response: Response) => response.json());
  }

  public getallpayterms() {
    return this.http.get(this.getpaymentterms)
      .map((response: Response) => response.json());
  }

  public addPaymentterms(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createpaymentterms, body)
      .map((response: Response) => response.json());
  }

  public deletePaymentterms(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletepaymentterms, options)
      .map((response: Response) => response.json());
  }

  public updatePaymentterms(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatepaymentterms, body)
      .map((response: Response) => response.json());
  }

  public getAllPorts() {
    return this.http.get(this.getports)
      .map((response: Response) => response.json());
  }

  public getPortsByStatus(data: any) {
    return this.http.get(this.getportbystatus + data)
      .map((response: Response) => response.json());
  }

  public addports(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createport, body)
      .map((response: Response) => response.json());
  }

  public deleteports(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deleteport, options)
      .map((response: Response) => response.json());
  }

  public updateports(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updateport, body)
      .map((response: Response) => response.json());
  }


  public getAllDeliveryTerms() {
    return this.http.get(this.getdelterms)
      .map((response: Response) => response.json());
  }

  public addDeliveryTerms(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createdelterms, body)
      .map((response: Response) => response.json());
  }

  public deleteDeliveryTerms(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletedelterms, options)
      .map((response: Response) => response.json());
  }

  public updateDeliveryTerms(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatedelterms, body)
      .map((response: Response) => response.json());
  }


  public getallContacts() {
    return this.http.get(this.getcontacts)
      .map((response: Response) => response.json());
  }

  public getContactsByCat(data: any) {
    return this.http.get(this.getcontactsbycat + data)
      .map((response: Response) => response.json());
  }

  public getContactsByID(data: any) {
    return this.http.get(this.getcontactsByID + data)
      .map((response: Response) => response.json());
  }

  public addContacts(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.createcontacts, body)
      .map((response: Response) => response.json());
  }

  public deleteContacts(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletecontacts, options)
      .map((response: Response) => response.json());
  }

  public updateContacts(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatecontacts, body)
      .map((response: Response) => response.json());
  }

  public ForgetPassword(data: any) {
    let body = new URLSearchParams();
    body.append('email', data);
    return this.http.post(this.forgetpassword, body)
      .map((response: Response) => response.json());
  }
}
