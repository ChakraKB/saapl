import { Injectable } from '@angular/core';
import { AppConstant } from '../../app.constant';
import {
  Http,
  RequestMethod,
  Response,
  RequestOptions,
  URLSearchParams,
  Headers
} from "@angular/http";

@Injectable()
export class ValveService {

  apiendpoint: string;
  getvalve: string;
  addnewvalve: string;
  deletevalve: string;
  updatevalve: string;
  getbystatus: string;

  constructor(
    private http: Http,
  ) {
    this.apiendpoint = AppConstant.API_ENDPOINT;
    this.getvalve = this.apiendpoint + AppConstant.API_URL.VALVE.GET;
    this.addnewvalve = this.apiendpoint + AppConstant.API_URL.VALVE.CREATE;
    this.updatevalve = this.apiendpoint + AppConstant.API_URL.VALVE.UPDATE;
    this.deletevalve = this.apiendpoint + AppConstant.API_URL.VALVE.DELETE;
    this.getbystatus = this.apiendpoint + AppConstant.API_URL.VALVE.GETBYSTATUS;
  }

  public getallValve() {
    return this.http.get(this.getvalve)
      .map((response: Response) => response.json());
  }
  public addValve(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.put(this.addnewvalve, body)
      .map((response: Response) => response.json());
  }

  public deleteValve(data: any) {
    let params = new URLSearchParams();
    params.append("id", data);
    let options = new RequestOptions({
      method: RequestMethod.Delete,
      body: params
    });
    return this.http.delete(this.deletevalve, options)
      .map((response: Response) => response.json());
  }

  public updateValve(data: any) {
    let body = new URLSearchParams();
    body.append('obj', JSON.stringify(data));
    return this.http.post(this.updatevalve, body)
      .map((response: Response) => response.json());
  }

  public getValveByStatus(data: any) {
    return this.http.get(this.getbystatus + data)
      .map((response: Response) => response.json());
  }

}