import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { DcService } from '../../services/dc/dc.service';
import { CompleterService } from 'ng2-completer';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
class Person {
  checkauthorize: boolean = false;
}
@Component({
  selector: 'app-create-dc',
  templateUrl: './create-dc.component.html',
  styleUrls: ['./create-dc.component.css']
})
export class CreateDcComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  dcform: FormGroup;
  formdata: any;
  tabletimeout: any;
  checkeditems: any[];
  orderdetails: any;
  dc: any = {};
  show_loader: Boolean = false;
  alldcsuppliers: any;
  dcsuppliers: any;
  dctype: any;
  dcid: any;
  orderForm: FormGroup;
  showDialog: boolean = false;
  loclist: any;
  index: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private dcservice: DcService,
    private http: Http,
    private router: Router,
    private completerService: CompleterService,
    private acroute: ActivatedRoute,
  ) {
    this.dctype = AppConstant.APP_CONST.DCType;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
    this.dcid = this.acroute.snapshot.queryParamMap.get('id');

    this.orderForm = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.checkeditems = [];
    this.loadtable();
    this.getdcsuppliers();
    this.dc.type = "Returnable"
    if (this.dcid) {
      this.selectsupplier({ "id": atob(this.dcid) })
      setTimeout(() => {
        var dcid = atob(this.dcid)
        var dcsupplier = _.findWhere(this.dcsuppliers, { "id": +dcid })
        this.dc.supplier = dcsupplier.supplier_name
      }, 100);
    }
  }

  loadtable() {
    this.persons = [];
    this.dtTrigger.next();
  }

  getdcsuppliers() {
    this.show_loader = true;
    this.dcservice.GetDCSuppliers(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.dcsuppliers = res.data;
        this.alldcsuppliers = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
      }
    })
  }

  selectsupplier(data) {
    this.checkeditems = []
    // if (data.originalObject) {
    //   var supp = data.originalObject
    // } else { var supp = data }
    if (!_.isEmpty(data)) {
      var supp = data
    } else {
      supp = _.findWhere(this.dcsuppliers, { "supplier_name": this.dc.supplier })
    }
    this.show_loader = true;
    this.dcservice.GetDCBySupplier(supp.id, this.dc.type).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.persons = res.data
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  showpop(i) {
    this.index = i
    this.loclist = this.persons[i].assembly_child;
    this.showDialog = true
  }

  close(i, flag) {
    this.showDialog = false;
  }

  // calcreqqty(item, i, index) {
  //   if (item.alloc_qty > this.persons[index].qty || item.alloc_qty > item.qty) {
  //     item.alloc_qty = 0
  //   } else {
  //     let self = this;
  //     var totreq = 0
  //     _.findWhere(this.loclist, function (val) {
  //       totreq += (+val.alloc_qty)
  //     })
  //     if (totreq > this.persons[index].qty) {
  //       item.alloc_qty = 0
  //     }
  //   }
  // }

  uncheckall() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  createdc() {
    var validdata: boolean = true;
    var validationmsg = [];

    if (!this.dc.supplier) {
      validdata = false;
      validationmsg.push("Please select supplier")
    } else if (!this.dc.dcdate) {
      validdata = false;
      validationmsg.push("Please select DC Date")
    } else if (!this.dc.vehicle_no) {
      validdata = false;
      validationmsg.push("Please enter vehicle no")
    } else if (!this.checkeditems.length) {
      validdata = false;
      validationmsg.push("Please select items")
    }
    console.log("checkeditems", this.checkeditems)

    if (validdata) {

      _.forEach(this.checkeditems, function (val) {
        // if (val.res_view) {
        //   var totreq = 0
        //   _.forEach(val.res_view, function (val) {
        //     totreq += (+val.alloc_qty)
        //   })
        //   if (totreq == 0 || totreq != val.qty) {
        //     validdata = false;
        //     validationmsg.push("Please select bin details for : " + val.item_no)
        //   }
        // }
        var prtcpy = val.item_id;
        var nxtprtcpy = val.next_stage_part_id;
        val.item_id = { 'id': prtcpy }
        val.next_stage_part_id = { 'id': nxtprtcpy }
        return val
      })
      var formdata =
        {
          "unit_id": this.unitid,
          "supplier": this.checkeditems[0].supplier,
          "supplier_id": { 'id': this.checkeditems[0].supplier_id },
          "vehicle_no": this.dc.vehicle_no,
          "dc_type": this.dc.type,
          "dc_date": this.dc.dcdate.formatted,
          "dcitems": this.checkeditems
        }
      console.log("formdata", formdata);
      this.show_loader = true;
      this.dcservice.CreateNewDC(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.router.navigate(['/deliverychallan'])
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }
}
