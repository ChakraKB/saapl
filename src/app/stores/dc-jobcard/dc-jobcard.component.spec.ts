import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcJobcardComponent } from './dc-jobcard.component';

describe('DcJobcardComponent', () => {
  let component: DcJobcardComponent;
  let fixture: ComponentFixture<DcJobcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcJobcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcJobcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
