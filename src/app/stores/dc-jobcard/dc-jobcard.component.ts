import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
declare let swal: any;
import { CompleterService } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { DcService } from '../../services/dc/dc.service';
import { PlanningService } from '../../services/Planning/planning.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-dc-jobcard',
  templateUrl: './dc-jobcard.component.html',
  styleUrls: ['./dc-jobcard.component.css']
})
export class DcJobcardComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  checkedpurchaseitems: any;
  show_loader: boolean = false;
  alloclist: any;
  workorderlist: any;
  toastertime: number;
  tosterminlength: number;
  supplierForm: FormGroup;
  contactlist: any;
  allcontacts: any;
  worktype: any;
  showDialog: boolean = false;
  reqQty: any;
  part_no: any;
  lineindex: any;
  nopaymentdata: any;
  checkeditems: any;
  allsuppliers: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  public UserDetails = JSON.parse(localStorage.getItem("UserDetails"));

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private completerService: CompleterService,
    private router: Router,
    private acroute: ActivatedRoute,
    private dcservice: DcService,
    private planservice: PlanningService,
    private userservices: UserService
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.worktype = AppConstant.APP_CONST.WorkTypes;
    this.supplierForm = this.fb.group({
      itemPayRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.getallocatedlist();
    this.getcontacts();
    this.checkeditems = [];
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getSupplierform(supplierForm) {
    return supplierForm.get('itemPayRows').controls
  }

  getcontacts() {
    this.contactlist = [];
    this.userservices.getContactsByCat("Sub-contract")
      .subscribe(res => {
        if (res) {
          this.allsuppliers = res.data;
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  getallocatedlist() {
    this.checkedpurchaseitems = [];
    this.show_loader = true;
    this.dcservice.GetStoresJCList(this.unitid).subscribe(res => {
      if (res.status == "success") {
        this.show_loader = false;
        this.workorderlist = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
        console.log("workorderlist", JSON.stringify(this.workorderlist))
      }
    }, err => {
      this.show_loader = false
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  showsupplierpop(item, i) {
    this.showDialog = true;
    this.reqQty = item.qty
    this.part_no = item.part_no
    this.lineindex = i
    if (item.supplier) {
      let paytemp = this.fb.array([]);
      for (let i = 0; i < item.supplier.length; i++) {
        paytemp.push(
          this.fb.group({
            id: item.supplier[i].id,
            supplier_name: item.supplier[i].supplier_name,
            qty: item.supplier[i].qty,
          })
        );
      }
      this.supplierForm = this.fb.group({
        itemPayRows: paytemp
      });
    } else {
      const control = <FormArray>this.supplierForm.controls["itemPayRows"];
      control.push(this.initPayItemRows());
    }
  }


  checkauthorized(status, item, i) {
    this.workorderlist[i].checked = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.workorderlist[i].checked = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.id == item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  check(data, index) {
    var total = 0
    _.forEach(this.supplierForm.value.itemPayRows, function (val) {
      var sum = +val.qty
      total += sum;
    })
    if (total > this.reqQty) {
      this.notif.warn('Warning', "Your Req Qty is " + this.reqQty, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  uncheckall() {
    for (var i = 0; i < this.workorderlist.length; i++) {
      this.workorderlist[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  selectedcontact(data, i) {
    if (data) {
      var part = data.originalObject
      this.workorderlist[i].next_stage_part_id = part.part_id
      this.workorderlist[i].next_stage_part_no = part.part_no
    }
  }

  selectedsupplier(data, index) {
    var supp = data.originalObject;
    let paytemp = this.fb.array([]);
    for (let i = 0; i < this.supplierForm.value.itemPayRows.length; i++) {
      if (index == i) {
        var supplier = _.findWhere(this.allsuppliers, { 'id': supp.id })
        paytemp.push(
          this.fb.group({
            id: supplier.id,
            supplier_name: supplier.supplier_name,
            qty: this.supplierForm.value.itemPayRows[i].qty,
          })
        );
      } else {
        paytemp.push(
          this.fb.group(this.supplierForm.value.itemPayRows[i]));
      }
    }
    this.supplierForm = this.fb.group({
      itemPayRows: paytemp
    });
  }

  initPayItemRows() {
    return this.fb.group({
      id: 0,
      supplier_name: "",
      qty: 0,
    });
  }

  addNewpaymentRow() {
    var totalpercentage = 0
    let self = this

    var data: boolean = true;
    var msg = []

    _.forEach(this.supplierForm.value.itemPayRows, function (item, key) {
      if (!self.supplierForm.value.itemPayRows.length) {
        data = true
      } else {
        if (item.qty) {
          var sum = +item.qty
          totalpercentage += sum;
        } else {
          data = false
          self.notif.warn('Warning', "Please Enter Qty", {
            timeOut: self.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: self.tosterminlength
          });
          return false;
        }
      }
    });
    if (data) {
      if (totalpercentage) {
        if (totalpercentage < this.reqQty) {
          this.nopaymentdata = true;
          const control = <FormArray>this.supplierForm.controls["itemPayRows"];
          control.push(this.initPayItemRows());
        }
        else {
          this.notif.warn('Warning', "Your Req Qty is " + this.reqQty, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          });
        }
      } else {
        this.nopaymentdata = true;
        const control = <FormArray>this.supplierForm.controls["itemPayRows"];
        control.push(this.initPayItemRows());
      }
    }
  }

  deletePayRow(index: number) {
    const control = <FormArray>this.supplierForm.controls["itemPayRows"];
    control.removeAt(index);
    if (this.supplierForm.value.itemPayRows.length == 0) {
      this.nopaymentdata = false;
    }
  }

  allocate(ind) {
    var total = 0;
    var valdata: boolean = true
    var valmsg = []
    _.forEach(this.supplierForm.value.itemPayRows, function (val) {
      var sum = +val.qty
      total += sum;
      if (_.isEmpty(val.supplier_name)) {
        valdata = false;
        valmsg.push("Please Select Supplier")
      }
    })
    if (total > this.reqQty || total < this.reqQty) {
      valdata = false;
      valmsg.push("Your Req Qty is " + this.reqQty)
    }
    if (valdata) {
      this.workorderlist[ind].supplier = this.supplierForm.value.itemPayRows;
      this.reset()
    } else {
      this.notif.warn('Warning', valmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
    console.log("allocate", this.workorderlist)
  }

  reset() {
    this.supplierForm.reset();
    this.supplierForm = this.fb.group({
      itemPayRows: this.fb.array([])
    });
    this.showDialog = false;
  }

  saveOrder() {
    var validdata: boolean = true;
    var validationMsg = [];
    if (!this.checkeditems.length) {
      validdata = false
      validationMsg.push('Please select any Item')
    } else {
      this.checkeditems.forEach(function (data, index, object) {
        if (data.workorder_type) {
          if (data.workorder_type == "External" && !data.stages) {
            if (!data.supplier) {
              validdata = false;
              validationMsg.push("Please Enter Supplier Details for " + data.part_no)
            }
          }
        } else {
          validdata = false;
          validationMsg.push("Please Select Work Type for " + data.part_no)
        }
      });
    }

    if (validdata) {
      var newlist = [];
      // this.checkeditems.forEach(element => {
      //   var list = _.omit(element, ['nxtstage', 'stages', 'next_stage']);
      //   newlist.push(list)
      // });
      var formdata =
        {
          "unit_id": this.unitid,
          "tdcview": this.checkeditems
        }
      console.log("formdata", JSON.stringify(this.checkeditems));
      this.show_loader = true;
      this.dcservice.CreateJCDC(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.getallocatedlist();
          this.checkeditems = [];
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
