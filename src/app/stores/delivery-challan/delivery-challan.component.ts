import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { DcService } from '../../services/dc/dc.service';
import { CompleterService } from 'ng2-completer';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delivery-challan',
  templateUrl: './delivery-challan.component.html',
  styleUrls: ['./delivery-challan.component.css']
})
export class DeliveryChallanComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any = [];
  dtTrigger: Subject<any> = new Subject();
  dcsuppliers: any;
  alldcsuppliers: any;
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  files: any;
  indentstatus: any;
  showDialog: boolean = false;
  showtab: boolean = false;
  nofiles: boolean = false;
  filter: any = {};
  dcid: any;
  loclist: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private dcservice: DcService,
    private http: Http,
    private completerService: CompleterService,
    private router: Router
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.searchdc();
    this.getdcsuppliers()
  }

  getdcsuppliers() {
    this.show_loader = true;
    this.dcservice.GetDCSuppliers(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.dcsuppliers = res.data;
      }
    })
  }

  searchdc() {
    this.show_loader = true;
    this.dcservice.GetDCList(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.persons = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.warn('Warning', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false;
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  redrctodc(id) {
    var dcid = btoa(id)
    this.router.navigate(['/createdc'], { queryParams: { "id": dcid } })
  }

  viewindent(data) {
    this.dcid = data.id;
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    });
  }

  viewLocation(data) {
    this.show_loader = true;
    this.dcservice.GetDCLocations(data.id).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.showDialog = true;
        this.loclist = res.data
      }
    })
  }
  dcitems: any
  dc_no: any
  viewdc(data) {
    this.dc_no = data.dc_no
    this.show_loader = true;
    this.dcservice.GetDCLocations(data.id).subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.dcitems = res.data;
        this.showDialog = true;
      } else {
        this.notif.warn('Warning', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.notif.warn('Warning', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })

    this.showDialog = true
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Order Confirmation</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          .print-view>tbody>tr>td,
          .print-view>tbody>tr>th,
          .print-view>tfoot>tr>td,
          .print-view>tfoot>tr>th,
          .print-view>thead>tr>td,
          .print-view>thead>tr>th {
            border: 1px solid #333 !important; padding:2px 3px !important
          }
          .print-view .print-label p { font-weight : 500 !important;}
          .print-view .print-label { font-weight : 500 !important;}
          .print-view .cs { background-color:#ccc !important;}
          .print-view .cs td { background-color:#ccc !important;}
           table.print-view td p {
           margin: 0!important;
          }
          @page {
            size: auto;
          }

          table.report-container {
          page-break-after:always;
          }
          thead.report-header {
          display:table-header-group;
          }
          tfoot.report-footer {
          display:table-footer-group;
          } 
          .print-view .bg-quoted {
            background-color: #dcffbc !important;
            font-weight : bold
        }
          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  close(i, flag) {
    this.showDialog = false;
  }

  notifycancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }
}
