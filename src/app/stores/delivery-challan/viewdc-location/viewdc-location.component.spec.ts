import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdcLocationComponent } from './viewdc-location.component';

describe('ViewdcLocationComponent', () => {
  let component: ViewdcLocationComponent;
  let fixture: ComponentFixture<ViewdcLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewdcLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdcLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
