import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService, IMessage } from '../../../services/master.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { DcService } from '../../../services/dc/dc.service';
@Component({
  selector: 'app-viewdc-location',
  templateUrl: './viewdc-location.component.html',
  styleUrls: ['./viewdc-location.component.css']
})
export class ViewdcLocationComponent implements OnInit {
  url: any;
  unitid: any;
  dcid: any;
  show_loader: any;
  showDialog: boolean = false;
  loclist: any;
  locations: any;
  index: any;
  dcdtls: any
  constructor(
    private masterservice: MasterService,
    private notif: NotificationsService,
    private acroute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private location: Location,
    private dcservice: DcService
  ) {
    this.url = this.router.url;
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));

    this.acroute.params.subscribe(params => {
      if (!_.isEmpty(params)) {
        this.dcid = params.id;
        console.log("urlparams", this.dcid);
      }
    });
  }

  ngOnInit() {
    this.getdcdetails();
  }

  getdcdetails() {
    this.show_loader = true;
    this.dcservice.GetDCLocations(this.dcid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.dcdtls = res.data;
        this.loclist = res.data.dcitems
      }
    })
  }

  viewLocation(data, i) {
    this.showDialog = true;
    this.index = i
    this.locations = data.stk_loc
  }

  close(i, flag) {
    this.showDialog = false;
  }

}
