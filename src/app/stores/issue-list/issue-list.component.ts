import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { CompleterService } from 'ng2-completer';
import { StoresService } from '../../services/stores/stores.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.css']
})
export class IssueListComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  filter: any = {};
  persons: any
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  allissues: any;
  showDialog: boolean = false;
  ilview: any;
  locations: any;
  reqdtls: any;
  to: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private completerService: CompleterService,
    private storesservice: StoresService,
    private cdRef: ChangeDetectorRef
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    let date = new Date();
    this.alterdateformat(date);
    var currentdate = AppConstant.APP_CONST.FIRST_CURRENTDATE.currentdate
    this.filter.from = { date: currentdate, formatted: this.to, jsdate: new Date() };
    this.filter.to = { date: currentdate, formatted: this.to, jsdate: new Date() };
    this.getallissues()
  }

  alterdateformat(date: any) {
    this.to = formatToDate(date);
    function formatToDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }

  getallissues() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        var validdate = this.masterservice.checkvalid(this.filter.from.jsdate, this.filter.to.jsdate)
        if (!validdate) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
      {
        "unit_id": this.unitid,
        "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
        "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
      }
      this.show_loader = true;
      this.storesservice.getIssuedList(obj)
        .subscribe(res => {
          this.show_loader = false;
          if (res.status == "success") {
            this.allissues = res.data
            if (this.dtElement.dtInstance) {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
              })
            } else
              this.dtTrigger.next();
          }
          else {
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          console.log("err", err)
          this.show_loader = false
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        });
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }

  }

  showpop(data) {
    this.showDialog = true;
    this.reqdtls = data
    this.locations = data.stk_loc
  }

  close() {
    this.showDialog = false
  }

}
