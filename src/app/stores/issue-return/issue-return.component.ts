import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import * as __ from 'lodash';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { saveAs as importedSaveAs } from "file-saver";
import { DepartmentsService } from '../../services/department/departments.service';
import { CompleterService } from 'ng2-completer';
import { ItemsService } from '../../services/items/items.service';
import { StoresService } from '../../services/stores/stores.service';

@Component({
  selector: 'app-issue-return',
  templateUrl: './issue-return.component.html',
  styleUrls: ['./issue-return.component.css']
})
export class IssueReturnComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  filter: any = {};
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  checkedindent: any;
  alldpts: any;
  allitems: any;
  items: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));

  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private dptservice: DepartmentsService,
    private completerService: CompleterService,
    private itemservice: ItemsService,
    private storeservice: StoresService
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.getdepts();
    this.getitems();
    // this.searchissues();
    this.checkedindent = [];
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  getdepts() {
    this.dptservice.getByStatus('active')
      .subscribe(res => {
        if (res) {
          this.alldpts = this.completerService.local(res.data, "department_name", 'department_name');
        }
      });
  }

  getitems() {
    this.show_loader = true;
    this.itemservice.getItemByStatus('active')
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.items = res.data;
          this.allitems = this.completerService.local(res.data, "component_desc,component,part_no", 'part_no');
        }
      });
  }

  searchissues() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    } else if (!this.filter.item) {
      validdata = false;
      validationerrorMsg.push("Please select Item");
    }
    if (validdata) {
      var item = _.findWhere(this.items, { "part_no": this.filter.item })
      var obj =
        {
          "unit_id": this.unitid,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
          "part_no": item.id,
          "department_name": this.filter.dpt,
        }
      this.show_loader = true;
      this.storeservice.GetIssueReturnList(obj).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  checklist(status, item, i) {
    if (status == true) {
      this.checkedindent.push(item);
    } else {
      this.checkedindent.forEach(function (data, index, object) {
        if (data.id === item.id) {
          object.splice(index, 1);
        }
      });
    }
    console.log("checkedindent", this.checkedindent)
  }

  checkcqty(Cqty, i) {
    if (this.persons[i].crt_quantity < Cqty) {
      this.persons[i].cancel_quantity = 0
    }
  }

  saveentry() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    if (!this.checkedindent.length) {
      validdata = false;
      validationerrorMsg.push("Please select any indent");
    } else {
      for (var i = 0; i < this.checkedindent.length; i++) {
        this.checkedindent[i].is_canceled = 1
        if (this.checkedindent[i].cancel_quantity == 0 || !this.checkedindent[i].cancel_reason) {
          validdata = false;
          validationerrorMsg.push("Please enter cancel qty & cancel reason");
        }
      }
      console.log(this.checkedindent)
    }
    if (validdata) {
      // this.show_loader = true;
      // this.purchaseindentservice.UpdateCancelIndent(this.checkedindent).subscribe(res => {
      //   if (res.status == "success") {
      //     this.show_loader = false;
      //     this.getapprovedlist();
      //     this.notif.success('Success', res.msg, {
      //       timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
      //       maxLength: this.tosterminlength
      //     })
      //   }
      // }, (err) => {
      //   this.show_loader = false;
      //   this.getapprovedlist();
      //   this.notif.error('Error', "Something Wrong", {
      //     timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
      //     maxLength: this.tosterminlength
      //   })
      // })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
