import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { DepartmentsService } from '../../services/department/departments.service';
import { CompleterService } from 'ng2-completer';
import { GrnService } from '../../services/grn/grn.service'
import { StoresService } from '../../services/stores/stores.service';
import { forEach } from '@angular/router/src/utils/collection';
import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
class Person {
  checkauthorize: boolean = false;
}

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  filter: any = {};
  persons: any
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  alldpts: any;
  dpt: any;
  allissues: any;
  showDialog: boolean = false;
  ilview: any;
  checkeditems: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private completerService: CompleterService,
    private grnservice: GrnService,
    private dptservice: DepartmentsService,
    private storesservice: StoresService,
    private cdRef: ChangeDetectorRef,
    private router: Router
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.getdepts()
    this.searchissues()
    this.checkeditems = []
  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  getdepts() {
    this.show_loader = true;
    this.dptservice.getByStatus('active')
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.alldpts = this.completerService.local(res.data, "department_name", 'department_name');
        }
      });
  }
  checkall(status) {
    let self = this;
    if (status == true) {
      this.checkeditems = [];
      _.forEach(this.allissues, function (val) {
        val.checkauthorize = true;
        self.checkeditems.push(val)
      })
    } else {
      _.forEach(this.allissues, function (val) {
        val.checkauthorize = false
      })
      this.checkeditems = [];
    }
    console.log("allchecked", this.checkeditems)
  }

  calc(obj, i) {
    if ((+obj.issue_qty) > obj.to_be_issued || (+obj.issue_qty) > obj.stock_qty) {
      obj.issue_qty = 0
    }
  }

  searchissues() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }

    if (validdata) {
      var obj =
      {
        "unit_id": this.unitid,
        "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
        "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
      }
      this.show_loader = true;
      this.storesservice.GetIssueList(obj)
        .subscribe(res => {
          this.show_loader = false;
          if (res) {
            this.allissues = res.data
            _.map(this.allissues, function (val) {
              val.to_be_issued = val.req_qty - val.already_issued
              if (val.stock_qty >= val.req_qty) {
                val.issue_qty = val.req_qty
              }
              return val
            })
            if (this.dtElement.dtInstance) {
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                this.dtTrigger.next();
              })
            } else
              this.dtTrigger.next();
          }
        });
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  showissuepopup(data) {
    this.ilview = data
    this.showDialog = true;
  }

  close() {
    this.showDialog = false;
  }

  checkauthorized(status, item, i) {
    this.allissues[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.allissues[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.eit_line_id === item.eit_line_id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }


  issue() {
    var validata: boolean = true;
    var validationMsg = []
    var tot = 0;
    var list = []
    let self = this
    if (!this.checkeditems.length) {
      validata = false;
      validationMsg.push("Please Select any Request")
    } else if (this.checkeditems.length) {
      _.forEach(this.checkeditems, function (key) {
        if (key.issue_qty == 0) {
          validata = false;
          validationMsg.push("Please enter qty for :" + key.request_no);
          return false
        }
      });
    }
    // _.forEach(self.ilview.ilview_res, function (key) {
    //   if ((+key.issue_qty) > key.qty) {
    //     key.issue_qty = 0
    //   }
    //   if (key.issue_qty != 0) {
    //     list.push(key)
    //     tot += key.issue_qty
    //     if (tot > self.ilview.to_be_issued) {
    //       key.issue_qty = 0
    //     }
    //   }
    // })
    // if (tot == 0) {
    //   validata = false;
    //   validationMsg.push("Please enter qty to any location")
    // } else if (tot > this.ilview.to_be_issued) {
    //   validata = false;
    //   validationMsg.push("Please enter qty to any location")
    // } else if (tot < this.ilview.to_be_issued) {
    //   validata = false;
    //   validationMsg.push("Issue Qty should be equal to Requested Qty")
    // }


    if (validata) {
      // this.ilview.ilview = list
      // this.ilview.status = 3;
      console.log("ilview", this.checkeditems);
      this.show_loader = true;
      this.storesservice.IssueEntry(this.checkeditems).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.notif.success(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
          this.searchissues();
          this.router.navigate(['/issuelist'])
        } else {
          this.notif.error(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.notif.error("Error", "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
