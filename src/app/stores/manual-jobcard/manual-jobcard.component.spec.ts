import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualJobcardComponent } from './manual-jobcard.component';

describe('ManualJobcardComponent', () => {
  let component: ManualJobcardComponent;
  let fixture: ComponentFixture<ManualJobcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualJobcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualJobcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
