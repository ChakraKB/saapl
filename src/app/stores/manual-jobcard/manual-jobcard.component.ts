import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DcService } from '../../services/dc/dc.service';
import { UserService } from '../../services/user/user.service';
import { Router, ActivatedRoute } from '@angular/router';
declare let swal: any;

@Component({
  selector: 'app-manual-jobcard',
  templateUrl: './manual-jobcard.component.html',
  styleUrls: ['./manual-jobcard.component.css']
})
export class ManualJobcardComponent implements OnInit {
  toastertime: number;
  tosterminlength: number;
  itemForm: FormGroup;
  supplierForm: FormGroup;
  show_loader: boolean = false;
  worktype: any;
  allsuppliers: any;
  allcontacts: any;
  showDialog: boolean = false;
  part_no: any;
  lineindex: any;
  allitems: any;
  items: any;
  reqQty: any;
  selectedorderLine: any;
  selecteditem: any;
  checkeditems: any = [];
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private completerService: CompleterService,
    private dcservice: DcService,
    private userservices: UserService,
    private router: Router,
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.worktype = AppConstant.APP_CONST.WorkTypes;
    this.supplierForm = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.getitems();
    this.getcontacts()
  }


  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getSupplierform(supplierfrm) {
    return supplierfrm.get('itemRows').controls
  }

  getcontacts() {
    this.userservices.getContactsByCat("Sub-contract")
      .subscribe(res => {
        if (res) {
          this.allsuppliers = res.data;
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  initPayItemRows() {
    return this.fb.group({
      id: 0,
      supplier_name: "",
      qty: 0,
    });
  }

  deleteRow(index: number) {
    const control = <FormArray>this.itemForm.controls["itemPayRows"];
    control.removeAt(index);
  }

  getitems() {
    this.show_loader = true;
    this.dcservice.GetStoresJCItems("Spare",this.unitid).subscribe(res => {
      this.show_loader = false;
      this.allitems = res.data;
      this.items = this.completerService.local(res.data, "part_desc,part,part_no", 'part_no');
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }
  item: any
  selectitem(item) {
    if (item) {
      var item = item.originalObject;
      this.show_loader = true;
      this.dcservice.GetJobCartItemList("Spare",item.part_id).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.item = res.data
        }
      })
    }
  }

  showsupplierpop(item, i) {
    this.showDialog = true;
    this.reqQty = (item.open_qty - item.tempdc_qty)
    this.part_no = item.part_no
    this.lineindex = i
    if (item.supplier) {
      let paytemp = this.fb.array([]);
      for (let i = 0; i < item.supplier.length; i++) {
        paytemp.push(
          this.fb.group({
            id: item.supplier[i].id,
            supplier_name: item.supplier[i].supplier_name,
            qty: item.supplier[i].qty,
          })
        );
      }
      this.supplierForm = this.fb.group({
        itemRows: paytemp
      });
    } else {
      const control = <FormArray>this.supplierForm.controls["itemRows"];
      control.push(this.initPayItemRows());
    }
  }

  addNewpaymentRow() {
    var totalpercentage = 0
    let self = this

    var data: boolean = true;
    var msg = []

    _.forEach(this.supplierForm.value.itemPayRows, function (item, key) {
      if (!self.supplierForm.value.itemPayRows.length) {
        data = true
      } else {
        if (item.qty) {
          var sum = +item.qty
          totalpercentage += sum;
        } else {
          data = false
          self.notif.warn('Warning', "Please Enter Qty", {
            timeOut: self.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: self.tosterminlength
          });
          return false;
        }
      }
    });
    if (data) {
      if (totalpercentage) {
        if (totalpercentage < this.reqQty) {
          const control = <FormArray>this.supplierForm.controls["itemRows"];
          control.push(this.initPayItemRows());
        }
        else {
          this.notif.warn('Warning', "Your Stock Qty is " + this.reqQty, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          });
        }
      } else {
        const control = <FormArray>this.supplierForm.controls["itemRows"];
        control.push(this.initPayItemRows());
      }
    }
  }

  deletePayRow(index: number) {
    const control = <FormArray>this.supplierForm.controls["itemRows"];
    control.removeAt(index);
  }

  check(data, index) {
    var total = 0
    _.forEach(this.supplierForm.value.itemRows, function (val) {
      var sum = +val.qty
      total += sum;
    })
    if (total > this.reqQty) {
      this.notif.warn('Warning', "Your Stock Qty is " + this.reqQty, {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  selectedsupplier(data, index) {
    if (data) {
      var supp = data.originalObject;
      let paytemp = this.fb.array([]);
      for (let i = 0; i < this.supplierForm.value.itemRows.length; i++) {
        if (index == i) {
          var supplier = _.findWhere(this.allsuppliers, { 'id': supp.id })
          paytemp.push(
            this.fb.group({
              id: supplier.id,
              supplier_name: supplier.supplier_name,
              qty: this.supplierForm.value.itemRows[i].qty,
            })
          );
        } else {
          paytemp.push(
            this.fb.group(this.supplierForm.value.itemRows[i]));
        }
      }
      this.supplierForm = this.fb.group({
        itemRows: paytemp
      });
    }
  }

  ontypechange(type) {
    if (type == "Internal") {
      this.item.supplier = ""
    } else return true;
    console.log("item", this.item)
  }

  allocate(ind) {
    var total = 0;
    var valdata: boolean = true
    var valmsg = []
    _.forEach(this.supplierForm.value.itemRows, function (val) {
      var sum = +val.qty
      total += sum;
      // if (_.isEmpty(val.supplier_name)) {
      //   valdata = false;
      //   valmsg.push("Please Select Supplier")
      // }
      if (val.id == 0 || !val.id) {
        valdata = false;
        valmsg.push("Please Select Supplier")
      }
      else if (val.qty == 0) {
        valdata = false;
        valmsg.push("Please Enter Qty for " + val.supplier_name)
      }
    })
    if (total > this.reqQty) {
      valdata = false;
      valmsg.push("Your Req Qty is " + this.reqQty)
    }
    if (valdata) {
      console.log("item", this.item)
      this.item[ind].supplier = this.supplierForm.value.itemRows;
      this.reset()
    } else {
      this.notif.warn('Warning', valmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  reset() {
    this.supplierForm.reset();
    this.supplierForm = this.fb.group({
      itemRows: this.fb.array([])
    });
    this.showDialog = false;
  }

  checkauthorized(status, item, i) {
    this.item[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.item[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.next_stage_part_id === item.next_stage_part_id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  saveOrder() {
    var validdata: boolean = true;
    var validationMsg = [];

    if (!this.checkeditems.length) {
      validdata = false;
      validationMsg.push("Please select any item")
    }
    _.forEach(this.checkeditems, function (val) {
      if (val.workorder_type) {
        if (val.workorder_type == "External") {
          if (!val.supplier) {
            validdata = false;
            validationMsg.push("Please Enter Supplier Details for " + val.part_no)
          }
        } else {
          val.supplier = []
        }
      } else {
        validdata = false;
        validationMsg.push("Please Select Work Type for " + val.part_no)
      }
    })


    if (validdata) {
      // var newlist = [];
      // newlist.push(this.item)
      var formdata =
      {
        "unit_id": this.unitid,
        "in_status": 2,
        "tdcview": this.checkeditems
      }
      console.log("formdata", JSON.stringify(formdata));
      this.show_loader = true;
      this.dcservice.CreateJCDC(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.resetscreen();
          this.router.navigate(['/storesjobcard']);
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  resetscreen() {
    this.item = [];
    this.selecteditem = "";
    this.reset();
    this.getitems();
    this.router.navigate(['/storesjobcard'])
  }

}
