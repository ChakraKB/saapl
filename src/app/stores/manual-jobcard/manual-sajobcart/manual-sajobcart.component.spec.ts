import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualSajobcartComponent } from './manual-sajobcart.component';

describe('ManualSajobcartComponent', () => {
  let component: ManualSajobcartComponent;
  let fixture: ComponentFixture<ManualSajobcartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualSajobcartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualSajobcartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
