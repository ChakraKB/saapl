import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../../services/master.service'
import { AppConstant } from '../../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DcService } from '../../../services/dc/dc.service';
import { UserService } from '../../../services/user/user.service';
import { Router, ActivatedRoute } from '@angular/router';
declare let swal: any;

@Component({
  selector: 'app-manual-sajobcart',
  templateUrl: './manual-sajobcart.component.html',
  styleUrls: ['./manual-sajobcart.component.css']
})
export class ManualSajobcartComponent implements OnInit {
  toastertime: number;
  tosterminlength: number;
  itemForm: FormGroup;
  supplierForm: FormGroup;
  show_loader: boolean = false;
  worktype: any;
  allsuppliers: any;
  allcontacts: any;
  showDialog: boolean = false;
  part_no: any;
  lineindex: any;
  allitems: any;
  items: any;
  reqQty: any;
  selectedorderLine: any;
  selecteditem: any;
  checkeditems: any = [];
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private completerService: CompleterService,
    private dcservice: DcService,
    private userservices: UserService,
    private router: Router,
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.worktype = AppConstant.APP_CONST.WorkTypes;
    this.supplierForm = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.Pqty = 0
    this.getitems();
    this.getcontacts()
  }
  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }

  getSupplierform(supplierfrm) {
    return supplierfrm.get('itemRows').controls
  }

  getcontacts() {
    this.userservices.getContactsByCat("Sub-contract")
      .subscribe(res => {
        if (res) {
          this.allsuppliers = res.data;
          this.allcontacts = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  getitems() {
    this.show_loader = true;
    this.dcservice.GetStoresJCItems("SubAssembly", this.unitid).subscribe(res => {
      this.show_loader = false;
      this.allitems = res.data;
      this.items = this.completerService.local(res.data, "part_desc,part,part_no", 'part_no');
    }, err => {
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  item: any;
  itemid: any
  supplierid: any;
  showsupp: boolean = true;
  selectitem(data) {
    if (data) {
      var item = data.originalObject;
      this.itemid = data.originalObject;
      if (item.workorder_type == "External") {
        this.showsupp = true;
      } else this.showsupp = false
      this.show_loader = true;
      this.dcservice.GetJobCartItemList("SubAssembly", item.part_id).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.item = res.data
        }
      })
    }
  }

  selectedsupplier(item) {
    if (item) {
      var item = item.originalObject;
      this.supplierid = item.id
    }
  }
  Pqty: any;
  selectedsupp: any
  checkbomqty(qty: any) {
    let self = this
    for (var i = 0; i < this.item.length; i++) {
      var reqqty = this.item[i].qty_cpy * (+qty);
      if ((this.item[i].open_qty - this.item[i].tempdc_qty) >= (reqqty)) {
        this.item[i].qty = reqqty
      }
      else {
        this.notif.warn('Warning', "Not Enough Stock for : " + this.item[i].part_no, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength,
        })
        this.Pqty = 0
        this.checkbomqty(this.Pqty)
        break;
      }
    }
  }

  savetoTempDC() {
    var validdata: boolean = true;
    var validationMsg = [];

    if (!this.selecteditem) {
      validdata = false;
      validationMsg.push("Please select Item")
    } else if (this.showsupp && !this.selectedsupp) {
      validdata = false;
      validationMsg.push("Please Select Supplier")
    } else if (this.Pqty == 0 || !this.Pqty) {
      validdata = false;
      validationMsg.push("Please Enter Qty")
    }

    if (validdata) {
      var formdata =
      {
        "unit_id": this.unitid,
        "in_status": 2,
        "part_id": this.itemid.part_id,
        "part_no": this.itemid.part_no,
        "part_desc": this.itemid.part_desc,
        "uom": this.itemid.uom,
        "workorder_type": this.itemid.workorder_type,
        "part": this.itemid.part,
        "part_type": this.itemid.part_type,
        "qty": this.Pqty,
        "supplier_name": this.selectedsupp,
        "supplier_id": this.supplierid,
        "tdcview": this.item
      }
      console.log("items", formdata)
      this.show_loader = true;
      this.dcservice.CreateSubAssJobcart(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength,
          })
          this.resetscreen();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength,
          })
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength,
        })
      })
    } else {
      this.notif.warn('Warning', validationMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength,
      })
    }
  }

  resetscreen() {
    this.router.navigate(['/storesjobcard'])
  }
}
