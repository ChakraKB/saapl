import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejInvoiceComponent } from './rej-invoice.component';

describe('RejInvoiceComponent', () => {
  let component: RejInvoiceComponent;
  let fixture: ComponentFixture<RejInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
