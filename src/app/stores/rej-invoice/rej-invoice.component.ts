import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { CompleterService } from 'ng2-completer';
import { StoresService } from '../../services/stores/stores.service';
declare let swal: any;
class Person {
  id: any;
}
@Component({
  selector: 'app-rej-invoice',
  templateUrl: './rej-invoice.component.html',
  styleUrls: ['./rej-invoice.component.css']
})
export class RejInvoiceComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  showDialog: boolean = false;
  showtab: boolean = false;
  nofiles: boolean = false;
  filter: any = {};
  checkeditems: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private completerService: CompleterService,
    private storeservice: StoresService
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.persons = [];
    this.checkeditems = []
    this.getrejinvlist();
  }

  getrejinvlist() {
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    // if (this.filter.from || this.filter.to) {
    //   if (!(this.filter.from && this.filter.to)) {
    //     validdata = false;
    //     validationerrorMsg.push("Please select both From & To Date");
    //   } else {
    //     if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
    //       validdata = false;
    //       validationerrorMsg.push("To date should be higher/equal to From date");
    //     }
    //   }
    // }
    if (validdata) {
      var obj =
        {
          "unit_id": this.unitid,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
        }
      this.show_loader = true;
      this.storeservice.GetRejectedInvList(this.unitid).subscribe(res => {
        this.show_loader = false
        if (res.status == "success") {
          this.persons = res.data
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  checkauthorized(status, item, i) {
    this.persons[i].checkauthorize = true
    if (status == true) {
      this.checkeditems.push(item)
    } else {
      this.persons[i].checkauthorize = false
      this.checkeditems.forEach(function (data, index, object) {
        if (data.rej_line_id === item.rej_line_id) {
          object.splice(index, 1);
        }
      });
    }
    console.log(this.checkeditems);
  }

  uncheckall() {
    for (var i = 0; i < this.persons.length; i++) {
      this.persons[i].checkauthorize = false;
      this.checkeditems = []
    }
    console.log("uncheked", this.checkeditems)
  }

  AuthorizeRejInvoice() {
    var validdata: boolean = true;
    var validationerrorMsg = [];

    if (!this.checkeditems.length) {
      validdata = false;
      validationerrorMsg.push("Please select any item")
    }

    if (validdata) {
      console.log("res", this.checkeditems);
      this.show_loader = true;
      this.storeservice.CreateRejectedDC(this.checkeditems)
        .subscribe(res => {
          if (res.status == "success") {
            this.getrejinvlist();
            this.notif.success('Success', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          } else {
            this.show_loader = false
            this.notif.error('Error', res.msg, {
              timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
              maxLength: this.tosterminlength
            })
          }
        }, err => {
          this.show_loader = false
          this.notif.error('Error', "Something Wrong", {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        });
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
