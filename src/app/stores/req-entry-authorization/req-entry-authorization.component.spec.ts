import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqEntryAuthorizationComponent } from './req-entry-authorization.component';

describe('ReqEntryAuthorizationComponent', () => {
  let component: ReqEntryAuthorizationComponent;
  let fixture: ComponentFixture<ReqEntryAuthorizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqEntryAuthorizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqEntryAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
