import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { CompleterService } from 'ng2-completer';
import { GrnService } from '../../services/grn/grn.service';

@Component({
  selector: 'app-req-entry-authorization',
  templateUrl: './req-entry-authorization.component.html',
  styleUrls: ['./req-entry-authorization.component.css']
})
export class ReqEntryAuthorizationComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  tabletimeout: any;
  show_loader: Boolean = false;
  toastertime: number;
  tosterminlength: number;
  indentstatus: any;
  filter: any = {};
  checkeditems: any;
  showDialog: any;
  checkallapp: boolean = false
  checkallrej: boolean = false
  authlist: any;
  nodata: boolean = true;
  show_btns: boolean = true;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private http: Http,
    private completerService: CompleterService,
    private grnservice: GrnService
  ) {
    this.indentstatus = AppConstant.APP_CONST.IndentStatus2
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.filter.status = 0;
    this.getlist();
  }

  getlist() {
    if (this.filter.status == '0') {
      this.show_btns = true
    } else this.show_btns = false
    var validdata: Boolean = true;
    var validationerrorMsg = [];
    if (this.filter.from || this.filter.to) {
      if (!(this.filter.from && this.filter.to)) {
        validdata = false;
        validationerrorMsg.push("Please select both From & To Date");
      } else {
        if (!(this.filter.from.jsdate <= this.filter.to.jsdate)) {
          validdata = false;
          validationerrorMsg.push("To date should be higher/equal to From date");
        }
      }
    }
    if (validdata) {
      var obj =
        {
          "unit_id": this.unitid,
          "from_date": _.isEmpty(this.filter.from) ? "" : this.filter.from.formatted,
          "to_date": _.isEmpty(this.filter.to) ? "" : this.filter.to.formatted,
          "status": this.filter.status
        }
      this.show_loader = true;
      this.grnservice.GetEntryAuthList(obj).subscribe(res => {
        this.show_loader = false;
        this.checkallapp = false;
        this.checkallrej = false;
        if (res.status == "success") {
          this.authlist = res.data;
          if (!res.data.length) this.nodata = true;
          else this.nodata = false
          _.map(this.authlist, function (num) {
            num.crt_qty = num.req_qty;
            num.approve = false;
            num.reject = false;
            num.checked = false;
            return num;
          });
          if (this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.dtTrigger.next();
            })
          } else
            this.dtTrigger.next();
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.show_loader = false;
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }



  checkallapplist(status) {
    this.checkallrej = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].status = 1;
        this.authlist[i].approve = true;
        this.authlist[i].reject = false;
      }
      console.log("allapp", this.authlist)
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].status = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
      }
    }
    console.log("allapprem", this.authlist)
  }

  checkallrejlist(status) {
    this.checkallapp = false;
    if (status == true) {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = true;
        this.authlist[i].status = 2;
        this.authlist[i].approve = false;
        this.authlist[i].reject = true;
      }
      console.log("allrej", this.authlist)
    } else {
      for (var i = 0; i < this.authlist.length; i++) {
        this.authlist[i].checked = false;
        this.authlist[i].status = 0;
        this.authlist[i].approve = false;
        this.authlist[i].reject = false;
      }
      console.log("allrej", this.authlist)
    }
  }

  checkapp(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].status = 1;
      this.authlist[i].approve = true;
      this.authlist[i].reject = false;
      console.log("app", this.authlist)
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].status = 0;
      console.log("apprem", this.authlist)
    }
  }

  checkrej(status, item, i) {
    if (status == true) {
      this.authlist[i].checked = true;
      this.authlist[i].status = 2;
      this.authlist[i].approve = false;
      this.authlist[i].reject = true;
      console.log("rej", this.authlist)
    } else {
      this.authlist[i].checked = false;
      this.authlist[i].status = 0;
      console.log("rej", this.authlist)
    }
  }

  authorizelist() {
    var newlist = [];
    var validdata: Boolean = true;
    var validationerrorMsg = [];

    var selectedauthlists = _.filter(this.authlist, function (num) { return num.checked == true; });

    if (!selectedauthlists.length) {
      validdata = false;
      validationerrorMsg.push("Nothing to authorize");
    }
    if (validdata) {
      var selectedauthlists = selectedauthlists.forEach(element => {
        var list = _.omit(element, ['approve', 'reject', 'checked']);
        newlist.push(list)
      });
      console.log("selectedauthlists", JSON.stringify(newlist))
      this.show_loader = true;
      this.grnservice.EntryAuthorize(newlist).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.getlist();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error(res.status, res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, (err) => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationerrorMsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

}
