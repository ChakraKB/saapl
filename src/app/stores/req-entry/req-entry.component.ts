import { AfterViewInit, Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter, } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MasterService } from '../../services/master.service';
import { DepartmentsService } from '../../services/department/departments.service';
import { PurchaseMastersService } from '../../services/Purchase/purchase-masters.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemsService } from '../../services/items/items.service';
import { StoresService } from '../../services/stores/stores.service';
import { GrnService } from '../../services/grn/grn.service';
@Component({
  selector: 'app-req-entry',
  templateUrl: './req-entry.component.html',
  styleUrls: ['./req-entry.component.css']
})
export class ReqEntryComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  myTabOption: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
    firstDayOfWeek: 'su',
    appendSelectorToBody: true,
    alignSelectorRight: true
  }
  re: any = {};
  toastertime: any;
  tosterminlength: any;
  unitid: any;
  reqentryForm: any;
  nodata: boolean = false;
  alldepts: any;
  allcc: any;
  show_loader: boolean = false;
  itemlist: any;
  allitems: any;
  items: any;
  allcostcenter: any;
  alldpt: any;
  selectedorderLine: any;
  todaydate: any
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notificate: NotificationsService,
    private dptservice: DepartmentsService,
    private purchasemaster: PurchaseMastersService,
    private _fb: FormBuilder,
    private completerService: CompleterService,
    private acroute: ActivatedRoute,
    private itemservice: ItemsService,
    private storeservice: StoresService,
    private grnservice: GrnService,
    private router: Router
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;

    this.reqentryForm = this._fb.group({
      itemRows: this._fb.array([])
    });

  }

  ngOnInit() {
    let date = new Date();
    this.alterdateformat(date);
    var currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    this.addNewRow();
    this.getdepartments();
    this.getitems();
    this.getCostcenter();
  }
  alterdateformat(date) {
    this.re.date = formatDate(date);
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [day, month, year].join('-');
    }
  }
  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }
  decimalnumber(event) {
    this.masterservice.setTwoNumberDecimal(event);
  }
  getitems() {
    this.show_loader = true;
    this.itemlist = [];
    this.itemservice.getItemByStatus('active')
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.allitems = res.data
          this.items = this.completerService.local(this.allitems, "component_desc,component,part_no", 'part_no');
        }
      });
  }

  getdepartments() {
    this.dptservice.getallDepartments()
      .subscribe(res => {
        if (res) {
          this.alldepts = res.data;
          this.alldpt = this.completerService.local(res.data, 'department_name', 'department_name');
        }
      });
  }

  getCostcenter() {
    this.purchasemaster.getallCostcenter().subscribe(res => {
      if (res) {
        this.allcc = res.data
        this.allcostcenter = this.completerService.local(res.data, 'cost_center', 'cost_center');
      }
    })
  }

  getreqentryForm(reqentryForm) {
    return reqentryForm.get('itemRows').controls
  }

  initItemRows() {
    return this._fb.group({
      id: "",
      part_no: "",
      part:"",
      partdesc: "",
      uom: "",
      stockqty: "",
      alrecqty: "",
      reqqty: "",
      costcenter: "",
      fincode: "",
      remarks: ""
    });
  }

  addNewRow() {
    this.nodata = true;
    const control = <FormArray>this.reqentryForm.controls["itemRows"];
    control.push(this.initItemRows());
  }
  deleteRow(index: number) {
    const control = <FormArray>this.reqentryForm.controls["itemRows"];
    control.removeAt(index);
    if (this.reqentryForm.value.itemRows.length == 0) {
      this.nodata = false;
    }
  }
  selectedcc(item, index) {

  }

  calculatetotal(item, ind) {
    this.selectedorderLine = item
    let temp = this._fb.array([]);
    if (item.reqqty > item.stockqty) {
      this.notificate.warn('Warning', "Request Qty should not exceed Stock Qty", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
      for (let i = 0; i < this.reqentryForm.value.itemRows.length; i++) {
        if (ind == i) {
          temp.push(
            this._fb.group({
              id: this.reqentryForm.value.itemRows[i].id,
              part_no: this.selectedorderLine.part_no,
              part: this.selectedorderLine.part,
              partdesc: this.selectedorderLine.partdesc,
              uom: this.selectedorderLine.uom,
              stockqty: this.selecteditem.open_qty,
              alrecqty: this.selecteditem.allocated_qty,
              reqqty: 0,
              costcenter: this.reqentryForm.value.itemRows[i].costcenter,
              fincode: this.reqentryForm.value.itemRows[i].fincode,
              remarks: ""
            })
          );
        } else {
          temp.push(
            this._fb.group(this.reqentryForm.value.itemRows[i]));
        }
      }
      this.reqentryForm = this._fb.group({
        itemRows: temp
      });
    }
  }

  selecteditem: any
  selectedline(item, index, data) {
    let temp = this._fb.array([]);
    this.selectedorderLine = item.originalObject;
    if (data == 'item') {
      this.show_loader = true;
      this.storeservice.getSTockQuantity(this.selectedorderLine.id).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.selecteditem = res.data[0];
          console.log("selectedorderLine", this.selectedorderLine)
          for (let i = 0; i < this.reqentryForm.value.itemRows.length; i++) {
            if (index == i) {
              temp.push(
                this._fb.group({
                  id: this.selectedorderLine.id,
                  part_no: this.selectedorderLine.part_no,
                  part: this.selectedorderLine.component,
                  partdesc: this.selectedorderLine.component_desc,
                  uom: this.selectedorderLine.uom.uom_name,
                  stockqty: this.selecteditem.open_qty,
                  alrecqty: this.selecteditem.allocated_qty,
                  reqqty: this.reqentryForm.value.itemRows[i].reqqty,
                  costcenter: this.reqentryForm.value.itemRows[i].costcenter,
                  fincode: this.reqentryForm.value.itemRows[i].fincode,
                  remarks: ""
                })
              );
            } else {
              temp.push(
                this._fb.group(this.reqentryForm.value.itemRows[i]));
            }
          }
          this.reqentryForm = this._fb.group({
            itemRows: temp
          });
        }
      })
    } else {
      for (let i = 0; i < this.reqentryForm.value.itemRows.length; i++) {
        if (index == i) {
          temp.push(
            this._fb.group({
              id: this.reqentryForm.value.itemRows[i].id,
              part_no: this.reqentryForm.value.itemRows[i].part_no,
              part: this.reqentryForm.value.itemRows[i].part,
              partdesc: this.reqentryForm.value.itemRows[i].partdesc,
              uom: this.reqentryForm.value.itemRows[i].uom,
              stockqty: this.reqentryForm.value.itemRows[i].stockqty,
              alrecqty: this.reqentryForm.value.itemRows[i].alrecqty,
              reqqty: this.reqentryForm.value.itemRows[i].reqqty,
              costcenter: this.selectedorderLine.cost_center,
              fincode: this.selectedorderLine.finance_code,
              remarks: ""
            })
          );
        } else {
          temp.push(
            this._fb.group(this.reqentryForm.value.itemRows[i]));
        }
      }
      this.reqentryForm = this._fb.group({
        itemRows: temp
      });
    }
    console.log(this.reqentryForm.value.itemRows);
  }
  eitems: any
  saveentry() {
    var validata: boolean = true;
    var validmsg = [];
    if (!this.re.dpt) {
      validata = false;
      validmsg.push("Pleas select Department")
    } else if (!this.re.reqno) {
      validata = false;
      validmsg.push("Pleas enter Request No")
    } else if (!this.re.on) {
      validata = false;
      validmsg.push("Pleas select Request On Date")
    }
    else {
      this.eitems = []
      for (var i = 0; i < this.reqentryForm.value.itemRows.length; i++) {
        var obj = {
          "item_id": this.reqentryForm.value.itemRows[i].id,
          "item_code": this.reqentryForm.value.itemRows[i].part_no,
          "part": this.reqentryForm.value.itemRows[i].part,
          "item_desc": this.reqentryForm.value.itemRows[i].partdesc,
          "uom": this.reqentryForm.value.itemRows[i].uom,
          "stock_qty": this.reqentryForm.value.itemRows[i].stockqty,
          "all_rec_qty": this.reqentryForm.value.itemRows[i].alrecqty,
          "req_qty": this.reqentryForm.value.itemRows[i].reqqty,
          "cost_center": this.reqentryForm.value.itemRows[i].costcenter,
          "finance_code": this.reqentryForm.value.itemRows[i].fincode,
          "remarks": this.reqentryForm.value.itemRows[i].remarks,
        }
        if (!this.reqentryForm.value.itemRows[i].part_no) {
          validata = false;
          validmsg.push("Pleas select Part No")
        } else if (!this.reqentryForm.value.itemRows[i].reqqty) {
          validata = false;
          validmsg.push("Pleas Enter Required Qty")
        } else if (!this.reqentryForm.value.itemRows[i].costcenter) {
          validata = false;
          validmsg.push("Pleas select costcenter")
        } else {
          this.eitems.push(obj)
        }
      }
    }

    if (validata) {
      var formdata =
      {
        'dept': this.re.dpt,
        'unit_id': this.unitid,
        'request_no': this.re.reqno,
        'required_on': this.re.on.formatted,
        'request_date': this.re.date,
        'eitems': this.eitems
      }
      console.log("formdata", formdata)
      this.show_loader = true;
      this.grnservice.CreateReqEntry(formdata).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          this.resetform();
          this.notificate.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notificate.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      })
    } else {
      this.notificate.warn('Warning', validmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }

  resetform() {
    this.re = {};
    this.re.on = "";
    let date = new Date();
    this.alterdateformat(date);
    var currentdate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
    this.reqentryForm = this._fb.group({
      itemRows: this._fb.array([])
    });
    this.addNewRow();
  }

}
