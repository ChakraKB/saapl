import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReworkjobcartComponent } from './reworkjobcart.component';

describe('ReworkjobcartComponent', () => {
  let component: ReworkjobcartComponent;
  let fixture: ComponentFixture<ReworkjobcartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReworkjobcartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReworkjobcartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
