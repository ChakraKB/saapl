import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesDcComponent } from './sales-dc.component';

describe('SalesDcComponent', () => {
  let component: SalesDcComponent;
  let fixture: ComponentFixture<SalesDcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesDcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesDcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
