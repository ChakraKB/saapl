import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service'
import { AppConstant } from '../../app.constant';
import { NotificationsService } from 'angular2-notifications';
import { CompleterService, CompleterData } from 'ng2-completer';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { DcService } from '../../services/dc/dc.service';
import { Router, ActivatedRoute } from '@angular/router';
import { InvoiceService } from '../../services/invoice/invoice.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
declare let swal: any;

@Component({
  selector: 'app-sales-dc',
  templateUrl: './sales-dc.component.html',
  styleUrls: ['./sales-dc.component.css']
})
export class SalesDcComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {};
  toastertime: number;
  tosterminlength: number;
  show_loader: boolean = false;
  showDialog: boolean = false;
  allinvoices: any
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private masterservice: MasterService,
    private notif: NotificationsService,
    private completerService: CompleterService,
    private dcservice: DcService,
    private router: Router,
    private invoiceservice: InvoiceService
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.getinvoicelist()
  }

  getinvoicelist() {
    this.show_loader = true;
    this.invoiceservice.GetSalesDCList(this.unitid).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.allinvoices = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      }
    })
  }

  createdc(data) {
    this.show_loader = true;
    this.invoiceservice.CreateSalesDC(data.invoice_no, data.id, data.packing_slip_ids).subscribe(res => {
      this.show_loader = false;
      if (res.status == "success") {
        this.getinvoicelist();
        this.notif.success('Success', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false;
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

}
