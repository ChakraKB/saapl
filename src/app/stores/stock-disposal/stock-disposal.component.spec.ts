import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockDisposalComponent } from './stock-disposal.component';

describe('StockDisposalComponent', () => {
  let component: StockDisposalComponent;
  let fixture: ComponentFixture<StockDisposalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockDisposalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockDisposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
