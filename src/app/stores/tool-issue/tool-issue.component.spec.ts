import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolIssueComponent } from './tool-issue.component';

describe('ToolIssueComponent', () => {
  let component: ToolIssueComponent;
  let fixture: ComponentFixture<ToolIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
