import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { RolesService } from '../../services/Roles/roles.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { DcService } from '../../services/dc/dc.service';
import { CompleterService } from 'ng2-completer';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { ItemsService } from '../../services/items/items.service';
import { ToolsService } from '../../services/tools/tools.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-tool-issue',
  templateUrl: './tool-issue.component.html',
  styleUrls: ['./tool-issue.component.css']
})
export class ToolIssueComponent implements OnInit {
  myOptions: INgxMyDpOptions = AppConstant.APP_CONST.DateformatOptions;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  persons: any;
  dtTrigger: Subject<any> = new Subject();
  toastertime: number;
  tosterminlength: number;
  toolform: FormGroup;
  formdata: any;
  tabletimeout: any;
  checkeditems: any[];
  orderdetails: any;
  dc: any = {};
  show_loader: Boolean = false;
  alldcsuppliers: any;
  dcsuppliers: any;
  dctype: any;
  allitems: any;
  items: any;
  selectedorderLine: any;
  public unitid = JSON.parse(localStorage.getItem("UnitId"));
  constructor(
    private fb: FormBuilder,
    private masterservice: MasterService,
    private notif: NotificationsService,
    private roleService: RolesService,
    private dcservice: DcService,
    private http: Http,
    private router: Router,
    private itemservice: ItemsService,
    private completerService: CompleterService,
    private toolservice: ToolsService,
    private userservices: UserService
  ) {
    this.dctype = AppConstant.APP_CONST.DCType;
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;

    this.toolform = this.fb.group({
      itemRows: this.fb.array([])
    });
  }

  ngOnInit() {
    this.checkeditems = [];
    this.getdcsuppliers();
    this.gettools();
    this.addNewRow();
    this.dc.type = "Returnable"
  }

  gettoolform(toolform) {
    return toolform.get('itemRows').controls
  }

  restricthypens(event) {
    this.masterservice.restricthyp(event);
  }

  initItemRows() {
    return this.fb.group({
      id: "",
      tool_no: "",
      tool_name: "",
      tool_desc: "",
      tool_qty: "",
      dc_qty: 0,
    });
  }

  addNewRow() {
    const control = <FormArray>this.toolform.controls["itemRows"];
    control.push(this.initItemRows());
  }

  deleteRow(index: number) {
    const control = <FormArray>this.toolform.controls["itemRows"];
    control.removeAt(index);
  }

  gettools() {
    this.show_loader = true;
    this.toolservice.getByStatus('active')
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.items = res.data
          this.allitems = this.completerService.local(this.items, "tool_description,tool_name,tool_no", 'tool_no');
        }
      });
  }

  getdcsuppliers() {
    this.show_loader = true;
    this.userservices.getContactsByCat("Customer")
      .subscribe(res => {
        this.show_loader = false;
        if (res) {
          this.dcsuppliers = res.data;
          this.alldcsuppliers = this.completerService.local(res.data, 'supplier_name', 'supplier_name');
        }
      });
  }

  // selectsupplier(data) {
  //   var supp = data.originalObject
  //   this.show_loader = true;
  //   this.dcservice.GetDCBySupplier(supp.id).subscribe(res => {
  //     this.show_loader = false;
  //     if (res.status == "success") {
  //       this.persons = res.data
  //       if (this.dtElement.dtInstance) {
  //         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //           dtInstance.destroy();
  //           this.dtTrigger.next();
  //         })
  //       } else
  //         this.dtTrigger.next();
  //     } else {
  //       this.notif.error('Error', res.msg, {
  //         timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
  //         maxLength: this.tosterminlength
  //       })
  //     }
  //   }, err => {
  //     this.notif.error('Error', "Something Wrong", {
  //       timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
  //       maxLength: this.tosterminlength
  //     })
  //   })
  // }

  selectedline(item, index) {
    this.selectedorderLine = item.originalObject;
    console.log("item====>", this.selectedorderLine)
    let temp = this.fb.array([]);
    for (let i = 0; i < this.toolform.value.itemRows.length; i++) {
      if (index == i) {
        temp.push(
          this.fb.group({
            id: this.selectedorderLine.id,
            tool_no: this.selectedorderLine.tool_no,
            tool_name: this.selectedorderLine.tool_name,
            tool_desc: this.selectedorderLine.tool_description,
            dc_qty: 0,
            tool_qty: this.selectedorderLine.open_quantity,
          })
        );
      } else {
        temp.push(
          this.fb.group(this.toolform.value.itemRows[i]));
      }
    }
    this.toolform = this.fb.group({
      itemRows: temp
    });
    console.log(this.toolform.value.itemRows);
  }

  checkqty(index) {
    let temp = this.fb.array([]);
    if ((+this.toolform.controls.itemRows.value[index].dc_qty) > this.toolform.controls.itemRows.value[index].tool_qty) {
      for (let i = 0; i < this.toolform.value.itemRows.length; i++) {
        if (index == i) {
          temp.push(
            this.fb.group({
              id: this.toolform.value.itemRows[i].id,
              tool_no: this.toolform.value.itemRows[i].tool_no,
              tool_name: this.toolform.value.itemRows[i].tool_name,
              tool_desc: this.toolform.value.itemRows[i].tool_desc,
              dc_qty: 0,
              tool_qty: this.toolform.value.itemRows[i].tool_qty,
            })
          );
        } else {
          temp.push(
            this.fb.group(this.toolform.value.itemRows[i]));
        }
      }
      this.toolform = this.fb.group({
        itemRows: temp
      });
    }
  }

  calcopen(i) {

  }

  createdc() {
    var validdata: boolean = true;
    var validationmsg = [];

    if (!this.dc.supplier) {
      validdata = false;
      validationmsg.push("Please select supplier")
    } else if (!this.dc.dcdate) {
      validdata = false;
      validationmsg.push("Please select DC Date")
    } else if (!this.dc.vehicle_no) {
      validdata = false;
      validationmsg.push("Please enter vehicle no")
    } else if (!this.toolform.value.itemRows.length) {
      validdata = false;
      validationmsg.push("Please select items")
    }

    _.forEach(this.toolform.value.itemRows, function (val) {
      if (!val.tool_no) {
        validdata = false;
        validationmsg.push("Please select Tool No")
      } else if (!val.dc_qty) {
        validdata = false;
        validationmsg.push("Please enter qty")
      }
    })

    console.log("checkeditems", this.checkeditems)
    if (validdata) {
      var supp = _.findWhere(this.dcsuppliers, { 'supplier_name': this.dc.supplier })
      var items = [];
      _.forEach(this.toolform.value.itemRows, function (val) {
        var a = {
          tools: { 'id': val.id },
          dc_quantity: val.dc_qty
        }
        items.push(a)
      })
      var obj =
        {
          "supplier": { 'id': supp.id },
          "return_type": this.dc.type,
          "tool_dc_date": this.dc.dcdate.formatted,
          "vechile_no": this.dc.vehicle_no,
          "toolsList": items
        }
      console.log("formdata", obj);
      this.show_loader = true;
      this.toolservice.CreateDCTool(obj).subscribe(res => {
        this.show_loader = false;
        if (res.status == "success") {
          // this.router.navigate(['/deliverychallan'])
          this.gettools()
          this.resetform();
          this.notif.success('Success', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.show_loader = false;
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      })
    } else {
      this.notif.warn('Warning', validationmsg[0], {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    }
  }
  resetform() {
    this.toolform.reset();
    this.toolform = this.fb.group({
      itemRows: this.fb.array([])
    });
    this.addNewRow();
    this.dc = {};
    this.dc.dcdate = "";
  }
}
