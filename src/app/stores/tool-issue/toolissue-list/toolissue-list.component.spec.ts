import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolissueListComponent } from './toolissue-list.component';

describe('ToolissueListComponent', () => {
  let component: ToolissueListComponent;
  let fixture: ComponentFixture<ToolissueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolissueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolissueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
