import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { AppConstant } from '../../../app.constant';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import * as _ from 'underscore';
import { ToolsService } from '../../../services/tools/tools.service';
import { MasterService } from '../../../services/master.service';
import { NotificationsService } from 'angular2-notifications';
class Person {
  role_name: string;
  description: string;
}

@Component({
  selector: 'app-toolissue-list',
  templateUrl: './toolissue-list.component.html',
  styleUrls: ['./toolissue-list.component.css']
})
export class ToolissueListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  persons: Person[] = [];
  show_loader: boolean = false;
  toastertime: number;
  tosterminlength: number;
  unitid: any;
  tabletimeout: any;
  dcdetails: any;
  showtab: boolean = false;
  dclinedetails: any;
  constructor(
    private toolservice: ToolsService,
    private masterservice: MasterService,
    private notif: NotificationsService,
  ) {
    this.unitid = JSON.parse(localStorage.getItem("UnitId"));
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
    this.tabletimeout = AppConstant.APP_CONST.Table_Trigger_timeout.timeout;
  }

  ngOnInit() {
    this.GetIssueList()
  }

  GetIssueList() {
    this.show_loader = true;
    this.toolservice.getToolsIssued().subscribe(res => {
      this.show_loader = false
      if (res.status == "success") {
        this.persons = res.data;
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          })
        } else
          this.dtTrigger.next();
      } else {
        this.notif.error('Error', res.msg, {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
      }
    }, err => {
      this.show_loader = false;
      this.notif.error('Error', "Something Wrong", {
        timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
        maxLength: this.tosterminlength
      })
    })
  }

  viewindent(data) {
    this.dcdetails = data;
    this.dclinedetails = this.dcdetails.toolsList
    this.showtab = true;
    setTimeout(() => {
      jQuery('#detail')[0].click();
    });
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Delivery Challan</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  calcqty() {
    var totqty = 0
    _.forEach(this.dclinedetails, function (val) {
      totqty += val.tools.quantity
    })
    return totqty
  }

  cancel() {
    this.showtab = false;
    setTimeout(() => {
      jQuery('#list')[0].click();
    });
  }

}
