import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { MasterService } from '../../services/master.service';
import { AppConstant } from '../../app.constant';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { DcService } from '../../services/dc/dc.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-view-dc',
  templateUrl: './view-dc.component.html',
  styleUrls: ['./view-dc.component.css']
})
export class ViewDcComponent implements OnInit {
  @Input() dcid: any;
  @Output() notifycancel: EventEmitter<any> = new EventEmitter();
  purDetails: any;
  show_loader: boolean = false;
  toastertime: any;
  tosterminlength: any;
  PurLineDetails: any;
  suppliers: any;
  supplierdtls: any;
  dcdetails: any;
  dclinedetails: any
  constructor(
    private dcservice: DcService,
    private notif: NotificationsService,
    private userservice: UserService
  ) {
    this.toastertime = AppConstant.APP_CONST.toaster.toaster_time;
    this.tosterminlength = AppConstant.APP_CONST.toaster.minlength;
  }

  ngOnInit() {
    this.getsuppliers();
    setTimeout(() => {
      this.viewdetails();
    });
  }

  getsuppliers() {
    this.userservice.getContactsByCat("Supplier").subscribe(res => {
      this.suppliers = res.data
    })
  }

  viewdetails() {
    if (this.dcid) {
      this.show_loader = true;
      this.dcservice.GetDCListByID(this.dcid).subscribe(res => {
        if (res.status == "success") {
          this.show_loader = false;
          this.dcdetails = res.data;
          this.dclinedetails = res.data.dcitems;
          // this.PurLineDetails = res.data.poitem;
          // this.supplierdtls = _.findWhere(this.suppliers, { id: res.data.supplier_id })
          // console.log("suppliers", this.supplierdtls);
        } else {
          this.notif.error('Error', res.msg, {
            timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
            maxLength: this.tosterminlength
          })
        }
      }, err => {
        this.notif.error('Error', "Something Wrong", {
          timeOut: this.toastertime, showProgressBar: true, pauseOnHover: true, clickToClose: true,
          maxLength: this.tosterminlength
        })
        this.cancel()
      })
    }
  }

  calcqty() {
    var totqty = 0
    _.forEach(this.dclinedetails, function (val) {
      var bomqty = 0;
      if (val.dc_sub_items) {
        _.forEach(val.dc_sub_items, function (value) {
          bomqty += value.qty
        })
      } else {
        bomqty += val.qty
      }
      totqty += bomqty
    })
    return totqty
  }

  calctotweight() {
    var totweight = 0
    _.forEach(this.dclinedetails, function (val) {
      var bomweight = 0;
      if (val.dc_sub_items) {
        _.forEach(val.dc_sub_items, function (value) {
          bomweight += value.total_weight
        })
      } else {
        bomweight += val.total_weight
      }
      totweight += bomweight
    })
    return totweight.toFixed(2)
  }

  calcweight(qty, weight, i) {
    var unitweight = qty * weight;
    this.dclinedetails[i].weight = unitweight
    return unitweight.toFixed(2)
  }

  printpos() {
    let printContents, popupWin;
    printContents = document.getElementById('printsection').innerHTML;
    popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SAAPL Work order</title>
          <link rel="stylesheet" type="text/css" href="assets/css/stylescopy.css" >
          <style>
          .print-view>tbody>tr>td,
          .print-view>tbody>tr>th,
          .print-view>tfoot>tr>td,
          .print-view>tfoot>tr>th,
          .print-view>thead>tr>td,
          .print-view>thead>tr>th {
            border: 1px solid #333 !important; padding:2px 3px !important
          }
          .print-view .print-label p { font-weight : 500 !important;}
          .print-view .print-label { font-weight : 500 !important;}
          .print-view .cs { background-color:#ccc !important;}
          .print-view .cs td { background-color:#ccc !important;}
           table.print-view td p {
           margin: 0!important;
          }
        table.report-container {
        page-break-after:always;
        }
        thead.report-header {
        display:table-header-group;
        }
        tfoot.report-footer {
        display:table-footer-group;
        } 
        </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  cancel() {
    this.notifycancel.next();
  }

}
